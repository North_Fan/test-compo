import request from '@/utils/request'
export function getCommonSelectByCode (parameter) {
  return request({
    url: `/basecenter/biz/service/codeitem/syscodeitemsByTypeCode`,
    method: 'get',
    params: parameter
  })
}
export function syncImportFileState (url, ossId) {
  return request({
    url,
    method: 'post',
    params: {
      ossId
    }
  })
}
export function singleFileUploaderPost (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    data
  })
}
/**
 * 提交文件
 * @param {Object} parameter
 * @returns
 */
export function singleFileSubmit (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    data
  })
}

/**
 * 上传文件错误详情文件的文件下载
 * @returns
 */
export function singleFileUploadErrorFileDownload (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'get',
    params: data
  })
}
/**
 * 问题记录管理文件校验
 * @returns
 */
export function checkoutFile (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'get',
    params: data
  })
}

/**
 * 下载文件
 * @returns
 */
export function singleFileUploadFileDownload (serviceUrl, data) {
  return request({
    url: serviceUrl + data,
    responseType: 'blob',
    method: 'post'
  })
}
/**
 * 下载模板文件
 * @returns
 */
export function downloadTempService (serviceUrl) {
  return request({
    url: serviceUrl,
    method: 'get',
    responseType: 'blob'
  })
}

/**
 * 下载模板文件
 * @returns
 */
export function departmentList (parameter) {
  return request({
    url: '/api/department',
    method: 'get',
    params: parameter
  })
}

/**
 * 下载模板文件
 * @returns
 */
export function getRoleList (parameter) {
  return request({
    url: '/api/roleList',
    method: 'post',
    params: parameter
  })
}
/**
 * 异步上传-状态轮询接口
 * @returns
 */
export function syncImportFileResult (url, ossId) {
  return request({
    url,
    method: 'get',
    params: {
      ossId
    }
  })
}
// sqcdp指标管理列表
export function getTableData (params) {
  return request.post('/sqcdp/biz/service/spectaculars/getSQCDPKPIData', params)
}
// 导出
export function getStandardExport (params) {
  return request.post('/sqcdp/biz/service/spectaculars/export', params)
}
// 新增
export function addQuotaList (params) {
  return request.post('/sqcdp/biz/service/spectaculars/addKPIList', params)
}
// 创建流程
export function createAddqzeroCenter (params) {
  return request.post('/sqcdp/biz/service/sqcdp/KpiCreate/create', params)
}
// 初始化流程
export function initAddqzeroCenter (params) {
  return request({
    url: `/sqcdp/biz/service/sqcdp/KpiCreate/init`,
    method: 'get',
    params: params
  })
}
// 查询SQCDP人员权限
export function queryOrgRoleMappingList (params) {
  return request({
    url: '/basecenter/biz/service/bSCxOrgRoleMappingDO/queryOrgRoleMappingList',
    method: 'post',
    data: params
  })
}

// 查询SQCDP人员权限
export function queryPageAuthority (params) {
  return request({
    url: '/basecenter/biz/service/bSCxOrgRoleMappingDO/queryPageAuthority',
    method: 'post',
    data: params
  })
}
