import request from '@/utils/request'

// 任务单关注接口
export function getTaskListData (data) {
  return request({
    url: `/adapter/biz/tasks-manager-app/tasks/listTasks`,
    method: 'post',
    data
  })
}

// 任务单关注接口
export function getFlowListData (data) {
  return request({
    url: `/flow-platform/model/list`,
    method: 'post',
    data
  })
}

export function exportToDoList (params) {
  return request({
    url: `/adapter/biz/tasks-manager-app/exportAll`,
    method: 'get',
    params: params
  })
}
