import request from '@/utils/request'

const interfaceMap = {
  getExampleData: '/sqcdp/biz/service/spectaculars/getExampleData',
  getSQCDPFillData: '/sqcdp/biz/service/spectaculars/getSQCDPFillData',
  getTrendData: '/sqcdp/biz/service/spectaculars/getTrendData',
  getFillProblem: '/sqcdp/biz/service/fillProblem/getFillProblem', // 查询提报问题数据
  updateFillProblem: '/sqcdp/biz/service/fillProblem/updateFillProblem', // 修改提报问题数据
  initFillProblem: '/sqcdp/biz/service/fillProblem/initFillProblem', // 提报问题数据初始值
  addFillProblem: '/sqcdp/biz/service/fillProblem/addFillProblem', // 添加提报问题数据
  pageQualityProcess: '/quality/biz/service/process/pageQualityProcess', // 产品故障分页查询
  queryOrgRoleMappingList: '/basecenter/biz/service/bSCxOrgRoleMappingDO/queryOrgRoleMappingList', // 组织人员映射分页
  addOrgRoleMapping: '/basecenter/biz/service/bSCxOrgRoleMappingDO/addOrgRoleMapping', // 组织人员信息新增
  delOrgRoleMapping: '/basecenter/biz/service/bSCxOrgRoleMappingDO/delOrgRoleMapping',
  getExampleAndFillData: '/sqcdp/biz/service/kpiFill/getExampleAndFillData',
  saveFillData: '/sqcdp/biz/service/kpiFill/saveFillData'

}
export default interfaceMap

export function getExampleData (parameter) {
  return request.post(interfaceMap.getExampleData, parameter)
  // return request({
  //   url: interfaceMap.getContractTreeData,
  //   method: 'post',
  //   data: parameter
  // })
}

export function getSQCDPFillData (parameter) {
  return request.post(interfaceMap.getSQCDPFillData, parameter)
}

export function getExampleAndFillData (parameter) {
  return request.post(interfaceMap.getExampleAndFillData, parameter)
}
// 保存接口
export function saveFillData (parameter) {
  return request.post(interfaceMap.saveFillData, parameter)
}

export function getTrendData (parameter) {
  return request.post(interfaceMap.getTrendData, parameter)
}

export function getFillProblem (parameter) {
  return request.post(interfaceMap.getFillProblem, parameter)
}
export function updateFillProblem (parameter) {
  return request.post(interfaceMap.updateFillProblem, parameter)
}

export function initFillProblem (parameter) {
  return request.get(interfaceMap.initFillProblem, parameter)
}
export function addFillProblem (parameter) {
  return request.post(interfaceMap.addFillProblem, parameter)
}

export function pageQualityProcess (parameter) {
  return request.post(interfaceMap.pageQualityProcess, parameter)
}

export function queryOrgRoleMappingList (parameter) {
  return request.post(interfaceMap.queryOrgRoleMappingList, parameter)
}

export function addOrgRoleMapping (parameter) {
  return request.post(interfaceMap.addOrgRoleMapping, parameter)
}

export function delOrgRoleMapping (parameter) {
  return request.post(interfaceMap.delOrgRoleMapping, parameter)
}

// export function delOrgRoleMapping () {
//   return request({
//     url: `/basecenter/biz/service/bSCxOrgRoleMappingDO/delOrgRoleMapping`,
//     method: 'delete'
//   })
// }

// export function delTaskRunAssembly (id) {
//   return request({
//     url: `/assignment/biz/service/assignment/del/${id}`,
//     method: 'delete'
//   })
// }
