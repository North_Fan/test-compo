import request from '@/utils/request'

// 任务单关注接口
export function planCheck (taskId) {
  return request({
    url: `/assignment/biz/service/flow/next/node/task/${taskId}`,
    method: 'get'
  })
}

// 获取待办详情 Mock接口
export function queryWfDetailMock (params) {
  return request({
    url: '/assignment/biz/service/instance/wf/todo/mock',
    method: 'get',
    params
  })
}

// 通过过程的节点获取流程组件的实例ID
export function getFlowInstanceId (params) {
  return request({
    url: interfaceMap.getFlowInstanceId,
    method: 'get',
    params
  })
}

  /**
*  更换执行器
* @param {Object} parameter
* @returns
*/
export function flowCheckBeforeSubmit (data) {
  return request({
    url: '/assignment/biz/service/flow/check',
    method: 'post',
    data
  })
 }

 // 作业设置变量
export function setTaskVariable (data) {
  return request({
    url: '/assignment/biz/service/flow/setTaskVariable',
    method: 'post',
    data
  })
}

// 转办
export function transferApi (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

// 作业设置变量
export function getAssignmentInfoByNodeCode (data) {
  return request({
    url: '/assignment/biz/service/instance/children/by/process',
    method: 'post',
    data
  })
}

/**
* 回退节点
* @param {Object} parameter
* @returns
*/
export function rollback (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

/**
* 关闭流程
* @param {Object} parameter
* @returns
*/
export function veto (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

/**
 * 待办详情数据
 * @param {Object} parameter
 * @returns
 */
export function queryWfDetail (params) {
  return request({
    url: '/assignment/biz/service/instance/wf/todo',
    method: 'get',
    params
  })
}


