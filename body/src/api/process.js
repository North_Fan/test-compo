import request from '@/utils/request'

const interfaceMap = {
  getProcessData: '/process/getProcessData',
  getUUid: '/basecenter/biz/service/util/getuuid',
  getProcessList: '/procintelligence/biz/service/processHeader/page',
  getPiDetail: '/procintelligence/biz/service/processHeader/get/',
  getPiInstanceDetail: '/procintelligence/biz/service/processHeader/instanceId/info/',
  saveProcess: '/procintelligence/biz/service/processHeader/save/process',
  getProcessTel: '/process/getProcessTel',
  getCreateTabs: '/process/getCreateTabs',
  getPiList: '/procintelligence/biz/service/processHeader/page',
  publishProcess: '/procintelligence/biz/service/processLine/publish',
  searchTreeData: '/process/biz/service/business/domain/get/lists',
  getTreeData: '/process/biz/service/business/domain/get/children',
  moveSaveTree: '/process/biz/service/business/domain/update/tree',
  AddDomain: '/process/biz/service/business/domain/add',
  getDomainDetail: '/process/biz/service/business/domain/detail/',
  getDomainHistory: '/process/biz/service/business/domain/history/list',
  responsibilityUpdate: '/process/biz/service/business/domain/responsibility/update',
  updateDomain: '/process/biz/service/business/domain/update',
  deleteDominNode: '/process/biz/service/business/domain/del/',
  exportDomain: '/process/biz/service/business/domain/export/'
}

export default interfaceMap

// 业务域列表
export function getProcessData (parameter) {
  return request({
    url: interfaceMap.getProcessData,
    method: 'post',
    data: parameter
  })
}

// 获取uid
export function getUUid (params) {
  return request({
    url: interfaceMap.getUUid,
    method: 'get',
    params
  })
}

// 获取过程列表
export function getProcessList (params) {
  return request({
    url: interfaceMap.getProcessList,
    method: 'post',
    data: params
  })
}

// 获取PI列表
export function getPiList (params) {
  return request({
    url: interfaceMap.getPiList,
    method: 'post',
    data: params
  })
}

// 获取过程设计器详情
export function getPiDetail (params) {
  return request({
    url: interfaceMap.getPiDetail + params.id,
    method: 'get'
  })
}

// 获取过程实例图的详情
export function getPiInstanceDetail (params) {
  return request({
    url: interfaceMap.getPiInstanceDetail + params.id,
    method: 'get'
  })
}

// 保存过程设计器
export function saveProcess (params) {
  return request({
    url: interfaceMap.saveProcess,
    method: 'post',
    data: params
  })
}

// 获取流程模板
export function getProcessTel (params) {
  return request({
    url: interfaceMap.getProcessTel,
    method: 'post',
    data: params
  })
}

// 获取tabs模板
export function getCreateTabs (params) {
  return request({
    url: interfaceMap.getCreateTabs,
    method: 'post',
    data: params
  })
}

// 发布过程
export function publishProcess (params) {
  return request({
    url: interfaceMap.publishProcess,
    method: 'post',
    data: params
  })
}

// 业务域树-模糊搜索
export function searchTreeData (params) {
  return request({
    url: interfaceMap.searchTreeData,
    method: 'post',
    data: params
  })
}

// 业务域树-获取业务域子节点
export function getTreeData (params) {
  return request({
    url: interfaceMap.getTreeData,
    method: 'post',
    data: params
  })
}
// 业务域树移动保存
export function moveSaveTree (params) {
  return request({
    url: interfaceMap.moveSaveTree,
    method: 'post',
    data: params
  })
}

// 添加业务域节点
export function AddDomain (params) {
  return request({
    url: interfaceMap.AddDomain,
    method: 'post',
    data: params
  })
}

// 业务域节点详情
export function getDomainDetail (params) {
  return request({
    url: interfaceMap.getDomainDetail + params,
    method: 'get'
  })
}

// 业务域历史记录
export function getDomainHistory (params) {
  return request({
    url: interfaceMap.getDomainHistory,
    method: 'post',
    data: params
  })
}

// 业务域职责
export function responsibilityUpdate (params) {
  return request({
    url: interfaceMap.responsibilityUpdate,
    method: 'post',
    data: params
  })
}
// 修改业务域节点
export function updateDomain (params) {
  return request({
    url: interfaceMap.updateDomain,
    method: 'post',
    data: params
  })
}

// 删除业务域节点
export function deleteDominNode (params) {
  return request({
    url: interfaceMap.deleteDominNode + params,
    method: 'get'
  })
}

// 导出业务域节点
export function exportDomain (params) {
  return request({
    url: interfaceMap.exportDomain + params,
    method: 'get'
  })
}
