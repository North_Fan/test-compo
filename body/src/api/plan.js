import request from '@/utils/request'
 /**
*  首页统计
* @param {Object} parameter
* @returns
*/
export function homeTaskRecord (data) {
  return request({
    url: '/plan/biz/service/pending/getCount',
    method: 'post',
    data
  })
 }

  /**
*  首页我的创建
* @param {Object} parameter
* @returns
*/
export function getHomeMyCreate (data) {
  return request({
    url: '/plan/biz/service/pending/getMyCreate',
    method: 'post',
    data
  })
 }

  /**
*  首页我的创建未读数
* @param {Object} parameter
* @returns
*/
export function getMyCreateCount () {
  return request({
    url: '/plan/biz/service/pending/getMyCreateCount',
    method: 'get'
  })
 }

 // 明细报表 table
export function getTaskListDetail (id) {
  return request({
    url: `/plan/biz/service/vessel/${id}`,
    method: 'get'
  })
}

   /**
*  首页我的任务
* @param {Object} parameter
* @returns
*/
export function getHomeMyTask (data) {
  return request({
    url: '/plan/biz/service/pending/getMyTask',
    method: 'post',
    data
  })
 }

/**
*  获取我的关注列表
* @param {Object} parameter
* @returns
*/
export function getNoticeList (data) {
  return request({
    url: '/plan/biz/service/pending/getMyAttention',
    method: 'post',
    data
  })
}

/**
*  获取我的关注列表
* @param {Object} parameter
* @returns
*/
export function getMyAttentionCount () {
  return request({
    url: '/plan/biz/service/pending/getMyAttentionCount',
    method: 'post'
  })
}

// // 综合计划-一般任务项-暂存
export function planAdd (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 综合计划-一般任务项-提交
export function planSubmit (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 计划模板设置
export function submitApprove (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}
