import request from '@/utils/request'

// 任务单关注接口
export function changeWatch (params) {
  return request({
    url: '/basecenter/biz/service/login/watch',
    method: 'post',
    data: params
  })
}

// 保存表头配置
export function columnsSave (params) {
  return request({
    url: '/basecenter/biz/service/login/auto/list',
    method: 'post',
    data: params
  })
}
// 查询SQCDP人员权限
export function queryOrgRoleMappingList (params) {
  return request({
    url: '/basecenter/biz/service/bSCxOrgRoleMappingDO/queryOrgRoleMappingList',
    method: 'post',
    data: params
  })
}

// 查询表头配置
export function searchColums (params) {
  return request({
    url: '/basecenter/biz/service/login/get/auto/list/' + params,
    method: 'get'
  })
}

// 获取用户信息
export function getUserInfo (params) {
  return request({
    url: '/basecenter/biz/service/login/get/user/info',
    method: 'get'
  })
}

// 获取用户信息
export function getUserDetail (data) {
  return request({
    url: '/basecenter/feignApi/userCenter/user/qryUserInfo',
    method: 'post',
    data
  })
}

// 获取选择的年度目标
export function getCrtAnnualtarget (params) {
  return request({
    url: `/basecenter/biz/service/login/get/annualtargetheader/${params}`,
    method: 'get'
  })
}

// 获取选择的年度目标
export function setCrtAnnualtarget (params) {
  return request({
    url: '/basecenter/biz/service/login/annualtargetheader',
    method: 'post',
    data: params
  })
}
// 获取自定义视图
export function requestCustomViewList (params) {
  return request({
    url: '/basecenter/biz/service/login/get/view/' + params,
    method: 'get'
  })
}
// 保存自定义视图
export function saveCustomViewList (params) {
  return request({
    url: '/basecenter/biz/service/login/view',
    method: 'post',
    data: params
  })
}

// 接口日志
export function dailyLogTrace (params) {
  return request({
    url: '/basecenter/biz/service/util/trace',
    method: 'get'
  })
}
export function dataDict (params) {
  return request({
    url: '/basecenter/biz/service/codetype/pageSyscodetype',
    method: 'post',
    data: params
  })
}
export function getSubdataDict (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/syscodeitemsByTypeCode?typeCode=' + params,
    method: 'get'
  })
}

// 查询数据字典类型
export function getSyscodetypes (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetypes?typeCode=' + params,
    method: 'get'
  })
}
// 添加数据字典一级节点
export function addNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype',
    method: 'post',
    data: params
  })
}
// 修改数据字典一级节点
export function updateNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype/update',
    method: 'post',
    data: params
  })
}

// 删除数据字典一级节点
export function deleteNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype/' + params,
    method: 'delete'
  })
}

// 添加数据字典
export function addSyscodeitem (params) {
  return request({
    url: ' /basecenter/biz/service/codeitem/syscodeitem',
    method: 'post',
    data: params
  })
}

// 查询数据字典
export function getSyscodeitem (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/getItemById?typeId=' + params,
    method: 'get'
  })
}

// 修改数据字典
export function updateSyscodeitem (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/syscodeitem/update',
    method: 'post',
    data: params
  })
}

/**
*  更新未读信息
* @param {Object} parameter
* @returns
*/
export function updateRead (params) {
  return request({
    url: `/basecenter/biz/service/message/updateRead/${params}`,
    method: 'post'
  })
 }

 export function singleFileUploadFileDownload (serviceUrl, data) {
  return request({
    url: serviceUrl + data,
    responseType: 'blob',
    method: 'post'
  })
}

/**
*  获取消息列表
* @param {Object} parameter
* @returns
*/
export function getMyMessList (data) {
  return request({
    url: '/basecenter/biz/service/message/pageMyMessage',
    method: 'post',
    data
  })
 }

 /**
*  获取未读消息数
* @param {Object} parameter
* @returns
*/
export function getUnreadCount () {
  return request({
    url: '/basecenter/biz/service/message/getUnreadCount',
    method: 'post'
  })
 }

 /**
* 回退节点
* @param {Object} parameter
* @returns
*/
export function rollback (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

/**
* 关闭流程
* @param {Object} parameter
* @returns
*/
export function veto (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}
// // 获取节点上次设置的候选人列表
// export function getLastCandidateOrOperator (data) {
//   const { processInstanceId = '', nodeCode = '' } = data
//   return request({
//     url: `/flow-platform/api/v1.0/public/getLastCandidateOrOperator?nodeCode=${nodeCode}&processInstanceId=${processInstanceId}`,
//     method: 'post',
//     data: {}
//   })
// }

// 根据ID找人员Name
export function findUserNameById (params) {
  return request({
    url: ' /basecenter/biz/service/codeitem/syscodeitem/' + params,
    method: 'delete'
  })
}

/**
 * 按钮权限查询
 * @returns
 */
 export function resourcePathQuery (path) {
  return request({
    url: '/basecenter/biz/service/user/resource?resourcePath=' + path,
    method: 'get'
  })
}

/**
 * 暂存
 * @param {*} url
 * @param {*} params
 * @returns
 */
export function planAdd (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

/**
 * 提交
 * @param {*} url
 * @param {*} params
 * @returns
 */
export function planSubmit (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 计划模板设置
export function submitApprove (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}
/**
 * 获取当前用户cookie
 * @returns
 */
export function xxlSsoSessionid () {
  return request({
    url: '/basecenter/biz/service/login/get/xxl_sso_sessionid',
    method: 'get'
  })
}
