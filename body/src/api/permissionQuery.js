import request from '@/utils/request'

const interfaceMap = {
  rolePermissionListQuery: '/master-data/roles/items'
}

export default interfaceMap

// 查询页面权限
export function rolePermissionListQuery (parameter) {
  return request({
    url: interfaceMap.rolePermissionListQuery,
    method: 'get',
    data: parameter
  })
}
