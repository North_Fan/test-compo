import request from '@/utils/request'
import qs from 'qs'

const interfaceMap = {
  getContractTreeData: '/contract/biz/contract/getLeftTree',
  getContractTypeTreeData: '/contract/getContractTypeTreeData',
  getContractTaskList: '/plan/biz/service/vessel/page',
  getContractTableData: '/contract/biz/contract/getContractList',
  getContractTemplate: '/contract/biz/service/contract/template/query',
  saveNewTemplate: '/contract/biz/service/contract/template/save',
  saveEditTemplate: '/contract/biz/service/contract/template/update',
  updateTemplateStatus: '/contract/biz/service/contract/template/changeStatus',
  getContractDemonstration: '/contract/getContractDemonstration',
  getContractnegotiationRecord: '/contract/getContractnegotiationRecord',
  getContractProgress: '/contract/getContractProgress',
  getProcessStatisticsList: '/contract/getProcessStatisticsList',
  getProcessStatisticsRecords: '/contract/getProcessStatisticsRecords',
  getProcessApplicationStatisticByCondition: '/contract/getProcessApplicationStatisticByCondition',
  getTaskdetailStatistics: '/contract/getTaskdetailStatistics',
  getDetailprocessApplicationData: '/contract/getDetailprocessApplicationData',
  getOverallprocessApplicationData: '/contract/getOverallprocessApplicationData',
  getContractCreate: '/contract/biz/service/contract/init',
  contractPreview: '/contract/biz/service/entry/contractPreview',
  contractEdit: '/contract/biz/service/entry/contractEdit',
  contractDownload: '/contract/biz/service/entry/contractDownload',
  contractEntry: '/contract/biz/service/entry/contractEntry',
  getContractBaseInfo: '/contract/biz/service/entry/getBaseInfo',
  getContractSeal: '/contract/biz/service/seal/getContractSeal',
  getApproveHistory: '/contract/biz/service/review/getApproveHistory',
  getTemplatePreviewUrl: '/contract/biz/service/contract/template/getPreviewUrl',
  getTemplateDownUrl: '/contract/biz/service/contract/template/getDownloadUrl',
  templateCreateUpload: '/contract/biz/service/wps/createUpload',
  deleteContractFile: '/contract/biz/service/contract/template/deleteFile',
  checkBookMark: '/contract/biz/service/contract/template/checkBookMark',
  detailDecision: '/contract/biz/negotiate/decision/detailDecision',
  detailRecord: '/contract/biz/negotiate/record/detailRecord',
  contractView: '/contract/biz/service/review/contractView',
  getBookMark: '/contract/biz/contract/getBookMark',
  negotiateCreateRecord: '/contract/biz/negotiate/record/create',
  negotiateCreateDecision: '/contract/biz/negotiate/decision/create',
  getTaskListDetail: '/contract/biz/contract/taskDetail',
  contractTextQuery: '/contract/biz/service/contract/text/query',
  contractTextSave: '/contract/biz/service/contract/text/save',
  contractTextUpdate: '/contract/biz/service/contract/text/update',
  contractTextChangeStatus: '/contract/biz/service/contract/text/changeStatus'
}

export default interfaceMap

// 获取系统视图树
export function getTreeData (parameter) {
  return request({
    url: interfaceMap.getContractTreeData,
    method: 'get',
    params: parameter
  })
}

// 获取类型视图树
export function getTypeTreeData (parameter) {
  return request({
    url: interfaceMap.getContractTypeTreeData,
    method: 'get',
    params: parameter
  })
}

// 获取合同列表
export function getTableData (parameter) {
  return request({
    url: interfaceMap.getContractTableData,
    method: 'post',
    data: parameter
  })
}

// 获取任务单列表
export function getTaskList (parameter) {
  return request({
    url: interfaceMap.getContractTaskList,
    method: 'post',
    data: parameter
  })
}

// 获取合同模板列表
export function getContractTemplate (parameter) {
  return request({
    url: interfaceMap.getContractTemplate,
    method: 'post',
    data: parameter
  })
}

// 新增合同模板
export function saveNewTemplate (parameter) {
  return request({
    url: interfaceMap.saveNewTemplate,
    method: 'post',
    data: parameter
  })
}

// 编辑合同模板
export function saveEditTemplate (parameter) {
  return request({
    url: interfaceMap.saveEditTemplate,
    method: 'post',
    data: parameter
  })
}

// 修改合同模板状态
export function updateTemplateStatus (parameter) {
  return request({
    url: interfaceMap.updateTemplateStatus,
    method: 'post',
    data: parameter
  })
}

// 获取合同示范文本
export function getContractDemonstration (parameter) {
  return request({
    url: interfaceMap.getContractDemonstration,
    method: 'get',
    params: parameter
  })
}

// 获取合同谈判记录
export function getContractnegotiationRecord (parameter) {
  return request({
    url: interfaceMap.getContractnegotiationRecord,
    method: 'get',
    params: parameter
  })
}

// 获取进展列表
export function getContractProgress (parameter) {
  return request({
    url: interfaceMap.getContractProgress,
    method: 'get',
    params: parameter
  })
}
// 获取合同审批记录
export function getApprovalRecord (parameter) {
  return request({
    url: interfaceMap.getApproveHistory,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计
export function getProcessStatisticsList (parameter) {
  return request({
    url: interfaceMap.getProcessStatisticsList,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计2
export function getProcessStatisticsRecords (parameter) {
  return request({
    url: interfaceMap.getProcessStatisticsRecords,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计-按业务域按单位
export function getProcessApplicationStatisticByCondition (parameter) {
  return request({
    url: interfaceMap.getProcessApplicationStatisticByCondition,
    method: 'get',
    params: parameter
  })
}

// 获取任务单详情统计
export function getTaskdetailStatistics (parameter) {
  return request({
    url: interfaceMap.getTaskdetailStatistics,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单详情实施及应用情况
export function getDetailprocessApplicationData (parameter) {
  return request({
    url: interfaceMap.getDetailprocessApplicationData,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单总体实施及应用情况
export function getOverallprocessApplicationData (parameter) {
  return request({
    url: interfaceMap.getOverallprocessApplicationData,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单总体实施及应用情况
export function getContractCreate (parameter) {
  return request({
    url: interfaceMap.getContractCreate,
    method: 'get',
    params: parameter
  })
}

// 合同预览
export function contractPreview (parameter) {
  return request({
    url: interfaceMap.contractPreview + '/' + parameter,
    method: 'get'
  })
}

// 合同在线编辑
export function contractOnLineEdit (parameter) {
  return request({
    url: interfaceMap.contractEdit + '/' + parameter,
    method: 'get'
  })
}

// 合同下载
export function contractDownload (parameter) {
  return request({
    url: interfaceMap.contractDownload + '/' + parameter,
    method: 'get'
  })
}

// 合同录入
export function contractEntry (parameter) {
  return request({
    url: interfaceMap.contractEntry + '/' + parameter,
    method: 'get'
  })
}
// 查看合同基本信息
export function getContractBaseInfo (parameter) {
  return request({
    url: interfaceMap.getContractBaseInfo + '?' + qs.stringify(parameter),
    method: 'get'
  })
}

// 合同用印
export function getContractSeal (parameter) {
  return request({
    url: interfaceMap.getContractSeal,
    method: 'get',
    params: parameter
  })
}

// 合同模板在线预览
export function getTemplatePreviewUrl (parameter) {
  return request({
    url: interfaceMap.getTemplatePreviewUrl + '/' + parameter,
    method: 'get'
  })
}

// 合同模板下载
export function getTemplateDownUrl (parameter) {
  return request({
    url: interfaceMap.getTemplateDownUrl + '/' + parameter,
    method: 'get'
  })
}

// 合同模板上传
export function templateCreateUpload (parameter) {
  console.log(parameter)
  return request({
    url: interfaceMap.templateCreateUpload + '/' + parameter.fileName + '/' + parameter.fileType,
    method: 'post',
    data: parameter.file
  })
}

// 合同模板--删除
export function deleteContractFile (parameter) {
  return request({
    url: interfaceMap.deleteContractFile + '/' + parameter,
    method: 'get'
  })
}

// 合同模板--校验书签
export function checkBookMark (parameter) {
  return request({
    url: interfaceMap.checkBookMark,
    method: 'post',
    data: parameter
  })
}

// 谈判决策详情
export function detailDecision (parameter) {
  return request({
    url: interfaceMap.detailDecision + '?recordObjIndex=' + parameter,
    method: 'post'
  })
}

// 谈判记录详情
export function detailRecordInfo (parameter) {
  return request({
    url: interfaceMap.detailRecord + '?recordObjIndex=' + parameter,
    method: 'post'
  })
}

// 合同查看
export function contractView (parameter) {
  return request({
    url: interfaceMap.contractView + '?' + qs.stringify(parameter),
    method: 'get'
  })
}

// 合同获取书签
export function getBookMark (parameter) {
  return request({
    url: interfaceMap.getBookMark + '/' + parameter,
    method: 'get'
  })
}

// 合同谈判记录添加
export function negotiateCreateRecord (parameter) {
  return request({
    url: interfaceMap.negotiateCreateRecord,
    method: 'post',
    data: parameter
  })
}

// 合同谈判决策添加
export function negotiateCreateDecision (parameter) {
  return request({
    url: interfaceMap.negotiateCreateDecision,
    method: 'post',
    data: parameter
  })
}

// 任务单详情
export function getTaskListDetail (parameter) {
  return request({
    url: interfaceMap.getTaskListDetail,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本list查询
export function contractTextQuery (parameter) {
  return request({
    url: interfaceMap.contractTextQuery,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本保存
export function contractTextSave (parameter) {
  return request({
    url: interfaceMap.contractTextSave,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本编辑
export function contractTextUpdate (parameter) {
  return request({
    url: interfaceMap.contractTextUpdate,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本修改状态
export function contractTextChangeStatus (parameter) {
  return request({
    url: interfaceMap.contractTextChangeStatus,
    method: 'post',
    data: parameter
  })
}
