import request from '@/utils/request'

const interfaceMap = {
  getOrgNameByOrgId: '/document/biz/service/base/searchUserOrDept',
  getDocTypeByCode: '/document/biz/service/getDocTypeByCode',
  getDefaultInnerDistribute: '/document/biz/service/general/getDefaultInnerDistribute',
  getInnerDistributeByObjId: '/document/biz/service/general/getInnerDistributeByObjId',
  outSubjectInfo: '/document/biz/service/subject/initInfo'
}

export default interfaceMap

// 预览文件获取二进制流 oss
export function getFileBlobPreviewFileOss (docId) {
  return request({
    url: `/filenode/biz/service/cmos-doc-node/normal/online-preview/${docId}`,
    responseType: 'blob',
    method: 'get'
  })
}

// 预览文件获取临时路径 wps
export function getFileBlobPreviewFile (oddId) {
  return request({
    url: `/filenode/biz/service/file-preview/get/url/${oddId}`,
    method: 'get'
  })
}

// 获取上传文件源数据，资料组件
export function getFileList (params) {
  return request({
    url: '/filenode/biz/service/cmos-doc-node/materials',
    method: 'post',
    data: params
  })
}

// 获取标准编码
export function getDocTypeByCode (params) {
  console.log(params)
  return request({
    url: `${interfaceMap.getDocTypeByCode}/${params}`,
    method: 'get'
  })
}

// 获取公司内部分发路线（默认）
export function getDefaultInnerDistribute () {
  return request({
    url: `${interfaceMap.getDefaultInnerDistribute}`,
    method: 'get'
  })
}

// 获取公司内部分发路线（实例）
export function getInnerDistributeByObjId (params) {
  return request({
    url: `${interfaceMap.getInnerDistributeByObjId}/${params}`,
    method: 'get'
  })
}

// 新建外协初始化
export function outSubjectInfo (params) {
  return request({
    url: `${interfaceMap.outSubjectInfo}`,
    method: 'post',
    data: params
  })
}
// 通过OSSId 批量获取上传文件信息
export function getAttachmentByOssIds (params) {
  return request({
    url: '/filenode/biz/service/cmos-doc-node/materials',
    method: 'post',
    data: params
  })
}
// 通过组织ID获取组织名称
export function getOrgNameByOrgId (params) {
  return request({
    url: `${interfaceMap.getOrgNameByOrgId}`,
    method: 'post',
    data: params
  })
}
