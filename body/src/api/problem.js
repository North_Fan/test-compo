import request from '@/utils/request'

const interfaceMap = {
  getProcessInstanceId: '/assignment/biz/service/instance/children/query',
  getFlowInstanceId: '/assignment/biz/service/instance/deep/query',
  getProblemList: '/quality/biz/service/question/page',
  createProblem: '/quality/biz/service/question/save',
  getProblemDetail: '/quality/biz/service/question/detail/',
  editProblem: '/quality/biz/service/question/edit',
  removeProblem: '/quality/biz/service/question/remove/',
  getProblemImportConfig: '/quality/biz/service/question/get/import/configure/',
  setProblemImportConfig: '/quality/biz/service/question/save/import/configure',
  importProblem: '/quality/biz/service/question/import',
  exportProblem: '/quality/biz/service/question/export',
  exportAllProblem: '/quality/biz/service/question/exportAll',
  getQualityProblemsType: '/problem/getQualityProblemsType',
  getAtaPSection: '/problem/getAtaPSection',
  getVehicles: '/quality/biz/service/code/manuSerialNum/page',
  getCauseAnalysis: '/quality/biz/service/code/reason/page',
  getCauseChildAnalysis: '/quality/biz/service/code/reason/child/',
  getProblemProcessData: '',
  getSourceProblemData: '/quality/biz/service/code/source/page',
  getLocationData: '/quality/biz/service/code/place/page',
  getAtaSectionData: '/quality/biz/service/code/ata/page',
  createEscapeTel: '/quality/biz/service/escape/template',
  getEscapeDetail: '/quality/biz/service/escape/',
  createAuditPlan: '/quality/biz/service/audit/add',
  editAuditPlan: '/quality/biz/service/audit/update',
  removeAuditPlan: '/quality/biz/service/audit/delete',
  queryAuditPlan: '/quality/biz/service/audit/list',
  initAuditPlan: '/quality/biz/service/audit/init'
}

export default interfaceMap

// 获取问题记录列表
export function getProblemList (parameter) {
  return request({
    url: interfaceMap.getProblemList,
    method: 'post',
    data: parameter
  })
}

// 创建记录
export function createProblem (parameter) {
  return request({
    url: interfaceMap.createProblem,
    method: 'post',
    data: parameter
  })
}

// 获取记录详情
export function getProblemDetail (params) {
  return request({
    url: interfaceMap.getProblemDetail + params.id,
    method: 'POST',
    data: params
  })
}

// 修改记录详情
export function editProblem (params) {
  return request({
    url: interfaceMap.editProblem,
    method: 'POST',
    data: params
  })
}

// 删除记录
export function removeProblem (params) {
  return request({
    url: interfaceMap.removeProblem + params.id,
    method: 'POST',
    data: params
  })
}

// 获取问题记录导入配置
export function getProblemImportConfig (params) {
  return request({
    url: interfaceMap.getProblemImportConfig + params.typeCode,
    method: 'POST',
    data: params
  })
}

// 设置问题记录导入配置
export function setProblemImportConfig (params) {
  return request({
    url: interfaceMap.setProblemImportConfig,
    method: 'POST',
    data: params
  })
}

// 导入问题记录
export function importProblem (params) {
  return request({
    url: interfaceMap.importProblem,
    method: 'POST',
    data: params
  })
}

// 导出问题记录
export function exportProblem (params) {
  return request({
    url: interfaceMap.exportProblem,
    method: 'POST',
    data: params
  })
}

// 导出全部问题记录
export function exportAllProblem (params) {
  return request({
    url: interfaceMap.exportAllProblem,
    method: 'POST',
    data: params
  })
}

export function getQualityProblemsType (parameter) {
  return request({
    url: interfaceMap.getQualityProblemsType,
    method: 'get',
    params: parameter
  })
}

// 获取ATA章节
export function getAtaPSection (parameter) {
  return request({
    url: interfaceMap.getQualityProblemsType,
    method: 'get',
    params: parameter
  })
}

// 获取飞机序列号
export function getVehicles (parameter) {
  return request({
    url: interfaceMap.getVehicles,
    method: 'post',
    data: parameter
  })
}

// 获取原因代码
export function getCauseAnalysis (parameter) {
  return request({
    url: interfaceMap.getCauseAnalysis,
    method: 'post',
    data: parameter
  })
}

// 获取原因代码的子集
export function getCauseChildAnalysis (parameter) {
  return request({
    url: interfaceMap.getCauseChildAnalysis + parameter.ID,
    method: 'post',
    data: parameter
  })
}

// 数据字典动态获取配置
export function getConfig (url, parameter) {
  return request({
    url,
    method: 'get',
    params: parameter
  })
}

// 动态的保存接口
export function saveDict (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

// 动态的加载字典表格
export function loadDictData (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

// 动态加载子集
export function loadChildDictData (url, parameter) {
  return request({
    url: url + parameter.id,
    method: 'post',
    data: parameter
  })
}

// 动态详情接口
export function getDictDetail (url, parameter) {
  return request({
    url: url + parameter.id,
    method: 'post',
    data: parameter
  })
}

// 动态删除接口
export function removeDictData (url, parameter) {
  return request({
    url,
    method: 'delete',
    data: parameter
  })
}

// 动态更新状态
export function updateDictState (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

export function getProblemProcessData (parameter) {
  return request({
    url: interfaceMap.getProblemProcessData,
    method: 'post',
    data: parameter
  })
}
export function getSourceProblemData (parameter) {
  return request({
    url: interfaceMap.getSourceProblemData,
    method: 'post',
    data: parameter
  })
}
export function getLocationData (parameter) {
  return request({
    url: interfaceMap.getLocationData,
    method: 'post',
    data: parameter
  })
}
export function getAtaSectionData (parameter) {
  return request({
    url: interfaceMap.getAtaSectionData,
    method: 'post',
    data: parameter
  })
}

export function getChildData (url, parameter) {
  return request({
    url: url + parameter.ID,
    method: 'post',
    data: parameter
  })
}

// 创建质量逃逸目模板
export function createEscapeTel (parameter) {
  return request({
    url: interfaceMap.createEscapeTel,
    method: 'post',
    data: parameter
  })
}

// 通过作业实例ID获取过程的ID
export function getProcessInstanceId (params) {
  return request({
    url: interfaceMap.getProcessInstanceId,
    method: 'get',
    params
  })
}

// 通过过程的节点获取流程组件的实例ID
export function getFlowInstanceId (params) {
  return request({
    url: interfaceMap.getFlowInstanceId,
    method: 'get',
    params
  })
}

// 查看问题过程详情
export function getEscapeDetail (parameter) {
  return request({
    url: interfaceMap.getEscapeDetail + parameter.id,
    method: 'get'
  })
}

// 添加审核计划
export function createAuditPlan (parameter) {
  return request({
    url: interfaceMap.createAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 修改审核计划
export function editAuditPlan (parameter) {
  return request({
    url: interfaceMap.editAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 删除审核计划
export function removeAuditPlan (parameter) {
  return request({
    url: interfaceMap.removeAuditPlan,
    method: 'post',
    data: parameter
  })
}
// 查询审核计划列表
export function queryAuditPlan (parameter) {
  return request({
    url: interfaceMap.queryAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 审核计划作业
export function initAuditPlan (parameter) {
  return request({
    url: interfaceMap.initAuditPlan,
    method: 'post',
    data: parameter
  })
}
