import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component
// import scriptUrl from '@/icons/iconfont/iconfont.js'
// import { Icon } from 'ant-design-vue'

// register globally
Vue.component('svg-icon', SvgIcon)

const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)

// const IconFont = Icon.createFromIconfontCN({
//   scriptUrl: scriptUrl
// })
// Vue.component('IconFont', IconFont)
