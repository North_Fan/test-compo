import Todo from '@/components/Todo'
import zhCN from 'ant-design-vue/lib/locale-provider/zh_CN'
export const TodoPlugins = {
  install (Vue) {
    Vue.prototype.$todoModal = function ({ params, cancel }) {
      this.$EventController.$on('closeTodoModal', () => {
        plugins.$destroy()
        div.remove()
        cancel()
        this.$EventController.$off('closeTodoModal')
      })
      const div = document.createElement('div')
      div.id = 'to_do_modal'
      document.body.appendChild(div)
      const routterContainer = document.querySelector('.routter-container')
      const height = routterContainer ? (routterContainer.offsetHeight - 48 - 65 || 710) : 710
      const Profile = Vue.extend({
        template: `
        <a-config-provider :locale="zhCN">
          <a-modal
            v-model="visible"
            :width="'100%'"

            :mask="true"
            :maskClosable="false"
            :destroyOnClose="true"
            :footer="null"
            @cancel="cancel"
            ref="todoModal"
          >
          <template slot="title">
          <span style="font-weight: 600;font-size:20px">待办详情</span>
          </template>
            <div :style="{ height, overflow:'hidden' }">
              <Todo :todoParams="params || {}" :isTodoDialog="isTodoDialog"></Todo>
            </div>
          </a-modal>
        </a-config-provider>
        `,
        components: {
          Todo
        },
        data: function () {
          return {
            visible: true,
            params,
            height: height + 'px',
            zhCN,
            isTodoDialog: true
          }
        },
        methods: {
          cancel () {
            plugins.$destroy()
            div.remove()
            cancel()
            this.$EventController.$off('closeTodoModal')
          }
        }
      })
      const todoModalIns = new Profile({
        router: this.$router,
        // i18n: this.$i18n,
        store: this.$store
      })
      const plugins = todoModalIns.$mount('#to_do_modal')
      return plugins
    }
  }
}
