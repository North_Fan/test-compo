import {
  findById
} from '@/utils/util'

const formItemList = [{
  queryId: 'KPI_NUMBER',
  queryName: '指标编号',
  placeholder: '请输入指标编号',
  inputType: 'input',
  disabledFlag: true
},
{
  queryId: 'KPI_NAME',
  queryName: '指标名称',
  placeholder: '请输入指标名称',
  inputType: 'input',
  disabledFlag: false
},
{
  queryId: 'KPI_CLASSIFY_VALUE',
  queryName: '类别',
  placeholder: '请选择类别',
  inputType: 'select',
  selectList: [
    {
      label: '安全（S）',
      value: 'S',
    },
    {
      label: '质量（Q）',
      value: 'Q',
    },
    {
      label: '成本（C）',
      value: 'C',
    },
    {
      label: '交付（D）',
      value: 'D',
    },
    {
      label: '人员能力（P）',
      value: 'P',
    },
  ],
  disabledFlag: false
},
{
  queryId: 'CREATEBY',
  queryName: '创建方',
  placeholder: '请输入创建方',
  inputType: 'slot',
  disabledFlag: true,
  slot: 'CREATEBY'
},
{
  queryId: 'GENERALITY',
  queryName: '通用性',
  placeholder: '请选择通用性',
  inputType: 'select',
  selectCode: 'SQCDP_KPI_USE_GENERALITY',
},
{
  queryId: 'EXPRESSION_FLAG',
  queryName: '是否有计算公式',
  placeholder: '请选择是否有计算公式',
  inputType: 'radio',
  selectList: [
    { label: '是', value: '是' },
    { label: '否', value: '否' }
  ],
  disabledFlag: false
},
{
  queryId: 'EXPRESSION',
  queryName: '计算公式',
  placeholder: '请输入计算公式',
  inputType: 'input',
  disabledFlag: false
},
{
  queryId: 'KPI_DESC',
  queryName: '指标描述',
  placeholder: '请输入指标描述',
  inputType: 'input',
  disabledFlag: false
},

{
  queryId: 'KPI_DEF_SOURCE',
  queryName: '指标来源',
  placeholder: ` `,
  inputType: 'textarea',
  disabledFlag: false
},
]
const layout = [{
  desc: '指标编号',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('KPI_NUMBER', formItemList)
},
{
  desc: '指标名称',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('KPI_NAME', formItemList)
},
{
  desc: '类别',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('KPI_CLASSIFY_VALUE', formItemList)
},
{
  desc: '创建方',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('CREATEBY', formItemList)
},
{
  desc: '通用性',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('GENERALITY', formItemList)
},
{
  desc: '是否有计算公式',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('EXPRESSION_FLAG', formItemList)
},
{
  desc: '计算公式',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('EXPRESSION', formItemList)
},
{
  desc: '指标描述',
  col: [8, 12, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('KPI_DESC', formItemList)
},
{
  desc: '指标来源',
  col: [24, 24, 24],
  flexLayout: false,
  labelCol: {
    span: 24
  },
  wrapperCol: {
    span: 24
  },
  item: findById('KPI_DEF_SOURCE', formItemList)
},
]

const itemData = {
  formItemList,
  layout
}
export default itemData
