import { findById } from '@/utils/util'

const formItemList = [
  {
    queryId: 'keyWord',
    queryName: '指标编号/指标名称',
    placeholder: '',
    inputType: 'input'
  },
  // {
  //   queryId: 'name',
  //   queryName: '指标名称',
  //   placeholder: '',
  //   inputType: 'input'
  // },
  {
    queryId: 'kpiClassifyValue',
    queryName: '指标类别',
    placeholder: '',
    inputType: 'select',
    selectList: [
      {
        label: '全部',
        value: '',
      },
      {
        label: '安全（S）',
        value: 'S',
      },
      {
        label: '质量（Q）',
        value: 'Q',
      },
      {
        label: '成本（C）',
        value: 'C',
      },
      {
        label: '交付（D）',
        value: 'D',
      },
      {
        label: '人员能力（P）',
        value: 'P',
      },
    ],
  },
  {
    queryId: 'generality',
    queryName: '通用性',
    placeholder: '',
    inputType: 'select',
    selectCode: 'SQCDP_KPI_USE_GENERALITY',
    // selectList: [
    //   {
    //     label: '通用',
    //     value: '1',
    //   },
    //   {
    //     label: '研发类',
    //     value: '2',
    //   },
    //   {
    //     label: '生产类',
    //     value: '3',
    //   },
    //   {
    //     label: '客服类',
    //     value: '4',
    //   },
    //   {
    //     label: '试飞类',
    //     value: '5',
    //   },
    //   {
    //     label: '营销类',
    //     value: '6',
    //   },
    // ],
  },
  {
    queryId: 'btnList',
    queryName: ' ',
    placeholder: '',
    inputType: 'slot',
    slot: 'btnList'
  }
]

const layout = [
  {
    col: [8, 8, 24],
    flexLayout: false,
    labelCol: { span: 9 },
    wrapperCol: { span: 15 },
    item: findById('keyWord', formItemList)
  },
  // {
  //   col: [8, 8, 24],
  //   flexLayout: false,
  //   labelCol: { span: 9 },
  //   wrapperCol: { span: 15 },
  //   item: findById('name', formItemList)
  // },
  {
    col: [8, 8, 24],
    flexLayout: false,
    labelCol: { span: 9 },
    wrapperCol: { span: 15 },
    item: findById('kpiClassifyValue', formItemList)
  },
  {
    col: [8, 8, 24],
    flexLayout: false,
    labelCol: { span: 9 },
    wrapperCol: { span: 15 },
    item: findById('generality', formItemList)
  },
  {
    col: [24, 24, 24],
    flexLayout: false,
    labelCol: { span: 19 },
    wrapperCol: { span: 4 },
    item: findById('btnList', formItemList)
  }
]
const DocumentData = {
  layout,
  formItemList
}
export default DocumentData
