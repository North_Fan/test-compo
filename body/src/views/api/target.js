import request from '@/utils/request'
const interfaceMap = {
  selectDemandTypeByParentId: '/plan/biz/service/demandOrderNumberDict/selectDemandTypeByParentId',
  demandMgtCreate: '/plan/biz/service/demandMgt/create',
  getDemandMgtPage: '/plan/biz/service/demandMgt/page',
  queryLinkedMileStone: '/plan/biz/service/aPlan/queryLinkedMileStone', // 查询关联里程碑
  getLinkedOrderNumber: '/plan/biz/service/aPlan/getLinkedOrderNumber',
  linkOrderNumber: '/plan/biz/service/aPlan/linkOrderNumber',
  pageOrderNumberVOByTypeCode: '/plan/biz/service/orderNumberMgt/pageOrderNumberVOByTypeCode',
  detail: '/plan/biz/service/aPlan/detail',
  // decomposeInit: '/plan/biz/service/aPlan/decomposeInit'// 创建下级计划
  decomposeInit: '/plan/biz/service/aPlanDecompose/decomposeInit',
  advancedQueryMileStone: '/plan/biz/service/aPlan/advancedQueryMileStone', // 查询并关联里程碑
  linkMileStone: '/plan/biz/service/aPlan/linkMileStone', // 关联里程碑
  unLinkMileStone: '/plan/biz/service/aPlan/unLinkMileStone', // 取消关联里程碑
  getLinkedVessels: '/plan/biz/service/aPlan/getLinkedVessels', // 查询已关联任务单
  linkVessel: '/plan/biz/service/aPlan/linkVessel', // 关联任务单
  unlink: '/plan/biz/service/aPlan/unlink', // 取消关联任务单
  orderNumberMgtDetail: '/plan/biz/service/orderNumberMgt/detail', // 取消关联任务单
  selectCategoryTreeByCode: 'biz/service/categoryTree/selectCategoryTreeByCode' /* 目录类型 */

}

// 令号详情
export function orderNumberMgtDetail (data) {
    return request({
      url: interfaceMap.orderNumberMgtDetail,
      method: 'post',
      data
    })
  }
// 需求类别
export function getNumTypeByParentCode (data) {
  return request({
    url: `${interfaceMap.selectDemandTypeByParentId}?parentId=${data.parentId}`,
    method: 'get'
  })
}

// 需求类别
export function demandMgtCreate (data) {
  return request({
    url: interfaceMap.demandMgtCreate,
    method: 'post',
    data
  })
}

// 需求类别
export function getDemandMgtPage (data) {
  return request({
    url: interfaceMap.getDemandMgtPage,
    method: 'post',
    data
  })
}
export function queryLinkedMileStone (data) {
  return request({
    url: `${interfaceMap.queryLinkedMileStone}?aPlanIdStr=${data}`,
    method: 'get'
  })
}
// 查询已关联令号
export function getLinkedOrderNumber (data) {
  return request({
    url: interfaceMap.getLinkedOrderNumber,
    method: 'post',
    data
  })
}
// 关联工作令号 linkOrderNumber
export function linkOrderNumber (data) {
  return request({
    url: interfaceMap.linkOrderNumber,
    method: 'post',
    data
  })
}
// 查询工作令号 pageOrderNumberVOByTypeCode
export function pageOrderNumberVOByTypeCode (data) {
  return request({
    url: interfaceMap.pageOrderNumberVOByTypeCode,
    method: 'post',
    data
  })
}
// 一本计划详情 detail
export function detail (data) {
  return request({
    url: interfaceMap.detail,
    method: 'post',
    data
  })
}
// decomposeInit
export function decomposeInit (data) {
  return request({
    url: interfaceMap.decomposeInit,
    method: 'post',
    data
  })
}
export function advancedQueryMileStone (data) {
  return request({
    url: interfaceMap.advancedQueryMileStone,
    method: 'post',
    data
  })
}
export function linkMileStone (data) {
  return request({
    url: interfaceMap.linkMileStone,
    method: 'post',
    data
  })
}
export function unLinkMileStone (data) {
  return request({
    url: interfaceMap.unLinkMileStone,
    method: 'post',
    data
  })
}
  export function getLinkedVessels (data) {
    return request({
      url: interfaceMap.getLinkedVessels,
      method: 'post',
      data
    })
  }
  export function linkVessel (data) {
    return request({
      url: interfaceMap.linkVessel,
      method: 'post',
      data
    })
  }
  export function unlink (data) {
    return request({
      url: interfaceMap.unlink,
      method: 'post',
      data
    })
  }
