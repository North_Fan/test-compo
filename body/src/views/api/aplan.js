import request from '@/utils/request'

const interfaceMap = {
  aPlanCreate: '/plan/biz/service/aPlan/init', /* 创建 */
  aPlanDetail: '/plan/biz/service/aPlan/detail', /* 详情 */
  aPlanDecomposeDetail: '/plan/biz/service/aPlanDecompose/detail', /* 详情 */
  aPlanDecomposeInit: '/plan/biz/service/aPlan/decomposeInit', /* 分解 */
  getAPlanPage: '/plan/biz/service/aPlan/pageVOByTypeCode',
  selectCategoryTreeByCode: '/plan/biz/service/categoryTree/selectCategoryTreeByCode', /* 目录类型 */
  selectCategoryTree: '/plan/biz/service/categoryTree/selectCategoryTree', // tree
  createProject: '/plan/biz/service/targetProject/createProject', // 项目新建
  updateProject: '/plan/biz/service/targetProject/updateProject', // 项目更新
  projectDetail: '/plan/biz/service/targetProject/projectDetail', // 项目详情
  unitBelong: '/plan/biz/service/targetProject/unitBelong', // 人员所属
  projectAdvancedQuery: '/plan/biz/service/targetProject/projectAdvancedQuery', // 项目查询
  aPlanAdvancedQuery: '/plan/biz/service/aPlan/aPlanAdvancedQuery', // tree查询一本计划
  annualTargetAdvancedQuery: '/plan/biz/service/aPlan/annualTargetAdvancedQuery',
  searchProjectByKind: '/plan/biz/service/projectMgtPage/searchProjectByKind' /* 1计划分类查项目 */,
  searchNetPlanByProject: '/plan/biz/service/projectMgtPage/searchNetPlanByProject' /* 2项目查所有网络计划（各级） */,
  searchMileStoneAndTargetByNetPlan: '/plan/biz/service/projectMgtPage/searchMileStoneAndTargetByNetPlan' /* 3网络计划查询里程碑和目标 */,
  searchVesselAndTaskByMileStone: '/plan/biz/service/projectMgtPage/searchVesselAndTaskByMileStone' /* 4里程碑查询关联的任务单及任务项 */,
  milestoneDetail: '/plan/biz/service/projectMgtPage/milestoneDetail' /* 里程碑详情 */,
  searchTaskByVessel: '/plan/biz/service/projectMgtPage/searchTaskByVessel'/* 5任务单查任务项 */,
  maintenanceTouchOff: '/plan/biz/service/maintenance/touchOff'/* 关联WBS热任务运维接口 */,
  selectNetNodeToLink: '/plan/biz/service/netPlan/selectNetNodeToLink'/* 关联WPS任务查询link关联 */

}
// 关联WBS热任务运维接口
export function maintenanceTouchOff (data) {
  return request({
    url: interfaceMap.maintenanceTouchOff,
    method: 'post',
    data
  })
}
// 关联WPS任务查询link关联
export function selectNetNodeToLink (data) {
  return request({
    url: interfaceMap.selectNetNodeToLink,
    method: 'get',
    params: data
  })
}
// 里程碑详情
export function milestoneDetail (data) {
  return request({
    url: interfaceMap.milestoneDetail,
    method: 'get',
    params: data
  })
}
// 1计划分类查项目
export function searchProjectByKind (data) {
  return request({
    url: interfaceMap.searchProjectByKind,
    method: 'post',
    data
  })
}
// 2项目查所有网络计划（各级）
export function searchNetPlanByProject (data) {
  return request({
    url: interfaceMap.searchNetPlanByProject,
    method: 'get',
    params: data
  })
}
// 3网络计划查询里程碑和目标
export function searchMileStoneAndTargetByNetPlan (data) {
  return request({
    url: interfaceMap.searchMileStoneAndTargetByNetPlan,
    method: 'get',
    params: data
  })
}

// 4里程碑查询关联的任务单及任务项
export function searchVesselAndTaskByMileStone (data) {
  return request({
    url: interfaceMap.searchVesselAndTaskByMileStone,
    method: 'get',
    params: data
  })
}
// 5任务单查任务项
export function searchTaskByVessel (data) {
  return request({
    url: interfaceMap.searchTaskByVessel,
    method: 'get',
    params: data
  })
}
// 创建项目
export function createProject (data) {
  return request({
    url: interfaceMap.createProject,
    method: 'post',
    data
  })
}
// 刷新项目
export function updateProject (data) {
  return request({
    url: interfaceMap.updateProject,
    method: 'post',
    data
  })
}
// 项目详情
export function projectDetail (data) {
  return request({
    url: interfaceMap.projectDetail,
    method: 'get',
    params: data
  })
}
// 项目详情
export function unitBelong (data) {
  return request({
    url: interfaceMap.unitBelong,
    method: 'get',
    params: data
  })
}
// 查询项目
export function projectAdvancedQuery (data) {
  return request({
    url: interfaceMap.projectAdvancedQuery,
    method: 'post',
    data
  })
}// 查询一本计划第二版
export function annualTargetAdvancedQuery (data) {
  return request({
    url: interfaceMap.annualTargetAdvancedQuery,
    method: 'post',
    data
  })
}
// 创建
export function aPlanCreate (data) {
  return request({
    url: interfaceMap.aPlanCreate,
    method: 'post',
    data
  })
}
// 分解
export function aPlanDecomposeInit (data) {
  return request({
    url: interfaceMap.aPlanDecomposeInit,
    method: 'post',
    data
  })
}
// 分解详情
export function aPlanDecomposeDetail (data) {
  return request({
    url: interfaceMap.aPlanDecomposeDetail,
    method: 'post',
    data
  })
}
/* // 目录树
export function selectCategoryTree (data) {
  return request({
    url: interfaceMap.selectCategoryTree,
    method: 'post',
    data
  })
} */

// /根据code查询分类树
export function selectCategoryTreeByCode (data) {
  return request({
    url: interfaceMap.selectCategoryTreeByCode,
    method: 'get',
    params: data
  })
}

// 分页查询
export function aPlanDetail (data) {
  return request({
    url: interfaceMap.aPlanDetail,
    method: 'post',
    data
  })
}

// 详情
export function getAPlanPage (data) {
  return request({
    url: interfaceMap.getAPlanPage,
    method: 'post',
    data
  })
}
// selectCategoryTree
export function selectCategoryTree (data) {
  return request({
    url: interfaceMap.selectCategoryTree,
    method: 'post',
    data
  })
}
// aPlanAdvancedQuery
export function aPlanAdvancedQuery (data) {
  return request({
    url: interfaceMap.aPlanAdvancedQuery,
    method: 'post',
    data
  })
}

// 校验文件
export function checkoutFile (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    params: data
  })
}
/**
 * 上传文件
 * @param {Object} parameter
 * @returns
 */
export function singleFileUploader (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    params: data
  })
}
/**
 * 上传文件
 * @param {Object} parameter
 * @returns
 */
export function singleFileUploaderPost (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    data
  })
}

export function syncImportFileState (url, ossId) {
  return request({
    url,
    method: 'post',
    params: {
      ossId
    }
  })
}
