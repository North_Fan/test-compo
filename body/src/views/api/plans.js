import request from '@/utils/request'

const interfaceMap = {
  getParentTask: '/plan/biz/service/feedback/explain/parentTask/get', /*  */
  getPlantreeData: '/plans/getPlantreeData',
  getPlanTreeDataChildren: '/plans/planTreeGetChildren',
  getPlanManagementData: '/plans/getPlanManagementData',
  getPlanHeaderData: '/plan/biz/service/vessel/page',
  getPlanDetailIdBeforeCreate: '/plans/getPlanDetailIdBeforeCreate',
  getModelPlanList: '/process/biz/service/start/flow/page',
  getCommonDocumentList: '/plans/getCommonDocumentList',
  getCommonDocumentChildrenList: '/plans/getCommonDocumentChildrenList',
  getVolumesDetail: '/plans/getVolumesDetail',
  getKPIFilterOptions: '/plans/getKPIFilterOptions',
  getKPIList: '/plans/getKPIList',
  getWBSCode: '/plans/getWBSCode',
  getWBSTableList: '/plans/getWBSTableList',
  getAssignmentInfo: '/plans/getAssignmentInfo',
  updateAssignmentInfo: '/plans/updateAssignmentInfo',
  getTaskInfo: '/plans/getTaskInfo',
  updateTaskInfo: '/plans/updateTaskInfo',
  findUserNameById: '/plans/findUserNameById',
  findDepartNameById: '/plans/findDepartNameById',
  disposeChecklistData: '/plan/biz/service/modelAccessControl/directOperateChecklistCatalog',
  disposeListOfDeliverablesFilesData: '/plan/biz/service/modelAccessControl/directOperateDeliverEntity',
  directDeleteModelAccessData: '/plan/biz/service/modelAccessControl/directDeleteModelAccessObj',
  MoveModelAccessData: '/plan/biz/service/modelAccessControl/directMoveModelAccessObj',
  directOperateReviewDemand: '/plan/biz/service/modelAccessControl/directOperateReviewDemand',
  findRoleNameById: '/plans/findRoleNameById',
  getWBSName: '/plans/getWBSName',
  getReporthours: '/plans/getReporthours',
  getWBSList: '/plan/biz/service/wbs/get/lists',
  getStatisticsList: '/plans/getStatisticsList',
  getWBSChildren: '/plans/getWBSChildren',
  saveChildrenWBS: '/plans/saveChildrenWBS',
  removeChildrenWBS: '/plans/removeChildrenWBS',
  verifyWBSChildren: '/plans/verifyWBSChildren',
  reportDetailTableList: '/plans/reportDetailTableList',
  getModelPlanReviewList: '/plan/biz/service/vessel/adjust/model/getImportant',
  getMinePlanApply: '/plan/biz/service/vessel/adjust/model/getMine',
  getPlanPointCallbackList: '/plan/biz/service/vessel/adjust/taskReturn',
  getInstanceFlow: '/plan/biz/service/related/process/list',
  getPlanTaskDetail: '/plan/biz/service/task/',
  saveWBSCode: '/plan/biz/service/wbs/save/wbs',
  getWBSCodeDetail: '/plan/biz/service/wbs/get/WbsById/',
  getTemplateList: '/plans/getTemplateList',
  updateWBS: '/plan/biz/service/wbs/update/wbs',
  deleteWBS: '/plan/biz/service/wbs/delete/',
  getWBSHistory: '/plan/biz/service/wbs/get/wbsHistory',
  getDecompose: '/plan/biz/service/task/decompose/create',
  getChangeDetail: '/plan/biz/service/task/change/create',
  getImportWbs: '/plan/biz/service/wbs/importWbs',
  getImportWbsWorkTime: '/plan/biz/service/wbs/importWbsWorkTime',
  subcontractTask: '/plan/biz/service/vessel/adjust/subcontractTask',
  subcontractCreate: '/plan/biz/service/subcontract/create',
  taskReturnCreate: '/plan/biz/service/taskReturn/create',
  cancelCheck: '/plan/biz/service/task/assign/cancel/check/',
  getqueryApproveHistorys: '/contract/biz/service/contract/getqueryApproveHistorys/',
  getPlanList: '/contract/biz/contract/getContractPerformancePlanList/',
  cardGetToken: '/plan/biz/third/authentication/getToken',
  restartTaskProcess: '/plan/biz/service/operation/restartTaskProcess',
  restartGeneralTaskProcess: '/plan/biz/service/operation/restartGeneralTaskProcess',
  stopPlanTask: '/plan/biz/service/operation/stopPlanTask',
  repairWorkHoursApproval: '/plan/biz/service/operation/repairWorkHoursApproval',
  updateDeliverableSelfInspection: '/plan/biz/service/modelAccessControl/updateDeliverableSelfInspection',
  orderNumberMgt: '  /plan/biz/service/orderNumberMgt/init',
  selectOrderNumberTypeByParentId: '/plan/biz/service/demandOrderNumberDict/selectOrderNumberTypeByParentId',
  pageOrderNumberVOByTypeCode: '/plan/biz/service/orderNumberMgt/pageOrderNumberVOByTypeCode',
  upPushMsgStatus: '/plan/biz/service/planTaskPushSupplier/upPushMsgStatus',
  getMyCharged: '/plan/biz/service/taskCenter/myCharged', // 任务中心我负责的
  getMyManaged: '/plan/biz/service/taskCenter/myManaged', // 任务中心我分管的
  getMyWatched: '/plan/biz/service/taskCenter/myWatched',
  getMyAssigned: '/plan/biz/service/taskCenter/myAssigned',
  getCalendarExec: '/sqcdp/biz/service/sop/calendarExec/list',
  getCalendarExecExport: '/sqcdp/biz/service/sop/calendarExec/export',
  getStandOperaEntry: '/sqcdp/biz/service/sop/standardOperationList/list',
  templateManageAdd: '/sqcdp/biz/service/sop/templateManage/add', // 作业新增
  templateManageList: '/sqcdp/biz/service/sop/templateManage/list',
  templateManageRemove: '/sqcdp/biz/service/sop/templateManage/remove',
  templateManageUpdate: '/sqcdp/biz/service/sop/templateManage/update',
  templateManageCopy: '/sqcdp/biz/service/sop/templateManage/copy',
  positionList: '/sqcdp/biz/service/sop/position/list', // sop岗位list
  positionAdd: '/sqcdp/biz/service/sop/position/add',
  positionRemove: '/sqcdp/biz/service/sop/position/remove',
  positionUpdate: '/sqcdp/biz/service/sop/position/update',
  updateCalendarExeStatusTiming: '/sqcdp/biz/service/sop/calendar/updateCalendarExeStatusTiming',
  calendarByPage: '/sqcdp/biz/service/sop/calendar/calendarByPage',
  updateCalendarDate: '/sqcdp/biz/service/sop/calendar/updateCalendarDate',
  queryResultExport: '/sqcdp/biz/service/sop/calendar/queryResultExport',
  standardOperationListCreate: '/sqcdp/biz/service/sop/standardOperationList/create',
  standardOperationListQuery: '/sqcdp/biz/service/sop/standardOperationList/list',
  standardOperationListExport: '/sqcdp/biz/service/sop/standardOperationList/export',
  standardOperationListDetail: '/sqcdp/biz/service/sop/standardOperationList/list',
  getExecuteTimeForCron: '/sqcdp/biz/service/sop/calendar/resolveCronStr',
  getTaskDetail: '/plan/biz/service/taskCenter/taskDetail', // 获取任务单详情
  myManagedStatistic: '/plan/biz/service/taskCenter/myManagedStatistic', // 我分管的统计

  exportCurrent: '/sqcdp/biz/service/sop/standardOperationList/exportCurrent'
}

export default interfaceMap
// 任务反馈表单说明-上级任务项
export function getParentTask (parameter) {
  return request({
    url: interfaceMap.getParentTask,
    method: 'post',
    data: parameter
  })
}
// 重启计划点反馈流程
export function repairWorkHoursApproval (parameter) {
  return request({
    url: `${interfaceMap.repairWorkHoursApproval}?taskCode=${parameter}`,
    method: 'post'
  })
}
// 重启计划点反馈流程
export function restartTaskProcess (parameter) {
  return request({
    url: `${interfaceMap.restartTaskProcess}?taskId=${parameter.taskId}&taskCode=${parameter.taskCode}`,
    method: 'get'
  })
}
// 重启综合计划反馈流程
export function restartGeneralTaskProcess (parameter) {
  return request({
    url: `${interfaceMap.restartGeneralTaskProcess}?taskId=${parameter.taskId}&taskCode=${parameter.taskCode}`,
    method: 'get'
  })
}

// 关闭任务项
export function stopPlanTask (parameter) {
  return request({
    url: `${interfaceMap.stopPlanTask}?taskId=${parameter.taskId}&taskCode=${parameter.taskCode}&chckTime=${parameter.checkTime}`,
    method: 'get'
  })
}
// 型号门禁-交付物自查
export function updateDeliverableSelfInspection (parameter) {
  return request({
    url: interfaceMap.updateDeliverableSelfInspection,
    method: 'post',
    data: parameter
  })
}
// 令号初始值
export function orderNumberMgt (parameter) {
  return request({
    url: interfaceMap.orderNumberMgt,
    method: 'post',
    data: parameter
  })
}
// 令号数据分页查询
export function pageOrderNumberVOByTypeCode (parameter) {
  return request({
    url: interfaceMap.pageOrderNumberVOByTypeCode,
    method: 'post',
    data: parameter
  })
}
// 驳回供应商状态
export function upPushMsgStatus (parameter) {
  return request({
    url: interfaceMap.upPushMsgStatus,
    method: 'post',
    data: parameter
  })
}
// 点检表查询
export function getCalendarExec (parameter) {
  return request({
    url: interfaceMap.getCalendarExec,
    method: 'post',
    data: parameter
  })
}
// 点检表导出
export function getCalendarExecExport (parameter) {
  return request({
    url: interfaceMap.getCalendarExecExport,
    method: 'post',
    data: parameter
  })
}

export function selectOrderNumberTypeByParentId (data) {
  return request({
    url: `${interfaceMap.selectOrderNumberTypeByParentId}?parentId=${data.parentId}`,
    method: 'get'
  })
}

// 查询计划分类管理页面列表数据
export function getPlanManagementData (parameter) {
  return request({
    url: interfaceMap.getPlanManagementData,
    method: 'get',
    data: parameter
  })
}
// 查询计划分类树数据
export function getPlantreeData (parameter) {
  return request({
    url: interfaceMap.getPlantreeData,
    method: 'get',
    data: parameter
  })
}

// 查询计划分类子节点数据
export function getPlanTreeDataChildren (parameter) {
  return request({
    url: interfaceMap.getPlanTreeDataChildren,
    method: 'get',
    data: parameter
  })
}

// 计划管理列表
export function getPlanHeaderData (parameter) {
  return request({
    url: interfaceMap.getPlanHeaderData,
    method: 'post',
    data: parameter
  })
}
// 查询
export function searchDocumentTableDataList (parameter) {
  return request({
    url: interfaceMap.searchDocumentTableDataList,
    method: 'post',
    params: parameter
  })
}

// 计划点详情默认值
export function getPlanDetailIdBeforeCreate () {
  return request({
    url: interfaceMap.getPlanDetailIdBeforeCreate,
    method: 'get'
  })
}

// 型号计划列表
export function getModelPlanList (params) {
  return request({
    url: interfaceMap.getModelPlanList,
    method: 'post',
    data: params
  })
}

// 通用文件列表
export function getCommonDocumentList (params) {
  return request({
    url: interfaceMap.getCommonDocumentList,
    method: 'get',
    params: params
  })
}

// 通用文件子列表
export function getCommonDocumentChildrenList (params) {
  return request({
    url: interfaceMap.getCommonDocumentChildrenList,
    method: 'get',
    params: params
  })
}

// 分册详情
export function getVolumesDetail (params) {
  return request({
    url: interfaceMap.getVolumesDetail,
    method: 'get',
    params: params
  })
}

// 获取年度目标过滤条件
export function getKPIFilterOptions (params) {
  return request({
    url: interfaceMap.getKPIFilterOptions,
    method: 'get',
    params: params
  })
}

// 年度计划列表
export function getKPIList (params) {
  return request({
    url: interfaceMap.getKPIList,
    method: 'get',
    params: params
  })
}

// 获取WBScode
export function getWBSCode (params) {
  return request({
    url: interfaceMap.getWBSCode,
    method: 'get',
    params: params
  })
}
// 获取WBSTableList
export function getWBSTableList (params) {
  return request({
    url: interfaceMap.getWBSTableList,
    method: 'get',
    params: params
  })
}

// 获取计划详情
export function getAssignmentInfo (params) {
  return request({
    url: interfaceMap.getAssignmentInfo,
    method: 'get',
    params: params
  })
}

// 更新计划详情
export function updateAssignmentInfo (params) {
  return request({
    url: interfaceMap.updateAssignmentInfo,
    method: 'post',
    params: params
  })
}
// 获取任务详情
export function getTaskInfo (params) {
  return request({
    url: interfaceMap.getAssignmentInfo,
    method: 'get',
    params: params
  })
}

// 更新任务详情
export function updateTaskInfo (params) {
  return request({
    url: interfaceMap.updateAssignmentInfo,
    method: 'post',
    params: params
  })
}

// 综合计划-一般任务项-暂存
export function planAdd (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 综合计划-一般任务项-提交
export function planSubmit (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 计划模板校验
export function planCheck (params) {
  return request({
    url: '/assignment/biz/service/user/check',
    method: 'get',
    params
  })
}

// 计划模板设置
export function planSet (params) {
  return request({
    url: '/assignment/biz/service/user/set',
    method: 'post',
    data: params
  })
}

// 计划模板设置
export function submitApprove (url, params) {
  return request({
    url,
    method: 'post',
    data: params
  })
}

// 根据ID找Role Name
export function findRoleNameById (params) {
  return request({
    url: interfaceMap.findRoleNameById,
    method: 'post',
    data: params
  })
}

// 根据ID找人员Name
export function findUserNameById (params) {
  return request({
    url: interfaceMap.findUserNameById,
    method: 'post',
    data: params
  })
}

// 计划分类新增
export function setPlanclassifyList (params) {
  return request({
    url: '/plan/biz/service/planclassify',
    method: 'post',
    data: params
  })
}

// 计划分类编辑
export function editPlanclassify (params) {
  return request({
    url: '/plan/biz/service/planclassify',
    method: 'patch',
    data: params
  })
}

// 计划分类删除
export function deletePlanclassify (params) {
  const { id } = params
  return request({
    url: `/plan/biz/service/planclassify/delete/${id}`,
    method: 'delete'
  })
}

// 计划分类列表子节点
export function getPlanclassifyChildrenList (params) {
  return request({
    url: '/plan/biz/service/planclassify/listByPId',
    method: 'get',
    params
  })
}

// 计划分类-通过ID查询详情
export function getPlanclassifyDetail (params) {
  return request({
    url: '/plan/biz/service/planclassify/selectById',
    method: 'get',
    params
  })
}

// 计划分类列表分页查询
export function getPlanclassifyList (params) {
  return request({
    url: '/plan/biz/service/planclassify/selectPageNode',
    method: 'get',
    params
  })
  /**
   * {
        adjustAssignmentId: '', // 调整任务id(调整表单)
        cateName: '', // '任务单类型名称'
        cateNumb: '', // '任务单类型编号'
        current,
        delFlag: '', // 逻辑删除标记
        feedbackAssignmentId: '', // 反馈任务id(反馈表单)
        id: '', // ID
        mainAssignmentId: '', // 编制任务id(编制表单)
        orders: [
          {
            asc: true,
            column: 1
          }
        ],
        parentId: '', // 父级ID
        resolveAssignmentId: '', // 分解任务id(分解表单)
        showFlag: '', // 是否隐藏
        size: pageSize,
        subType: '', // 任务组任务类型
        taskCate: '', // 任务类型
        total: 0,
        version: '', // 数据版本(保留)
        createTime: '', // 创建时间
        createBy: '', // 创建人
        updateTime: '', // 更新时间
        updateBy: '' // 更新人
      }
   */
}

// 查询计划分类树数据
export function getPlanclassifyAllList () {
  return request({
    url: '/plan/biz/service/planclassify/selectAllList',
    method: 'get'
  })
}
// 查询型号门禁管理分类树数据
export function getModelAccessControlList (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listChildChecklistCatalog?parentCatalog=' + params,
    method: 'post'
  })
}
// 型号门禁-查询两层子检查单目录
export function getlistChildChecklistCatalogIn2 (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listChildChecklistCatalogIn2Level?parentCatalog=' + params,
    method: 'post'
  })
}
// 查询型号门禁管理检查单目录数据
export function getModelAccessControlData (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/queryChecklistCatalog?catalogId=' + params,
    method: 'post'
  })
}

// 查询型号门禁管理交付物单个数据
export function getModelAccessDeliverData (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/queryDeliverEntity?entityId=' + params,
    method: 'post'
  })
}
// 查询型号门禁管理交付物
export function getListOfDeliverablesFilesData (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listDeliverEntity?parentCatalog=' + params,
    method: 'post'
  })
}
// 处理检查单目录(增删改)
export function disposeChecklistData (params) {
  return request({
    url: interfaceMap.disposeChecklistData,
    method: 'post',
    data: params
  })
}
// 处理交付物(增删改)
export function disposeListOfDeliverablesFilesData (params) {
  return request({
    url: interfaceMap.disposeListOfDeliverablesFilesData,
    method: 'post',
    data: params
  })
}
// 型号门禁-直接删除对象
export function directDeleteModelAccessData (params) {
  return request({
    url: interfaceMap.directDeleteModelAccessData,
    method: 'post',
    data: params
  })
}
// 型号门禁-直接增删评审要求对象
export function directOperateReviewDemand (params) {
  return request({
    url: interfaceMap.directOperateReviewDemand,
    method: 'post',
    data: params
  })
}
// 型号门禁-直接移动对象
export function MoveModelAccessData (params) {
  return request({
    url: interfaceMap.MoveModelAccessData,
    method: 'post',
    data: params
  })
}

// 型号门禁-查看修订历史
export function getListEditHistory (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listEditHistory?number=' + params,
    method: 'post'
  })
}
// 型号门禁-查看子评审要求修订历史
export function getListEditHistoryList (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listReviewDemandEditHistory?parentIdentity=' + params,
    method: 'post'
  })
}
// 型号门禁-查询子评审要求对象
export function ReviewDemandList (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/listReviewDemand?parentNumber=' + params,
    method: 'post'
  })
}
// 型号门禁-生成对象新序号
export function getNewModelAccessNumber (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/getNewModelAccessNumber?objCode=' + params,
    method: 'post'
  })
}
// 型号门禁-导出数据
export function entranceGuardExport (params) {
  return request({
    url: '/plan/biz/service/modelAccessControl/exportData?catalogId=' + params,
    method: 'post'
  })
}

// 根据ID找部门Name
export function findDepartNameById (params) {
  return request({
    url: interfaceMap.findDepartNameById,
    method: 'post',
    data: params
  })
}

// 根据WBScode获取WBS name
export function getWBSName (params) {
  return request({
    url: interfaceMap.getWBSName,
    method: 'get',
    data: params
  })
}

// 工时填报table
export function getReporthours (parameter) {
  return request({
    // url: interfaceMap.getReporthours,
    url: '/plan/biz/service/task/hours/page',
    method: 'post',
    data: parameter
  })
}

// 工时填报table
export function getGroupReporthours (parameter) {
  return request({
    url: '/plan/biz/service/task/hours/group/page',
    method: 'post',
    data: parameter
  })
}

// 获取WBS列表
export function getWBSList (params) {
  return request({
    url: interfaceMap.getWBSList,
    method: 'post',
    data: params
  })
}

// 任务项统计
export function getStatisticsList (params) {
  return request({
    url: interfaceMap.getStatisticsList,
    method: 'get',
    params: params
  })
}

// WBS 子项
export function getWBSChildren (params) {
  return request({
    url: interfaceMap.getWBSChildren,
    method: 'get',
    params: params
  })
}

// 验证WBS 子项
export function verifyWBSChildren (params) {
  return request({
    url: interfaceMap.verifyWBSChildren,
    method: 'post',
    params: params
  })
}

// 添加WBS 子项
export function saveChildrenWBS (params) {
  return request({
    url: interfaceMap.saveChildrenWBS,
    method: 'post',
    params: params
  })
}

// 添加WBS 子项
export function removeChildrenWBS (params) {
  return request({
    url: interfaceMap.removeChildrenWBS,
    method: 'post',
    params: params
  })
}

// 添加完整WBS
export function addNewWBS (params) {
  return request({
    url: interfaceMap.removeChildrenWBS,
    method: 'post',
    params: params
  })
}

// 明细报表 table
export function reportDetailTableList (params) {
  return request({
    url: interfaceMap.reportDetailTableList,
    method: 'get',
    params: params
  })
}

// 明细报表 table
export function getTaskListDetail (id) {
  return request({
    url: `/plan/biz/service/vessel/${id}`,
    method: 'get'
  })
}

// 查询任务单详情信息
export function getTaskDetailByid (id) {
  return request({
    url: `/plan/biz/service/vessel/detail/${id}`,
    method: 'get'
  })
}

// 创建任务单接口
export function getCreateTabs (params) {
  return request({
    url: '/plan/biz/service/vessel/create',
    method: 'post',
    data: params
  })
}

// 调整任务单前置校验
export function vesselAdjustCheck (vesselId) {
  return request({
    url: `/plan/biz/service/vessel/adjust/check/${vesselId}`,
    method: 'get'
  })
}

// 调整任务单接口
export function getAdjustTabs (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/create',
    method: 'post',
    data: params
  })
}

// 批量调整任务项前置校验
export function taskAdjustCheck (vesselId) {
  return request({
    url: `/plan/biz/service/adjust/check/${vesselId}`,
    method: 'get'
  })
}

// 批量调整任务项接口
export function getAdjustTaskItemsTab (params) {
  return request({
    url: '/plan/biz/service/adjust/create',
    method: 'post',
    data: params
  })
}

// 计划点转包--list
export function subcontractTask (params) {
  return request({
    url: interfaceMap.subcontractTask,
    method: 'post',
    data: params
  })
}

// 型号计划待送审列表
export function getModelPlanReviewList (params) {
  return request({
    url: interfaceMap.getModelPlanReviewList,
    method: 'post',
    data: params
  })
}
// 我的申请
export function getMinePlanApply (params) {
  return request({
    url: interfaceMap.getMinePlanApply,
    method: 'post',
    data: params
  })
}

// 获取计划任务项详情接口
export function getPlanTaskDetail (id) {
  return request({
    url: `/plan/biz/service/task/${id}`,
    method: 'get'
  })
}

// 获取质量问题反馈任务项详情接口
export function getPlanTaskDetailQuality (id) {
  return request({
    url: `/plan/biz/service/task/detail/${id}`,
    method: 'get'
  })
}

// 获取计划点详情
export function getExplainTaskDetail (id) {
  return request({
    url: `/plan/biz/service/feedback/explain/task/detail/${id}`,
    method: 'get'
  })
}

// 获取计划任务项详情接口 -有校验
export function getPlanTaskDetailWithCheck (id) {
  return request({
    url: `/plan/biz/service/task/with/check/${id}`,
    method: 'get'
  })
}

// 计划点驳回列表
export function getPlanPointCallbackList (params) {
  return request({
    url: interfaceMap.getPlanPointCallbackList,
    method: 'post',
    data: params
  })
}
// 获取计划点流程
export function getInstanceFlow (params) {
  return request({
    url: interfaceMap.getInstanceFlow,
    method: 'post',
    data: params
  })
}

// 新建wbscode
export function saveWBSCode (params) {
  return request({
    url: interfaceMap.saveWBSCode,
    method: 'post',
    data: params
  })
}

// 编辑wbscode
export function getWBSCodeDetail (params) {
  return request({
    url: interfaceMap.getWBSCodeDetail + params,
    method: 'get'
  })
}

// 反馈流程列表
export function getTemplateList (params) {
  return request({
    url: interfaceMap.getTemplateList,
    method: 'get',
    params
  })
}
// 修改WBS
export function updateWBS (params) {
  return request({
    url: interfaceMap.updateWBS,
    method: 'post',
    data: params
  })
}
// 删除WBS
export function deleteWBS (params) {
  return request({
    url: interfaceMap.deleteWBS + params,
    method: 'delete'
  })
}
// WBS操作历史
export function getWBSHistory (params) {
  return request({
    url: interfaceMap.getWBSHistory,
    method: 'get',
    params: params
  })
}

// 我的任务分解
export function getDecompose (params) {
  return request({
    url: interfaceMap.getDecompose,
    method: 'post',
    data: params
  })
}

// 我的任务调整任务项
export function getChangeDetail (params) {
  return request({
    url: interfaceMap.getChangeDetail,
    method: 'post',
    data: params
  })
}

// 相关任务单列表
export function getRelatedList (params) {
  return request({
    url: '/plan/biz/service/relate/vessel/list',
    method: 'post',
    data: params
  })
}

// 相关任务单保存
export function saveRelatedList (params) {
  return request({
    url: '/plan/biz/service/relate/vessel/save',
    method: 'post',
    data: params
  })
}

// 相关任务单删除
export function delRelatedList (params) {
  return request({
    url: '/plan/biz/service/relate/batch/del',
    method: 'post',
    data: params
  })
}

// 相关任务单删除
export function getRelatedTaskItemList (params) {
  return request({
    url: '/plan/biz/service/relate/task/list',
    method: 'post',
    data: params
  })
}

// 相关任务单
export function saveRelatedTaskItemList (params) {
  return request({
    url: '/plan/biz/service/relate/task/save',
    method: 'post',
    data: params
  })
}

// 任务管理列表分页接口
export function getTaskManagementListData (parameter) {
  return request({
    url: '/plan/biz/service/task/page',
    method: 'post',
    data: parameter
  })
}

// 任务管理导出V2接口
export function getResultListData (parameter) {
  return request({
    url: '/plan/biz/service/task/exportPlanTaskV2',
    method: 'post',
    data: parameter
  })
}
// 任务管理导出结果查询
export function getViewExportData (parameter) {
  return request({
    url: '/plan/biz/service/exportRecord/findAllExportRecordByPage',
    method: 'post',
    data: parameter
  })
}
// 任务管理数据导出(包含修改状态)
export function ExportInfoManagementStatus (id) {
  return request({
    url: `/plan/biz/service/exportRecord/updateRecordByOssId?ossId=${id}`,
    method: 'get'
  })
}
// 门禁高级查询列表分页接口
export function getAdvancedQueryListData (parameter) {
  return request({
    url: '/plan/biz/service/modelAccessControl/advancedQuery',
    method: 'post',
    data: parameter
  })
}

// 任务管理数据导出
export function exportTaskManagement (parameter) {
  return request({
    url: '/plan/biz/service/task/exportPlanTask',
    method: 'post',
    data: parameter
  })
  /**
   * {
      "bizScenario": {},
      "chargeDepartment": "string",
      "chargeDepartmentAlias": "string",
      "compDept": "string",
      "compTimeEnd": "2021-11-12T09:50:03.798Z",
      "compTimeStart": "2021-11-12T09:50:03.798Z",
      "containAssignPerson": false,
      "containSubOrg": false,
      "evalPers": "string",
      "keyValue": "string",
      "planClassifyId": 0,
      "summaryMethod": "string",
      "taskCode": "string",
      "taskName": "string",
      "taskNature": "string",
      "taskOrigin": "string",
      "taskStateList": [
        "string"
      ],
      "taskType": "string",
      "vesselCode": "string",
      "vesselName": "string"
    }
  */
}

// 任务管理数据导出
export function syncExportInfoManagement (parameter) {
  return request({
    url: '/plan/biz/service/task/syncExportInfo',
    method: 'get',
    data: parameter
  })
}

// 任务管理数据导出--重置缓存状态
export function reloadExportInfoManagement (parameter) {
  return request({
    url: '/plan/biz/service/task/reloadExportInfo',
    method: 'get',
    data: parameter
  })
}

// 年度目标新增
// export function getPlnxannualtargetheader (parameter) {
//   return request({
//     url: '/plan/biz/service/plnxannualtargetheader',
//     method: 'post',
//     data: parameter
//   })
// }

// 年度目标模板查询
export function getPlnxannualtargetheaderCreate (parameter) {
  return request({
    url: '/plan/biz/service/plnxannualtargetheader/create',
    method: 'post',
    data: parameter
  })
}

// 年度管理列表分页接口
export function getPageListByEntity (parameter) {
  return request({
    url: '/plan/biz/service/plnxannualtargetheader/pageListByEntity',
    method: 'post',
    data: parameter
  })
}
// 年度管理列表分页接口
export function getPageListByOrganization (parameter) {
  return request({
    url: '/plan/biz/service/plnxannualtargetheader/pageListByOrganization',
    method: 'post',
    data: parameter
  })
}

// 型号计划列表-年度目标节点下任务单任务项节点
export function getTaskByAnnualTargetId (parameter) {
  return request({
    url: '/plan/biz/service/vessel/pageByAnnualTargetId',
    method: 'post',
    data: parameter
  })
}

// 型号计划列表-任务单任务项节点下任务单任务项节点
export function getTaskNodeById (parameter) {
  return request({
    url: '/plan/biz/service/vessel/getNodeById',
    method: 'post',
    data: parameter
  })
}
// 一卡一单列表接口
export function getOneCardOneOrderList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/pagePlan',
    method: 'post',
    data: params
  })
}
// 一卡一单历史列表接口
export function pageCardOperRecord (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/pageCardOperRecord',
    method: 'post',
    data: params
  })
}
// 创建一卡一单权限校验
export function permissionCheckForAdd (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/permissionCheck',
    method: 'post',
    data: params
  })
}
// 调整一卡一单权限校验
export function permissionCheckForAdjust (params) {
  return request({
    url: '/plan/biz/service/cardOrderAdjust/permissionCheck',
    method: 'post',
    data: params
  })
}
// 升级一卡一单权限校验
export function permissionCheckForLeaveUp (params) {
  return request({
    url: '/plan/biz/service/cardOrderTemplate/update/permissionCheck',
    method: 'post',
    data: params
  })
}
// 升级一卡一单权限流程
export function leaveupCard (params) {
  return request({
    url: '/plan/biz/service/cardOrderTemplate/update/create',
    method: 'post',
    data: params
  })
}
// 一卡一单模板详情
export function cardOrderTemplateDetail (params) {
  return request({
    url: '/plan/biz/service/cardOrderTemplate/getDetailInfo',
    method: 'post',
    data: params
  })
}
// 删除一卡一单
export function deleteByTaskId (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/deleteByTaskId',
    method: 'get',
    params
  })
}
// 一卡一单模板导入
export function importCardOrderTemplate (params) {
  return request({
    url: '/plan/biz/service/cardOrderTemplate/import',
    method: 'get',
    params
  })
}

// 一卡一单模板引用
export function getCardOrderFromTemplate (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getCardOrderFromTemplate',
    method: 'get',
    params
  })
}
// 渲染调整一卡一单页面接口
export function showEditOneCardOneOrder (params) {
  return request({
    url: '/plan/biz/service/cardOrderAdjust/create',
    method: 'post',
    data: params
  })
}

// 暂存一卡一单接口
export function oneCardOneOrderForSave (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/save',
    method: 'post',
    data: params
  })
}

// 创建一卡一单模板
export function createOneCardOneOrderTemplate (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/create',
    method: 'post',
    data: params
  })
}
// 一卡一单详情模板
export function oneCardOneOrderDetailTemplate (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getDetailInfo',
    method: 'post',
    data: params
  })
}
// 一卡一单部分送审
export function oneCardOneOrderReviewPart (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/create/selected',
    method: 'post',
    data: params
  })
}
// 一卡一单全部送审
export function oneCardOneOrderReviewAll (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/create/all',
    method: 'post',
    data: params
  })
}
// 一卡一单模板库列表
export function queryTemplateList (params) {
  return request({
    url: '/plan/biz/service/cardOrderTemplate/queryTemplateList',
    method: 'post',
    data: params
  })
}

// 一卡一单填写结果
export function getLinkPdmList (data) {
  return request({
    url: `/plan/biz/service/cardOrderHeader/getLinkPdmList`,
    method: 'post',
    data
  })
}

// 一卡一单查看结果
export function getCardCheckItemList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getCheckItemList',
    method: 'post',
    data: params
  })
}

// 一卡一单保存检查结果
export function saveCheckResult (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/checkResult/save',
    method: 'post',
    data: params
  })
}

// 一卡一单导入
export function importCardOrder (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/importCardOrder',
    method: 'get',
    params
  })
}

// PDM查询一卡一单填写列表
export function getCheckItemListToPDM (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getCheckItemListToPDM',
    method: 'post',
    data: params
  })
}

// PDM保存一卡一单填写列表
export function saveCheckResultFromPDM (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/checkResultFromPDM/save',
    method: 'post',
    data: params
  })
}

// 一卡一单查看结果
export function getCheckResultList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getCheckResultList',
    method: 'post',
    data: params
  })
}

// 一卡一单统计
export function getOneCardStatisticsList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getStatisticsList',
    method: 'post',
    data: params
  })
}

// 计划点与一卡一单明细
export function getStatisticsDetailList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/getStatisticsDetailList',
    method: 'post',
    data: params
  })
}

// 导出计划点与一卡一单明细
export function exportStatisticsDetailList (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/exportStatisticsDetailList',
    method: 'post',
    data: params
  })
}

// 通过OSSId 批量获取上传文件信息
export function getAttachmentByOssIds (params) {
  return request({
    url: '/filenode/biz/service/cmos-doc-node/materials',
    method: 'post',
    data: params
  })
}

// 提交一卡一接口
export function oneCardOneOrderForSubmit (params) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/submit',
    method: 'post',
    data: params
  })
}
// 调整卡单编制信息
export function cardOrderCompAdjustPermissionCheck (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/permissionCheck',
    method: 'post',
    data: params
  })
}
// 调整卡单编制信息页 调整卡单编制信息
export function cardOrderCompAdjustUpdate (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/updateAdjust',
    method: 'get',
    params
  })
}
// 调整卡单编制信息
export function cardOrderCompAdjustsaveAdjust (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/saveAdjust',
    method: 'post',
    data: params
  })
}
// 一卡一单取消卡单编制信息
export function cancelCardOrderCompAdjust (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/cancelAdjust',
    method: 'get',
    params
  })
}
// 调整卡单编制信息分页查询
export function getWaitAuditPage (params) {
  return request({
    url: '/plan/biz/service/cardOrderCompAdjust/getWaitAuditPage',
    method: 'post',
    data: params
  })
}

// 型号计划-调整-新增任务
export function modalPlanAdjustAddTask (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/modelplan/save',
    method: 'post',
    data: params
  })
}

// 型号计划-调整-新增计划点
export function modalPlanAdjustAddPlanPoint (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/modeltask/save',
    method: 'post',
    data: params
  })
}

// 型号计划-模板查询
export function modalPlanSearch (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/modelcreate',
    method: 'post',
    data: params
  })
}

// 型号计划-模板查询
export function getImportantByIds (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/model/getImportantByIds',
    method: 'post',
    data: params
  })
}

// 型号计划-模板查询
export function getTimeByIds (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/model/getTimeByIds',
    method: 'post',
    data: params
  })
}

// 获取型号计划任务单详情
export function getPlanHeaderNotTaskById (id) {
  return request({
    url: `/plan/biz/service/vessel/getPlanHeaderNotTaskById/${id}`,
    method: 'get'
  })
}

// 获取型号计划计划点详情
export function getPlanPointDetailById (id) {
  return request({
    url: `/plan/biz/service/task/${id}`,
    method: 'get'
  })
}

// 检查型号计划任务单任务项时候可以变更
export function checkChangePossibility (params) {
  return request({
    url: `/plan/biz/service/vessel/adjust/modeltask/objIndexById`,
    method: 'get',
    params
  })
}

// 保存型号计划变更的任务单数据
export function saveTaskChange (data) {
  return request({
    url: `/plan/biz/service/vessel/adjust/modeltask/adjustHeader`,
    method: 'post',
    data
  })
}

// 保存型号计划变更的任务项数据
export function savePlanPointChange (data) {
  return request({
    url: `/plan/biz/service/vessel/adjust/modeltask/adjustTask`,
    method: 'post',
    data
  })
}

// 删除型号计划调整的任务单计划点数据
export function delAdjustTaskPlanPoint (data) {
  return request({
    url: `/plan/biz/service/vessel/adjust/modeltask/delAdjustTask`,
    method: 'delete',
    data
  })
}

// 删除型号计划调整的任务单计划点数据
export function cancelAdjustTaskPlanPoint (data) {
  return request({
    url: `/plan/biz/service/vessel/adjust/modeltask/cancelAdjust`,
    method: 'post',
    data
  })
}

// 搜索分类计划节点
export function searchPlanclassifyByName (params) {
  return request({
    url: '/plan/biz/service/planclassify/selectByName',
    method: 'get',
    params
  })
}

// 导入计划获取模板接口
export function getImportPlanTemplate (data) {
  return request({
    url: '/plan/biz/service/wbs/render',
    method: 'post',
    data
  })
}

// 型号计划-根据ID判断是否可以调整->修改
export function chackUpdateById (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/modeltask/chackUpdateById',
    method: 'get',
    params
  })
}

// 型号计划-检查是否可以在此节点下添加任务单或任务项
export function checkAddAuthority (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/checkAddAuthority',
    method: 'get',
    params
  })
}

// 工时团队列表
export function getTeamWorkingHoursList (data) {
  return request({
    url: '/plan/biz/service/task/hours/group/page',
    method: 'post',
    data
  })
}

// 任务项团队成员列表
export function getTaskItemUserList (params) {
  return request({
    url: '/plan/biz/service/task/hours/group/non-page',
    method: 'get',
    params
  })
}

// 编辑团队
export function editUserHours (data) {
  return request({
    url: '/plan/biz/service/task/hours/group/save',
    method: 'post',
    data
  })
}
// 工时填报列表
export function getWorkingHoursList (data) {
  return request({
    url: '/plan/biz/service/task/hours/page',
    method: 'post',
    data
  })
}
// 工时填报列表-不分页
export function getWorkingHoursListNonPage (data) {
  return request({
    url: '/plan/biz/service/task/hours/non-page',
    method: 'post',
    data
  })
}
// 保存工时
export function saveWorkingHours (data) {
  return request({
    url: '/plan/biz/service/task/hours/save',
    method: 'post',
    data
  })
}
// 提交工时
export function commitWorkingHours (data) {
  return request({
    url: '/plan/biz/service/task/hours/commit',
    method: 'post',
    data
  })
}
// 撤回工时
export function revokeWorkingHours (data) {
  return request({
    url: '/plan/biz/service/task/hours/revoke',
    method: 'post',
    data
  })
}
// 查询当前登录人的IPT人员配置当量
export function queryIptConfigWorkHourByRencentUser (data) {
  return request({
    url: '/plan/biz/service/plnxIptMemberEquivalentHour/queryIptConfigWorkHourByRencentUser',
    method: 'get',
    data
  })
}

// 型号计划直接修改 - directUpdate
export function directUpdate (data) {
  return request({
    url: '/plan/biz/service/vessel/adjust/directUpdate',
    method: 'post',
    data
  })
}

// 型号计划关闭任务 - 获取模板
export function getCloseTaskTemp (data) {
  return request({
    url: '/plan/biz/service/close/create',
    method: 'post',
    data
  })
}

// 型号计划关闭任务 - 获取模板
export function getTaskPlanForClose (data) {
  return request({
    url: '/plan/biz/service/close/getTaskPlanForClose',
    method: 'post',
    data
  })
}

export function getImportWbs (params) {
  return request({
    url: interfaceMap.getImportWbs,
    method: 'get',
    params
  })
}

export function getImportWbsWorkTime (params) {
  return request({
    url: interfaceMap.getImportWbsWorkTime,
    method: 'get',
    params
  })
}

// 我的任务-分解任务-校验
export function decomposeTaskItemCheck (taskId) {
  return request({
    url: `/plan/biz/service/task/decompose/check/${taskId}`,
    method: 'get'
  })
}

// 我的任务分解 - 前置校验
export function checkChangeDetail (taskId) {
  return request({
    url: `/plan/biz/service/task/change/check/${taskId}`,
    method: 'get'
  })
}

// 任务反馈表单说明-进展-分页
export function getFeedbackExplainProcess (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/process/page`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-进展-删除
export function delFeedbackExplainProcess (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/process/remove`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-进展-新增
export function addFeedbackExplainProcess (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/process/save`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-进展-更新
export function updateFeedbackExplainProcess (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/process/update`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-下级任务项详情
export function feedbackExplainSubordinateDetail (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/subordinate/detail`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-下级任务项列表
export function getFeedbackExplainSubordinate (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/subordinate/page`,
    method: 'post',
    data
  })
}

// 任务反馈表单说明-PDM查询
export function queryPDMData (data) {
  return request({
    url: `/plan/biz/service/task/feedback/page/pdm`,
    method: 'post',
    data
  })
  /**
   * {
   *  "docName": "string",
   *  "docNumber": "string",
   *  "docState": "string",
   *  "extValues": {},
   *  "pageCount": 0,
   *  "pageOffset": 0,
   *  "sysType": "string"
   * }
   */
}

// 任务反馈表单说明-已关联PDM查询
export function queryLinkedPDMData (taskId) {
  return request({
    url: `/plan/biz/service/task/feedback/link/pdm/list/${taskId}`,
    method: 'get'
  })
}

// 任务反馈表单说明-关联PDM查询
export function linkPDMData2Task (data) {
  return request({
    url: `/plan/biz/service/task/feedback/link/pdm`,
    method: 'post',
    data
  })
}

// 我的任务-指派校验
export function assignPreCheck (taskId) {
  return request({
    url: `/plan/biz/service/task/assign/check/${taskId}`,
    method: 'get'
  })
}

// 我的任务-新增指派
export function saveAssign (data) {
  return request({
    url: `/plan/biz/service/task/assign/save`,
    method: 'post',
    data
  })
}

// 我的任务-指派历史
export function assignHistory (taskId) {
  return request({
    url: `/plan/biz/service/task/assign/history/${taskId}`,
    method: 'get'
  })
}

// 我的任务-指派历史
export function planExportAll (params) {
  return request({
    url: `/plan/biz/service/vessel/adjust/exportAll`,
    method: 'get',
    params
  })
}

// 任务反馈表单说明-实动工时-查询
export function queryFeedbackActualWorkHours (taskId) {
  return request({
    url: `/plan/biz/service/feedback/explain/task/actualWorkHours/${taskId}`,
    method: 'get'
  })
}

// 任务反馈表单说明-实动工时-查询
export function feedbackActualWorkHoursCommit (data) {
  return request({
    url: `/plan/biz/service/feedback/explain/task/actualWorkHours/commit`,
    method: 'post',
    data
    // [
    //   {
    //     "bizScenario": {},
    //     "id": "string",
    //     "type": "string"
    //   }
    // ]
  })
}

// 任务反馈表单说明-实动工时-查询
export function getFeedbackCheckList (params) {
  return request({
    url: `/plan/biz/service/task/inspect/item/non-page`,
    method: 'get',
    params
  })
}

// 任务反馈表单说明-实动工时-查询
export function changeFeedbackCheckListStatus (data) {
  return request({
    url: `/plan/biz/service/task/inspect/item/btachChangeInspectResult`,
    method: 'post',
    data
  })
}

// 我的任务-指派撤回
export function recallAssign (data) {
  return request({
    url: `/plan/biz/service/task/assign/cancel/recall/` + data,
    method: 'get'
  })
}

// 任务单详情-任务项列表上方导出数据按钮
export function exportPlanTaskInfo (id) {
  return request({
    url: `/plan/biz/service/task/exportPlanTaskInfo/${id}`,
    method: 'get'
  })
}

// 设置定期报送
export function taskCreateConfig (data) {
  return request({
    url: `/plan/biz/service/task/job/createConfig`,
    method: 'post',
    data
  })
}

// 查看定期报送详情
export function getTaskConfigDetail (data) {
  return request({
    url: `/plan/biz/service/task/job/detailConfig/${data.taskId}`,
    method: 'post',
    data
  })
}

// 手动发定期报送待办
export function manualSendTodo (data) {
  return request({
    url: `/plan/api/service/task/job/create`,
    method: 'post',
    data
  })
}

// 更新定期报送
export function taskUpdateConfig (data) {
  return request({
    url: `/plan/biz/service/task/job/updateConfig`,
    method: 'post',
    data
  })
}

// 查看报送表单列表
export function getTaskList (data) {
  return request({
    url: `/plan/biz/service/task/job/trace/page`,
    method: 'post',
    data
  })
}
// 查看报送单详情
export function getSubmitFormDetail (data) {
  return request({
    url: `/plan/biz/service/task/job/detail`,
    method: 'post',
    data
  })
}
// 创建任务单-所属部门前置校验
export function percheckForPlanclassify (planClassifyId) {
  return request({
    url: `/plan/biz/service/vessel/add/vessel/check/${planClassifyId}`,
    method: 'get'
  })
}

// 计划点转包--作业
export function subcontractCreate (params) {
  return request({
    url: interfaceMap.subcontractCreate,
    method: 'post',
    data: params
  })
}

// 计划点转包--作业
export function importFromSubTask (data) {
  return request({
    url: `/plan/biz/service/task/enclosure/for/children`,
    method: 'post',
    data
  })
}

// 计划点转包--作业
export function multApplyExamines (params) {
  return request({
    url: `/plan/biz/service/vessel/adjust/examines`,
    method: 'get',
    params
  })
}
//
export function cancelCheck (planId) {
  return request({
    url: interfaceMap.cancelCheck + planId,
    method: 'get'
  })
}

// 计划点回退--作业
export function taskReturnCreate (params) {
  return request({
    url: interfaceMap.taskReturnCreate,
    method: 'post',
    data: params
  })
}

// 任务项统计 - 按任务单统计
export function statisticByHeader (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByHeader',
    method: 'post',
    data
  })
}

// 任务项统计 - 按责任人统计任务项接口
export function statisticByChargeDepartment (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByChargeDepartment',
    method: 'post',
    data
  })
}

// 任务项统计 - 按责任主体统计任务项接口
export function statisticByChargeDepartmentAlias (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByChargeDepartmentAlias',
    method: 'post',
    data
  })
}

// 任务项统计 - 按编制单位统计任务项接口
export function statisticByCompDept (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByCompDept',
    method: 'post',
    data
  })
}

// 任务项统计 - 按计划类型统计任务项接口
export function statisticByPlanClassify (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByPlanClassify',
    method: 'post',
    data
  })
}

// 任务项统计 - 按WBS任务统计任务项接口
export function statisticByWBS (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByWBS',
    method: 'post',
    data
  })
}

// 任务项统计 - 按年度目标统计任务项接口
export function statisticByTarget (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByTarget',
    method: 'post',
    data
  })
}

// 型号计划-全部导出异步查询状态
export function adjustSyncExportInfo (params) {
  return request({
    url: '/plan/biz/service/vessel/adjust/syncExportInfo',
    method: 'get',
    params
  })
}
export function getqueryApproveHistorys (planId) {
  return request({
    url: interfaceMap.getqueryApproveHistorys + planId,
    method: 'get'
  })
}
// getPlanList
export function getPlanList (planId) {
  return request({
    url: interfaceMap.getPlanList + planId,
    method: 'get'
  })
}
// 按WBS任务统计任务项导出接口
export function statisticByWBSExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByWBSExport',
    method: 'post',
    data
  })
}
// 按年度目标统计任务项导出接口
export function statisticByTargetExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByTargetExport',
    method: 'post',
    data
  })
}
// 按计划类型统计任务项导出接口
export function statisticByPlanClassifyExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByPlanClassifyExport',
    method: 'post',
    data
  })
}
// 按任务单统计任务项导出接口
export function statisticByHeaderExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByHeaderExport',
    method: 'post',
    data
  })
}

// 按任务单统计任务项导出接口
export function statisticByChargeDepartmentExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByChargeDepartmentExport',
    method: 'post',
    data
  })
}

// 按责任主体统计任务项导出接口
export function statisticByChargeDepartmentAliasExport (data) {
  return request({
    url: '/plan/biz/service/task/statistic/statisticByChargeDepartmentAliasExport',
    method: 'post',
    data
  })
}

// 获取quick BI 报表地址
export function cardGetToken (data) {
  return request({
    url: interfaceMap.cardGetToken,
    method: 'post',
    data
  })
}

// 获取研发大脑路径 dev-brains
export function getDevBrainFileUrl () {
  return request({
    url: `/plan/biz/service/PLNxCardOrder/ThirdPlatform/devBrain`,
    method: 'get'
  })
}
// 型号计划-- 创建一卡一单-- 商飞大脑导入模版
export function getCardTempListByBrain (data) {
  console.log('paramsparams', data)
  return request({
    url: `/plan/biz/service/PLNxCardOrder/ThirdPlatform/getCardTempListByDevBrain`,
    method: 'post',
    data
  })
}
export function getCardTempOssId (params) {
  // 选择推入导入模版
  return request({
    url: `/plan/biz/service/PLNxCardOrder/ThirdPlatform/getCardTempOssId/${params}`,
    method: 'get'
  })
}
// 任务中心-我负责的
export function getMyCharged (data) {
  // 选择推入导入模版
  return request({
    url: interfaceMap.getMyCharged,
    method: 'post',
    data
  })
}
// 获取标准作业列表入口初始化数据
export function getStandOperaEntry (params) {
  return request({
    // url: `/sqcdp/biz/service/sop/standardOperationList/list`,
    url: interfaceMap.getStandOperaEntry,
    method: 'post',
    data: params
  })
}
// 标准作业列表提交
export function standOperaSubmit (params) {
  return request({
    url: `/sqcdp/biz/service/sop/standardOperationList/submit`,
    method: 'post',
    params
  })
}
export function templateManageAdd (data) {
  return request({
    url: interfaceMap.templateManageAdd,
    method: 'post',
    data
  })
}
// 任务中心-我分管的
export function getMyManaged (data) {
  // 选择推入导入模版
  return request({
    url: interfaceMap.getMyManaged,
    method: 'post',
    data
  })
}
export function templateManageList (data) {
  return request({
    url: interfaceMap.templateManageList,
    method: 'post',
    data
  })
}
// 任务中心-我关注的
export function getMyWatched (data) {
  // 选择推入导入模版
  return request({
    url: interfaceMap.getMyWatched,
    method: 'post',
    data
  })
}
export function templateManageRemove (data) {
  return request({
    url: interfaceMap.templateManageRemove,
    method: 'post',
    data
  })
}
// 任务中心-我指派的
export function getMyAssigned (data) {
  // 选择推入导入模版
  return request({
    url: interfaceMap.getMyAssigned,
    method: 'post',
    data
  })
}
export function templateManageUpdate (data) {
  return request({
    url: interfaceMap.templateManageUpdate,
    method: 'post',
    data
  })
}

export function templateManageCopy (data) {
  return request({
    url: interfaceMap.templateManageCopy,
    method: 'post',
    data
  })
}
  export function positionList (data) {
    return request({
      url: interfaceMap.positionList,
      method: 'post',
      data
    })
  }
  export function positionAdd (data) {
    return request({
      url: interfaceMap.positionAdd,
      method: 'post',
      data
    })
  }
  export function positionRemove (data) {
    return request({
      url: interfaceMap.positionRemove,
      method: 'post',
      data
    })
  }
  export function positionUpdate (data) {
    return request({
      url: interfaceMap.positionUpdate,
      method: 'post',
      data
    })
  }
  // updateCalendarExeStatusTiming
  export function updateCalendarExeStatusTiming (data) {
    return request({
      url: `/sqcdp/biz/service/sop/calendar/updateCalendarExeStatusTiming${data}`,
      method: 'get'
    })
  }
  // calendarByPage
  export function calendarByPage (data) {
    return request({
      url: interfaceMap.calendarByPage,
      method: 'post',
      data
    })
  }
  // updateCalendarDate
  export function updateCalendarDate (data) {
    return request({
      url: interfaceMap.updateCalendarDate,
      method: 'post',
      data
    })
  }
  // queryResultExport
  export function queryResultExport (data) {
    return request({
      url: interfaceMap.queryResultExport,
      method: 'post',
      data
    })
  }
  export function standardOperationListCreate (data) {
    return request({
      url: interfaceMap.standardOperationListCreate,
      method: 'post',
      data
    })
  }
  // 所有标准作业表格
  export function standardOperationListQuery (data) {
    return request({
      url: interfaceMap.standardOperationListQuery,
      method: 'post',
      data
    })
  }
  export function standardOperationListExport (data) {
    return request({
      url: interfaceMap.standardOperationListExport,
      method: 'post',
      data
    })
  }
  export function standardOperationListDetail (data) {
    return request({
      url: interfaceMap.standardOperationListDetail,
      method: 'post',
      data
    })
  }
  export function getExecuteTimeForCron (data) {
     return request({
      url: interfaceMap.getExecuteTimeForCron,
      method: 'post',
      data
     })
  }
// 获取任务单详情-任务中心
export function getTaskDetail (params) {
  // 选择推入导入模版
  return request({
    url: interfaceMap.getTaskDetail,
    method: 'get',
    params
  })
}

// 我分管的-统计
export function getMyManagedStatistic (params) {
  return request({
    url: interfaceMap.myManagedStatistic,
    method: 'post',
    params
  })
}
  export function exportCurrent (data) {
    return request({
     url: interfaceMap.exportCurrent,
     method: 'post',
     data
    })
 }
// 知识卡片模板库列表
export function getCardKnowledgeData (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/knowledgeCardPageResult`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片模板库列表的新增
export function addCardKnowledgeInfo (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/createTemplate`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片模板库的详情
export function getCardKnowledgeDetail (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/detailTemplate?objectIndex=${parameter.objectId}`,
    method: 'post'
  })
}
// 知识卡片模板库的编辑
export function editCardKnowledgeInfo (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/updateTemplate`,
    method: 'post',
    data: parameter
  })
}
// 模板对应实例
export function getPageInstanceByTPIndex (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/pageResultInstance`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片实例详情
export function getDetailInstance (id) {
  return request({
    url: `/plan/biz/service/knowledgeCard/detailInstance?objectIndex=${id}`,
    method: 'post'
  })
}
// 知识卡片实例编辑
export function editInstance (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/updateInstance`,
    method: 'post',
    data: parameter
  })
}
