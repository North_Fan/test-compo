import request from '@/utils/request'
import qs from 'qs'

const interfaceMap = {
  businessDomainListQuery: '/process/businessDomainListQuery',
  businessDomainTreeQuery: '/process/businessDomainTreeQuery',
  businessDomainQueryTreeHasChildren: '/process/businessDomainQueryTreeHasChildren',
  businessDomainTreeFilter: '/process/businessDomainTreeFilter',
  businessDomainTreeGetChildren: '/process/businessDomainTreeGetChildren',
  businessDomainTreeGetContent: '/process/businessDomainTreeGetContent?id=',
  businessDomainTreeEditContent: '/process/businessDomainTreeEditContent?id=',
  businessDomainEditTable: '/process/businessDomainEditTable?id=',
  businessDomainHistoryListData: '/process/businessDomainHistoryListData',
  taskFormTabQuery: '/process/taskFormTabQuery',
  advancedTabQuery: '/process/advancedTabQuery',
  taskRunAssemblyList: '/process/taskRunAssemblyList',
  taskTabsList: '/process/taskTabsList',
  reportFormsTagList: '/process/reportFormsTagList',
  getCommonSelectByCode: '/basecenter/biz/service/codeitem/syscodeitemsByTypeCode',
  getOperator:'/quality/biz/service/operator/page', //运营人字典
  homeTaskRecord: '/process/homeTaskRecord',
  homeTaskList: '/process/homeTaskList',
  taskTreeDataList: '/process/taskTreeDataList',
  taskTypeList: '/process/taskTypeList'

}

export default interfaceMap

// 业务域列表
export function businessDomainListQuery (parameter) {
  return request({
    url: interfaceMap.businessDomainListQuery,
    method: 'get',
    params: parameter
  })
}

// 高级表格查询
export function advancedTabQuery (parameter) {
  return request({
    url: interfaceMap.advancedTabQuery,
    method: 'get',
    params: parameter
  })
}

// tab页面list
export function taskFormTabQuery (parameter) {
  return request({
    url: interfaceMap.taskFormTabQuery,
    method: 'get',
    params: parameter
  })
}

export function testApi2 (parameter) {
  return request({
    url: '/test/testApi2' + qs.stringify(parameter),
    method: 'get'
  })
}

// 业务域树
export function businessDomainTreeQuery (parameter) {
  return request({
    url: interfaceMap.businessDomainTreeQuery,
    method: 'post',
    data: parameter
  })
}
// 业务域树有子节点
export function businessDomainQueryTreeHasChildren (parameter) {
  return request({
    url: interfaceMap.businessDomainQueryTreeHasChildren,
    method: 'post',
    data: parameter
  })
}

// 业务域树filter
export function businessDomainTreeFilter (parameter) {
  return request({
    url: interfaceMap.businessDomainTreeFilter,
    method: 'post',
    data: parameter
  })
}

// 业务域树获得子节点
export function businessDomainTreeGetChildren (parameter) {
  return request({
    url: interfaceMap.businessDomainTreeGetChildren,
    method: 'post',
    params: parameter
  })
}

// 计划管理获取子节点
export function planTreeDataChildren (parameter) {
  return request({
    url: interfaceMap.planTreeDataChildren,
    method: 'post',
    params: parameter
  })
}

// 业务域树获得节点内容
export function businessDomainTreeGetContent (parameter) {
  return request({
    url: interfaceMap.businessDomainTreeGetContent + parameter,
    method: 'get'
  })
}

// 业务域树节点编辑内容
export function businessDomainTreeEditContent (parameter) {
  return request({
    url: interfaceMap.businessDomainTreeEditContent + parameter,
    method: 'get'
  })
}

// 任务tabs列表数组
export function taskTabsList (parameter) {
  return request({
    url: interfaceMap.taskTabsList,
    method: 'post',
    data: parameter
  })
}
// 业务域表格编辑
export function businessDomainEditTable (parameter) {
  return request({
    url: interfaceMap.businessDomainEditTable + parameter,
    method: 'get'
  })
}

// 业务域查看历史表格数据
export function businessDomainHistoryListData (parameter) {
  return request({
    url: interfaceMap.businessDomainHistoryListData,
    method: 'get',
    params: parameter
  })
}

export function getCommonSelectByCode (parameter) {
  return request({
    url: interfaceMap.getCommonSelectByCode,
    method: 'get',
    params: parameter
  })
}

//运营人
export function getOperator (parameter) {
  return request({
    url: interfaceMap.getOperator,
    method: 'post',
    data: parameter
  })
}

export function reportFormsTagList (parameter) {
  return request({
    url: interfaceMap.reportFormsTagList,
    method: 'post',
    params: parameter
  })
}

// 首页任务数量统计
export function homeTaskRecord (parameter) {
  return request({
    url: interfaceMap.homeTaskRecord,
    method: 'get',
    params: parameter
  })
}

// 首页任务列表
export function homeTaskList (parameter) {
  return request({
    url: interfaceMap.homeTaskList,
    method: 'post',
    data: parameter
  })
}

export function taskTypeList (parameter) {
  return request({
    url: interfaceMap.taskTypeList,
    method: 'post',
    data: parameter
  })
}

// 首页任务列表
export function taskTreeDataList (parameter) {
  return request({
    url: interfaceMap.taskTreeDataList,
    method: 'post',
    data: parameter
  })
}
