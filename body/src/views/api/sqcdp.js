import request from '@/utils/request'

export function create (param) {
  return request({
    url: '/sqcdp/biz/service/kpichange/create',
    method: 'post',
    data: param
  })
}

export function getKPITemplate (param) {
  return request({
    url: '/sqcdp/biz/service/kpichange/getKPITemplate',
    method: 'post',
    data: param
  })
}
// 问题
export function getQuestionBoardData (param) {
  return request({
    url: '/quality/biz/service/question/getQuestionBoardData',
    method: 'post',
    data: param
  })
}
export function getOrgLastKpi (param) {
  return request({
    url: `/sqcdp/biz/service/kpichange/getOrgLastKpi`,
    method: 'post',
    data: param
  })
}
// 问题趋势
export function getQuestionBoardTrend (param) {
  return request({
    url: '/quality/biz/service/question/getQuestionBoardTrend',
    method: 'post',
    data: param
  })
}
// 风险
export function getRiskBoardData (param) {
  return request({
    url: '/quality/biz/service/risk/getRiskBoardData',
    method: 'post',
    data: param
  })
}
// 风险趋势
export function getRiskBoardTrend (param) {
  return request({
    url: '/quality/biz/service/risk/getRiskBoardTrend',
    method: 'post',
    data: param
  })
}
