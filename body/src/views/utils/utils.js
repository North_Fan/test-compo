import request from '@/utils/request'
// 随机颜色
const randomColor = () => {
  const letters = '0123456789ABCDEF'
  let color = '#'
  for (let i = 1; i <= 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  // console.log(color)
  return color
}

// 校验用户权限
export function checkUserRole (data) {
  return request({
      url: `/xbom/biz/asgTask/checkUserRole`,
      method: 'post',
      data
  })
}
// 模拟数据
const mockData = (total, infoDict) => {
  const records = []
  for (let i = 0; i < total; i++) {
    const currentColor = randomColor()
    records.push({
      text: i + 1,
      color: currentColor,
      // strokeColor: currentColor
      strokeColor: 'red'
    })
  }
  this.$set(infoDict, 'records', records)
  this.$set(infoDict, 'totalPeaces', records.length)
}

// 同步等待
const sleep = (time) => {
  return new Promise(resolve => {
    setTimeout(resolve, time)
  })
}

// 计算圆上坐标
const calCoordinatesOnCircle = (centerX, centerY, r, radio) => {
  return {
    x: centerX + r * Math.cos(radio),
    y: centerY + r * Math.sin(radio)
  }
}

// 计算矩形中心
const calRectCentre = (startX, startY, width, height) => {
  return {
    x: startX + width / 2,
    y: startY + height / 2
  }
}

// 计算圆环外接框的坐标信息
const calRingCoordinates = (centerX, centerY, r1, r2, startRadio, offsetRadio) => {
  const firstPoint = calCoordinatesOnCircle(centerX, centerY, r1, startRadio)
  const secondPoint = calCoordinatesOnCircle(centerX, centerY, r1, startRadio + offsetRadio)
  const thirdPoint = calCoordinatesOnCircle(centerX, centerY, r2, startRadio)
  const fourthPoint = calCoordinatesOnCircle(centerX, centerY, r2, startRadio + offsetRadio)
  // // console.log('firstPoint',firstPoint)
  const xList = [firstPoint.x, secondPoint.x, thirdPoint.x, fourthPoint.x]
  const yList = [firstPoint.y, secondPoint.y, thirdPoint.y, fourthPoint.y]
  return {
    minX: xList.sort((a, b) => b - a).reverse()[0],
    maxX: xList.sort((a, b) => b - a)[0],
    minY: yList.sort((a, b) => b - a).reverse()[0],
    maxY: yList.sort((a, b) => b - a)[0]
  }
}

// 依据半径和角度计算圆环面积
const calRingArea = (r1, r2, radio) => {
  return radio * (Math.pow(r1, 2) - Math.pow(r2, 2)) / 2
}

// 依据半径和面积计算圆环角度
const calRingAngle = (r1, r2, s) => {
  return 2 * s / (Math.pow(r1, 2) - Math.pow(r2, 2))
}

// 获取画布并绘制辅助线
const getCanvase = (canvasName, width, height, showGuideLines, lineWidth, lineCap) => {
  const c = document.getElementById(canvasName)
  const ctx = c.getContext('2d')
  ctx.clearRect(0, 0, width, height)
  if (showGuideLines) {
    ctx.strokeStyle = '#C0C0C0'
    // 设置虚线间隔
    const horizontalLineDash = []
    const verticalLineDash = []
    const dashGapCount = 50
    const horizontalGap = width / dashGapCount
    const verticalGap = height / dashGapCount
    for (let i = 0; i < dashGapCount; i++) {
      horizontalLineDash.push(horizontalGap)
      verticalLineDash.push(verticalGap)
    }
    // 画横向辅助线
    ctx.strokeRect(0, 0, width, height)
    ctx.setLineDash(horizontalLineDash)
    ctx.beginPath()
    ctx.moveTo(0, height / 4)
    ctx.lineTo(width, height / 4)
    ctx.stroke()
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(0, height / 2)
    ctx.lineTo(width, height / 2)
    ctx.stroke()
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(0, height * 3 / 4)
    ctx.lineTo(width, height * 3 / 4)
    ctx.stroke()
    ctx.closePath()
    // 画纵向辅助线
    ctx.setLineDash(verticalLineDash)
    ctx.beginPath()
    ctx.moveTo(width / 2, 0)
    ctx.lineTo(width / 2, height)
    ctx.stroke()
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(width / 4, 0)
    ctx.lineTo(width / 4, height)
    ctx.stroke()
    ctx.closePath()

    ctx.beginPath()
    ctx.moveTo(width * 3 / 4, 0)
    ctx.lineTo(width * 3 / 4, height)
    ctx.stroke()
    ctx.closePath()
    ctx.save()
    ctx.restore()
    ctx.setLineDash([])
  }
  ctx.lineWidth = lineWidth
  ctx.lineCap = lineCap
  return ctx
}

// 描边矩形
const drawStrokeRect = (ctx, strokeColor, startX, startY, width, height, topLineFlag, bottomLineFlag, leftLineFlag, rightLineFlag, showStrokeLines) => {
  if (showStrokeLines && strokeColor) {
    const minX = startX + width * (width > 0 ? 0 : 1)
    const maxX = startX + width * (width <= 0 ? 0 : 1)
    const minY = startY + height * (height > 0 ? 0 : 1)
    const maxY = startY + height * (height <= 0 ? 0 : 1)
    // console.log(startX, startY, width, height)
    // console.log(minX, maxX, minY, maxY)
    ctx.strokeStyle = strokeColor
    ctx.beginPath()
    ctx.moveTo(minX, minY)
    if (topLineFlag) {
      ctx.lineTo(maxX, minY)
    } else {
      ctx.moveTo(maxX, minY)
    }

    if (rightLineFlag) {
      ctx.lineTo(maxX, maxY)
    } else {
      ctx.moveTo(maxX, maxY)
    }

    if (bottomLineFlag) {
      ctx.lineTo(minX, maxY)
    } else {
      ctx.moveTo(minX, maxY)
    }

    if (leftLineFlag) {
      ctx.lineTo(minX, minY)
    }
    ctx.moveTo(minX, minY)
    ctx.stroke()
  }
}

// 描边圆环
const drawStrokeRing = (ctx, strokeColor, centerX, centerY, R1, R2, startRadio, endRadio, startLineFlag, endLineFlag, showStrokeLines) => {
  if (showStrokeLines && strokeColor) {
    ctx.strokeStyle = strokeColor

    ctx.beginPath()
    ctx.arc(centerX, centerY, R1, startRadio, endRadio, true)
    ctx.stroke()
    ctx.save()

    ctx.beginPath()
    ctx.arc(centerX, centerY, R2, endRadio, startRadio)
    ctx.stroke()
    ctx.save()

    if (startLineFlag) {
      const lineStartPoint = calCoordinatesOnCircle(centerX, centerY, R1, startRadio)
      const lineEndPoint = calCoordinatesOnCircle(centerX, centerY, R2, startRadio)
      ctx.beginPath()
      ctx.moveTo(lineStartPoint.x, lineStartPoint.y)
      ctx.lineTo(lineEndPoint.x, lineEndPoint.y)
      ctx.stroke()
      ctx.save()
    }

    if (endLineFlag) {
      const lineStartPoint = calCoordinatesOnCircle(centerX, centerY, R1, endRadio)
      const lineEndPoint = calCoordinatesOnCircle(centerX, centerY, R2, endRadio)
      ctx.beginPath()
      ctx.moveTo(lineStartPoint.x, lineStartPoint.y)
      ctx.lineTo(lineEndPoint.x, lineEndPoint.y)
      ctx.stroke()
      ctx.save()
    }
  }
}

// 描边Q点
const drawQPoint = (ctx, strokeColor, insideCircle, pentagon, strokeFlag, lineWidth, lineCap) => {
  const { insideCircleStartX, insideCircleStartY, insideCircleR, insideCircleStartRadio, insideCircleEndRadio } = insideCircle
  const { lineStartX, lineStartY, firstX, firstY, secondX, secondY, thirdX, thirdY, lineEndX, lineEndY } = pentagon
  ctx.strokeStyle = strokeColor || 'red'
  // ctx.lineWidth = lineWidth
  // ctx.lineCap = lineCap
  // 绘制Q点圆弧
  ctx.beginPath()
  ctx.arc(insideCircleStartX, insideCircleStartY, insideCircleR, insideCircleStartRadio, insideCircleEndRadio, true)
  if (strokeFlag) {
    ctx.stroke()
  }
  ctx.fill()
  // 绘制Q点五边形
  ctx.beginPath()
  ctx.moveTo(lineStartX, lineStartY)
  ctx.lineTo(firstX, firstY)
  ctx.lineTo(secondX, secondY)
  ctx.lineTo(thirdX, thirdY)
  ctx.lineTo(lineEndX, lineEndY)
  if (strokeFlag) {
    ctx.stroke()
  }
  ctx.fill()
}

export {
  randomColor,
  mockData,
  sleep,
  calRectCentre,
  calRingCoordinates,
  calCoordinatesOnCircle,
  calRingArea,
  calRingAngle,
  getCanvase,
  drawStrokeRect,
  drawStrokeRing,
  drawQPoint
}
