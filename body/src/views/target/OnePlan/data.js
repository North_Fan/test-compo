import { findById } from '@/utils/util'

const formItemList = [
  {
    queryId: 'NUMBER',
    queryName: '目标编号',
    placeholder: '',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'PLAN_STATUS',
    queryName: '目标状态',
    placeholder: '',
    inputType: 'select',
    selectCode: 'APLAN_STATUS_COMMON',
    disabledFlag: true
  }, {
    queryId: 'empty',
    queryName: '',
    placeholder: '',
    inputType: 'slot',
    disabledFlag: true
  },
  {
    queryId: 'TARGETNAME',
    queryName: '目标名称',
    placeholder: '',
    inputType: 'input'
  },
  {
    queryId: 'TARGETKIND_NAME',
    queryName: '目标分类',
    placeholder: '',
    inputType: 'input',
    // inputType: 'slot',
    // slot: 'TARGETKIND_NAME',
    disabledFlag: true

  },
  {
    queryId: 'ANNUAL',
    queryName: '年度',
    placeholder: '',
    inputType: 'select',
    selectCode: 'APLAN_ANNUAL'
  },
  {
    queryId: 'TARGETLEVEL',
    queryName: '层级',
    inputType: 'slot',
    slot: 'TARGETLEVEL'
  },
  {
    queryId: 'CHARGEDEPARTMENTALIAS',
    queryName: '责任主体',
    inputType: 'slot',
    slot: 'CHARGEDEPARTMENTALIAS'
  },
  {
    queryId: 'RES_PER',
    queryName: '责任人',
    inputType: 'slot',
    slot: 'RES_PER'
  },
  {
    queryId: 'ORDER_NO',
    queryName: '令号',
    inputType: 'slot',
    slot: 'ORDER_NO'
  },
  {
    queryId: 'START_DATE',
    queryName: '开始时间',
    placeholder: '',
    inputType: 'date'
  },
  {
    queryId: 'END_DATE',
    queryName: '结束时间',
    placeholder: '',
    inputType: 'date'
  }
]
const layout = [
  {
    desc: '目标编号',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('NUMBER', formItemList)
  },
  {
    desc: 'empty',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('empty', formItemList)
  }, {
    desc: '目标状态',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('PLAN_STATUS', formItemList)
  },
  {
    desc: '目标名称',
    col: [24, 24, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('TARGETNAME', formItemList)
  },
  {
    desc: '目标分类',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('TARGETKIND_NAME', formItemList)
  },
  {
    desc: '年度',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('ANNUAL', formItemList)
  },
  {
    desc: '层级',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('TARGETLEVEL', formItemList)
  },
  {
    desc: '责任主体',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('CHARGEDEPARTMENTALIAS', formItemList)
  },
  {
    desc: '责任人',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('RES_PER', formItemList)
  },
  {
    desc: '令号',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('ORDER_NO', formItemList)
  },
  {
    desc: '开始时间',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('START_DATE', formItemList)
  },
  {
    desc: '结束时间',
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('END_DATE', formItemList)
  }
]

const itemData = {
  formItemList,
  layout
}
export default itemData
