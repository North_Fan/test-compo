// import { options } from 'less'
import Mock from 'mockjs2'
import {
  builder,
  getQueryParameters
} from '../util'

const totalCount = 5701

const modelPlanList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      modelPlanName: '3231',
      level: '集结点',
      responsibleTeam: '一般任务',
      responsibleUnit: '任务单信息',
      responsiblePerson: '任务项分解',
      dueDate: '任务项调整',
      state: {
        label: '已发布',
        state: '0'
      },
      type: '1',
      orderState: 'select_3',
      processName: '流程实例名称',
      confirmFinishTime: 'select_4',
      children: [
        {
          key: tmpKey + 1,
          id: tmpKey + 1,
          rowKey: tmpKey + 1,
          modelPlanName: '3333',
          level: '子集',
          responsibleTeam: '一般任务',
          responsibleUnit: '任务单信息',
          responsiblePerson: '任务项分解',
          dueDate: '任务项调整',
          state: '任务项反馈',
          orderState: 'select_3',
          processName: '流程实例名称',
          confirmFinishTime: 'select_4',
          type: '2',
          children: [
            {
              key: tmpKey + 2,
              id: tmpKey + 2,
              rowKey: tmpKey + 2,
              modelPlanName: '3333',
              level: '子集',
              responsibleTeam: '一般任务',
              responsibleUnit: '任务单信息',
              responsiblePerson: '任务项分解',
              dueDate: '任务项调整',
              state: '任务项反馈',
              orderState: 'select_3',
              processName: '流程实例名称',
              confirmFinishTime: 'select_4',
              type: '3'
            }
          ]
        }
      ],
      hasChild: true
    })
    i += 2
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

// 通用文件数据列表
const getCommonDocumentList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      modelPlanName: 'CX 4036-001',
      level: 'ARJ21-700飞机现场工程技术支援',
      vesselChar: 'A',
      chargeDepartmentAlias: '有效',
      Prepared: '平台管理员/admin',
      publishDepartment: '',
      publishDate: '',
      documentLevel: '中心级',
      newCode: '',
      Outsourcing: ''
    },
    {
      key: tmpKey + 1,
      id: tmpKey + 1,
      rowKey: tmpKey + 1,
      modelPlanName: 'FA4021-001',
      level: 'C919应急离机蓄电池、主电池',
      vesselChar: 'A',
      chargeDepartmentAlias: '有效',
      Prepared: '平台管理员/admin',
      publishDepartment: '',
      publishDate: '',
      documentLevel: '中心级',
      newCode: '',
      Outsourcing: ''
    })
    i += 1
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

// 通用文件子数据列表
const getCommonDocumentChildrenList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      modelPlanName: 'PR1171.1',
      level: '分册分册1',
      vesselChar: 'A',
      chargeDepartmentAlias: '有效',
      Prepared: '平台管理员/admin',
      publishDepartment: '上海航空工业（集团）有限公司',
      publishDate: '2021-09-12',
      documentLevel: '中国商飞级',
      newCode: '86a8906f6fb1b76601',
      Outsourcing: '权限'
    },
    {
      key: tmpKey + 1,
      id: tmpKey + 1,
      rowKey: tmpKey + 1,
      modelPlanName: 'PR1115',
      level: 'SHOUFUSHIDI',
      vesselChar: 'A',
      chargeDepartmentAlias: '有效',
      Prepared: '高伟伟/410880',
      publishDepartment: '客服公司/经营发展部',
      publishDate: '2020-10-27',
      documentLevel: '中心级',
      newCode: '',
      Outsourcing: ''
    })
    i += 1
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

// 通用文件查看分册详情
const getVolumesDetail = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize

  for (let i = 1; i < 5; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      volumesCode: 'PR1171.1'
    },
    {
      key: tmpKey + 1,
      id: tmpKey + 1,
      rowKey: tmpKey + 1,
      volumesCode: 'PR1171.2'
    })
    i += 1
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const planDetailId = () => {
  const result = Math.floor(Math.random() * 10)
  return builder({
    data: result
  })
}

const kpiFilterList = (options) => {
  const parameters = getQueryParameters(options)
  const type = parameters[0]
  let result = []
  if (type === 'type') {
    result = [
      {
        key: 'all',
        value: '全部'
      },
      {
        key: 'A1',
        value: 'A1'
      },
      {
        key: 'A2',
        value: 'A2'
      }
    ]
  } else if (type === 'age') {
    result = [
      {
        key: '2021',
        value: '2021年'
      },
      {
        key: '2022',
        value: '2022年'
      },
      {
        key: '2023',
        value: '2023年'
      },
      {
        key: '2024',
        value: '2024年'
      }
    ]
  } else if (type === 'level') {
    result = [
      {
        key: 'company',
        value: '公司级'
      },
      {
        key: 'center',
        value: '中心级'
      }
    ]
  }
  return result
}

const kpiList = (options) => {
  const parameters = getQueryParameters(options)
  let result = [
    {
      targetInfo: 1,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 2,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 3,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 4,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 5,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 6,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 7,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    },
    {
      targetInfo: 8,
      responsibleSubject: `Edward King `,
      age: 32,
      targetLevel: `London, Park Lane no.`
    }
  ]
  if (parameters.type !== 'all') {
    result = result.slice(0, 2)
  }
  return result
}

const assignmentInfo = (options) => {
  // const parameters = getQueryParameters(options)
  return {
    responsiblePerson: '123pt',
    commentator: 'pt',
    companyProcedures: '公司程序',
    companySpecification: '公司标准规范',
    projectProcedure: '项目程序',
    projectSpecification: '项目标准规范',
    responsibleTeam: '责任团队',
    startDate: '2021-11-11',
    dueDate: '2021-11-11',
    ATC: '2021-11-11',
    comfirmComplatedTime: '2021-11-11',
    planWork: '计划工时（小时）',
    complateForm: '完成形式',
    completionCriteria: '完成标准',
    checkGroup_1: 0,
    proposedOrganization: '提出组织',
    responsibilityTopicsType: '责任主体类型',
    associationAgreement: '关联协议名称',
    groupOfOverseasGroups: '关联出国团组名称',
    checkGroup_2: [2, 4, 5],
    memo: '备注',
    sendDept: '发送部门',
    code: '令号',
    formOwner: '表单所有者',
    beforeFormNumber: '上一表单号',
    selfFormNumber: '本表单号',
    nextFormNumber: '下一表单号'

  }
}
const newAssignmentInfo = (options) => {
  console.log(options)
  return {
    responsiblePerson: '平台管理员',
    commentator: 'pt',
    companyProcedures: '公司程序',
    companySpecification: '公司标准规范',
    projectProcedure: '项目程序',
    projectSpecification: '项目标准规范',
    responsibleTeam: '责任团队',
    startDate: '开始时间',
    dueDate: '应完时间',
    ATC: '实际完成时间',
    comfirmComplatedTime: '确认完成时间',
    planWork: '计划工时（小时）',
    complateForm: '完成形式',
    completionCriteria: '完成标准',
    checkGroup_1: 0,
    proposedOrganization: '提出组织',
    responsibilityTopicsType: '责任主体类型',
    associationAgreement: '关联协议名称',
    groupOfOverseasGroups: '关联出国团组名称',
    checkGroup_2: [2, 4, 5],
    memo: '备注',
    sendDept: '发送部门',
    code: '令号',
    formOwner: '表单所有者',
    beforeFormNumber: '上一表单号',
    selfFormNumber: '本表单号',
    nextFormNumber: '下一表单号'

  }
}
const taskInfo = (options) => {
  // const parameters = getQueryParameters(options)
  return {
    responsiblePerson: '平台管理员',
    companyProcedures: '公司程序',
    companySpecification: '公司标准规范',
    projectProcedure: '项目程序',
    projectSpecification: '项目标准规范',
    responsibleTeam: '责任团队',
    startDate: '开始时间',
    dueDate: '应完时间',
    ATC: '实际完成时间',
    planWork: '计划工时（小时）',
    proposedOrganization: '提出组织',
    responsibilityTopicsType: '责任主体类型',
    associationAgreement: '关联协议名称',
    groupOfOverseasGroups: '关联出国团组名称',
    checkGroup_2: [2, 4, 5],
    memo: '备注',
    sendDept: '发送部门',
    code: '令号',
    formOwner: '表单所有者',
    beforeFormNumber: '上一表单号',
    selfFormNumber: '本表单号',
    nextFormNumber: '下一表单号'

  }
}
const newTaskInfo = (options) => {
  return {
    responsiblePerson: '平台管理员',
    companyProcedures: '公司程序',
    companySpecification: '公司标准规范',
    projectProcedure: '项目程序',
    projectSpecification: '项目标准规范',
    responsibleTeam: '责任团队',
    startDate: '开始时间',
    dueDate: '应完时间',
    ATC: '实际完成时间',
    planWork: '计划工时（小时）',
    proposedOrganization: '提出组织',
    responsibilityTopicsType: '责任主体类型',
    associationAgreement: '关联协议名称',
    groupOfOverseasGroups: '关联出国团组名称',
    checkGroup_2: [2, 4, 5],
    memo: '备注',
    sendDept: '发送部门',
    code: '令号',
    formOwner: '表单所有者',
    beforeFormNumber: '上一表单号',
    selfFormNumber: '本表单号',
    nextFormNumber: '下一表单号'
  }
}
const WBSList = (options) => {
  const result = [
    { key: 'ARJ21', lable: 'ARJ21111111' },
    { key: 'ARJ22', lable: 'ARJ21111112' },
    { key: 'ARJ23', lable: 'ARJ21111113' }]
    return builder({
      data: result
    })
}

const WBSTableList = (options) => {
  const result = [
    {
      key: 1,
      name: '设计优化',
      no: 'A',
      time: '',
      summary: '',
      code: '1',
      children: [
        {
          key: 11,
          name: '设计优化',
          no: 'A.1',
           time: 1,
          summary: '',
          code: '2'
        },
        {
          key: 12,
          name: '设计优化',
          no: 'A.2',
           time: 1,
          summary: '',
          code: '3',
          children: [
            {
              key: 121,
              name: '设计优化',
              no: 'A.2.1',
              summary: '',
              code: '4'
            }
          ]
        },
        {
          key: 13,
          name: '设计优化',
          no: 'A.3',
          summary: '',
          code: '5',
          children: [
            {
              key: 131,
              name: '设计优化',
              no: 'A.3.1',
              summary: '',
              code: '6',
              children: [
                {
                  key: 1311,
                  name: '设计优化',
                  no: 'A.3.1.1',
                  summary: '',
                  code: '7'
                },
                {
                  key: 1312,
                  name: '设计优化',
                  no: 'A.3.1.2',
                  summary: '',
                  code: '8'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      key: 2,
      name: '设计优化',
      no: 'B',
      summary: '',
      code: '9'
    }
  ]
  return builder({
    data: result
  })
}

const WBSName = (options) => {
  const result = '传值获取names'
  return builder({
    data: result
  })
}

const reporthoursList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNo = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNo - 1) * pageSize
  const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1
  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      no: 'No' + '-' + tmpKey,
      taskname: '7.12',
      gold: '7月1年度目标',
      wbscode: '<数字样机设计>',
      state: '执行中',
      dutetime: '2021-10-21',
      workhours: '10.5/75.0',
      date0: '0',
      date1: '0',
      date2: '0',
      date3: '0',
      date4: '0',
      date5: '0',
      date6: '0',
      teamnumber: '平台管理员',
      company: '',
      team: '',
      approvedhours: '',
      delflag: false
    })
  }

  console.log('dsadsada', result)

  return builder({
    pageSize: pageSize,
    pageNo: pageNo,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}
const wbsList = (options) => {
  const parameters = getQueryParameters(options)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
        key: tmpKey,
        WBSName: 'wbs1',
        no: tmpKey,
        id: tmpKey,
        responsibleTeam: 'Team1',
        planHour: '20',
        accountControlor: 'AAA',
        standardHour: '24',
        children: [{
          key: tmpKey + 1,
          WBSName: 'children-wbs1',
          no: tmpKey + 1,
          id: tmpKey + new Date(),
          responsibleTeam: 'Team1',
          planHour: '20',
          accountControlor: 'AAA',
          standardHour: '24',
          children: [{
            key: tmpKey + 2,
            WBSName: 'grand-children-wbs1',
            no: tmpKey + 2,
            id: tmpKey + Math.random(),
            responsibleTeam: 'Team1',
            planHour: '20',
            accountControlor: 'AAA',
            standardHour: '24'
            }]
        }]
    })
    i += 2
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}
const transferWBS = (options) => {
  const parameters = getQueryParameters(options)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
        key: tmpKey,
        name: 'wbs' + i + 1,
        no: tmpKey,
        id: tmpKey,
        responsibleTeam: 'Team' + i + 1,
        responsibleUnit: 'Unit' + i + 1,
        responsiblePerson: 'Person' + i + 1,
        shouldFinishTime: '24',
        children: [{
          key: tmpKey + 1,
          name: 'children-wbs' + i + 2,
          no: tmpKey + 1,
          id: tmpKey + 1,
          responsibleTeam: 'Team' + i + 2,
          responsibleUnit: 'Unit' + i + 2,
          responsiblePerson: 'Person' + i + 2,
          shouldFinishTime: '24',
          children: [{
            key: tmpKey + 2,
            name: 'grand-children-wbs1',
            no: tmpKey + 2,
            id: tmpKey + 2,
            responsibleTeam: 'Team' + i + 3,
            responsibleUnit: 'Unit' + i + 3,
            responsiblePerson: 'Person' + i + 3
            }]
        }]
    })
    i += 2
  }
  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const statisticsList = (options) => {
  const parameters = getQueryParameters(options)
  let dataMobel = {
    total: '20',
    unfinished: 'APS项目',
    overTime: 'RWW',
    overTimeDone: 'AOIU',
    complated: 'ODS',
    pc: '90%',
    adjs: '4',
    numberOfSubmissions: '4',
    doneNumberOfSubmissions: '3',
    unfinishNumberOfSubmissions: '1',
    submissionRates: '93%'
  }
  if (parameters.type === 'subject') {
    dataMobel = { responseBody: '责任主体', ...dataMobel }
  } else if (parameters.type === 'principals') {
    dataMobel = { personNumber: '122', ...dataMobel }
  } else if (parameters.type === 'task') {
    dataMobel = { taskName: '10月任务', taskNo: '001', ...dataMobel }
  } else if (parameters.type === 'project') {
    dataMobel = { WBSName: 'SB90001', WBSNo: '004', ...dataMobel }
  } else if (parameters.type === 'planType') {
    dataMobel = { planName: '10月份设计计划', ...dataMobel }
  }
  const result = []
  for (let i = 0; i < 20; i++) {
    result.push({ id: i + 1, ...dataMobel })
  }

  return result
}

const WBSChildren = (options) => {
  return [
    {
      id: '1',
      age: '2020',
      standardHour: 23
    },
    {
      id: '2',
      age: '2021',
      standardHour: 25
    },
    {
      id: '3',
      age: '2022',
      standardHour: 12
    },
    {
      id: '4',
      age: '2023',
      standardHour: 4
    },
    {
      id: '5',
      age: '2024',
      standardHour: 7
    }
  ]
}

const verifyWBSChildren = (options, type = false) => {
  const parameters = type ? options : getQueryParameters(options)
  const orgData = WBSChildren()
  return !!orgData.find(f => f.age === parameters.age)
}

const removeChildrenWBS = (options) => {
  const parameters = getQueryParameters(options)
  const params = Object.values(parameters)
  console.log('params ------------------', params)
  let result = WBSChildren()
  params.forEach(e => {
    result = result.filter(f => f.id !== e)
  })
  console.log('result ------------------', result)
  return result
}

const saveChildrenWBS = (options) => {
  const parameters = getQueryParameters(options)
  const verifyValue = verifyWBSChildren(parameters, true)
  let result = WBSChildren()
  if (verifyValue) {
    const itemIndex = result.findIndex(f => f.age === parameters.age)
    result[itemIndex].standardHour = parameters.standardHour
  } else {
    result = [parameters, ...result]
  }
  return result
}

const addNewWBS = (options) => {
  const { parentId, wbsInfo } = getQueryParameters(options)

  let result = wbsList()
  if (parentId) {
    result = [wbsInfo, ...result]
  } else {

  }
  return result
}

// const addNewWBS = (options) => {
//   const parameters = getQueryParameters(options)
// }

const reportDetailTableList = (options) => {
  const result = [
    {
      applicant: '平台管理远',
      applicantTime: '2021-08-21',
      state: '成功',
      querycriteria: '完整2021-03-21至2021-8-21中国国商用飞机',
      operation: '下载结果',
      handle: '已处理'
    },
    {
      applicant: '平台管理远',
      applicantTime: '2021-08-21',
      state: '成功',
      querycriteria: '完整2021-03-21至2021-8-21中国国商用飞机',
      operation: '取消导出',
      handle: '处理'
    }
  ]
  return builder({
    data: result
  })
}

const ModelPlanReviewList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      reviewName: '3231',
      code: 'REVOCE',
      changeType: '一般任务',
      applicant: '申请人',
      changeBeforeData: '',
      changeAfterData: '',
      state: {
        label: '已发布',
        state: '0'
      },
      type: '1',
      orderState: 'select_3',
      confirmFinishTime: 'select_4',
      hasChild: true
    })
    i += 2
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const callbackPlanPoint = (options) => {
  const parameters = getQueryParameters(options)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      name: '根节点',
      level: '一般任务',
      changeType: '一般任务',
      responsibleTeam: '任务单信息',
      responsibleUnit: '任务项分解',
      responsiblePerson: '任务项调整',
      dueDate: '点点点',
      state: '1'
    })
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}
const getTemplateList = (options) => {
  const parameters = getQueryParameters(options)
  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      submitDate: '根节点' + tmpKey,
      submitter: '一般任务',
      timeReport: '一般任务',
      type: '任务单信息',
      comment: '任务项分解',
      actuallyHours: '任务项调整',
      state: '点点点',
      // 检查单
      templateName: '根节点' + tmpKey,
      inspectName: '一般任务',
      templateComment: '一般任务',
      // 下级任务单
      name: '根节点' + tmpKey,
      responsiblePerson: '哈哈哈',
      shouldComplateTime: '2021-11-11',
      source: '导入',
      otherTime: (tmpKey + 1) * 10
    })
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}
// plans
Mock.mock(/\/plans\/getPlanDetailIdBeforeCreate/, 'get', planDetailId)
Mock.mock(/\/plans\/getModelPlanList/, 'get', modelPlanList)
Mock.mock(/\/plans\/getKPIFilterOptions/, 'get', kpiFilterList)
Mock.mock(/\/plans\/getKPIList/, 'get', kpiList)
Mock.mock(/\/plans\/getWBSCode/, 'get', WBSList)
Mock.mock(/\/plans\/getWBSTableList/, 'get', WBSTableList)
Mock.mock(/\/plans\/getAssignmentInfo/, 'get', assignmentInfo)
Mock.mock(/\/plans\/updateAssignmentInfo/, 'post', newAssignmentInfo)
Mock.mock(/\/plans\/getTaskInfo/, 'get', taskInfo)
Mock.mock(/\/plans\/updateTaskInfo/, 'post', newTaskInfo)
Mock.mock(/\/plans\/getWBSName/, 'get', WBSName)
Mock.mock(/\/plans\/getReporthours/, 'get', reporthoursList)
Mock.mock(/\/plans\/getWBSList/, 'get', wbsList)
Mock.mock(/\/plans\/transferWBS/, 'get', transferWBS)
Mock.mock(/\/plans\/getStatisticsList/, 'get', statisticsList)
Mock.mock(/\/plans\/getWBSChildren/, 'get', WBSChildren)
Mock.mock(/\/plans\/verifyWBSChildren/, 'post', verifyWBSChildren)
Mock.mock(/\/plans\/addNewWBS/, 'post', addNewWBS)
Mock.mock(/\/plans\/saveChildrenWBS/, 'post', saveChildrenWBS)
Mock.mock(/\/plans\/removeChildrenWBS/, 'post', removeChildrenWBS)
Mock.mock(/\/plans\/reportDetailTableList/, 'get', reportDetailTableList)
Mock.mock(/\/plans\/getModelPlanReviewList/, 'get', ModelPlanReviewList)
Mock.mock(/\/plans\/getPlanPointCallbackList/, 'get', callbackPlanPoint)
Mock.mock(/\/plans\/getTemplateList/, 'get', getTemplateList)
Mock.mock(/\/plans\/getCommonDocumentList/, 'get', getCommonDocumentList)
Mock.mock(/\/plans\/getCommonDocumentChildrenList/, 'get', getCommonDocumentChildrenList)
Mock.mock(/\/plans\/getVolumesDetail/, 'get', getVolumesDetail)
