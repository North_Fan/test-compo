import Mock from 'mockjs2'
import {
  builder,
  getQueryParameters,
  getBody
} from '../util'

const totalCount = 57011

const serverList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)

  const result = []
  const pageNo = parseInt(1)
  const pageSize = parseInt(10)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNo - 1) * pageSize
  const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      no: 'No ' + tmpKey,
      roleDept: '中国商飞',
      duty: '决策',
      role: '组织',
      delflag: false
    })
  }

  return builder({
    pageSize: pageSize,
    pageNo: pageNo,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const tabList = (options) => {
  const result = [{
    id: 'FormTask1',
    name: '任务单'
  },
    {
      id: 'FormTask2',
      name: '任务项'
    },
    {
      id: 'DynamicForm',
      name: '详情页'
    }
  ]
  return builder({
    data: result
  })
}

const advancedList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      no: 'No' + '-' + tmpKey,
      input_1: 'input_1',
      input_2: 'input_2',
      input_3: 'input_3',
      input_4: 'input_4',
      input_5: 'input_5',
      select_1: 'select_1',
      select_2: 'select_2',
      select_3: 'select_3',
      select_4: 'select_4',
      select_5: 'select_5',
      date_1: 'date_1',
      date_2: 'date_2',
      date_3: 'date_3',
      date_4: 'date_4',
      date_5: 'date_5',
      delflag: false
    })
  }

  console.log('dsadsada', result)

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const historyListData = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNo = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNo - 1) * pageSize
  const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1
  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      no: 'No' + '-' + tmpKey,
      modifiedby: '平台管理员',
      time: '2020-06-08 10:44:00',
      operation: 'UPDATE',
      businessdomainNumber: '14491',
      businessdomainName: '管理产品生命周期',
      parentnode: '14463',
      nodetype: '业务域组',
      businessdomainType: '业务',
      businessdomainDesc: '管理产品生命周期',
      owner: '',
      businessdomainCategory: '人力资源',
      remarks: '',
      validitymark: '有效',
      delflag: false
    })
  }

  console.log('dsadsada', result)

  return builder({
    pageSize: pageSize,
    pageNo: pageNo,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const treeDataList = (options) => {
  console.log('传参', options)
  const result = [{
    id: '000',
    key: '中国商飞',
    title: '中国商飞',
    parentId: 0,
    power: 'admin',
    slots: {
      icon: 'folder'
    },
    children: [{
      id: '001',
      key: '1管理产品生命周期域项目群',
      title: '1管理产品生命周期域项目群',
      power: 'admin',
      type: '1',
      slots: {
        icon: 'folder'
      },
      children: [{
        id: '101',
        key: '1A管理项目群',
        title: '1A管理项目群',
        power: 'admin',
        slots: {
          icon: 'folderOpen'
        },
        children: [{
          id: '10101',
          key: '1A.01管理项风险和机遇',
          title: '1A.01管理项风险和机遇',
          power: 'admin',
          slots: {
            icon: 'folderOpen'
          }
        }]
      },
        {
          id: '102',
          key: '1B管理产品生命周期',
          title: '1B管理产品生命周期',
          power: 'admin',
          slots: {
            icon: 'folder'
          }
        },
        {
          id: '103',
          key: '1C2月20.1',
          title: '1C2月20.1',
          power: 'admin',
          slots: {
            icon: 'folder'
          }
        }
      ]
    },
      {
        id: '002',
        key: '2管理供应商',
        title: '2管理供应商',
        power: 'edit',
        slots: {
          icon: 'folderGray'
        },
        children: [{
          id: '201',
          key: '2A供应商过程控制',
          title: '2A供应商过程控制',
          power: 'edit'
        },
          {
            id: '202',
            key: '2B供应商谈判与合同管理',
            power: 'edit',
            title: '2B供应商谈判与合同管理'
          },
          {
            id: '203',
            key: '2C管理战略采购',
            power: 'edit',
            title: '2C管理战略采购'
          },
          {
            id: '204',
            key: '2D开发与选择供应商',
            power: 'edit',
            title: '2D开发与选择供应商'
          },
          {
            id: '204',
            key: '2D开发与选择供应商1',
            power: 'edit',
            title: '2D开发与选择供应商'
          },
          {
            id: '205',
            key: '2E实施采购',
            power: 'edit',
            title: '2E实施采购'
          }
        ]
      },
      {
        id: '003',
        key: '3管理公司',
        title: '3管理公司',
        power: '123',
        slots: {
          icon: 'folderGray'
        }
      },
      {
        id: '004',
        key: '4管理客户与市场',
        title: '4管理客户与市场',
        power: '123',
        slots: {
          icon: 'folderGray'
        }
      },
      {
        id: '005',
        key: '5开发产品和服务',
        title: '5开发产品和服务',
        power: '123',
        slots: {
          icon: 'folderGray'
        }
      },
      {
        id: '006',
        key: '6开发产品和服务',
        title: '6开发产品和服务',
        power: '123',
        slots: {
          icon: 'folderGray'
        }
      },
      {
        id: '007',
        key: '7生产产品',
        title: '7生产产品',
        power: '123',
        slots: {
          icon: 'folderGray'
        }
      },
      {
        id: '008',
        key: '8提供客户服务',
        title: '8提供客户服务',
        power: '123',
        type: 'disabled',
        slots: {
          icon: 'folderGray'
        }
      }
    ]
  }]
  return builder({
    data: result
  })
}

const treeDataListhasChildren = (options) => {
  console.log('传参11111111', options)
  const result = [{
    id: '000',
    key: '中国商非',
    title: '中国商非',
    parentId: 0,
    power: 'admin',
    children: [{
      id: '001',
      key: '1管理产品生命周期域项目群',
      title: '1管理产品生命周期域项目群',
      power: 'admin',
      children: [{
        id: '101',
        key: '1A管理项目群',
        title: '1A管理项目群',
        power: 'admin',
        children: [{
          id: '10101',
          key: '1A.01管理项风险和机遇',
          title: '1A.01管理项风险和机遇',
          power: 'admin'
        }]
      }
      ]
    }
    ]
  }]
  return builder({
    data: result
  })
}

const planTreeDataList = (options) => {
  console.log('传参', options)
  const result = [{
    id: '000',
    key: '全部计划类型',
    title: '全部计划类型',
    parentId: 0,
    power: 'admin',
    slots: {
      icon: 'folder'
    },
    children: [{
      id: '001',
      key: '总部综合计划',
      title: '总部综合计划',
      power: 'admin',
      slots: {
        icon: 'folder'
      },
      children: []
    },
      {
        id: '002',
        key: '各中心综合计划',
        title: '各中心综合计划',
        power: 'admin',
        slots: {
          icon: 'folder'
        },
        children: []
      },
      {
        id: '003',
        key: '专项计划',
        title: '专项计划',
        power: 'admin',
        slots: {
          icon: 'folder'
        }
      }
    ]
  }]
  return builder({
    data: result
  })
}

const treeDataListFilter = (options) => {
  console.log('传参Filter', options)
  const result = [{
    id: '000',
    key: '中国商飞',
    title: '中国商飞',
    slots: {
      icon: 'folder'
    },
    children: [{
      id: '001',
      key: '1管理产品生命周期域项目群',
      title: '1管理产品生命周期域项目群',
      slots: {
        icon: 'folder'
      },
      children: [{
        id: '101',
        key: '1A管理项目群',
        title: '1A管理项目群',
        slots: {
          icon: 'file'
        },
        isLeaf: true
      },
        {
          id: '102',
          key: '1B管理产品生命周期',
          title: '1B管理产品生命周期',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        },
        {
          id: '103',
          key: '1C2月20.1',
          title: '1C2月20.1',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        }
      ]
    },
      {
        id: '002',
        key: '2管理供应商',
        title: '2管理供应商',
        slots: {
          icon: 'folderGray'
        },
        children: [{
          id: '201',
          key: '2A供应商过程控制',
          title: '2A供应商过程控制',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        }]
      }
    ]
  }]
  return builder({
    data: result
  })
}

const treeDataChildren = (options) => {
  console.log('options', options)
  const parameters = getQueryParameters(options)

  const result = [{
    key: Math.floor(Math.random(1) * 10000) + '.xx-业务域',
    title: Math.floor(Math.random(1) * 10000) + '.xx-业务域',
    slots: {
      icon: 'folder'
    },
    type: parameters.disabled ? 'disabled' : ''
  }]
  return builder({
    data: result
  })
}

const planTreeDataChildren = (options) => {
  console.log('options', options)
  const parameters = getQueryParameters(options)

  const result = [{
    key: Math.floor(Math.random(1) * 10000) + '.xx-业务域',
    title: Math.floor(Math.random(1) * 10000) + '.xx-业务域',
    slots: {
      icon: 'folder'
    },
    type: parameters.disabled ? 'disabled' : ''
  }]
  return builder({
    data: result
  })
}

const reportTagList = (options) => {
  const result = [{
    id: 'tag0',
    name: 'baidu',
    url: 'http://www.baidu.com/',
    type: 'iframe'
  },
    {
      id: 'tag1',
      name: 'baidu',
      url: 'http://www.baidu.com?id=111&type=add',
      type: 'iframe'
    },
    {
      id: 'tag2',
      name: '基础表单',
      url: '/form/basic-form',
      type: 'vue'
    },
    {
      id: 'tag3',
      name: '基础表单',
      url: '/form/basic-form?id=1234&statu=yes',
      type: 'vue'
    }
  ]
  return builder({
    data: result
  })
}

const businessList = (options) => {
  const result = [{
    name: 'tag1',
    id: '23456',
    type: 'edit'
  },
    {
      name: 'tag2',
      id: '44444',
      type: 'vue'
    },
    {
      name: 'tag3',
      id: '555',
      type: 'add',
      statu: 'no'
    }
  ]
  return builder({
    data: result
  })
}

const taskModel = (options) => {
  const result = [{
    name: 'model1',
    id: 'modelId1'
  },
    {
      name: 'model2',
      id: 'modelId2'
    },
    {
      name: 'model3',
      id: 'modelId3'
    }
  ]
  return builder({
    data: result
  })
}

const assemblyList = (options) => {
  const result = [{
    name: 'name1',
    code: 'code1',
    formName: 'formName1',
    process: 'process1',
    countNumber: 'countNumber1'
  },
    {
      name: 'name2',
      code: 'code2',
      formName: 'formName2',
      process: 'process2',
      countNumber: 'countNumber2'
    },
    {
      name: 'name3',
      code: 'code3',
      formName: 'formName3',
      process: 'process3',
      countNumber: 'countNumber3'
    }
  ]
  return builder({
    data: result
  })
}

const treeDataCentent = (options) => {
  const result = [{
    businessCode: '111111',
    businessType: '种类',
    businessName: '名字',
    businessDes: '描述',
    businessBigType: '大类',
    flag: '标记',
    owner: '所有者',
    onlyCode: '唯一标识',
    executor: '执行方',
    director: '主管方',
    prepared: '编制人',
    remarks: '备注',
    tableList: [{
      id: 1,
      duty: '决策',
      roleType: '组织',
      department: '上海航空工业',
      flag: '有效'
    },
      {
        id: 2,
        duty: '决策',
        roleType: '组织',
        department: '上海航空工业',
        flag: '有效'
      },
      {
        id: 3,
        duty: '决策',
        roleType: '组织',
        department: '上海航空工业',
        flag: '有效'
      },
      {
        id: 4,
        duty: '决策',
        roleType: '组织',
        department: '上海航空工业',
        flag: '有效'
      }
    ]
  }]
  return builder({
    data: result
  })
}

const treeEditData = (options) => {
  console.log('options', options)
  const result = {
    treeaddbusinessType: 'treeaddbusinessType',
    treeaddnodeName: 'treeaddnodeName',
    treeaddenableFlg: 'treeaddenableFlg',
    treeaddbusinessTag: 'treeaddbusinessTag',
    treeaddbusinessDesc: 'treeaddbusinessDesc',
    treeaddowner: 'treeaddowner',
    treeaddmemo: 'treeaddmemo'
  }
  return builder({
    data: result
  })
}

const customView = (options) => {
  console.log('options', options)

  const result = [{
    id: '0324',
    icon: 'cluster',
    colsNames: ['no', 'input_1', 'input_2', 'select_1', 'select_2', 'select_3'],
    viewName: '我的视图 view7',
    advSearchForm: {
      no: '555',
      input_1: 'cc',
      select_1: 'a1-1'
    }
  },
    {
      id: '03444',
      icon: 'cluster',
      colsNames: ['no', 'input_3', 'input_4', 'select_1', 'select_4', 'select_5'],
      viewName: '我的视图 view8',
      advSearchForm: {
        no: '666',
        select_1: 'a1-2',
        input_4: 'ddd'
      }
    }
  ]

  return builder({
    data: result
  })
}

const taskTabsList = (options) => {
  const result001 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '任务项列表',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/plans/PlanningManagement/TaskItemList',
      'businessCode': 'plan',
      'params': {}
    },
    'tab': [
      {
        'extValues': {},
        'taskElementCode': 'taskItemList',
        'taskElementName': '任务项列表',
        'taskElementUrl': 'views/plans/PlanningManagement/TaskItemList',
        'tabCode': null,
        'formCode': 'taskBasicForm',
        'formUrl': 'views/plans/PlanningManagement/TaskItemList',
        'order': 0,
        'params': {}
      }
    ],
    'commonToolBar': {
      'commitButton': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'hasTempSave': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {},
    'permissions': {
      'taskItemList': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      },
      'taskBasicForm': {
        'nomarlplanview.useredit': 'h',
        'readOnly': 'true'
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  const result002 =
    {
      tInsId: 1231231231242,
      commonToolBar: {
        save: {
          permission: 's',
          url: 'plan/tempcreate'
        },
        commit: {
          permission: 'h'
        }
      },
      startTemplateData: {
        'startTemplateCode': 'mouldFrame',
        'startTemplateName': '调整任务单基本信息页面',
        'startTemplateUrl': 'components/MouldFrame/MouldFrame',
        formCode: 'adjustTaskBasic',
        formUrl: 'views/plans/PlanningManagement/AdjustTaskBasic',
        businessCode: 'plan',
        params: {
          noAccessCheck: 'true',
          hasFileTab: 'false'
        }
      },
      taskData: {
        adjustTaskBasic: {
          CHARGEDEPARTMENTALIAS_NAME: 'maguangxin',
          CHARGEDEPARTMENTALIAS: 'pt'
        }
      },
      permissions: {
        global: {
          readOnly: false
        }
      },
      pInsId: '01029389182882',
      fInsId: 88178231238991
    }
  const result003 = {
    tInsId: 1231231231242,
    commonToolBar: {
      hasTempSave: {
        permission: 's',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'primaryTaskItem',
      startTemplateName: '任务项反馈',
      startTemplateUrl: 'views/task/PrimaryTaskItem/index',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      taskAdd: {
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: false
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }
  const result004 = {
    tInsId: 1231231231242,
    commonToolBar: {
      hasTempSave: {
        permission: 's',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'AdjustTaskItemForm',
      startTemplateName: '调整任务项',
      startTemplateUrl: 'views/plans/PlanningManagement/AdjustTaskItemForm',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      taskAdd: {
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: false
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result005 = {
    tInsId: 1231231231242,
    commonToolBar: {
      hasTempSave: {
        permission: 's',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'resolveTaskItemList',
      startTemplateName: '分解任务项',
      startTemplateUrl: 'views/plans/PlanningManagement/ResolveTaskItemList',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      taskAdd: {
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: false
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result006 = {
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    'startTemplateData': {
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '调整任务项',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'adjustTaskItems',
      formUrl: 'views/plans/PlanningManagement/AdjustTaskItems/AdjustTaskItems',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      'adjustTaskItems': {}
    },
    'permissions': {
      'adjustTaskItems': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result007 = {
    tInsId: 1231231231241,
    commonToolBar: {
      hasTempSave: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      },
      knowledge: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      history: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'taskBasicForm',
      startTemplateName: '任务单表单',
      startTemplateUrl: 'views/plans/PlanningManagement/TaskBasicForm',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    tab: [{
      taskElementCode: 'taskItemList',
      taskElementName: '任务项列表',
      taskElementUrl: 'views/plans/PlanningManagement/TaskItemList',
      tabCode: 'tab1Code',
      formCode: 'from1Code',
      order: 1,
      params: {
        readOnly: 'true',
        editPanel: 'false'
      }
    }],
    taskData: {
      taskBasicForm: {
        test1: 'test1',
        test2: 'test2',
        test3: 'test3'
      },
      BusinessDomain: {
        componentPanel: 'h'
      },
      taskItemList: [123]
    },
    permissions: {
      global: {
        readOnly: true
      },
      taskBasicForm: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      taskItemList: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result008 = {
    tInsId: 1231231231241,
    commonToolBar: {
      hasTempSave: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      },
      knowledge: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      history: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'taskBasicForm',
      startTemplateName: '督办任务单表单',
      startTemplateUrl: 'views/plans/PlanningManagement/SuperintendentTaskForm',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    tab: [{
      taskElementCode: 'taskItemList',
      taskElementName: '督办任务项列表',
      taskElementUrl: 'views/plans/PlanningManagement/SupervisionTaskItemList',
      tabCode: 'tab1Code',
      formCode: 'from1Code',
      order: 1,
      params: {
        readOnly: 'true',
        editPanel: 'false'
      }
    }],
    taskData: {},
    permissions: {
      global: {
        readOnly: false
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result009 = {
    baseInfo: {
      tInsId: 1231231231242
    },
    commonToolBar: {
      hasTempSave: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'planDetail',
      startTemplateName: '计划点详情页',
      startTemplateUrl: 'views/plans/PlanDetail/index',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      planDetail: {
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1,
        taskNo_1: '1234'
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: true
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result010 = {
    baseInfo: {
      tInsId: 1231231231242
    },
    commonToolBar: {
      hasTempSave: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'WBSDetail',
      startTemplateName: 'wbs 详情页',
      startTemplateUrl: 'views/plans/WBS/WBSDetail/index',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      WBSDetail: {
        responsiblePerson: [{ id: '123', name: '456' }],
        suggestionSingleCardPerson: [{ id: '123', name: '456' }],
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1,
        taskNo_1: '1234'
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: true
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result011 = {
    baseInfo: {
      tInsId: 1231231231242
    },
    commonToolBar: {
      hasTempSave: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      commitButton: {
        permission: 'h'
      },
      knowledge: {
        permission: 'h',
        url: 'plan/tempcreate'
      },
      history: {
        permission: 'h'
      }
    },
    startTemplateData: {
      startTemplateCode: 'taskCard',
      startTemplateName: '创建一卡一单',
      startTemplateUrl: 'views/plans/OneOnOneManagement/TaskCard/index',
      businessCode: 'plan',
      params: {
        noAccessCheck: 'true',
        hasFileTab: 'false'
      }
    },
    taskData: {
      taskAdd: {
        listTask: [{
          a: 1,
          b: 2,
          c: 3
        }, {
          a: 2,
          b: 2,
          c: 3
        }, {
          a: 4,
          b: 2,
          c: 3
        }],
        x: 1
      },
      BusinessDomain: {
        componentPanel: 'h'
      }
    },
    permissions: {
      global: {
        readOnly: false
      },
      taskAdd: {
        readOnly: 'true',
        'nomarlplanview.useredit': 'h'
      },
      BusinessDomain: {
        listTask: 'r'
      }
      // 'views/processManage/BusinessDomain/index': {
      //   componentPanel: 'h'
      // }
    },
    pInsId: '01029389182882',
    fInsId: 88178231238991
  }

  const result012 = {

    extValues: {},
    assignmentVersionId: '1453234915218157568',
    objectType: null,
    objectViewIndex: null,
    startTemplateData: {
      'extValues': {},
      'startTemplateCode': 'closeTask',
      'startTemplateName': '关闭任务',
      'startTemplateUrl': 'views/plans/ModelPlanList/CloseTask/index',
      'businessCode': 'plan',
      'params': {}
    },
    'tab': [
      {
        // 'extValues': {},
        // 'taskElementCode': 'closeTask',
        // 'taskElementName': '关闭任务',
        // 'taskElementUrl': 'views/plans/ModelPlanList/CloseTask/index',
        // 'tabCode': null,
        // 'formCode': '',
        // 'order': 0,
        // 'params': {}
      }
    ],
    'commonToolBar': {
      'commitButton': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'hasTempSave': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      'closeTask': {
        'fromData': {
          'taskname': '1234',
          'personliable': '责任人',
          'responsibleteam': '责任团队',
          'responsibleunit': '责任单位',
          'endtime': '2021-09-08',
          'closetime': '2021-08-09'
        },
        'tableData': [
          {
            'no': 1,
            'name': '完成C919飞机飞机手册',
            'responsibleteam': 'C919大型客机团队',
            'responsibleunit': '飞机架构集成工程',
            'personliable': '李强',
            'endtime': '2021-09-09',
            'closetime': '2021-09-09'
          }
        ]
      }
    },
    'permissions': {
      'taskItemList': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      },
      'taskBasicForm': {
        'nomarlplanview.useredit': 'h',
        'readOnly': 'true'
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null

  }

  const result013 =
    {
      'extValues': {},
      'assignmentVersionId': '1453234915218157568',
      'objectType': null,
      'objectViewIndex': null,
      'startTemplateData': {
        'extValues': {},
        'startTemplateCode': 'SubmitForApproval',
        'startTemplateName': '表单信息',
        'startTemplateUrl': 'views/plans/ModelPlanList/SubmitForApproval/index',
        'businessCode': 'plan',
        'params': {}
      },
      'tab': [
        {
          // 'extValues': {},
          // 'taskElementCode': 'closeTask',
          // 'taskElementName': '关闭任务',
          // 'taskElementUrl': 'views/plans/ModelPlanList/CloseTask/index',
          // 'tabCode': null,
          // 'formCode': '',
          // 'order': 0,
          // 'params': {}
        }
      ],
      'commonToolBar': {
        'commitButton': {
          'permission': 's',
          'url': 'plan/v1/plan/header/add'
        },
        'history': {
          'permission': 'h'
        },
        'hasTempSave': {
          'permission': 's',
          'url': 'plan/tempcreate'
        },
        'knowledge': {
          'permission': 'h',
          'url': 'plan/tempcreate'
        }
      },
      'taskData': {
        'SubmitForApproval': {
          'tableData1': [
            {
              'version': 1,
              'accountWBS': 'accountWBS',
              'nowPlanWorks': 'C919大型客机团队',
              'changePlanWorks': '飞机架构集成工程',
              'changeAfterPlanWorks': '34',
              'standardWorks': '12'
            }
          ],
          'tableData2': [
            {
              'key': 1,
              'name': '123',
              'code': '456',
              'changeType': '新增',
              'applicant': '平台管理员',
              'beforeData': '',
              'afterData': ''
            }
          ]
        }
      },
      'permissions': {
        'taskItemList': {
          'listTask': 'r'
        },
        'global': {
          'readOnly': false
        },
        'taskBasicForm': {
          'nomarlplanview.useredit': 'h',
          'readOnly': 'true'
        }
      },
      'pInsId': null,
      'fInsId': null,
      'pinsId': null,
      'finsId': null
    }
  const result014 =
    {
      'assignmentVersionId': '1453234915218157568',
      'objectType': null,
      'objectViewIndex': null,
      'startTemplateData': {
        'extValues': {},
        'startTemplateCode': 'mouldFrame',
        'startTemplateName': '任务单',
        'startTemplateUrl': 'components/MouldFrame/MouldFrame',
        formCode: 'taskBasicForm',
        formUrl: 'views/plans/ModelPlanList/AddTask/index',
        'businessCode': 'plan',
        'params': {}
      },
      'commonToolBar': {
        'submit': {
          'permission': 's',
          'url': 'plan/v1/plan/header/add'
        },
        'history': {
          'permission': 's'
        },
        'save': {
          'permission': 's',
          'url': 'plan/tempcreate'
        },
        'knowledge': {
          'permission': 's',
          'url': 'plan/tempcreate'
        }
      },
      'taskData': {},
      'permissions': {
        'global': {
          'readOnly': false
        }
      },
      'pInsId': null,
      'fInsId': null,
      'pinsId': null,
      'finsId': null
    }
  const result015 =
    {
      'assignmentVersionId': '1453234915218157568',
      'objectType': null,
      'objectViewIndex': null,
      'startTemplateData': {
        'extValues': {},
        'startTemplateCode': 'mouldFrame',
        'startTemplateName': '任务单',
        'startTemplateUrl': 'components/MouldFrame/MouldFrame',
        formCode: 'importPlan',
        formUrl: 'views/plans/ModelPlanList/ImportPlan/index',
        'businessCode': 'plan',
        'params': {}
      },
      'commonToolBar': {
        'submit': {
          'permission': 's',
          'url': 'plan/v1/plan/header/add'
        },
        'history': {
          'permission': 's'
        },
        'save': {
          'permission': 's',
          'url': 'plan/tempcreate'
        },
        'knowledge': {
          'permission': 's',
          'url': 'plan/tempcreate'
        }
      },
      'taskData': {},
      'permissions': {
        'global': {
          'readOnly': false
        }
      },
      'pInsId': null,
      'fInsId': null,
      'pinsId': null,
      'finsId': null
    }
  console.log(JSON.parse(options.body), 'options')
  var result = []
  var str = JSON.parse(options.body).assignmentVersionId
  console.log('str', str)
  if (str === '001') {
    result = result001
  } else if (str === '002') {
    result = result002
  } else if (str === '003') {
    result = result003
  } else if (str === '004') {
    result = result004
  } else if (str === '005') {
    result = result005
  } else if (str === '006') {
    result = result006
  } else if (str === '007') {
    result = result007
  } else if (str === '008') {
    result = result008
  } else if (str === '009') {
    result = result009
  } else if (str === '010') {
    result = result010
  } else if (str === '011') {
    result = result011
  } else if (str === '012') {
    result = result012
  } else if (str === '013') {
    result = result013
  } else if (str === '014') {
    result = result014
  } else if (str === '015') {
    result = result015
  }
  console.log('result', result)
  return builder({
    data: result
  })
}

const getCreateTabs = (options) => {
  // 逻辑任务单
  const CMOS_TASK_LIST_CHAR_4 = {
    'extValues': {},
    'assignmentVersionId': 'CMOS_TASK_LIST_CHAR_4',
    'objectType': null,
    'objectViewIndex': null,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '逻辑任务单',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'logicTaskForm',
      formUrl: 'views/process/processManagement/taskTemplete/TaskTempleteForm',
      'businessCode': 'plan',
      'params': {}
    },
    'tab': [
      {
        'extValues': {},
        'taskElementCode': 'logicTaskItem',
        'taskElementName': '逻辑任务项',
        'taskElementUrl': 'views/processManage/ProcessDesigner',
        'tabCode': null,
        'formCode': '',
        'formUrl': '',
        'order': 0,
        'params': {}
      }
    ],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      logicTaskForm: {},
      logicTaskItem: {}
    },
    'permissions': {
      'taskItemList': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      },
      'taskBasicForm': {
        'nomarlplanview.useredit': 'h',
        'readOnly': 'true'
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  // 一般任务单
  const CMOS_TASK_LIST_CHAR_1 = {
    'extValues': {},
    'assignmentVersionId': 'CMOS_TASK_LIST_CHAR_1',
    'objectType': null,
    'objectViewIndex': null,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '一般任务单',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'generalTaskForm',
      formUrl: 'views/process/processManagement/generalTaskTemplete/GeneralTaskTemplete',
      'businessCode': 'plan',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      generalTaskForm: {
        test: '一般任务单测试数据'
      }
    },
    'permissions': {
      'taskItemList': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      },
      'taskBasicForm': {
        'nomarlplanview.useredit': 'h',
        'readOnly': 'true'
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  // 流程表单
  const CMOS_TASK_LIST_CHAR_2 = {
    'extValues': {},
    'assignmentVersionId': 'CMOS_TASK_LIST_CHAR_1',
    'objectType': null,
    'objectViewIndex': null,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '流程表单',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'flowTakskForm',
      formUrl: 'views/process/processManagement/flowTaskTemplete/FlowTaskTemplete',
      'businessCode': 'plan',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'history': {
        'permission': 's'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      flowTakskForm: {
        test: '一般任务单测试数据'
      }
    },
    'permissions': {
      'taskItemList': {
        'listTask': 'r'
      },
      'global': {
        'readOnly': false
      },
      'taskBasicForm': {
        'nomarlplanview.useredit': 'h',
        'readOnly': 'true'
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  console.log(JSON.parse(options.body), 'options')
  var result = []
  var str = JSON.parse(options.body).assignmentVersionId
  console.log('str', str)
  // 逻辑任务单
  if (str === 'CMOS_TASK_LIST_CHAR_4') {
    result = CMOS_TASK_LIST_CHAR_4
  } else if (str === 'CMOS_TASK_LIST_CHAR_1') {
    // 一般任务单
    result = CMOS_TASK_LIST_CHAR_1
  } else if (str === 'CMOS_TASK_LIST_CHAR_2') {
    // 一般任务单
    result = CMOS_TASK_LIST_CHAR_2
  }
  console.log('result', result)
  return builder({
    data: result
  })
}

const tableEditData = (options) => {
  const result = {
    role: '组织', // 添加-角色类型
    duty: '决策',
    organization: '上海航空工业',
    enableFlg: '1'
  }
  return builder({
    data: result
  })
}

const selectListData = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  let result = {}
  if (parameters.code === 'select1') {
    result = [{
      label: 'select1-1',
      value: 'select1-1'
    }, {
      label: 'select1-2',
      value: 'select1-2'
    }]
  }

  if (parameters.code === 'select2') {
    result = [{
      label: 'a1-1',
      value: 'a1-1'
    }, {
      label: 'a2-2',
      value: 'a2-2'
    }]
  }
  // 从文档中心引用  创建人
  if (parameters.code === 'creator') {
    result = [{
      label: '人1',
      value: 'a1-1'
    }, {
      label: '人2',
      value: 'a2-2'
    }]
  }

  // 从文档中心引用  发布单位
  if (parameters.code === 'issuedby') {
    result = [{
      label: '单位1',
      value: 'a1-1'
    }, {
      label: '单位2',
      value: 'a2-2'
    }]
  }

  // 从文档中心引用  发布部门
  if (parameters.code === 'department') {
    result = [{
      label: '部门1',
      value: 'a1-1'
    }, {
      label: '部门2',
      value: 'a2-2'
    }]
  }
  if (parameters.code === 'statisticsSource') {
    result = [{
      label: '新建',
      value: 'add'
    }, {
      label: '分解',
      value: 'remove'
    }]
  }
  // 工时填报  任务项类型
  if (parameters.code === 'workinghours') {
    result = [{
      label: '请选择',
      value: 'a1-1'
    }, {
      label: '类型1',
      value: 'a2-2'
    }]
  }
  // 明细报表  数据类型
  if (parameters.code === 'dataSelect') {
    result = [{
      label: '任务单',
      value: 'a1-1'
    }, {
      label: '任务项',
      value: 'a2-2'
    }]
  }

  // 获取年份
  if (parameters.code === 'ages') {
    const y = new Date().getFullYear()
    let res = []
    for (let i = 0; i <= 15; i++) {
      if (i < 15) {
        res = [{ label: (y - i) + '', value: (y - i) }, ...res]
      } else {
        for (let x = 1; x < 16; x++) {
          res.push({ value: (y + x) + '', label: (y + x) })
        }
      }
    }
    result = res
  }
  return builder({
    data: result
  })
}

const taskTableList = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)
  const result = []
  const pageNum = parseInt(parameters.pageNum)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      no: 'No ' + tmpKey,
      storeFlag: '关注',
      vesselName: '问题管理任务单',
      vesselChar: '类型',
      chargeDepartmentAlias: '上海航空工业（集团）有限公司',
      chargeDepartment: '平台管理员',
      creationDate: '2021-07-22',
      compDept: '上海航空工业（集团）有限公司',
      compPers: '平台管理员',
      approvalPers: '平台管理员',
      vesselState: '未完成',
      progressNum: 50,
      taskNum: 100,
      percent: '100.0%'
    })
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

const homeTaskRecord = (options) => {
  const result = {
    myTaskName: '我的任务',
    myTaskNumber: '1032',
    myProcedureName: '我的流程',
    myProcedureNumber: '7971',
    myNoticeName: '我的关注',
    myNoticeNumber: '6',
    myCreateName: '我的创建',
    myCreateNumber: '351'
  }
  return builder({
    data: result
  })
}

const homeTaskList = (options) => {
  const result1 = [{
    taskName: '2021年C919计划模拟导入',
    taskItem: '2021年C919计划模拟导入',
    finishTime: '2021-12-31',
    appraise: '曹居易',
    formation: '曹居易',
    flag: '已分解',
    statu: 'doing'
  },
    {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已指派',
      statu: 'closing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '调整中',
      statu: 'expect'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已填写进展',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '定时报送',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }, {
      taskName: '2021年C919计划模拟导入',
      taskItem: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      appraise: '曹居易',
      formation: '曹居易',
      flag: '已分解',
      statu: 'doing'
    }
  ]

  const result2 = [{
    taskTitle: '2021年C919计划模拟导入',
    taskNode: '2021年C919计划模拟导入',
    url: '',
    type: 'in',
    finishTime: '2021-12-31',
    createProple: '曹居易',
    createTime: '2021-12-31'
  },
    {
      taskTitle: '2021年C919计划模拟导入',
      taskNode: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      url: '',
      type: 'in',
      createProple: '曹居易',
      createTime: '2021-12-31'
    },
    {
      taskTitle: '2021年C919计划模拟导入',
      taskNode: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      url: 'www.baidu.com',
      type: 'out',
      createTime: '2021-12-31'
    },
    {
      taskTitle: '2021年C919计划模拟导入',
      taskNode: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      url: 'www.baidu.com',
      type: 'out',
      createTime: '2021-12-31'
    },
    {
      taskTitle: '2021年C919计划模拟导入',
      taskNode: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      url: 'www.baidu.com',
      type: 'out',
      createTime: '2021-12-31'
    },
    {
      taskTitle: '2021年C919计划模拟导入',
      taskNode: '2021年C919计划模拟导入',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      url: 'https://www.baidu.com',
      type: 'out',
      createTime: '2021-12-31'
    }
  ]

  const result3 = [{
    questionName: '2021年C919计划模拟导入',
    busUnit: '2021年C919计划模拟导入',
    buePeople: '2021-12-31',
    finishTime: '2021-12-31',
    createProple: '曹居易',
    createTime: '2021-12-31',
    noticeWay: '自动'
  },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    },
    {
      questionName: '2021年C919计划模拟导入',
      busUnit: '2021年C919计划模拟导入',
      buePeople: '2021-12-31',
      finishTime: '2021-12-31',
      createProple: '曹居易',
      createTime: '2021-12-31',
      noticeWay: '自动'
    }
  ]

  const result4 = [{
    taskName: '2021年C919计划模拟导入',
    busMain: '2021年C919计划模拟导入',
    buePeople: '平台管理员',
    startTime: '2021-12-31',
    planFinishTime: '2021-12-31'
  },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    },
    {
      taskName: '2021年C919计划模拟导入',
      busMain: '2021年C919计划模拟导入',
      buePeople: '平台管理员',
      startTime: '2021-12-31',
      planFinishTime: '2021-12-31'
    }

  ]
  let result = []
  console.log(options)
  var str = JSON.parse(options.body).id
  if (str === '1') {
    result = result1
  } else if (str === '2') {
    result = result2
  } else if (str === '3') {
    result = result3
  } else if (str === '4') {
    result = result4
  }
  return builder({
    data: result
  })
}

const taskTypeList = (options) => {
  const result001 = {
    typeName: '任务单发布'
  }
  const result002 = {
    typeName: '督办任务单发布'
  }
  let result = []
  console.log(options)
  var str = JSON.parse(options.body).id
  if (str === '001') {
    result = result001
  } else {
    result = result002
  }
  return builder({
    data: result
  })
}

// 从文档中心引用table
const documentTableDataList = (options) => {
  const result = [
    {
      key: 1,
      no: 1,
      docName: '文件',
      docVersion: '',
      englishName: '',
      state: '',
      concentrated: '',
      creator: '',
      department: '',
      creatorDate: '',
      releaseDate: '',
      docType: '',
      docClass: '',
      newNumber: '',
      permission: '',
      code: '333333'
    }
  ]
  return builder({
    data: result
  })
}

// 根据ID找人员Name
const findUserNameById = (options) => {
  const parameters = getBody(options)
  const userArr = [
    {
      name: '平台管理员',
      id: 'pt'
    },
    {
      name: '123管理员',
      id: '123pt'
    }
  ]
  const result = []
  parameters.id.forEach(item => {
    const obj = userArr.find(item2 => item2.id === item)
    if (obj) {
      result.push(obj)
    }
  })
  return builder({
    data: result
  })
}

// 根据ID找部门Name
const findDepartNameById = (options) => {
  const parameters = getBody(options)
  const userArr = [
    // {
    //   name: 'aa部门',
    //   id: 'pt'
    // },
    // {
    //   name: 'bb部门',
    //   id: '123pt'
    // }
  ]
  const result = []
  parameters.id.forEach(item => {
    const obj = userArr.find(item2 => item2.id === item)
    if (obj) {
      result.push(obj)
    }
  })
  return builder({
    data: result
  })
}

// 根据ID找Role Name
const findRoleNameById = (options) => {
  console.log(options, 'options')
  const result = [
    { id: '1', roleName: '测试角色' }
  ]
  return builder({
    data: result
  })
}

const taskTreeDataList = (options) => {
  console.log('传参', options)
  const result = [{
    id: '000',
    key: '全部计划类型',
    title: '全部计划类型',
    count: '10',
    parentId: 0,
    power: 'admin',
    slots: {
      icon: 'folder'
    },
    children: [{
      id: '001',
      key: '总部综合计划',
      count: '7',
      title: '总部综合计划',
      power: 'admin',
      slots: {
        icon: 'folder'
      },
      children: []
    },
      {
        id: '002',
        key: '各中心综合计划',
        count: '3',
        title: '各中心综合计划',
        power: 'admin',
        slots: {
          icon: 'folder'
        },
        children: []
      },
      {
        id: '003',
        key: '专项计划',
        count: '0',
        title: '专项计划',
        power: 'admin',
        slots: {
          icon: 'folder'
        }
      }
    ]
  }]
  return builder({
    data: result
  })
}

// const planTreeDataListData = (options) => {
//   console.log('传参', options)
//   const result = [
//     { 'adjustAssignmentId': '1451012098391961600', 'adjustAssignmentName': '', 'cateName': '测试任务类型01', 'cateNumb': 't001', 'feedbackAssignmentId': '1451012098391961600', 'feedbackAssignmentName': '', 'mainAssignmentId': '1452866182451482624,1451012098391961600', 'mainAssignmentName': '', 'parentId': 0, 'resolveAssignmentName': '', 'showFlag': '0', 'subType': '', 'taskCate': '0,1,2,3,4,5,6,7', 'wbsFlag': '0', 'resolveAssignmentId': '1451012098391961600', 'id': '0' }
//   ]
//   return builder({
//     data: result
//   })
// }

const todoMockData = (options) => {
  const result001 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123456,
    insId: '11',
    tInsId: 123456,
    processNodeCode: 'CONTRACTWRITE',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同拟定',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractProtocol/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        CONTRACTNAME: 'test-20211207',
        FILENAME: 'contract.doc',
        FILEID: '398985138265923584',
        EDITFLAG: '1',
        SYNERGYFLAG: '0',
        TEMPLATEID: '001',
        TEMPLATENAME: 'template001'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  const result002 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    processNodeCode: 'CONTRACTCREATE',
    assimentInstanceId: '001',
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同分发',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'contractDistribution',
      formUrl: 'views/contract/ContractDistribution/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {},
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  const result003 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTASSESS',
    assimentInstanceId: 123,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同评估',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractAssessment/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
        // 'url': 'contract/cmos/document/v1/contract/submit'
      },
      'save': {
        'permission': 's',
        // 'url': 'contract/cmos/document/v1/contract/save'
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        TASKSOURCE: '合同',
        TASKNO: '1234',
        FORMNO: '5678',
        CONTRACTNAME: 'xxxx',
        COMPILEDATE: '',
        FINALPERSTATE: '1',
        CHANGEAGREEMENTNUMBER: 'AABB',
        ABNORMALPERSTATUS: '1',
        ISDISPUTESHANDLED: '1',
        ISCONTRACTFILING: '1',
        REASONSOLUTIONFIRST: 'XXXXXX',
        ISILLEGAL: '1',
        ISACHIEVEPURPOSE: '1',
        REASONSOLUTIONSECOND: 'zzzzz',
        REASONSOLUTIONTHIRD: '1',
        DISPATCHDEPARTMENT: '部门',
        ORDERNO: '',
        FORMOWNER: 'admin',
        PREVFORMNUMBER: '',
        FORMNUMBER: '',
        NEXTFORMNUMBER: ''
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }
  const result004 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    processNodeCode: 'CONTRACTCREATE',
    assimentInstanceId: '001',
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同模板',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'contractTemplate',
      formUrl: 'views/contract/ContractTemplate/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': 'plan/v1/plan/header/add'
      },
      'save': {
        'permission': 's',
        'url': 'plan/tempcreate'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {},
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result005 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123456,
    insId: '11',
    tInsId: 123456,
    processNodeCode: 'CONTRACTINPUT',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同录入',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractEntry/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        CONTRACTNAME: 'test-20211207',
        FILENAME: 'contract.doc'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result006 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123456,
    insId: '11',
    tInsId: 123456,
    processNodeCode: 'CONTRACTNEGOTIATE',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '谈判合同表',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractNegotiation/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        CONTRACTNAME: 'contractName',
        FILENAME: 'contract.doc',
        FILEID: '398985138265923584',
        NEGOTIATESUMMARY: '谈判合同表',
        RECORDLIST: [{
          RECORDOBJINDEX: '111',
          COMPLETESTATUS: '未完成',
          NEGOTIATETYPE: '谈判事项决策CODE',
          NEGOTIATETYPEDES: '谈判事项决策DESC',
          PRECAUTIONS: 'YYYYYYY',
          EDITTIME: '2021-10-21'
      }]
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result007 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTINPUT',
    assimentInstanceId: 123,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '谈判记录表',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/NegotiationRecord/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/negotiate/record/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/negotiate/record/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        TASKSOURCE: '来源1',
        FORMNO: '1394798327',
        TASKNO: 'task132321',
        EDITTIME: '2020-3-3',
        PROJECTNAME: '中国商飞',
        OPPONENTCOMPANY: '对方单位',
        NEGOTIATETIME: '2021-2-4',
        NEGOTIATEPLACE: '上海',
        RECORDER: '张三',
        NEGOTIATEROUND: '第一轮',
        OPPONENTNEGOITATENAME: '李四',
        POST: '助理',
        NEGOITATERECORD: '结果',
        MAINNEGOTIATOR: '小明',
        OTHERNEGOTIATOR: '小刚',
        DISPATCHDEPARTMENT: '部门1',
        ORDERSIGN: '号令',
        FORMOWNER: '小丽',
        PREVFORMNUMBER: 'FORM_2134',
        FORMNUMBER: 'FORM_4234',
        NEXTFORMNUMBER: 'FORM_9283'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result008 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTDISTRIBUTE',
    assimentInstanceId: 123,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同分发',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractDistribution/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        CONTRACTNAME: 'test-20211208',
        CONTRACTNUM: '97225',
        NUMBER: '1133000',
        EXTERNAL: [
          {
            'OPPOSITE_NAME': '张三',
            'OPPOSITE_RECEIVED_NAME': '拉法',
            'FILES': '142143242,12423354645656765'
        }],
        INSIDE: [
          {
            'RECEIVED_ID': '1234',
            'RECEIVED_NAME': 'laflafs',
            'RECEIVED_DEPT_ID': '243254343',
            'RECEIVED_DEPT_NAME': '案件法律010'
          }]
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result009 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTINPUT',
    assimentInstanceId: 123,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '谈判决策表',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/NegotiatorDecision/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/negotiate/decision/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/negotiate/decision/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        TASKSOURCE: '来源1',
        FORMNO: '1394798327',
        TASKNO: 'task132321',
        EDITTIME: '2020-3-3',
        PROJECTTYPE: '1',
        CONTRACTNAME: '合同名称',
        DESCRIPTION: '描述-2-4',
        EFFECTCHAPTE: 'mainContract',
        APPLICANT: '张三',
        NEGOTIATELEADER: '李四',
        DISPATCHDEPARTMENT: '信息部',
        ORDERSIGN: '号令',
        FORMOWNER: '小丽',
        PREVFORMNUMBER: 'FORM_2134',
        FORMNUMBER: 'FORM_4234',
        NEXTFORMNUMBER: 'FORM_9283'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result999 = {
    'extValues': {

    },
    'assignmentVersionId': '1466316672134520832',
    'startTemplateData': {
      'extValues': {

      },
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '任务项反馈',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      'businessCode': 'plan',
      'formCode': 'feedbackform',
      'formUrl': 'views/task/PrimaryTaskItem/index',
      'objectType': 'plan|normal_plan|task_feed_backs',
      'objectViewIndex': 'feedback',
      'objectTypeVersion': 1,
      'params': {

      }
    },
    'tab': [
      {
        'extValues': {

        },
        'taskElementCode': 'progress',
        'taskElementName': '进展',
        'taskElementUrl': 'views/plans/ordinaryFeedback/progress',
        'tabCode': null,
        'formCode': '',
        'order': 0,
        'params': {

        }
      },
      {
        'extValues': {

        },
        'taskElementCode': 'subTask',
        'taskElementName': '下级任务项',
        'taskElementUrl': 'views/plans/ordinaryFeedback/SubTask',
        'tabCode': null,
        'formCode': '',
        'order': 1,
        'params': {

        }
      },
      {
        'extValues': {

        },
        'taskElementCode': 'taskDetail',
        'taskElementName': '任务项详情',
        'taskElementUrl': 'views/plans/ordinaryFeedback/TaskDetail',
        'tabCode': null,
        'formCode': '',
        'order': 2,
        'params': {

        }
      },
      {
        'extValues': {

        },
        'taskElementCode': 'actWorkingHours',
        'taskElementName': '实动工时',
        'taskElementUrl': 'views/plans/ordinaryFeedback/DirectLaborHours',
        'tabCode': null,
        'formCode': '',
        'order': 3,
        'params': {

        }
      },
      {
        'extValues': {

        },
        'taskElementCode': 'checkList',
        'taskElementName': '检查单',
        'taskElementUrl': 'views/plans/ordinaryFeedback/CheckList',
        'tabCode': null,
        'formCode': '',
        'order': 4,
        'params': {

        }
      }
    ],
    'commonToolBar': {
      'addSignature': {
        'permission': 'h',
        'url': '/flow-platform/api/v1.0/public/addSignTask'
      },
      'transfer': {
        'permission': 'h',
        'url': '/flow-platform/api/v1.0/public/dispatchWorkItem'
      },
      'submit': {
        'permission': 's',
        'url': '/plan/biz/service/task/feedback/submit'
      },
      'adjust': {
        'permission': 'h',
        'url': '/assignment/biz/service/instance/wf/submit'
      },
      'signIn': {
        'permission': 'h',
        'url': ''
      },
      'save': {
        'permission': 's',
        'url': '/plan/biz/service/task/feedback/save'
      },
      'history': {
        'permission': 's'
      },
      'veto': {
        'permission': 'h',
        'url': '/flow-platform/api/v1.0/public/terminateProcessInstance'
      },
      'knowledge': {
        'permission': 's'
      }
    },
    'taskData': {
      'feedbackform': {
        'ASSIGNMENTINSTANCEID': '1481599126626562048',
        'ASSIGNNUM': '0',
        'taskObjMap': {
          'ORDERNUM': 0,
          'TASKNATURE_NAME': '一般任务项',
          'ASSIGNMENTINSTANCEID': '1481599126626562048',
          'CHARGEDEPARTMENTALIAS': '10664',
          'ASSIDEPT_NAME': '',
          'TASKCODE': 'RWXP22005284-2',
          'TASKORIGIN': 'TASK_ITEM_SOURCE_1',
          'CHARGEDEPARTMENT': '998011301117',
          'PLANCLASSIFYID_NAME': '任务单--请勿修改,如修改请联系陶紫建 20211123',
          'FOCUSPERSON_NAME': '',
          'ASSIGNMENTVERSIONID': '1466316672134520832',
          'ASSIDEPT': '',
          'VESSELID': '1481560602822627328',
          'TASKORIGIN_NAME': '新建',
          'EVALPERS': '998011301117',
          'ID': '1481595609188581376',
          'ISCHANGE': '1',
          'DESIGNATEDPERSON': '',
          'TASKSTATE_NAME': '执行中',
          'COMPPERS': '998011301117',
          'EVALPERS_NAME': '质检员',
          'PLANCLASSIFYID': '1463069636478042112',
          'COMPPERS_NAME': '质检员',
          'TASKNAME': 'test',
          'TASKNATURE': 'COMAC_TASK_ITEM_NATURE_1',
          'ASSIGNMENTVERSIONID_NAME': '普通反馈流程',
          'COMPTIME': '2022-01-13T17:35:38+0800',
          'STARTTIME': '2022-01-13T17:34:54+0800',
          'VESSELSTATE': 'PLAN_POINT_STATE_5',
          'CHARGEDEPARTMENT_NAME': '质检员',
          'FOCUSPERSON': '',
          'TASKSTATE': 'PLAN_POINT_STATE_2',
          'DESIGNATEDPERSON_NAME': '',
          'CHARGEDEPARTMENTALIAS_NAME': '测试1234',
          'ISDECOMPOSE': '1'
        },
        'linkPdmNum': 0,
        'CHARGEDEPARTMENT': '998011301117',
        'EVALPERS': '998011301117',
        'TASKNAME': 'test',
        'TASKOBJINDEX': 'CMOS-PLN:OIV:1481595609188581377:001',
        'mainObjIndex': 'CMOS-PLN:OIV:1481599125969879040:001'
      }
    },
    'permissions': {
      'taskDetail': {
        'readOnly': 'false'
      },
      'feedbackform': {
        'readOnly': 'false'
      },
      'subTask': {
        'readOnly': 'false'
      },
      'actWorkingHours': {
        'readOnly': 'false'
      },
      'progress': {
        'readOnly': 'false'
      },
      'global': {
        'readOnly': 'false'
      }
    },
    'processInstanceId': null,
    'flowInstanceId': '93567e78746911ec9f24923ba80f2f45',
    'tInsId': '1481599126626562048',
    'processNodeCode': null,
    'flowNodeCode': 'FEEDBACK_01',
    'mainObjIndex': 'CMOS-PLN:OIV:1481599125969879040:001',
    'title': null,
    'flowNodeId': '93e9bdfc746911ec9f24923ba80f2f45',
    'tinsId': 1481599126626562048
  }
  const result010 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123456,
    insId: '11',
    tInsId: 123456,
    processNodeCode: 'CONTRACTAUDIT',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '审批记录表',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ApprovalRecord/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        contractName: 'contractName',
        contractIndex: '97225'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result011 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123456,
    insId: '11',
    tInsId: 123456,
    processNodeCode: 'CONTRACTSEAL',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同用印',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractUseSeal/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result012 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTPERFORMANCE',
    assimentInstanceId: 123,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '合同履约',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/ContractPromise/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        TASKCODE: '任务项编号',
        TASKNAME: '任务项名称',
        CONTRACTCHANGEAGREEMENTNUMBER: '合同变更协议号',
        CONTRACTNAME: '合同名称',
        CONTRACTNUMBER: '合同编号',
        CONTRACTSERIALNUMBER: '合同流水号',
        DELAYSITUATIONDESCRIPTION: '延迟情况说明',
        DELIVERYUPLOAD: '交付物上传',
        EXCEPTIONSITUATIONDESCRIPTION: '异常情况说明',
        EXCEPTIONSITUATIONTYPE: '异常情况类型',
        FULFILLMENTPLANNUMBER: '履约计划编号',
        ISABNORMALWHENFILLING: '截止填表时是否异常',
        ISCONTRACTCHANGED: '1',
        ISINVOLVERECEIVEPAYNODE: '1',
        ISITACTUALLYDELAYED: '1',
        PERFORMANCEACTUALACCEPTTIME: '2020-01-02',
        PERFORMANCEACTUALCOMPLETECONS: '履约实际完成情况结论',
        PERFORMANCEACTUALDELIVERABLE: '履约实际交付物',
        PERFORMANCEACTUALDELIVERYTIME: '2020-01-03',
        PERFORMANCECLAUSECONTENT: '履约条款内容',
        PERFORMANCEDELIVERABLE: '履约交付物',
        PERFORMANCENODECOMPLETESTAND: '履约节点完成标准',
        PERFORMANCEPLANDELIVERYTIME: '2020-01-04',
        PERFORMANCERESPONSIBILITYDEP: '履约责任部门',
        PERFORMER: '履约责任人',
        RECEIVEPAYTYPE: '收付款类型',
        REMARKS: '备注'
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  const result013 = {
    'extValues': {},
    'assignmentVersionId': '1453234915218157568',
    'objectType': null,
    'objectViewIndex': null,
    instanceId: 123,
    insId: '11',
    tInsId: 123,
    processNodeCode: 'CONTRACTTEAM',
    assimentInstanceId: 123456,
    'startTemplateData': {
      'extValues': {},
      'startTemplateCode': 'mouldFrame',
      'startTemplateName': '团队组建',
      'startTemplateUrl': 'components/MouldFrame/MouldFrame',
      formCode: 'taskBasicForm',
      formUrl: 'views/contract/TeamBuild/index',
      'businessCode': 'contract',
      'params': {}
    },
    'tab': [],
    'commonToolBar': {
      'submit': {
        'permission': 's',
        'url': '/contract/biz/contract/submit'
      },
      'save': {
        'permission': 's',
        'url': '/contract/biz/contract/save'
      },
      'history': {
        'permission': 's'
      },
      'knowledge': {
        'permission': 's',
        'url': 'plan/tempcreate'
      }
    },
    'taskData': {
      taskBasicForm: {
        TASKSOURCE: '任务来源',
        FORMNO: '表单单号',
        SOURCENUMBER: '来源编号',
        COMPILEDATE: '2020-03-03',
        ISBUILDTEAM: '1',
        BUILDDEPT: 'pt',
        BUILDDEPT_NAME: 'aa部门',
        CONTRACTNAME: '合同名称',
        NUMB: '编号',
        PROJECTTYPE: 'CONTRACT_OBJECT_TYPE_1',
        CONTRACTTYPE: 'CONTRACT_TYPE_RADIO_1',
        DESCRIPTION: '合同谈判背景说明、主要目标',
        BUILDER: '99779009',
        BUILDER_NAME: 'test919',
        BUILDDEPTSECTION: '10664',
        BUILDDEPTSECTION_NAME: '测试1234',
        BUILDDEPTPRINCIPAL: '99779009',
        BUILDDEPTPRINCIPAL_NAME: 'test919',
        SENDDEPART: '发送部门',
        ORDERSIGN: '令号',
        FORMOWNER: '小丽',
        PREVFORMNUMBER: 'FORM_2134',
        FORMNUMBER: 'FORM_4234',
        NEXTFORMNUMBER: 'FORM_9283',
        dataSource: [
          {
            key: 1,
            personnel: '99779009',
            persopnel_name: 'test919',
            department: 'pt',
            department_name: 'aa部门'
          },
          {
            key: 2,
            personnel: '99779009',
            persopnel_name: 'test919',
            department: 'pt',
            department_name: 'aa部门'
          },
          {
            key: 3,
            personnel: '99779009',
            persopnel_name: 'test919',
            department: 'pt',
            department_name: 'aa部门'
          }
        ]
      }
    },
    'permissions': {
      'global': {
        'readOnly': false
      }
    },
    'pInsId': null,
    'fInsId': null,
    'pinsId': null,
    'finsId': null
  }

  console.log(options, 'mcok todo')
  var result = []
  const reg = new RegExp(`(?<=\\b${'processInstanceId'}=)[^&]*`)
  const value = options.url.match(reg)
  var str = value ? value[0] : ''
  console.log('str', str)
  if (str === '001') {
    result = result001
  } else if (str === '002') {
    result = result002
  } else if (str === '003') {
    result = result003
  } else if (str === '004') {
    result = result004
  } else if (str === '005') {
    result = result005
  } else if (str === '006') {
    result = result006
  } else if (str === '007') {
    result = result007
  } else if (str === '008') {
    result = result008
  } else if (str === '009') {
    result = result009
  } else if (str === '010') {
    result = result010
  } else if (str === '011') {
    result = result011
  } else if (str === '012') {
    result = result012
  } else if (str === '013') {
    result = result013
  } else if (str === '999') {
    result = result999
  } else {
    // null
  }
  console.log('result', result)
  return builder({
    data: result
  })
}
Mock.mock(/\/process\/businessDomainListQuery/, 'get', serverList)
Mock.mock(/\/process\/taskFormTabQuery/, 'get', tabList)
Mock.mock(/\/process\/advancedTabQuery/, 'get', advancedList)
Mock.mock(/\/process\/getCommonSelectByCode/, 'post', selectListData)
Mock.mock(/\/process\/businessDomainTreeQuery/, 'post', treeDataList)
Mock.mock(/\/process\/businessDomainQueryTreeHasChildren/, 'post', treeDataListhasChildren)

Mock.mock(/\/process\/businessDomainTreeFilter/, 'post', treeDataListFilter)
Mock.mock(/\/process\/businessDomainTreeGetChildren/, 'post', treeDataChildren)

Mock.mock(/\/process\/businessDomainTreeGetContent/, 'get', treeDataCentent)
Mock.mock(/\/process\/businessDomainTreeEditContent/, 'get', treeEditData)
Mock.mock(/\/process\/businessDomainEditTable/, 'get', tableEditData)
Mock.mock(/\/process\/businessDomainHistoryListData/, 'get', historyListData)

Mock.mock(/\/process\/reportFormsTagList/, 'get', reportTagList)
Mock.mock(/\/process\/businessTagList/, 'get', businessList)
Mock.mock(/\/process\/taskModel/, 'get', taskModel)
Mock.mock(/\/process\/taskRunAssemblyList/, 'get', assemblyList)
Mock.mock(/\/process\/customeView/, 'get', customView)

Mock.mock(/\/process\/taskTabsList/, 'post', taskTabsList)
Mock.mock(/\/process\/getCreateTabs/, 'post', getCreateTabs)
// plans
Mock.mock(/\/plans\/getPlanManagementData/, 'get', treeDataList)
Mock.mock(/\/plans\/getPlantreeData/, 'get', planTreeDataList)
Mock.mock(/\/plans\/planTreeGetChildren/, 'get', planTreeDataChildren)
Mock.mock(/\/plans\/getPlanHeaderData/, 'post', taskTableList)
Mock.mock(/\/plans\/searchDocumentTableDataList/, 'post', documentTableDataList)
Mock.mock(/\/plans\/findUserNameById/, 'post', findUserNameById)
Mock.mock(/\/plans\/findDepartNameById/, 'post', findDepartNameById)
Mock.mock(/\/plans\/findRoleNameById/, 'post', findRoleNameById)

Mock.mock(/\/process\/homeTaskRecord/, 'get', homeTaskRecord)
Mock.mock(/\/process\/homeTaskList/, 'post', homeTaskList)
Mock.mock(/\/process\/taskTreeDataList/, 'post', taskTreeDataList)
Mock.mock(/\/process\/taskTypeList/, 'post', taskTypeList)
// Mock.mock(/\/plan\/cmos\/plan\/v1\/planclassify\/selectPageNode/, 'get', planTreeDataListData)
// Mock.mock(/\/plan\/cmos\/plan\/v1\/planclassify\/selectAllList/, 'get', planTreeDataListData)
Mock.mock(/\/assignment\/biz\/service\/instance\/wf\/todo\/mock/, 'get', todoMockData)
