import Mock from 'mockjs2'
import { builder } from '../util'

const permissionData = (options) => {
  const result = [
    {
      id: 'FormTask1',
      name: '任务单'
    },
    {
      id: 'FormTask2',
      name: '任务项'
    },
    {
      id: 'DynamicForm',
      name: '详情页'
    }
  ]
  return builder({
    data: result
  })
}

Mock.mock(/\/master-data\/roles\/item/, 'get', permissionData)
