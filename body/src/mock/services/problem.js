import Mock from 'mockjs2'
import {
  builder
} from '../util'

const treeDataList = (options) => {
  console.log('传参', options)
  const result = [{
    id: '000',
    key: '全面质量问题监控',
    title: '全面质量问题监控',
    parentId: 0,
    power: 'admin',
    slots: {
      icon: 'folder'
    },
    children: [{
      id: '001',
      key: '型号事件',
      title: '型号事件',
      power: 'admin',
      type: '1',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '002',
      key: '产品故障-航线问题',
      title: '产品故障-航线问题',
      power: 'edit',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '003',
      key: '产品故障-生产试飞问题',
      title: '产品故障-生产试飞问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '004',
      key: '产品故障-研制试飞问题',
      title: '产品故障-研制试飞问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '005',
      key: '产品质量',
      title: '产品质量',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '006',
      key: '质量逃逸',
      title: '质量逃逸',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '007',
      key: '技术质量评审',
      title: '技术质量评审',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '008',
      key: '质量审核-内审',
      title: '质量审核-内审',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '009',
      key: '质量审核-局方',
      title: '质量审核-局方',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '010',
      key: '地面支援设备问题',
      title: '地面支援设备问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '011',
      key: '技术出版问题',
      title: '技术出版问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '012',
      key: '培训问题',
      title: '培训问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '013',
      key: '让步问题',
      title: '让步问题',
      power: '123',
      slots: {
        icon: 'folder'
      }
    },
    {
      id: '014',
      key: '产品建议',
      title: '产品建议',
      power: '123',
      slots: {
        icon: 'folder'
      }
    }
    ]
  }]
  return builder({
    data: result
  })
}

Mock.mock(/\/problem\/getQualityProblemsType/, 'get', treeDataList)
