import Mock from 'mockjs2'
import {
  builder
} from '../util'

const info = options => {
  console.log('options', options)
  const userInfo = {
    id: '4291d7da9005377ec9aec4a71ea837f',
    name: '商飞',
    username: 'admin',
    password: '',
    avatar: '/user.png',
    status: 1,
    telephone: '',
    lastLoginIp: '27.154.74.117',
    lastLoginTime: 1534837621348,
    creatorId: 'admin',
    createTime: 1497160610259,
    merchantCode: 'TLif2btpzg079h15bk',
    deleted: 0,
    roleId: 'admin',
    role: {}
  }
  // role
  const roleObj = {
    id: 'admin',
    name: '管理员',
    describe: '拥有所有权限',
    status: 1,
    creatorId: 'system',
    createTime: 1497160610259,
    deleted: 0,
    permissions: [
      // {
      //   roleId: 'admin',
      //   permissionId: 'process',
      //   actions:
      //     '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
      //   actionList: null,
      //   dataAccess: null
      // },
      {
        roleId: 'admin',
        permissionId: 'business-domain',
        permissionName: 'process.business-domain',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'task-form',
        permissionName: 'process.task-form',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'classification-management',
        permissionName: 'plans.classification-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'planning-management',
        permissionName: 'plans.planning-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'model-project-management',
        permissionName: 'plans.model-project-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'task-management',
        permissionName: 'plans.task-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'my-mission',
        permissionName: 'plans.my-mission',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'one-on-one-management',
        permissionName: 'plans.one-on-one-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'one-on-one-management-template',
        permissionName: 'plans.one-on-one-management.template',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'annual-target',
        permissionName: 'plans.annual-target',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'model-project-management',
        permissionName: 'plans.model-project-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'WBS',
        permissionName: 'plans.WBS',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'working-hours-management',
        permissionName: 'plans.working-hours-management',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'report-detaile',
        permissionName: 'plans.report-detaile',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'team-woring-hours',
        permissionName: 'plans.team-woring-hours',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'plans-form',
        permissionName: 'plans.plans-form',
        actions: null,
        actionEntitySet: null,
        actionList: ['add', 'delete'],
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'dashboard',
        permissionName: '仪表盘',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'exception',
        permissionName: '异常页面权限',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'result',
        permissionName: '结果权限',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'profile',
        permissionName: '详细页权限',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '表格权限',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'form',
        permissionName: '表单权限',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'order',
        permissionName: '订单管理',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'permission',
        permissionName: '权限管理',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'role',
        permissionName: '角色管理',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '桌子管理',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'user',
        permissionName: '用户管理',
        actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
        actionEntitySet: [{
          action: 'add',
          describe: '新增',
          defaultCheck: false
        },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          },
          {
            action: 'export',
            describe: '导出',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      }
    ]
  }

  roleObj.permissions.push({
    roleId: 'admin',
    permissionId: 'support',
    permissionName: '超级模块',
    actions: '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
    actionEntitySet: [{
      action: 'add',
      describe: '新增',
      defaultCheck: false
    },
      {
        action: 'import',
        describe: '导入',
        defaultCheck: false
      },
      {
        action: 'get',
        describe: '详情',
        defaultCheck: false
      },
      {
        action: 'update',
        describe: '修改',
        defaultCheck: false
      },
      {
        action: 'delete',
        describe: '删除',
        defaultCheck: false
      },
      {
        action: 'export',
        describe: '导出',
        defaultCheck: false
      }
    ],
    actionList: null,
    dataAccess: null
  })

  userInfo.role = roleObj
  return builder(userInfo)
}

const userNav = options => {
  const nav = [
    // dashboard
    // {
    //   name: 'dashboard',
    //   parentId: 0,
    //   id: 1,
    //   meta: {
    //     icon: 'dashboard',
    //     title: '仪表盘',
    //     show: true
    //   },
    //   component: 'RouteView',
    //   redirect: '/dashboard/analysis'
    // },
    {
      name: 'Analysis',
      parentId: 1,
      id: 2,
      meta: {
        title: '分析页',
        show: true
      },
      component: 'Analysis',
      path: '/dashboard/analysis'
    },
    // DEMO
    // {
    //   name: 'demo',
    //   parentId: 0,
    //   id: 990,
    //   meta: {
    //     icon: 'unordered-list',
    //     title: 'DEMO'
    //   },
    //   redirect: '/demo/task-form',
    //   component: 'RouteView'
    // },
    // {
    //   name: 'business-domain',
    //   parentId: 990,
    //   id: 991,
    //   meta: {
    //     title: 'router.processManage.BusinessDomain'
    //   },
    //   component: 'BusinessDomain'
    // },
    {
      name: 'task-form',
      parentId: 990,
      id: 992,
      meta: {
        title: 'router.processManage.TaskForm'
      },
      component: 'TaskForm'
    },
    {
      name: 'dynamic-form',
      parentId: 990,
      id: 993,
      meta: {
        title: 'router.processManage.DynamicForm'
      },
      component: 'DynamicForm'
    },
    {
      name: 'table-form',
      parentId: 600,
      id: 994,
      meta: {
        title: 'router.processManage.TableForm'
      },
      component: 'TableForm'
    },
    {
      name: 'process-designer',
      parentId: 990,
      id: 995,
      meta: {
        title: 'router.processManage.ProcessDesigner'
      },
      component: 'ProcessDesigner'
    },
    {
      name: 'task',
      parentId: 0,
      id: 880,
      meta: {
        icon: 'unordered-list',
        title: '作业'
      },
      redirect: '/task/taskList',
      component: 'RouteView'
    },
    {
      name: 'system-mgt',
      parentId: 0,
      id: 885,
      meta: {
        icon: 'unordered-list',
        title: '系统管理'
      },
      redirect: '/task/taskList',
      component: 'RouteView'
    },
    {
      name: 'task-list',
      parentId: 880,
      id: 881,
      meta: {
        title: 'router.task.TaskList',
        keepAlive: true
      },
      component: 'TaskList'
    },
    {
      name: 'task-add',
      parentId: 880,
      id: 882,
      meta: {
        title: 'router.task.TaskAdd',
        keepAlive: true
      },
      component: 'TaskAdd'
    },
    {
      name: 'dailyLog',
      parentId: 880,
      id: 2000,
      meta: {
        title: '日志',
        hiddenHeaderContent: true
      },
      component: 'Daily'
    },
    {
      name: 'changeDepartment',
      parentId: 880,
      id: 2001,
      meta: {
        title: '切换部门',
        hiddenHeaderContent: true
      },
      component: 'ChangeDepartment'
    }, {
      name: 'setUp',
      parentId: 880,
      id: 2002,
      meta: {
        title: '设置',
        hiddenHeaderContent: true
      },
      component: 'SetUp'
    },
    {
      name: 'dataDict',
      parentId: 885,
      id: 2003,
      meta: {
        title: '数据字典',
        hiddenHeaderContent: false
      },
      component: 'DataDict'
    },

    {
      name: 'to-do-list',
      parentId: 880,
      id: 884,
      meta: {
        title: 'router.task.ToDoList'
      },
      component: 'ToDoList'
    },
    {
      name: 'home-task',
      parentId: 880,
      id: 883,
      meta: {
        title: 'router.task.HomeTask'
      },
      component: 'HomeTask'
    },
    // 计划
    {
      name: 'plans',
      parentId: 0,
      id: 100,
      meta: {
        title: '计划',
        icon: 'unordered-list'
      },
      component: 'RouteView'
    },
    {
      name: 'todo',
      parentId: 100,
      id: 10086,
      meta: {
        title: '待办',
        hiddenHeaderContent: true
      },
      component: 'Todo'
    },
    {
      name: 'classification-management',
      parentId: 100,
      id: 101,
      meta: {
        title: 'menu.plans.classification-management'
      },
      component: 'ClassificationManagement'
    },
    {
      name: 'planning-management',
      parentId: 100,
      id: 102,
      meta: {
        title: 'menu.plans.planning-management'
      },
      component: 'PlanningManagement'
    },

    {
      name: 'task-management',
      parentId: 100,
      id: 103,
      meta: {
        title: 'menu.plans.task-management'
      },
      component: 'TaskManagement'
    },
    {
      name: 'my-mission',
      parentId: 100,
      id: 104,
      meta: {
        title: 'menu.plans.my-mission'
      },
      component: 'MyMission'
    },
    {
      name: 'one-on-one-management',
      parentId: 100,
      id: 105,
      meta: {
        title: 'menu.plans.one-on-one-management'
      },
      component: 'OneOnOneManagement'
    },
    {
      name: 'one-on-one-management-template',
      parentId: 100,
      id: 10501,
      meta: {
        title: '一卡一单管理模板库'
      },
      component: 'OneOnOneManagementTemplate'
    },
    {
      name: 'annual-target',
      parentId: 100,
      id: 106,
      meta: {
        title: 'menu.plans.annual-target'
      },
      component: 'AnnualTarget'
    },
    {
      name: 'WBS',
      parentId: 100,
      id: 108,
      meta: {
        title: 'menu.plans.WBS'
      },
      component: 'WBS'
    },
    {
      name: 'working-hours-management',
      parentId: 100,
      id: 109,
      meta: {
        title: 'menu.plans.working-hours-management'
      },
      component: 'WorkingHoursManagement',
      redirect: '/plans/working-hours-management/working-hours'
    },
    {
      name: 'working-hours',
      parentId: 109,
      id: 1091,
      meta: {
        title: 'menu.plans.working-hours'
      },
      component: 'WorkingHours'
    },
    {
      name: 'team-woring-hours',
      parentId: 109,
      id: 1092,
      meta: {
        title: 'menu.plans.team-woring-hours'
      },
      component: 'TeamWoringHours'
    },
    {
      name: 'report-detaile',
      parentId: 109,
      id: 1093,
      meta: {
        title: 'menu.plans.report-detaile'
      },
      component: 'ReportDetaile'
    },
    {
      name: 'model-project-management',
      parentId: 100,
      id: 119,
      meta: {
        title: 'menu.plans.model-project-management'
      },
      component: 'ModelPlanList'
    },
    // 过程
    {
      name: 'processs',
      parentId: 0,
      id: 200,
      meta: {
        title: '过程',
        icon: 'unordered-list'
      },
      component: 'RouteView'
    },
    {
      name: 'process-management',
      parentId: 200,
      id: 201,
      meta: {
        title: 'menu.process.process-management'
      },
      component: 'ProcessManagement'
    },
    {
      name: 'pi-management',
      parentId: 200,
      id: 202,
      meta: {
        title: 'menu.process.pi-management'
      },
      component: 'PiManagement'
    },
    {
      name: 'initiating-process',
      parentId: 200,
      id: 203,
      meta: {
        title: 'menu.process.initiating-process'
      },
      component: 'InitiatingProcess'
    },
    {
      name: 'business-domain',
      parentId: 200,
      id: 204,
      meta: {
        title: 'menu.process.BusinessDomain'
      },
      component: 'BusinessDomain'
    },
    // 文档
    {
      name: 'documents',
      parentId: 0,
      id: 500,
      meta: {
        title: '文档',
        icon: 'unordered-list'
      },
      component: 'RouteView'
    },
    {
      name: 'type-management',
      parentId: 500,
      id: 501,
      meta: {
        title: 'menu.document.type-management'
      },
      component: 'TypeManagement'
    },
    {
      name: 'subject-no',
      parentId: 500,
      id: 502,
      meta: {
        title: 'menu.document.subject-no'
      },
      component: 'SubjectNo'
    },
    {
      name: 'subject-document',
      parentId: 500,
      id: 503,
      meta: {
        title: 'menu.document.subject-document'
      },
      component: 'SubjectDocument'
    },
    {
      name: 'document-list',
      parentId: 500,
      id: 504,
      meta: {
        title: 'menu.document.document-list'
      },
      component: 'DocumentList'
    },
    {
      name: 'document-search',
      parentId: 500,
      id: 505,
      meta: {
        title: 'menu.document.document-search'
      },
      component: 'DocumentSearch'
    },
    {
      name: 'standard-system',
      parentId: 500,
      id: 505,
      meta: {
        title: 'menu.document.standard-system'
      },
      component: 'StandardSystem'
    },
    {
      name: 'standard-type-management',
      parentId: 500,
      id: 510,
      meta: {
        title: 'menu.document.standard-type-management'
      },
      component: 'StandardTypeManagement'
    },
    // 改善
    {
      name: 'problem',
      parentId: 0,
      id: 400,
      meta: {
        title: '改善',
        icon: 'unordered-list'
      },
      component: 'RouteView'
    },
    {
      name: 'problem-home',
      parentId: 400,
      id: 401,
      meta: {
        title: '首页待办'
      },
      component: 'ProblemHome'
    },
    {
      name: 'problem-control',
      parentId: 400,
      id: 402,
      meta: {
        title: '问题管控'
      },
      component: 'ProblemControl'
    },
    {
      name: 'audit-plan',
      parentId: 400,
      id: 406,
      meta: {
        title: '审核计划'
      },
      component: 'AuditPlan'
    },
    {
      name: 'problem-report',
      parentId: 400,
      id: 403,
      meta: {
        title: '统计报表'
      },
      component: 'RouteView'
    },
    {
      name: 'product-failure',
      parentId: 403,
      id: 4031,
      meta: {
        title: '产品故障问题'
      },
      component: 'ProductFailure'
    },
    {
      name: 'problem-audit',
      parentId: 403,
      id: 4032,
      meta: {
        title: '审核问题'
      },
      component: 'ProblemAudit'
    },
    {
      name: 'problem-dict',
      parentId: 400,
      id: 405,
      meta: {
        title: '数据字典'
      },
      component: 'ProblemDict'
    },
    {
      name: 'problem-record',
      parentId: 400,
      id: 408,
      meta: {
        title: '问题记录'
      },
      component: 'ProblemRecord'
    },
    // 合同
    {
      name: 'contractManage',
      parentId: 0,
      id: 600,
      meta: {
        title: '合同',
        icon: 'unordered-list'
      },
      component: 'RouteView'
    },
    {
      name: 'contract-home',
      parentId: 600,
      id: 601,
      meta: {
        title: '合同管理'
      },
      component: 'ContractHome'
    },
    // {
    //   name: 'contract-editItem',
    //   parentId: 600,
    //   id: 602,
    //   meta: {
    //     title: '编辑任务项'
    //   },
    //   component: 'ContractTask'
    // },
    // {
    //   name: 'team-build',
    //   parentId: 600,
    //   id: 603,
    //   meta: {
    //     title: '谈判团队组建'
    //   },
    //   component: 'TeamBuild'
    // },
    // {
    //   name: 'contract-negotiation',
    //   parentId: 600,
    //   id: 604,
    //   meta: {
    //     title: '谈判合同表'
    //   },
    //   component: 'ContractNegotiation'
    // },
    // {
    //   name: 'negotiation-record',
    //   parentId: 600,
    //   id: 605,
    //   meta: {
    //     title: '谈判记录表'
    //   },
    //   component: 'NegotiationRecord'
    // },
    // {
    //   name: 'contract-protocol',
    //   parentId: 600,
    //   id: 606,
    //   meta: {
    //     title: '合同拟制'
    //   },
    //   component: 'ContractProtocol'
    // },
    {
      name: 'contract-template',
      parentId: 600,
      id: 607,
      meta: {
        title: '合同模板'
      },
      component: 'ContractTemplate'
    },
    {
      name: 'contract-system',
      parentId: 600,
      id: 608,
      meta: {
        title: '原合同系统'
      },
      component: 'OldContractSystem'
    }
    // {
    //   name: 'contract-demonstration',
    //   parentId: 600,
    //   id: 608,
    //   meta: {
    //     title: '合同示范文本'
    //   },
    //   component: 'ContractDemonstration'
    // },
    // {
    //   name: 'contract-useSeal',
    //   parentId: 600,
    //   id: 609,
    //   meta: {
    //     title: '合同用印'
    //   },
    //   component: 'ContractUseSeal'
    // },
    // {
    //   name: 'contract-promise',
    //   parentId: 600,
    //   id: 610,
    //   meta: {
    //     title: '合同履约'
    //   },
    //   component: 'ContractPromise'
    // },
    // {
    //   name: 'negotiator-decision',
    //   parentId: 600,
    //   id: 611,
    //   meta: {
    //     title: '谈判事项通报/决策表'
    //   },
    //   component: 'NegotiatorDecision'
    // },
    // {
    //   name: 'contract-entry',
    //   parentId: 600,
    //   id: 612,
    //   meta: {
    //     title: '合同录入'
    //   },
    //   component: 'ContractEntry'
    // },
    // {
    //   name: 'contract-distribution',
    //   parentId: 600,
    //   id: 613,
    //   meta: {
    //     title: '合同分发'
    //   },
    //   component: 'ContractDistribution'
    // },
    // {
    //   name: 'contract-progress',
    //   parentId: 600,
    //   id: 614,
    //   meta: {
    //     title: '进展'
    //   },
    //   component: 'ContractProgress'
    // },
    // {
    //   name: 'contract-assessment',
    //   parentId: 600,
    //   id: 615,
    //   meta: {
    //     title: '合同后评估表'
    //   },
    //   component: 'ContractAssessment'
    // },
    // {
    //   name: 'approval-record',
    //   parentId: 600,
    //   id: 616,
    //   meta: {
    //     title: '审批记录表'
    //   },
    //   component: 'ApprovalRecord'
    // },
    // {
    //   name: 'overallprocess-application',
    //   parentId: 600,
    //   id: 617,
    //   meta: {
    //     title: '流程表单总体实施及应用情况'
    //   },
    //   component: 'OverallprocessApplication'
    // },
    // {
    //   name: 'detailprocess-application',
    //   parentId: 600,
    //   id: 618,
    //   meta: {
    //     title: '流程表单详情实施及应用情况'
    //   },
    //   component: 'DetailprocessApplication'
    // },
    // {
    //   name: 'taskdetail-statistics',
    //   parentId: 600,
    //   id: 619,
    //   meta: {
    //     title: '任务单详情统计'
    //   },
    //   component: 'TaskdetailStatistics'
    // },
    // {
    //   name: 'processApplication-statistics',
    //   parentId: 600,
    //   id: 620,
    //   meta: {
    //     title: '流程单应用情况统计'
    //   },
    //   component: 'ProcessApplicationStatistics'
    // },
    // {
    //   name: 'processApplication-statistics2',
    //   parentId: 600,
    //   id: 621,
    //   meta: {
    //     title: '流程单应用情况统计2'
    //   },
    //   component: 'ProcessApplicationStatistics2'
    // },
    // {
    //   name: 'processApplication-statistics3',
    //   parentId: 600,
    //   id: 621,
    //   meta: {
    //     title: '流程单应用情况统计-按业务域按单位'
    //   },
    //   component: 'ProcessApplicationStatisticByCondition'
    // }
  ]
  const json = builder(nav)
  console.log('json', json)
  return json
}

Mock.mock(/\/user\/getAllUserInfo/, 'get', info)
Mock.mock(/\/user\/nav/, 'get', userNav)
