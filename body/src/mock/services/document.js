import Mock from 'mockjs2'
import { builder, getQueryParameters } from '../util'

const totalCount = 5701

const treeData = (option) => {
  const result1 = [
    {
      id: '000',
      key: 'subject_file',
      title: '课题文件',
      parentId: 0,
      power: 'admin',
      url: 'views/document/SubjectDocument/children/DocumentList',
      slots: {
        icon: 'folder'
      },
      children: [
        {
          id: '001',
          key: 'subject_file|XMJYS',
          title: '项目建议书(PP)',
          parentId: 0,
          power: 'admin',
          url: 'views/document/SubjectDocument/children/DocumentList',
          slots: {
            icon: 'folder'
          }
        },
        {
          id: '002',
          key: 'D|subject_file|RWS',
          title: ' 任务书(AS)',
          parentId: 0,
          power: 'admin',
          url: 'views/document/SubjectDocument/children/DocumentList',
          slots: {
            icon: 'folder'
          }
        },
        {
          id: '003',
          key: 'subject_file|FA',
          title: '  方案(SC)',
          parentId: 0,
          power: 'admin',
          url: 'views/document/SubjectDocument/children/DocumentList',
          slots: {
            icon: 'folder'
          }
        },
        {
          id: '004',
          key: 'subject_file|BG',
          title: '报告(RP)',
          parentId: 0,
          power: 'admin',
          url: 'views/document/SubjectDocument/children/DocumentList',
          slots: {
            icon: 'folder'
          }
        }
      ]
    }
  ]
  const result2 = [
    {
      id: '000',
      key: 'subject_file',
      title: '标准',
      parentId: 0,
      power: 'admin',
      url: 'views/document/SubjectDocument/children/DocumentList',
      slots: {
        icon: 'folder'
      },
      children: [
        {
          id: '001',
          key: 'subject_file|XMJYS',
          title: '外部标准',
          parentId: 0,
          power: 'admin',
          url: 'views/document/ExternalStandard/children/ExternalStandardList',
          slots: {
            icon: 'folder'
          },
          children: [
            {
              id: '001',
              key: 'subject_file|GJBZ',
              title: '国家标准',
              parentId: 0,
              power: 'admin',
              url: 'views/document/ExternalStandard/children/ExternalStandardList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '001',
              key: 'subject_file|HYBZ',
              title: '行业标准',
              parentId: 0,
              power: 'admin',
              url: 'views/document/ExternalStandard/children/ExternalStandardList',
              slots: {
                icon: 'folder'
              }
            }
          ]
        },
        {
          id: '002',
          key: 'subject_file|RWS',
          title: '公司标准',
          parentId: 0,
          power: 'admin',
          url: 'views/document/CompanyStandard/children/CompanyStandardParentList',
          slots: {
            icon: 'folder'
          },
          children: [
            {
              id: '006',
              key: 'subject_file|JCBZ',
              title: '基础标准（FD）',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CompanyStandard/children/CompanyStandardList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '007',
              key: 'subject_file|JSBZ',
              title: '技术标准',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CompanyStandard/children/CompanyStandardParentList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '008',
              key: 'subject_file|GLBZ',
              title: '管理标准（MS）',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CompanyStandard/children/CompanyStandardList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '009',
              key: 'subject_file|GZBZ',
              title: '工作标准（WS）',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CompanyStandard/children/CompanyStandardList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '010',
              key: 'subject_file|QT',
              title: '其他',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CompanyStandard/children/CompanyStandardList',
              slots: {
                icon: 'folder'
              }
            }
          ]
        },
        {
          id: '003',
          key: 'subject_file|FA',
          title: '公司规范',
          parentId: 0,
          power: 'admin',
          url: 'views/document/SubjectDocument/children/DocumentList',
          slots: {
            icon: 'folder'
          }
        },
        {
          id: '004',
          key: 'D|common_file',
          title: '通用文件',
          parentId: 0,
          power: 'admin',
          url: 'views/document/CommonDocument/children/CommonDocumentList',
          slots: {
            icon: 'folder'
          },
          children: [
            {
              id: '005',
              key: 'D|common_file|M',
              title: '管理手册（M）',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CommonDocument/children/CommonDocumentChildrenList',
              slots: {
                icon: 'folder'
              }
            },
            {
              id: '011',
              key: 'D|common_file|QD',
              title: '过程定义文件（QD）',
              parentId: 0,
              power: 'admin',
              url: 'views/document/CommonDocument/children/CommonDocumentChildrenList',
              slots: {
                icon: 'folder'
              }
            }
          ]
        }
      ]
    }
  ]
  const result3 = [
    {
      id: '000',
      key: 'subject_file',
      title: 'E01需求工程',
      parentId: 0,
      power: 'admin',
      url: 'views/document/StandardSystem/children/StandardMainList',
      slots: {
        icon: 'folder'
      },
      children: [
        {
          id: '001',
          key: 'subject_file|XMJYS',
          title: 'E01需求工程',
          parentId: 0,
          power: 'admin',
          url: 'views/document/StandardSystem/children/StandardSystemList',
          slots: {
            icon: 'folder'
          }
        }
          ]
        }
      ]
  console.log('option', option)
  const result = option.body === '1' ? result2 : option.body === '3' ? result3 : result1
  return builder({
    data: result
  })
}

const childrenData = () => {
  const result = []
  return builder({
    data: result
  })
}

// 公司标准表格数据
const getCompanyStandardData = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.current)
  const pageSize = parseInt(parameters.size)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      standardName: '固定资产分类与代码',
      standardVersion: 'Q/C IT 0180-2012',
      standardStatus: '现行',
      releaseDate: '2012-10-03',
      substituteStandard: ''
    },
    {
      key: tmpKey + 1,
      id: tmpKey + 1,
      rowKey: tmpKey + 1,
      standardName: '民用飞机地面支援设备需求分析规范',
      standardVersion: 'Q/C CE 0140-2012',
      standardStatus: '现行',
      releaseDate: '2012-05-26',
      substituteStandard: 'Q/C AM 0015-2012'
    })
    i += 1
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

// 公司标准表格数据
const getExternalStandardData = (options) => {
  // eslint-disable-next-line no-unused-vars
  const parameters = getQueryParameters(options)
  console.log('options', options, parameters)

  const result = []
  const pageNum = parseInt(parameters.current)
  const pageSize = parseInt(parameters.size)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNum - 1) * pageSize
  const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      key: tmpKey,
      id: tmpKey,
      rowKey: tmpKey,
      standardName: 'Condition monitoring and diagnostics of maching',
      standardVersion: 'ISO 17359-2011',
      standardStatus: '现行',
      releaseDate: '2012-10-03',
      substituteStandard: ''
    },
    {
      key: tmpKey + 1,
      id: tmpKey + 1,
      rowKey: tmpKey + 1,
      standardName: 'Aircraft--Ground support electrical supplies',
      standardVersion: 'ISO 6858-1982',
      standardStatus: '现行',
      releaseDate: '2012-05-26',
      substituteStandard: '23'
    })
    i += 1
  }

  return builder({
    pageSize: pageSize,
    pageNum: pageNum,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

Mock.mock(/\/documents\/getTreeData/, 'get', treeData)
Mock.mock(/\/documents\/getChildrenData/, 'get', childrenData)
Mock.mock(/\/documents\/getCompanyStandardData/, 'get', getCompanyStandardData)
Mock.mock(/\/documents\/getExternalStandardData/, 'get', getExternalStandardData)
