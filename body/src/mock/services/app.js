import Mock from 'mockjs2'
import { builder, getQueryParameters } from '../util'

const userTree = () => {
  const data = [
    {
      name: '中国商飞',
      id: '0-0',
      children: [
        {
          name: '中国商用飞机有限责任公司',
          id: '0-0-0',
          children: [
            { name: '0-0-0-0', id: '0-0-0-0', isLeaf: true },
            { name: '0-0-0-1', id: '0-0-0-1', isLeaf: true },
            { name: '0-0-0-2', id: '0-0-0-2', isLeaf: true }
          ]
        }
      ]
    },
    {
      name: '上海飞机设计研究院',
      id: '0-1',
      children: [
        { name: '0-1-0-0', id: '0-1-0-0', isLeaf: true },
        { name: '0-1-0-1', id: '0-1-0-1' },
        { name: '0-1-0-2', id: '0-1-0-2' }
      ]
    },
    {
      name: '0-2',
      id: '0-2',
      isLeaf: true
    }
  ]
  return builder({
    data
  })
}

const recommandUser = () => {
  const data = [
    // {
    //   name: '平台管理员',
    //   id: 'pt'
    // },
    // {
    //   name: '123管理员',
    //   id: '123pt'
    // }
  ]

  return builder({
    data
  })
}

const recommandDe = () => {
  const data = [
    // {
    //   name: 'aa部门',
    //   id: 'pt'
    // },
    // {
    //   name: 'bb部门',
    //   id: '123pt'
    // }
  ]

  return builder({
    data
  })
}

const singleFileUpload = () => {
  const random = Math.random()
  console.log('random', random)
  if (random > 0.3 && random <= 0.7) {
    return builder({
      success: true,
      data: [{
        id: '001',
        name: 'xxxx'
      },
      {
        id: '002',
        name: 'yyyy'
      }]
    })
  } else if (random <= 0.3) {
    return builder({
      success: false,
      data: null
    })
  } else if (random > 0.7) {
    return builder({
      success: false,
      data: [
        '第一行编号错误，编号错误原因：这是一个错误',
        '第6行编号错误，编号错误原因：这是一个错误',
        '第236行编号错误，编号错误原因：这是一个错误，真的是一个错误怎么怎么样',
        '第2386行编号错误，编号错误原因：这是一个错误，真的是一个错误怎么怎么样'
      ]
    })
  }
}

const departmentList = () => {
  const data = [
    {
      name: '中国商飞',
      id: 'pt'
    },
    {
      name: '局方',
      id: '123pt'
    }
  ]

  return builder({
    data
  })
}

const roleList = (options) => {
 const totalCount = 1024
 const parameters = getQueryParameters(options)
 const result = []
 const pageNum = parseInt(parameters.pageNum)
 const pageSize = parseInt(parameters.pageSize)
 const totalPage = Math.ceil(totalCount / pageSize)
 const key = (pageNum - 1) * pageSize
 const next = (pageNum >= totalPage ? (totalCount % pageSize) : pageSize) + 1
 for (let i = 1; i < next; i++) {
   const tmpKey = key + i
   result.push({
     key: tmpKey,
     id: tmpKey,
     no: 'No ' + tmpKey,
     roleName: tmpKey + ' Name',
     roleDesc: tmpKey + ' Desc'
   })
 }

 return builder({
   pageSize: pageSize,
   pageNum: pageNum,
   totalCount: totalCount,
   totalPage: totalPage,
   data: result
 })
}

const errorDetailDowloadUrl = () => {
  return builder({
    success: true
  })
}

const getProcessUserData = () => {
  const treeData = []
  const listData = []

  const autoSelect = true
  const userSelect = true
  const userListType = 'tree'

  const data = {
    autoSelect, // 是否自动选择 0 不允许 1 允许
    userSelect, // 是否允许用户选择 0 不允许 1 允许
    userListType, // 待选择列表展现形式 树 tree 列表 list
    listData,
    treeData
  }

  return builder({
    data
  })
}

const reviewersSubmit = () => {
  return builder({
    success: true
  })
}

const optionList = [
  {
    url: '/user/userTree',
    method: 'get',
    dataBuilder: userTree
  },
  {
    url: '/user/recommandUser/list',
    method: 'get',
    dataBuilder: recommandUser
  },
  {
    url: '/department/departmentTree',
    method: 'get',
    dataBuilder: userTree
  },
  {
    url: '/user/recommandDepartment/list',
    method: 'get',
    dataBuilder: recommandDe
  },
  {
    url: '/api/upload',
    method: 'post',
    dataBuilder: singleFileUpload
  },
  {
    url: '/api/errorDetailDowloadUrl',
    method: 'get',
    dataBuilder: errorDetailDowloadUrl
  },
  {
    url: '/api/dowloadTempUrl',
    method: 'get',
    dataBuilder: errorDetailDowloadUrl
  },
  {
    url: '/api/fileSubmit',
    method: 'get',
    dataBuilder: errorDetailDowloadUrl
  },
  {
    url: '/api/department',
    method: 'get',
    dataBuilder: departmentList
  },
  {
    url: '/api/roleList',
    method: 'post',
    dataBuilder: roleList
  },
  {
    url: '/api/process/reviewers',
    method: 'get',
    dataBuilder: getProcessUserData
  },
  {
    url: '/api/process/reviewers/submit',
    method: 'post',
    dataBuilder: reviewersSubmit
  }
]

optionList.forEach(option => {
  Mock.mock(RegExp(option.url + '.*'), option.method, option.dataBuilder)
})
