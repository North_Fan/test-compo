import Mock from 'mockjs2'
import {
  builder
} from '../util'

const getTableData = (options) => {
  const tableData = [
    {
      serialNumber: 21954,
      number: 'COMAC-SHGS-2092034',
      name: '合同书',
      category: '一般合同',
      undertaker: '冯士德',
      undertakerDepartment: '信息化中心',
      type: '委托',
      otherParty: '北京民用飞机技术研究中心',
      otherPartySupplement: '金航数码',
      direction: '收款',
      amountOfMoney: 'RMB:300.000.0',
      state: '已用印',
      signingTime: '2021-03-03'
    },
    {
      serialNumber: 21955,
      number: 'COMAC-SHGS-2092034',
      name: '合同书',
      category: '一般合同',
      undertaker: '冯士德',
      undertakerDepartment: '信息化中心',
      type: '委托',
      otherParty: '北京民用飞机技术研究中心',
      otherPartySupplement: '金航数码',
      direction: '收款',
      amountOfMoney: 'RMB:300.000.0',
      state: '已用印',
      signingTime: '2021-03-03'
    },
    {
      serialNumber: 21956,
      number: 'COMAC-SHGS-2092034',
      name: '合同书',
      category: '一般合同',
      undertaker: '冯士德',
      undertakerDepartment: '信息化中心',
      type: '委托',
      otherParty: '北京民用飞机技术研究中心',
      otherPartySupplement: '金航数码',
      direction: '收款',
      amountOfMoney: 'RMB:300.000.0',
      state: '已用印',
      signingTime: '2021-03-03'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getTaskList = (options) => {
  const TaskList = [
    {
      serialNumber: 1,
      number: 'PMDP21044540',
      name: 'CMQ0810001',
      personLiable: '张三',
      subjectOfResponsibility: 'CMOS团队',
      hierarchy: '总部部门',
      startTime: '2020-2-2',
      endTime: '2020-3-3',
      preparedCompany: '',
      preparedPerson: '',
      planType: '',
      taskOrderNature: '',
      describe: ''
    },
    {
      serialNumber: 1,
      number: 'PMDP21044540',
      name: 'CMQ0810001',
      personLiable: '李四',
      subjectOfResponsibility: '信息化中心',
      hierarchy: '总部部门',
      startTime: '2020-2-7',
      endTime: '2020-4-4',
      preparedCompany: '',
      preparedPerson: '',
      planType: '',
      taskOrderNature: '',
      describe: ''
    }
  ]

  return builder({
    data: TaskList,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getTreeData = (options) => {
  const treeData = [
    {
      title: '合同管理',
      key: '0',
      slots: {
        icon: 'folder'
      },
      children: [
        {
          title: '中国商用飞机有限责任公司',
          key: '0-0',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        },
        {
          title: '上海飞机设计研究院',
          key: '0-1',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        },
        {
          title: '上海飞机制造有限公司',
          key: '0-2',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        }
      ]
    }
  ]

  return builder({
    data: treeData
  })
}

const getTypeTreeData = (options) => {
  const treeData = [
    {
      title: '合同管理',
      key: '0',
      slots: {
        icon: 'folder'
      },
      children: [
        {
          title: '采购',
          key: '0-0',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        },
        {
          title: '其他',
          key: '0-1',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        },
        {
          title: '赠与',
          key: '0-2',
          slots: {
            icon: 'file'
          },
          isLeaf: true
        }
      ]
    }
  ]

  return builder({
    data: treeData
  })
}

// const getContractTemplateList = (options) => {
//   const templateList = [
//     { id: 1, name: 'text111' },
//     { id: 2, name: 'shangfei' },
//     { id: 3, name: '合同模板' },
//     { id: 4, name: '模板名称' },
//     { id: 5, name: '项目模板' },
//     { id: 6, name: '好味道合同模板' },
//     { id: 7, name: '新的项目模板' },
//     { id: 8, name: '最终模板' },
//     { id: 9, name: '正是模板' }
//   ]

//   return builder({
//     data: templateList
//   })
// }

const getContractTemplate = (options) => {
  const tableData = [
    {
      templateNo: 'ZZXC0730',
      templateName: 'ZZXC0730',
      remarks: '空文档',
      state: '启用',
      edition: '1',
      id: '001'
    },
    {
      templateNo: 'ceshi1230',
      templateName: 'ceshi1230',
      remarks: 'ceshi1230',
      state: '启用',
      edition: '3',
      id: '002'
    },
    {
      templateNo: '法律部1',
      templateName: '技术服务合同',
      remarks: '测试',
      state: '启用',
      edition: '3',
      id: '003'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getContractDemonstration = (options) => {
  const tableData = [
    {
      id: '001',
      textType: '采购合同001',
      fileName: '学生作品',
      remarks: '空文档',
      state: '启用',
      edition: '1.0',
      creator: '张三',
      createTime: '2020-2-2',
      modifier: '李四',
      modifyTime: '2020-2-2'
    },
    {
      id: '002',
      textType: '采购合同002',
      fileName: '学生作品',
      remarks: '空文档',
      state: '禁用',
      edition: '3.0',
      creator: '张三',
      createTime: '2021-2-2',
      modifier: '李四',
      modifyTime: '2028-2-2'
    },
    {
      id: '003',
      textType: '采购合同003',
      fileName: '学生作品',
      remarks: '空文档',
      state: '禁用',
      edition: '2.0',
      creator: '张三',
      createTime: '2022-2-2',
      modifier: '李四',
      modifyTime: '2023-2-2'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getContractnegotiationRecord = (options) => {
  const tableData = [
    {
      key: 1,
      date: '2021-10-21',
      mention: '部门1',
      type: '谈判事项决策',
      status: '未完成'
    },
    {
      key: 2,
      date: '2021-10-22',
      mention: '部门2',
      type: '谈判事项决策',
      status: '未完成'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getContractProgress = (options) => {
  const tableData = [
    {
      id: 1,
      description: '的撒发地方盛大发的撒发地方盛大发的撒发地方盛大发的撒发地方盛大发',
      unableToComplete: '是',
      reason: '谈判事项决策',
      analysis: '是打发士大夫',
      plan: '法撒旦法',
      completionTime: '第三份',
      publisher: '张三',
      releaseDate: '2020-3-3'
    },
    {
      id: 2,
      description: '吏课升斗积吏课升斗积吏课升斗积吏课升斗积吏课升斗积吏课升斗积',
      unableToComplete: '是',
      reason: '而第三份',
      analysis: '风格的',
      plan: '程序梵蒂冈',
      completionTime: '地方',
      publisher: '李四',
      releaseDate: '2022-3-3'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getApprovalRecord = (options) => {
  const tableData = [
    {
      id: '1',
      activityName: '经办人',
      partiName: '张三',
      department: '流程与IT平台（信息化中心）',
      finishDate: '2021-08-05 15:53:21',
      approveResult: '同意',
      approveRemark: '同意'
    },
    {
      id: '2',
      activityName: '经办人',
      partiName: '张三',
      department: '流程与IT平台（信息化中心）',
      finishDate: '2021-08-05 15:53:21',
      approveResult: '同意',
      approveRemark: '同意'
    },
    {
      id: '3',
      activityName: '经办人',
      partiName: '张三',
      department: '流程与IT平台（信息化中心）',
      finishDate: '2021-08-05 15:53:21',
      approveResult: '同意',
      approveRemark: '同意'
    },
    {
      id: '4',
      activityName: '经办人',
      partiName: '张三',
      department: '流程与IT平台（信息化中心）',
      finishDate: '2021-08-05 15:53:21',
      approveResult: '同意',
      approveRemark: '同意'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}
const getProcessStatisticsList = (options) => {
  const tableData = [
    {
      id: 1,
      description: '的撒发地方盛大发的撒发地方盛大发的撒发地方盛大发的撒发地方盛大发',
      unableToComplete: '43',
      reason: '3',
      analysis: '5'
    },
    {
      id: 2,
      description: '吏课升斗积吏课升斗积吏课升斗积吏课升斗积吏课升斗积吏课升斗积',
      unableToComplete: '0',
      reason: '0',
      analysis: '0'
    }
  ]
  console.log(333333333)

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}
const getProcessStatisticsRecords = (options) => {
  const tableData = [
    {
      id: 1,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户与市场',
      second: '4a',
      unableToComplete: '43',
      reason: '3',
      analysis: '5'
    },
    {
      id: 2,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户与市场',
      second: '4b',
      unableToComplete: '0',
      reason: '0',
      analysis: '0'
    },
    {
      id: 4,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户ceshi',
      second: '4s',
      unableToComplete: '43',
      reason: '3',
      analysis: '5'
    },
    {
      id: 5,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户ceshi',
      second: '4f',
      unableToComplete: '0',
      reason: '0',
      analysis: '0'
    }
  ]
  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}
const getProcessApplicationStatisticByCondition = (options) => {
  const tableData = [
    {
      id: 1,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户与市场',
      second: '4a',
      unableToComplete: '43',
      reason: '3',
      analysis: '5'
    },
    {
      id: 2,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户与市场',
      second: '4b',
      unableToComplete: '0',
      reason: '0',
      analysis: '0'
    },
    {
      id: 4,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户ceshi',
      second: '4s',
      unableToComplete: '43',
      reason: '3',
      analysis: '5'
    },
    {
      id: 5,
      description: '中国商用飞机有限责任公司',
      first: '4管理客户ceshi',
      second: '4f',
      unableToComplete: '0',
      reason: '0',
      analysis: '0'
    }
  ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getTaskdetailStatistics = (options) => {
  const tableData = [
    {
      key: '1',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      task: '23',
      yifabu: '是',
      shenqianzhong: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '2',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      task: '23',
      yifabu: '是',
      shenqianzhong: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '3',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      task: '23',
      yifabu: '是',
      shenqianzhong: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '4',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      task: '23',
      yifabu: '是',
      shenqianzhong: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '5',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      task: '23',
      yifabu: '是',
      shenqianzhong: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    }
    ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getDetailprocessApplicationData = (options) => {
  const tableData = [
    {
      key: '1',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      processForm: '12345678',
      yishishi: '是',
      shishifangshi: '文档化',
      lisanbiaodan: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '2',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      processForm: '12345678',
      yishishi: '是',
      shishifangshi: '文档化',
      lisanbiaodan: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '3',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      processForm: '12345678',
      yishishi: '是',
      shishifangshi: '文档化',
      lisanbiaodan: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '4',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      processForm: '12345678',
      yishishi: '是',
      shishifangshi: '文档化',
      lisanbiaodan: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '5',
      zeroLevelBusies: '管理产品生命周期与项目群',
      oneLevelBusies: '管理项目群',
      processForm: '12345678',
      yishishi: '是',
      shishifangshi: '文档化',
      lisanbiaodan: '否',
      faqishu: '1',
      wanchengshu: '2',
      zongliuzhongzhuan: '3'
    },
    {
      key: '6',
      zeroLevelBusies: '合计',
      oneLevelBusies: '',
      yishishi: '',
      shishifangshi: '',
      lisanbiaodan: '',
      faqishu: '5',
      wanchengshu: '10',
      zongliuzhongzhuan: '15'
    }
    ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

const getOverallprocessApplicationData = (options) => {
  const tableData = [
    {
      key: '1',
      zeroLevelBusies: '管理客户与市场',
      oneLevelBusies: '策划产品与服务',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    },
    {
      key: '2',
      zeroLevelBusies: '管理客户与市场',
      oneLevelBusies: '提供客户解决方案',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    },
    {
      key: '3',
      zeroLevelBusies: '管理客户与市场',
      oneLevelBusies: '管理销售',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    },
    {
      key: '4',
      zeroLevelBusies: '管理客户与市场',
      oneLevelBusies: '业务域',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    },
    {
      key: '5',
      zeroLevelBusies: '管理客户与市场',
      oneLevelBusies: '测试',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    },
    {
      key: '6',
      zeroLevelBusies: '合计',
      oneLevelBusies: '',
      yishenqing: '0',
      wendanghua: '0',
      jiegouhua: '0',
      lisanbiaodan: '0',
      faqishu: '0',
      wanchengshu: '0',
      zongliuzhongzhuan: '0'
    }
    ]

  return builder({
    data: tableData,
    totalCount: 99,
    pageSize: 10,
    pageNum: 1
  })
}

Mock.mock(/\/contract\/getContractTreeData/, 'get', getTreeData)
Mock.mock(/\/contract\/getContractTypeTreeData/, 'get', getTypeTreeData)
Mock.mock(/\/contract\/getContractTableData/, 'get', getTableData)
Mock.mock(/\/contract\/getContractTaskList/, 'get', getTaskList)
// Mock.mock(/\/contract\/getContractTemplateList/, 'post', getContractTemplateList)
Mock.mock(/\/contract\/getContractTemplate/, 'get', getContractTemplate)
Mock.mock(/\/contract\/getContractDemonstration/, 'get', getContractDemonstration)
Mock.mock(/\/contract\/getContractnegotiationRecord/, 'get', getContractnegotiationRecord)
Mock.mock(/\/contract\/getContractProgress/, 'get', getContractProgress)
Mock.mock(/\/contract\/getApprovalRecord/, 'post', getApprovalRecord)
Mock.mock(/\/contract\/getProcessStatisticsList/, 'get', getProcessStatisticsList)
Mock.mock(/\/contract\/getProcessStatisticsRecords/, 'get', getProcessStatisticsRecords)
Mock.mock(/\/contract\/getProcessApplicationStatisticByCondition/, 'get', getProcessApplicationStatisticByCondition)
Mock.mock(/\/contract\/getTaskdetailStatistics/, 'post', getTaskdetailStatistics)
Mock.mock(/\/contract\/getDetailprocessApplicationData/, 'post', getDetailprocessApplicationData)
Mock.mock(/\/contract\/getOverallprocessApplicationData/, 'post', getOverallprocessApplicationData)
