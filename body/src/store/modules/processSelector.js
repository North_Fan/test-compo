const processSelector = {
  state: {
    processNodeList: [],
    processName: '',
    isProcessInstanceId: false,
    isUpdTask: false,
    modelKey: ''
  },

  mutations: {
    SET_PROCESS_NODE: (state, processNodeList) => {
      state.processNodeList = processNodeList
    },
    SET_PROCESS_NAME: (state, processName) => {
      state.processName = processName
    },
    SET_ISPROCESS_INSTANCEID: (state, isProcessInstanceId) => {
      state.isProcessInstanceId = isProcessInstanceId
    }
  }
}

export default processSelector
