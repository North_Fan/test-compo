### AdvTable组件 API

| 属性名称               | 描述                           | 是否必填                            | 属性类型           | 可选值 | 默认值 |
| ---------------------- | ------------------------------ | ----------------------------------- | ------------------ | ---------- | ------ |
| buttonTitle            | 按钮名称                       | 是                                  | String            |              | ""   |
| modelTitle             | 弹窗名称                       | 是                                  | String            |              | ""   |
| size                   | 弹窗规格                       | 否                                  | String            | middle/large | "middle"   |
| dialogStyle            | 配置样式                       | 否                                  | String            |              | ""   |
| dialogBool             | 弹窗显示隐藏                    | 是                                  | Boolean           |              | false |
| footer                 | 页脚按钮显示隐藏                | 否                                  | String           | null/''       | ""    |



### 自定义列布局

```vue
<configDialog 
    @handleOk="handleOk" 
    size="middle" 
    dialogStyle="color:red"
    :dialogBool="bool"
    :footer='null'
    buttonTitle="测试Dialog" 
    modelTitle="测试Dialog" />

<script>
export default {
    data() {
        return {
            bool:false
        }
    },
    methods:{
        handleOk(){
            //点击弹窗确定时，向父组件发送消息
        }
    }
}   
</script>
```

