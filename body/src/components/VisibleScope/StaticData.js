import { findById } from '@/utils/util'
const formItemList = [
  {
    queryId: 'userScope',
    queryName: '人员',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'userScope'
  },
  {
    queryId: 'departScope',
    queryName: '部门',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'departScope'
  },
  {
    queryId: 'roleScope',
    queryName: '角色',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'roleScope'
  }
]

const layout = formItemList.map(item => ({
  col: [24, 24, 24],
  item: findById(item.queryId, formItemList)
}))

const option = {
  layout,
  formItemList
}

export {
  option
}
