## MultiSelection组件

### 属性

| 属性名称       | 描述              | 是否必填 | 属性类型                | 可选值             | 默认值   | 备注 |
| -------------- | ----------------- | -------- | ----------------------- | ------------------ | -------- | ---- |
| v-model(value) | 选中值得value集合 | 是       | Array<String \| Number> |                    |          |      |
| size           | 组件的Size        | 否       | String                  | small,normal,large | normal   |      |
| title          | 选择模态框标题    | 否       | String                  |                    | "选择框" |      |
| checkboxList   | checkbox的集合    | 是       | Array<CheckboxItem>     |                    | []       |      |
| disabled       | 选择禁用          | 否       | Boolean                 |                    | false    |      |
| placeholder    | 占位文字          | 否       | String                  |                    | ""       |      |

#### CheckboxItem

| 属性名称 | 描述     | 是否必填 | 属性类型         | 可选值 | 默认值 | 备注 |
| -------- | -------- | -------- | ---------------- | ------ | ------ | ---- |
| label    | 选项名称 | 是       | String           |        |        |      |
| value    | 选项值   | 是       | String \| Number |        |        |      |



### 事件

| 名称   | 描述               | 参数                                     | 返回值 | 备注 |
| ------ | ------------------ | ---------------------------------------- | ------ | ---- |
| change | 模态框点击确认回调 | Array<String \| Number>(与v-model值相同) |        |      |
|        |                    |                                          |        |      |



#### 示例

```vue
<MultiSelection :checkboxList="checkboxList" v-model="selectValues" placeholder="你好啊"></MultiSelection>

<script>
export default {
    data() {
        return {
            checkboxList: [],
            selectValues: [1]
        }
    },
    methods: {
        getCheckboxList() {
            settimeout(() => {
                this.checkboxList = [
                    {
                        label: '123',
                        value: 0
                    },
                    {
                        label: '1234',
                        value: 1
                    },
                    {
                        label: '1235',
                        value: 2
                    },
                    {
                        label: '12223',
                        value: 3
                    }
                ]
            }, 1000)
        }
    },
    created() {
        this.getCheckboxList()
    }
}
</script>
```

