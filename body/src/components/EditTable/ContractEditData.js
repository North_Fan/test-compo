import { findById } from '@/utils/util'
import moment from 'moment'
const formItemList = [
   {
    queryId: 'TASKCODE',
    queryName: '编号',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'treeaddbusinessType',
    queryName: '名称',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'description',
    queryName: '描述:',
    placeholder: '请输入',
    inputType: 'textarea',
    col: [24, 24, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 }
  },
  {
    queryId: 'CHARGEDEPARTMENTALIAS',
    queryName: '责任主体',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'CHARGEDEPARTMENTALIAS'
  },
  {
    queryId: 'CHARGEDEPARTMENT',
    queryName: '责任人',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'CHARGEDEPARTMENT'
  },
  {
    queryId: 'treeaddbusinessTag',
    queryName: '开始时间',
    placeholder: '请输入',
    inputType: 'date',
    disabledDate (current) {
      return current && current < moment().subtract(1, 'day')
    }
  },
  {
    queryId: 'treeaddbusinessDesc',
    queryName: '应完时间',
    placeholder: '请输入',
    inputType: 'date',
    disabledDate (current, record) {
      return current && current < moment().subtract(1, 'day')
    }
  },
  {
    queryId: 'treeaddowner',
    queryName: '完成形式',
    placeholder: '请输入',
    inputType: 'input'
  },
  {
    queryId: 'editUser',
    queryName: '计划工时',
    placeholder: '请输入',
    inputType: 'input'
  },
  {
    queryId: 'EVALPERS',
    queryName: '评价人',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'EVALPERS'
  },
  {
    queryId: 'ISDECOMPOSE',
    queryName: '是否允许分解',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'ISDECOMPOSE',
    disabledFlag: true
  },
  {
    queryId: 'ISCHANGE',
    queryName: '是否允许调整',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'ISCHANGE'
  },
  {
    queryId: 'title',
    queryName: '',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'title',
    col: [24, 24, 24]
  },
  {
    queryId: 'ASSIDEPT',
    queryName: '协助部门',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'ASSIDEPT'
  },
  {
    queryId: 'TASKTYPE',
    queryName: '任务项类型',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'TASKTYPE'
  },
  {
    queryId: 'treeaddId',
    queryName: '业务编号',
    placeholder: '请输入',
    inputType: 'input'
  },
  {
    queryId: 'VISIBILITYSCOPE',
    queryName: '可见范围',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'VISIBILITYSCOPE'
  },
  {
    queryId: 'FOCUSPERSON',
    queryName: '关注人',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'FOCUSPERSON'
  },
  {
    queryId: 'treeaddI3d',
    queryName: '输入',
    placeholder: '请输入',
    inputType: 'input'
  },
  {
    queryId: 'treeaddws',
    queryName: '输出',
    placeholder: '请输入',
    inputType: 'input'
  },
  {
    queryId: 'treeadd3222',
    queryName: '准出条件',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'treddFix2',
    queryName: '专业',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'tree22addI3d',
    queryName: '成熟度',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'tretyddws',
    queryName: '机型',
    placeholder: '请选择',
    inputType: 'slot',
    slot: 'AIRCRAFTTYPE'
  },
  {
    queryId: 'treeaddI3dee',
    queryName: '架次',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'treeaddwsn',
    queryName: '图号/件号',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'treeadd3sta',
    queryName: '状态',
    placeholder: '请输入',
    inputType: 'select',
    selectCode: 'ITEM_STATE',
    disabledFlag: true
  },
  {
    queryId: 'treddFix2tt',
    queryName: '实际完成时间',
    placeholder: '请输入',
    inputType: 'date',
    disabledFlag: true
  },
  {
    queryId: 'tree22addI3dt',
    queryName: '确认完成时间',
    placeholder: '请输入',
    inputType: 'date',
    disabledFlag: true
  },
  {
    queryId: 'tretyd44dws',
    queryName: '执行流程',
    placeholder: '请输入',
    inputType: 'input',
    disabledFlag: true
  },
  {
    queryId: 'UPLOADFILE',
    queryName: '附件',
    placeholder: '请输入',
    inputType: 'slot',
    slot: 'UPLOADFILE',
    col: [24, 24, 24]
  }
]
const dynamicAddData = {
    layout: formItemList.map(item => {
      return {
        col: item.col || [8, 12, 24],
        flexLayout: item.flexLayout || false,
        labelCol: item.labelCol,
        wrapperCol: item.wrapperCol,
        item: findById(item.queryId, formItemList)
      }
    }),
  formItemList
}
export default dynamicAddData
