// eslint-disable-next-line
import {
  BasicLayout,
  BlankLayout,
  RouteView as LayoutRouteView
} from '@/layouts'

const RouteView = {
  name: 'RouteView',
  render: h => h(LayoutRouteView)
}

export const asyncRouterMap = [{
  path: '/',
  name: 'index',
  component: BasicLayout,
  meta: {
    keepAlive: true,
    title: 'menu.home'
  },
  redirect: '/task/home-task',
  children: [
    {
      path: '/work-panel',
      name: 'workPanel',
      component: () => import(/* webpackChunkName: "workPanel" */ '@/views/workPanel'),
      meta: {
        keepAlive: true,
        title: '工作台',
        icon: 'unordered-list',
        isScroll: true
      }
    },
    {
      path: '/processs',
      name: 'processs',
      redirect: '/processs/process-management',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.processs',
        icon: 'unordered-list',
        permission: ['processs']
      },
      children: [
        {
          path: '/processs/process-management',
          name: 'ProcessManagement',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/processManagement'),
          meta: {
            title: 'menu.processs.process-management',
            keepAlive: true,
            permission: ['process-management']
          }
        },
        {
          path: '/processs/pi-management',
          name: 'PiManagement',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/PiManagement'),
          meta: {
            title: 'menu.processs.pi-management',
            keepAlive: true,
            permission: ['pi-management']
          }
        },
        {
          path: '/processs/initiating-process',
          name: 'InitiatingProcess',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/InitiatingProcess'),
          meta: {
            title: 'menu.processs.initiating-process',
            keepAlive: true,
            permission: ['initiating-process']
          }
        },
        {
          path: '/processs/customer-service',
          name: 'CustomerService',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/CustomerService'),
          meta: {
            title: 'menu.processs.customer-service',
            keepAlive: true,
            permission: ['customer-service']
          }
        },
        {
          path: '/processs/CMSC',
          name: 'CMSC',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/CMSC'),
          meta: {
            title: 'menu.processs.CMSC',
            keepAlive: true,
            permission: ['CMSC']
          }
        },
        {
          path: '/processs/business-domain',
          name: 'BusinessDomain',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/process/BusinessDomain'),
          meta: {
            title: 'menu.processs.business-domain',
            keepAlive: true,
            permission: ['business-domain']
          }
        },
        {
          path: '/process/task-form',
          name: 'TaskForm',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/processManage/TaskForm'),
          meta: {
            title: 'router.processManage.TaskForm',
            keepAlive: true,
            permission: ['task-form']
          }
        },
        {
          path: '/process/dynamic-form',
          name: 'DynamicForm',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/processManage/DynamicForm'),
          meta: {
            title: 'router.processManage.DynamicForm',
            keepAlive: true,
            permission: ['dynamic-form']
          }
        },
        {
          path: '/process/table-form',
          name: 'TableForm',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/processManage/TableForm'),
          meta: {
            title: 'router.processManage.TableForm',
            keepAlive: true,
            permission: ['table-form']
          }
        },
        {
          path: '/process/process-designer',
          name: 'ProcessDesigner',
          component: () => import(/* webpackChunkName: "processManage" */ '@/views/processManage/ProcessDesigner'),
          meta: {
            title: 'router.processManage.ProcessDesigner',
            keepAlive: true,
            permission: ['process-designer']
          }
        }
      ]
    },
    {
      path: '/task',
      name: 'task',
      redirect: '/task/task-list',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.task',
        icon: 'unordered-list',
        permission: ['task']
      },
      children: [{
        path: '/task/task-list',
        name: 'TaskList',
        component: () => import(/* webpackChunkName: "task" */ '@/views/task/taskList'),
        meta: {
          title: 'router.task.TaskList',
          keepAlive: true,
          permission: ['task-list']
        }
      },
      {
        path: '/task/task-add',
        name: 'TaskAdd',
        component: () => import(/* webpackChunkName: "task" */ '@/views/task/taskAdd'),
        meta: {
          title: 'router.task.TaskAdd',
          keepAlive: true,
          permission: ['task-add']
        }
      },
      {
        path: '/task/home-task',
        name: 'HomeTask',
        component: () => import(/* webpackChunkName: "task" */ '@/views/task/homeTask'),
        meta: {
          title: 'router.task.HomeTask',
          keepAlive: true,
          permission: ['home-task']
        }
      },
      {
        path: '/task/to-do-list',
        name: 'ToDoList',
        component: () => import(/* webpackChunkName: "task" */ '@/views/task/ToDoList'),
        meta: {
          title: 'router.task.ToDoList',
          keepAlive: true,
          permission: ['to-do-list']
        }
      },
      {
        path: '/task/remote-computer-control',
        name: 'ToDoList',
        component: () => import(/* webpackChunkName: "task" */ '@/views/task/RemoteComputerControl'),
        meta: {
          title: 'router.task.RemoteComputerControl',
          keepAlive: true,
          permission: ['remote-computer-control']
        }
      },
      {
        path: '/task/dailyLog',
        name: 'Daily',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/user/DailyLog'),
        meta: {
          title: 'router.task.DailyLog',
          keepAlive: true,
          permission: ['daily'],
          hiddenHeaderContent: true
        }
      },
      {
        path: '/task/ChangeDepartment',
        name: 'changeDepartment',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/user/UserInfo/changeDepartment'),
        meta: {
          title: 'router.task.ChangeDepartment',
          keepAlive: true,
          permission: ['changeDepartment'],
          hiddenHeaderContent: true
        }
      },
      {
        path: '/task/SetUp',
        name: 'setUp',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/user/UserInfo/SetUp'),
        meta: {
          title: 'router.task.SetUp',
          keepAlive: true,
          permission: ['setUp'],
          hiddenHeaderContent: true
        }
      }
      ]
    },
    {
        path: '/target',
        name: 'target',
        redirect: '/target/demand-management',
        component: RouteView,
        meta: {
          keepAlive: true,
          title: 'menu.target',
          icon: 'unordered-list',
          permission: ['target']
        },
        children: [
          {
            path: '/target/demand-management',
            name: 'DemandManagement',
            component: () =>
              import(/* webpackChunkName: "documentManagement" */ '@/views/target/DemandManagement/index.vue'),
            meta: {
              title: 'menu.target.demand-management',
              keepAlive: true,
              permission: ['demand-management']
            }
          },
          {
            path: '/target/order-management',
            name: 'OrderManagement',
            component: () =>
              import(/* webpackChunkName: "documentManagement" */ '@/views/target/OrderManagement/index.vue'),
            meta: {
              title: 'menu.target.order-management',
              keepAlive: true,
              permission: ['order-management']
            }
          },
          /* 刷新 */
          {
            path: '/target/aplan-management',
            name: 'APlanManagement',
            component: () =>
              import(/* webpackChunkName: "documentManagement" */ '@/views/target/OnePlan/OnePlanManage.vue'),
            meta: {
              title: 'menu.target.aplan-management',
              keepAlive: true,
              permission: ['aplan-management']
            }
          },
          {
            path: '/target/netpaln-management',
            name: 'NetPlanManagement',
            component: () =>
              import(/* webpackChunkName: "documentManagement" */ '@/views/target/NetworkPlan/NetworkPlanManage.vue'),
            meta: {
              title: 'menu.target.netplan-management',
              keepAlive: true,
              permission: ['netplan-management']
            }
          }
        ]
    },
    // plans
    {
      path: '/plans',
      name: 'plans',
      redirect: '/plans/classification-management',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.plans',
        icon: 'unordered-list',
        permission: ['plans']
      },
      children: [{
        path: '/plans/classification-management',
        name: 'ClassificationManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ClassificationManagement'),
        meta: {
          title: 'menu.plans.classification-management',
          keepAlive: true,
          permission: ['classification-management']
        }
      },
      {
        path: '/plans/planning-management',
        name: 'PlanningManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/PlanningManagement'),
        meta: {
          title: 'menu.plans.planning-management',
          keepAlive: true,
          permission: ['planning-management']
        }
      },
      {
        path: '/plans/planning-management/model-access-control-management',
        name: 'ModelAccessControlManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/PlanningManagement/ModelAccessControlManagement/index.vue'),
        meta: {
          title: 'menu.plans.model-access-control-management',
          keepAlive: true,
          permission: ['model-access-control-management']
        }
      },
      {
        path: '/plans/model-project-management',
        name: 'ModelPlanList',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ModelPlanList'),
        meta: {
          title: 'menu.plans.model-project-management',
          keepAlive: true,
          permission: ['model-project-management']
        }
      },
      {
        path: '/plans/task-management',
        name: 'TaskManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/TaskManagement'),
        meta: {
          title: 'menu.plans.task-management',
          keepAlive: true,
          permission: ['task-management']
        }
      },
      {
        path: '/plans/my-mission',
        name: 'MyMission',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/MyMission'),
        meta: {
          title: 'menu.plans.my-mission',
          keepAlive: true,
          permission: ['my-mission']
        }
      },
      {
        path: '/plans/one-on-one-management',
        name: 'OneOnOneManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/OneOnOneManagement'),
        meta: {
          title: 'menu.plans.one-on-one-management',
          keepAlive: true,
          permission: ['one-on-one-management']
        }
      },
      {
        path: '/plans/one-on-one-management-template',
        name: 'OneOnOneManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/OneOnOneManagement/Template'),
        meta: {
          title: 'menu.plans.one-on-one-management-template',
          keepAlive: true,
          permission: ['one-on-one-management-template']
        }
      },
      {
        path: '/plans/annual-target',
        name: 'AnnualTarget',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/AnnualTarget'),
        meta: {
          title: 'menu.plans.annual-target',
          keepAlive: true,
          permission: ['annual-target']
        }
      },
      {
        path: '/plans/WBS',
        name: 'WBS',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WBS'),
        meta: {
          title: 'menu.plans.WBS',
          keepAlive: true,
          permission: ['WBS']
        }
      },
      {
        path: '/plans/chief-engineer-conference-system',
        name: 'ChiefEngineerConferenceSystem',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ChiefEngineerConSys'),
        meta: {
          title: '总师会议系统',
          keepAlive: true,
          permission: ['chief-engineer-conference-system']
        }
      },
      {
        path: '/plans/issue-management',
        name: 'IssueManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ChiefEngineerConSys/IssueManage'),
        meta: {
          title: '议题管理',
          keepAlive: true,
          permission: ['issue-management']
        }
      },
      {
        path: '/plans/initiating-minutes-sheet',
        name: 'InitiatingMinutesSheet',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ChiefEngineerConSys/InitMinSheet'),
        meta: {
          title: '总师会议纪要',
          keepAlive: true,
          permission: ['initiating-minutes-sheet']
        }
      },
      {
        path: '/plans/add-result-page/:sysType/:userCode/:docId',
        name: 'AddResultPage',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ModelPlanList/AddResultPage'),
        meta: {
          title: '添加结果',
          keepAlive: true,
          permission: ['add-result-page']
        }
      },
      {
        path: '/plans/view-result-page/:docId',
        name: 'ViewResultPage',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ModelPlanList/ViewResultPage'),
        meta: {
          title: '查询结果',
          keepAlive: true,
          permission: ['view-result-page']
        }
      },
      {
        path: '/task/homeTask/my-reading',
        name: 'ViewResultPage',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/task/homeTask/MyReading'),
        meta: {
          title: '我的消息',
          keepAlive: true,
          permission: ['my-reading']
        }
      },
      {
        path: '/plans/working-hours-management',
        redirect: '/plans/working-hours-management/working-hours',
        name: 'WorkingHoursManagement',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WorkingHoursManagement'),
        meta: {
          title: 'menu.plans.working-hours-management',
          keepAlive: true,
          permission: ['working-hours-management']
        },
        children: [
          {
            path: '/plans/working-hours-management/working-hours',
            name: 'WorkingHours',
            component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WorkingHoursManagement/WorkingHoursFrontendPage'),
            meta: {
              title: 'menu.plans.working-hours',
              keepAlive: true,
              permission: ['working-hours']
            }
          },
          {
            path: '/plans/working-hours-management/team-woring-hours',
            name: 'TeamWoringHours',
            component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WorkingHoursManagement/TeamWoringHours'),
            meta: {
              title: 'menu.plans.team-woring-hours',
              keepAlive: true,
              permission: ['team-woring-hours']
            }
          },
          {
            path: '/plans/working-hours-management/report-detaile',
            name: 'ReportDetaile',
            component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WorkingHoursManagement/ReportDetaile'),
            meta: {
              title: 'menu.plans.report-detaile',
              keepAlive: true,
              permission: ['report-detaile']
            }
          },
          {
            path: '/plans/working-hours-management/report-earned',
            name: 'ReportEarned',
            component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/WorkingHoursManagement/ReportEarned/index.vue'),
            meta: {
              title: 'menu.plans.report-earned',
              keepAlive: true,
              permission: ['report-earned']
            }
          },
          {
            path: '/plans/working-hours-management/working-hours-frontend-page',
            name: 'WorkingHoursFrontendPage',
            component: () => import(/* webpackChunkName: "WorkingHoursFrontendPage" */ '@/views/plans/WorkingHoursManagement/WorkingHours'),
            meta: {
              title: 'menu.plans.working-hours',
              keepAlive: true,
              permission: ['working-hours-frontend-page'],
              hiddenHeaderContent: true
            }
          }
        ]
      },
      {
        path: '/plans/todo',
        name: 'Todo',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/Todo'),
        meta: {
          title: 'menu.plans.Todo',
          keepAlive: true,
          permission: ['todo'],
          hiddenHeaderContent: true
        }
      },
      {
        path: '/plans/toread',
        name: 'ToRead',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/ToRead'),
        meta: {
          title: 'menu.plans.ToRead',
          keepAlive: true,
          permission: ['toread'],
          hiddenHeaderContent: true
        }
      }
      ]
    },
    {
      path: '/documents',
      name: 'documents',
      redirect: '/documents/type-management',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.documents',
        icon: 'unordered-list',
        permission: ['documents']
      },
      children: [{
        path: '/documents/type-management',
        name: 'TypeManagement',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/TypeManagement'),
        meta: {
          title: 'menu.documents.type-management',
          keepAlive: true,
          permission: ['type-management']
        }
      }, {
        path: '/documents/subject-no',
        name: 'SubjectNo',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/SubjectNo'),
        meta: {
          title: 'menu.documents.subject-no',
          keepAlive: true,
          permission: ['subject-no']
        }
      }, {
        path: '/documents/subject-document',
        name: 'SubjectDocument',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/SubjectDocument'),
        meta: {
          title: 'menu.documents.subject-document',
          keepAlive: true,
          permission: ['subject-document']
        }
      }, {
        path: '/documents/document-list',
        name: 'DocumentList',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/DocumentList'),
        meta: {
          title: 'menu.documents.document-list',
          keepAlive: true,
          permission: ['document-list']
        }
      }, {
        path: '/documents/document-search',
        name: 'DocumentSearch',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/DocumentSearch'),
        meta: {
          title: 'menu.documents.document-search',
          keepAlive: true,
          permission: ['document-search']
        }
      }, {
        path: '/documents/standard-system',
        name: 'StandardSystem',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/StandardSystem'),
        meta: {
          title: 'menu.documents.standard-system',
          keepAlive: true,
          permission: ['standard-system']
        }
      }, {
        path: '/documents/standard-type-management',
        name: 'StandardTypeManagement',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/StandardTypeManagement'),
        meta: {
          title: 'menu.documents.standard-type-management',
          keepAlive: true,
          permission: ['standard-type-management']
        }
      },
      {
        path: '/documents/professional-activities',
        name: 'ProfessionalActivities',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/CommonDocument/CommonDocumentChildrenPop/ProfessionalActivities'),
        meta: {
          title: 'menu.documents.professional-activities',
          keepAlive: true,
          permission: ['professional-activities']
        }
      }, {
        path: '/document/help-document',
        name: 'HelpDocument',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/HelpDocument'),
        meta: {
          title: 'menu.document.help-document',
          keepAlive: true,
          permission: ['help-document']
        }
      }, {
        path: '/documents/standard-data-base',
        name: 'StandardDataBase',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/StandardDataBase'),
        meta: {
          title: 'menu.documents.standard-data-base',
          keepAlive: true,
          permission: ['standard-data-base']
        }
      },
      {
        path: '/documents/official-form-system',
        name: 'OfficialFormSystem',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/OfficialFormSystem/children/OfficialFormSystemList'),
        meta: {
          title: 'menu.documents.official-form-system',
          keepAlive: true,
          permission: ['official-form-system']
        }
      }, {
        path: '/documents/maintenance-tools',
        name: 'DocumentMaintenanceTools',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/DocumentMaintenanceTools/DocumentMaintenanceTools'),
        meta: {
          title: 'menu.documents.maintenance-tools',
          keepAlive: true,
          permission: ['maintenance-tools']
        }
      }, {
        // 受控表格发放工具
        path: '/documents/samc-tool',
        name: 'DocumentSAMCTool',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/document/SAMCTool'),
        meta: {
          title: 'menu.documents.samc-tool',
          keepAlive: true,
          permission: ['maintenance-tools']
        }
      }
      ]
    },
    {
      path: '/problem',
      name: 'problem',
      redirect: '/problem/problem-home',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.problem',
        icon: 'unordered-list',
        permission: ['problem']
      },
      children: [{
        path: '/problem/problem-home',
        name: 'ProblemHome',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemHome'),
        meta: {
          title: 'menu.problem.problem-home',
          keepAlive: true,
          permission: ['problem-home']
        }
      }, {
        path: '/problem/qzero-task',
        name: 'PzeroTask',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/QzeroMain/ToDoAndToRead'),
        meta: {
          title: 'menu.problem.qzero-task',
          keepAlive: true,
          permission: ['qzero-task']
        }
      }, {
        path: '/problem/problem-control',
        name: 'ProblemControl',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemControl'),
        meta: {
          title: 'menu.problem.problem-control',
          keepAlive: true,
          permission: ['problem-control']
        }
      }, {
        path: '/problem/qzero-main',
        name: 'ProblemQzeroMain',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/QzeroMain'),
        meta: {
          title: 'menu.problem.qzero-main',
          keepAlive: true,
          permission: ['problem-qzero-main']
        }
        },
      {
        path: '/problem/problem-examine',
        name: 'ProblemExamine',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/AuditPlan'),
        meta: {
          title: 'menu.problem.problem-examine',
          keepAlive: true,
          permission: ['problem-examine']
        }
      },
      {
        path: '/problem/product-failure-report',
        name: 'ProblemProductFailureReport',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemProductFailureReport'),
        meta: {
          title: 'menu.problem.product-failure-report',
          keepAlive: true,
          permission: ['problem-failure-report']
        }
      }, {
        path: '/problem/audit-plan-report',
        name: 'ProblemAuditPlanReport',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemAuditPlanReport'),
        meta: {
          title: 'menu.problem.audit-plan-report',
          keepAlive: true,
          permission: ['audit-plan-report']
        }
        },
    {
        path: '/problem/total-problem-management',
        name: 'ProblemTotalProblemManagement',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/TotalProblemManagement'),
        meta: {
          title: 'menu.problem.total-problem-management',
          keepAlive: true,
          permission: ['problem-total-problem-management']
        }
        },
      {
        path: '/problem/total-problem-task',
        name: 'ProblemTotalProblemTask',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/TotalProblemManagement/HomeToDo'),
        meta: {
          title: 'menu.problem.total-problem-task',
          keepAlive: true,
          permission: ['problem-total-problem-task']
        }
      },
            {
        path: '/problem/sort-management',
        name: 'ProblemSortManagement',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemDict'),
        meta: {
          title: 'menu.problem.sort-management',
          keepAlive: true,
          permission: ['problem-sort-management']
        }
      },
      {
        path: '/problem/product-failure',
        name: 'ProductFailure',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProductFailure'),
        meta: {
          title: 'menu.problem.product-failure',
          keepAlive: true,
          permission: ['problem-failure']
        }
      }, {
        path: '/problem/product-report/problem-audit',
        name: 'ProblemAudit',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemAudit'),
        meta: {
          title: 'menu.problem.product-audit',
          keepAlive: true,
          permission: ['problem-audit']
        }
      }, {
        path: '/problem/problem-dict',
        name: 'ProblemDict',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemDict'),
        meta: {
          title: 'menu.problem.product-dict',
          keepAlive: true,
          permission: ['problem-dict']
        }
      }, {
        path: '/problem/problem-record',
        name: 'ProblemRecord',
        component: () => import(/* webpackChunkName: "problemManagement" */ '@/views/problem/ProblemRecord'),
        meta: {
          title: 'menu.problem.product-record',
          keepAlive: true,
          permission: ['problem-record']
        }
      }]
    },
    {
      path: '/risk',
      name: 'risk',
      redirect: '/risk/home',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.risk',
        icon: 'unordered-list',
        permission: ['risk-home']
      },
      children: [{
        path: '/risk/main-page',
        name: 'RiskResourceList',
        component: () => import(/* webpackChunkName: "documentManagement" */ '@/views/problem/RiskManagement/RiskResourceList/RiskResourceList'),
        meta: {
          title: '风险管理',
          keepAlive: true,
          permission: ['risk-main-page']
        }
      }]
    },
    {
      path: '/contractManage',
      name: 'contractManage',
      redirect: '/contractManage/contract-home',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.contractManage',
        icon: 'unordered-list',
        permission: ['contractManage']
      },
      children: [{
        path: '/contractManage/contract-home',
        name: 'ContractHome',
        component: () => import(/* webpackChunkName: "contractManagement" */ '@/views/contract/ContractHome'),
        meta: {
          title: 'menu.contractManage.contract-home',
          keepAlive: true,
          permission: ['contract-home'],
          params: {
          paramName: '1'
          }
        }
      }, {
        path: '/contractManage/contract-template',
        name: 'ContractTemplate',
        component: () => import(/* webpackChunkName: "contractManagement" */ '@/views/contract/ContractTemplate'),
        meta: {
          title: 'menu.contractManage.contract-template',
          keepAlive: true,
          permission: ['contract-template']
        }
      },
      {
        path: '/contractManage/contract-report',
        name: 'ContractReport',
        component: () => import(/* webpackChunkName: "contractManagement" */ '@/views/contract/ContractReport'),
        meta: {
          title: 'menu.contractManage.contract-report',
          keepAlive: true,
          permission: ['contract-report']
        }
      },
      {
        path: '/contractManage/origin-contract',
        name: 'OriginReport',
        component: () => import(/* webpackChunkName: "contractManagement" */ '@/views/contract/OriginContract'),
        meta: {
          title: 'menu.contractManage.origin-contract',
          keepAlive: true,
          permission: ['origin-contract']
        }
      }, {
        path: '/contractManage/contract-demonstration',
        name: 'ContractDemonstration',
        component: () => import(/* webpackChunkName: "contractManagement" */ '@/views/contract/ContractDemonstration'),
        meta: {
          title: 'menu.contractManage.contract-demonstration',
          keepAlive: true,
          permission: ['contract-demonstration']
        }
      }, {
        path: '/contractManage/contract-promise',
        name: 'ContractPromise',
        component: () => import('@/views/contract/ContractPromise'),
        meta: {
          title: 'menu.contractManage.contract-promise',
          keepAlive: true,
          permission: ['contract-promise']
        }
      }, {
        path: '/contractManage/contract-assessment',
        name: 'ContractAssessment',
        component: () => import('@/views/contract/ContractAssessment'),
        meta: {
          title: 'menu.contractManage.contract-assessment',
          keepAlive: true,
          permission: ['contract-assessment']
        }
      },
      {
        path: '/contractManage/large-screen-display',
        name: 'LargeScreenDisplay',
        component: () => import('@/views/contract/ContractHome'),
        meta: {
          title: 'menu.contractManage.large-screen-display',
          keepAlive: true,
          permission: ['large-screen-display'],
          params: {
          paramName: '0'
          }
        }
      },
      {
        path: '/contractManage/contract-filling',
        name: 'ContractFilling',
        component: () => import('@/views/contract/ContractArchive'),
        meta: {
          title: 'menu.contractManage.contract-filling',
          keepAlive: true,
          permission: ['contract-filling']
        }
      }
    ]
    },
    {
      path: '/systemManage',
      name: 'systemManage',
      redirect: '/system-mgt/DataDict',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.systemManage',
        icon: 'unordered-list',
        permission: ['systemManage']
      },
      children: [{
        path: '/system-mgt/dataDict',
        name: 'systemMgtDataDict',
        component: () => import(/* webpackChunkName: "systemManage" */ '@/views/other/DataDict'),
        meta: {
          title: 'menu.systemManage.data-dict',
          keepAlive: true,
          permission: ['DataDict']
        }
      }]
    },
    {
      path: '/xbom',
      name: 'xbom',
      component: RouteView,
      children: [
        {
          path: '/xbom/xbom-home',
          name: 'XbomHome',
          component: () => import('@/views/xbom/Home/index'),
          meta: {
            title: '主界面',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-change-workable',
          name: 'XbomChangeWorkable',
          component: () => import('@/views/xbom/AlterationWorkable/index'),
          meta: {
            title: '变更落实单',
            keepAlive: true
          }
        },
        //
        {
          path: '/xbom/xbom-ecp-list',
          name: 'XbomEcpList',
          component: () => import('@/views/xbom/AlterationWorkable/ECPTable'),
          meta: {
            title: '工程更改建议',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-matter',
          name: 'XbomMatter',
          component: () => import('@/views/xbom/Matter/index'),
          meta: {
            title: '件号管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-product',
          name: 'XbomProduct',
          component: () => import('@/views/xbom/Product/index'),
          meta: {
            title: '型号管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-primary-node',
          name: 'XbomPrimaryNode',
          component: () => import('@/views/xbom/PrimaryNode/index'),
          meta: {
            title: '元结点',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-manager-structure',
          name: 'XbomManagerStructure',
          component: () => import('@/views/xbom/ManagerStructure/index'),
          meta: {
            title: '管理结构',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-manager-structure-model',
          name: 'XbomManagerStructureModel',
          component: () => import('@/views/xbom/ManagerStructureModel/index'),
          meta: {
            title: '型号管理结构',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-bom-permission-manager',
          name: 'XbomBomPermissionManager',
          component: () => import('@/views/xbom/BomPermissionManager/index'),
          meta: {
            title: 'BOM权限管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-matter-document',
          name: 'XbomMatterDocument',
          component: () => import('@/views/xbom/MatterDocument/index'),
          meta: {
            title: '件号文档管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-digifax-manager',
          name: 'XbomDigifaxManager',
          component: () => import('@/views/xbom/DigifaxManager/index'),
          meta: {
            title: '图号管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-import-sign-off',
          name: 'XbomImportSignOff',
          component: () => import('@/views/xbom/ImportSignOff/index'),
          meta: {
            title: '签审包导入',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-change-workable',
          name: 'XbomChangeWorkable',
          component: () => import('@/views/xbom/AlterationWorkable/index'),
          meta: {
            title: '变更落实单',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-factory',
          name: 'XbomFactory',
          component: () => import('@/views/xbom/Factory/index'),
          meta: {
            title: '工厂/供应商管理',
            keepAlive: true
          }
        },
        {
          path: '/xbom/xbom-my-report',
          name: 'XbomMyReport',
          component: () => import('@/views/xbom/MyReport/index'),
          meta: {
            title: '我的报表',
            keepAlive: true
          }
        }
      ]
    },
    {
      path: '/project',
      name: 'project',
      redirect: '/project/evaluation-model-management',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.project',
        icon: 'unordered-list',
        permission: ['project']
      },
      children: [
        {
          path: '/project/evaluation-model-management',
          name: 'EvaluationModelEvaluation',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/project/EvaluationModelManagement'),
          meta: {
            title: 'menu.project.evaluation-model-management',
            keepAlive: true,
            permission: ['evaluation-model-management']
          }
        },
        {
          path: '/project/evaluation-project-management',
          name: 'evaluationProjectManagement',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/project/EvaluationProjectManagement'),
          meta: {
            title: 'menu.project.evaluation-project-management',
            keepAlive: true,
            permission: ['evaluation-project-management']
          }
        },
        {
          path: '/project/policy-and-regulation',
          name: 'policyAndRegulation',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/process/ComplianceManagement/PolicyAndRegulation/PolicyAndRegulation'),
          meta: {
            title: '政策与法规',
            keepAlive: true,
            permission: ['policy-and-regulation']
          }
        },
        {
          path: '/project/compliance-case',
          name: 'complianceCase',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/process/ComplianceManagement/ComplianceCase/ComplianceCase'),
          meta: {
            title: '合规案例',
            keepAlive: true,
            permission: ['compliance-case']
          }
        },
        {
          path: '/project/emphasis-compliance',
          name: 'emphasisCompliance',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/process/ComplianceManagement/EmphasisCompliance/EmphasisCompliance'),
          meta: {
            title: '重点合规岗位',
            keepAlive: true,
            permission: ['emphasis-compliance']
          }
        },
        {
          path: '/project/compliance-training',
          name: 'complianceTraining',
          component: () => import(/* webpackChunkName: "projectManagement" */ '@/views/process/ComplianceManagement/ComplianceTraining/ComplianceTraining'),
          meta: {
            title: '合规培训',
            keepAlive: true,
            permission: ['compliance-training']
          }
        }
      ]
    },
    {
      path: '/knowledge',
      name: 'knowledge',
      redirect: '/knowledge/kw-home',
      component: RouteView,
      meta: {
        keepAlive: true,
        title: 'menu.knowledge',
        icon: 'unordered-list',
        permission: ['knowledge']
      },
      children: [
        {
          path: '/knowledge/kw-home',
          name: 'knowledgeHome',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/Home'),
          meta: {
            // title: 'menu.knowledge.knowledge-home',
            title: '主界面',
            keepAlive: true,
            permission: ['knowledge-home']
          }
        },
        {
          path: '/knowledge/kw-technical-management',
          name: 'TechnicalManagement',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/TechnicalCaseManagement'),
          meta: {
            // title: 'menu.knowledge.kw-technical-case',
            title: '案例库',
            keepAlive: true,
            permission: ['knowledge-technical-case']
          }
        },
        {
          path: '/knowledge/kw-wall-fame',
          name: 'WallFame',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/WallFame'),
          meta: {
            // title: 'menu.knowledge.kw-technical-case',
            title: '荣誉墙',
            keepAlive: true,
            permission: ['knowledge-wall-fame']
          }
        },
        {
          path: '/knowledge/kw-experience-management',
          name: 'TechnicalManagement',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/ExperienceExtractionManagement'),
          meta: {
            // title: 'menu.knowledge.kw-experience-management',
            title: '最佳实践项（最佳工作法）',
            keepAlive: true,
            permission: ['knowledge-experience-management']
          }
        },
        {
          path: '/knowledge/kw-standard-system',
          name: 'StandardSystemManagement',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/StandardSystemManagement'),
          meta: {
            // title: 'menu.knowledge.kw-experience-management',
            title: '知识体系',
            keepAlive: true,
            permission: ['knowledge-standard-system']
          }
        },
        {
          path: '/knowledge/stakeholder',
          name: 'stakeholder',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/StakeHolder'),
          meta: {
            // title: 'menu.knowledge.kw-experience-management',
            title: '干系人',
            keepAlive: true,
            permission: ['knowledge-standard-management']
          }
        },
        {
          path: '/knowledge/center',
          name: 'personalCenter',
          component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/knowledge/PersonalCenter'),
          meta: {
            title: '个人中心',
            keepAlive: true,
            permission: ['setUp'],
            hiddenHeaderContent: true
          }
        },
        {
          path: '/knowledge/map',
          name: 'knowledgeMap',
          component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/knowledge/KnowledgeMap'),
          meta: {
            title: '岗位知识地图',
            keepAlive: true,
            permission: ['setUp'],
            hiddenHeaderContent: true
          }
        },
        {
          path: '/knowledge/kw-standard-management',
          name: 'StandardSystemObjectManagement',
          component: () => import(/* webpackChunkName: "knowledgeManage" */ '@/views/knowledge/StandardObjectManagement'),
          meta: {
            // title: 'menu.knowledge.kw-experience-management',
            title: '知识体系类型管理',
            keepAlive: true,
            permission: ['knowledge-standard-management']
          }
        }

      ]
    }
  ]
},
{
  path: '/HelpDoc',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/other/HelpDoc')
},
{
  path: '/LargeScreenDisplay',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/other/LargeScreenDisplay')
},
{
  path: '/plans/OneOnOneManagement/Template/BeyondTheBanner',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/plans/OneOnOneManagement/Template/BeyondTheBanner')
},
{
  path: '/LargeScreenDisplayPage',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/contract/LargeScreenDisplay/LargeScreenDisplayPage')
},
{
  path: '*',
  redirect: '/404',
  hidden: true
},
{
  path: '/404',
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
},
{
  path: '/403',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403')
},
{
  path: '/500',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500')
},
{
  path: '/browser-wrong',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/BrowserWrong')
}
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [{
  path: '/user',
  component: BlankLayout,
  redirect: '/user/login',
  hidden: true,
  children: [{
    path: 'login',
    name: 'login',
    component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
  }]
},
{
  path: '/403',
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403')
},
{
  path: '/404',
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
},
{
  path: '/500',
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500')
},
{
  path: '/HelpDoc',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/other/HelpDoc')
},
{
  path: '/LargeScreenDisplay',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/other/LargeScreenDisplay')
},
{
  path: '/browser-wrong',
  hidden: true,
  component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/BrowserWrong')
}
]
