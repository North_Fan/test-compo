### AdvTable组件 API

| 属性名称               | 描述                           | 是否必填                            | 属性类型           | 可选值 | 默认值 |
| ---------------------- | ------------------------------ | ----------------------------------- | ------------------ | ------ | ------ |
| buttonTitle            | 按钮名称                       | 是                                  | String            |        | ""   |
| modelTitle             | 弹窗名称                       | 是                                  | String            |        | ""   |
| groupId                | 后端接口请求路径                | 是                                  | String            |        | ""   |
| tabArray               | 自定义tabs对象集合              | 是                                  | Array<CusColumn>  |        | []   |

### CusColumn属性

| 属性名称        | 描述                                       | 是否必填 | 属性类型   | 可选值               | 默认值 |
| --------------- | ------------------------------------------ | -------- | ---------- | -------------------- | ------ |
| <extend Column> | Ant Design Vue -- Table > Column的所有属性 |          |            |                      |        |
| name            | 用于匹配tab列表的id                        | 是       | Boolean    |                      | null   |
| id              | 对象ID，校验唯一性                       | 是       | String     | input, select, radio |        |



### 自定义列布局

```vue
<configTabs
    @btnClick="showModal"
    :groupId="testId"
    :tabArray="tabArray"
    buttonTitle="配置"
    modelTitle="测试配置"
/>

<script>
export default {
    data() {
        return {
            testId:'111',
            tabArray:[],
        }
    },
    methods:{
        showModal(){
            businessTagList({ id: this.groupId }).then((res) => {
                this.tabArray = res.result.data
            })
        }
    }
}   
</script>
```

