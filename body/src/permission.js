import router from './router'
import store from './store'
import request from './utils/yunqiaoRequest'
import NProgress from 'nprogress' // progress bar
import '@/components/NProgress/nprogress.less' // progress bar custom style
import SubappContainer from '@/layouts/SubappContainer'
import { setDocumentTitle, domTitle } from '@/utils/domUtil'
// import {  ACCESS_TOKEN} from '@/store/mutation-types'
// import { i18nRender } from '@/locales'
// import Vue from 'vue'
NProgress.configure({
  showSpinner: false
}) // NProgress Configuration

// const allowList = ['login'] // no redirect allowList
const loginRoutePath = '/user/login'
const defaultRoutePath = '/control/SQCDP/task/home-task'
// 匹配路由
const matchPath = (path, routers, result = {}) => {
  // console.log('------------matchPath', path, routers, result)
  for (const item of routers) {
    try {
      if (item.path.includes(path)) {
        result.matching = true
      }
    } catch (error) {
      result.matching = false
    }

    if (item.children) {
      matchPath(path, item.children, result)
    }
  }

  return result.matching
}

router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  to.meta && typeof to.meta.title !== 'undefined' && setDocumentTitle(`${to.meta.title} - ${domTitle}`)
  /* has token */
  // 对接后端sso，不在做原来token判断，统一用cookie
  // if (storage.get(ACCESS_TOKEN)) {
  if (to.path === loginRoutePath) {
    next({
      path: defaultRoutePath
    })
    NProgress.done()
  } else {
    console.log(store.getters.roles, '----1343')
    // check login user.roles is null
    if (store.getters.roles.length === 0) {
      // request login userInfo
      // Vue.prototype.$loading.show()
      store
        .dispatch('GetInfo')
        .then((res) => {
          // console.log(res.result.role, '----00')
          // const roles = res.result && res.result.role
          // generate dynamic router
          store
            .dispatch('GenerateRoutes', {
              // roles
            })
            .then(async () => {
              // 根据roles权限生成可访问的路由表
              // 动态添加可访问路由表
              // VueRouter@3.5.0+ New API
              store.getters.addRouters.forEach((r) => {
                router.addRoute(r)
              })
              /* ********************云巧portal*************************** */
              // const siteInfo = getSiteInfo()
              // const menuInfo = getMenuInfo()
              request.get('/site', { maxRedirects: 2 }).then((site) => {
                const { data: siteInfo } = site
                console.log('云巧站点信息1：', siteInfo)
                request.get('/site/menus').then((menu) => {
                  const { data: menuInfo } = menu
                  console.log('云巧菜单信息1：', menuInfo)
                  // Vue.prototype.$loading.hide()
                  const menuAdapter = (menu) => {
                    let ItemChildren = null
                    if (menu.children) {
                      ItemChildren = menu.children.map(menuAdapter)
                    }
                    return {
                      path: menu.path || defaultRoutePath,
                      name: menu.name,
                      meta: {
                        title: menu.name
                      },
                      component: SubappContainer,
                      children: ItemChildren
                    }
                  }
                  menuInfo.forEach((item) => {
                    router.addRoute('index', menuAdapter(item))
                  })
                  // 通配兜底注册子应用路由
                  siteInfo.apps.forEach((item) => {
                    router.addRoute('index', {
                      path: `/${item.appCode}/*`,
                      meta: {
                        title: item.name
                      },
                      component: SubappContainer
                    })
                  })
                  /* ******************************************************* */
                  const routers = store.getters.addRouters[0]
                  const path = to.path
                  const matchAll = matchPath(path, [routers])
                  const matchMenu = matchPath(path, menuInfo)

                  // 白名单路由
                  const others = [
                    {
                      path: '/plans/todo'
                    }
                  ]
                  const matchOthers = matchPath(path, others)

                  // 不在匹配
                  if (!matchMenu && !matchAll && !matchOthers) {
                    next({
                      path: '/403',
                      replace: true
                    })
                  } else {
                    // 请求带有 redirect 重定向时，登录自动重定向到该地址
                    const redirect = decodeURIComponent(from.query.redirect || to.path)
                    if (to.path === redirect) {
                      // set the replace: true so the navigation will not leave a history record
                      next({
                        ...to,
                        replace: true
                      })
                    } else {
                      // 跳转到目的路由
                      next({
                        path: redirect
                      })
                    }
                  }
                })
              })
              // 准确注册menu中的路由（部分layout组件的菜单会根据路由配置生成跳转链接）
            })
        })
        .catch(() => {
          this.$error({
            title: '错误',
            content: '请求用户信息失败，请重试'
          })
          // 失败时，获取用户信息失败时，调用登出，来清空历史保留信息
          store.dispatch('Logout').then(() => {
            next({
              path: loginRoutePath,
              query: {
                redirect: to.fullPath
              }
            })
          })
        })
    } else {
      next()
    }
  }
  // } else {
  //   if (allowList.includes(to.name)) {
  //     // 在免登录名单，直接进入
  //     next()
  //   } else {
  //     next({
  //       path: loginRoutePath,
  //       query: {
  //         redirect: to.fullPath
  //       }
  //     })
  //     NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
  //   }
  // }
})

router.afterEach(() => {
  NProgress.done() // finish progress bar
})
