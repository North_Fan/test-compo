import antdEnUS from 'ant-design-vue/es/locale-provider/en_US'
import momentEU from 'moment/locale/eu'
import global from './en-US/global'

import menu from './en-US/menu'
import setting from './en-US/setting'
import user from './en-US/user'

import dashboard from './en-US/dashboard'
import plans from './en-US/plans/plans'

const components = {
  antLocale: antdEnUS,
  momentName: 'eu',
  momentLocale: momentEU
}

export default {
  message: '-',
  'navBar.lang': 'lang',
  'layouts.usermenu.dialog.title': 'Message',
  'layouts.usermenu.dialog.content': 'Are you sure you would like to logout?',
  'layouts.userLayout.title': 'Ant Design is the most influential web design specification in Xihu district',
  'router.processManage.BusinessDomain': 'BusinessDomain',
  'router.processManage.TaskForm': 'TaskForm',
  'router.processManage.DynamicForm': 'DynamicForm',
  'router.processManage.TableForm': 'TableForm',
  'router.processManage.ProcessDesigner': 'ProcessDesigner',
  'router.task.HomeTask': 'HomeTask',
  'router.task.TaskList': 'TaskList',
  'router.task.TaskAdd': 'TaskAdd',
  'router.task.ToDoList': 'ToDoList',
  'router.task.ChangeDepartment': 'ChangeDepartment',
  'router.task.SetUp': 'SetUp',
  ...components,
  ...global,
  ...menu,
  ...setting,
  ...user,
  ...dashboard,
  ...plans
}
