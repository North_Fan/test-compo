import antd from 'ant-design-vue/es/locale-provider/zh_CN'
import momentCN from 'moment/locale/zh-cn'
import global from './zh-CN/global'

import menu from './zh-CN/menu'
import setting from './zh-CN/setting'
import user from './zh-CN/user'
import dashboard from './zh-CN/dashboard'
import plans from './zh-CN/plans/plans'

const components = {
  antLocale: antd,
  momentName: 'zh-cn',
  momentLocale: momentCN
}

export default {
  message: '-',
  'navBar.lang': '切换语言',
  'layouts.usermenu.dialog.title': '信息',
  'layouts.usermenu.dialog.content': '您确定要注销吗？',
  'layouts.userLayout.title': 'Ant Design 是西湖区最具影响力的 Web 设计规范',
  'router.processManage.BusinessDomain': '业务域管理',
  'router.processManage.TaskForm': '任务单',
  'router.processManage.DynamicForm': '动态表单',
  'router.processManage.TableForm': '高级表格',
  'router.processManage.ProcessDesigner': '过程设计器',
  'router.task.TaskList': '作业列表',
  'router.task.TaskAdd': '作业添加',
  'router.task.ToDoList': '待办列表',
  'router.task.ChangeDepartment': '切换部门',
  'router.task.SetUp': '设置',
  'router.task.HomeTask': '首页',
  ...components,
  ...global,
  ...menu,
  ...setting,
  ...user,
  ...dashboard,
  ...plans
}
