export default {
  data () {
    return {
      // echarts的实例
      myChart: null
    }
  },
  methods: {
    // 图标初始化
    initData (data) {
      this.myChart = this.$echarts.init(this.$refs["chart"]);
      var option = this.configOption(data);
      this.myChart.setOption(option);
      // window.addEventListener("resize", this.screenAdatper);
    },
    screenAdatper () {
      this.myChart && this.myChart.resize()
    },
    // 根据不同的屏幕宽度换算字体大小
    transformFontSize (fontsize) {
      const width = window.innerWidth
      // const width = window.screen.width
      const ratio = width / 1920
      return parseInt(fontsize * ratio) <= 10 ? 10 : parseInt(fontsize * ratio)
    }
  },
  beforeDestroy () {
    // 组件销毁前移除监听,防止内存泄露
    window.removeEventListener('resize', this.screenAdatper)
    this.myChart = null
  }
}