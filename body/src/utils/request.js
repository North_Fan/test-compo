import axios from 'axios'
import store from '@/store'
import storage from 'store'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
import { ACCESS_TOKEN } from '@/store/mutation-types'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: '',
  timeout: 6000, // 请求超时时间
  headers: {
    __portal_is_backend_api: 'TRUE'
  }
})

// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    // 从 localstorage 获取 token
    const token = storage.get(ACCESS_TOKEN)
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed'
      })
      if (token) {
        store.dispatch('Logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  const token = storage.get(ACCESS_TOKEN)
  config.headers['Cache-Control'] = 'no-cache'
  config.headers.Pragma = 'no-cache'
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers['Access-Token'] = token
  }
  config.headers.__gsuite_user_id = '16193'
  config.headers.__gsuite_user_name = 'FSD'
  // config.headers['__gsuite_user_id'] = '99801136037'
  // config.headers['__gsuite_user_name'] = 'QYY'
  // config.headers['__gsuite_user_id'] = '8750'
  // config.headers['__gsuite_user_name'] = 'WSQ'
  // config.headers['__gsuite_user_id'] = '99726981'
  // config.headers['__gsuite_user_name'] = '戚迎迎'
  // config.headers['__gsuite_user_id'] = '16828'
  // config.headers['__gsuite_user_name'] = 'WJ'
  // config.headers.__gsuite_user_id = '998011308715'
  // config.headers.__gsuite_user_name = 'LCX'
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  // 成功
  if (response.data.code === 501) {
    // 方便联调，501跳转暂时屏蔽
    let jumpUrl = ''
    if (response.data.loginPageUrl) {
      jumpUrl = response.data.loginPageUrl
    }
    if (response.data.data && response.data.data.loginPageUrl) {
      jumpUrl = response.data.data.loginPageUrl
    }
    window.location.href = jumpUrl + window.location.href
  }

  if (response.data.code === 'ConsoleNeedLogin') {
    window.location.href = '/logout'
  }
  return response.data
}, errorHandler)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
