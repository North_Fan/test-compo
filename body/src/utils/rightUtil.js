import { rolePermissionListQuery } from '@/api/permissionQuery'
const codeList = {}
const _R = (element) => {
  element.setAttribute('disabled', `disabled`)
  element.setAttribute('readonly', `readonly`)
  const classes = element.getAttribute('class')
  element.setAttribute('class', `${classes} is-disabled`)
}
const _S = (element) => {
  const classes = element.getAttribute('class')
  if (classes) {
    const newClass = classes.replace('hide', '')
    element.setAttribute('class', `${newClass}`)
  }
}
const _H = (element) => {
  const classes = element.getAttribute('class')
  element.setAttribute('class', `${classes} hide`)
}
const _E = (element) => {
  element.removeAttribute('disabled')
  element.removeAttribute('readonly')
  const classes = element.getAttribute('class')
  if (classes) {
    const newClass = classes.replace('is-disabled', '')
    element.setAttribute('class', `${newClass}`)
  }
}
const rightFn = {
  _R, // 只读
  _S, // show
  _H, // hide
  _E // enabled
}
class RightUtil {
  el = null;
  id = '';
  scope = {};
  rightValue = '';
  constructor (e, code = [], rightStr, sco) {
    this.el = e
    this.id = rightStr
    this.scope = sco
    this.handleNeedRightElements(code)
  }

  handleNeedRightElements (code = []) {
    this.getRigthCode(code, (codeValue) => {
      this.getRightByFn(this.id, this.el, codeValue)
      this.setChildrenRight(this.el, this.id, codeValue)
    })
  }

  getRightByFn (id, element, codeValue) {
    const fnName = id + codeValue
    let hasSelfFn = false
    if (this.scope[fnName]) {
      hasSelfFn = true
    }
    const fn = this.scope[fnName] || rightFn[codeValue]
    fn.call(this, element)
    return hasSelfFn
  }

  setChildrenRight (item, parentId, codeValue) {
    if (item.children.length) {
      for (let i = 0; i < item.children.length; i++) {
        const list = item.children[i]
        const idx = list.getAttribute('right-str') || parentId || ''
        const hasSelfFn = this.getRightByFn(idx, list, codeValue)
        if (hasSelfFn) {
          break
        }
        this.setChildrenRight(list, idx, codeValue)
      }
    }
  }

  async getRigthCode (rightCode, callback) {
    const body = codeList[rightCode]
    if (!body) {
      // mock
      const data = {
        body: [{
          permissions: 'R'
        }]
      }

      await rolePermissionListQuery(rightCode).then(res => {
      })

      // const data = await this.scope.$api.$ajax({
      //   url: `/master-data/roles/items/${rightCode}`,
      //   method: 'get'
      // })

      // data.body = 'E';
      if (data && data.body && data.body.length > 0) {
        codeList[rightCode] = data.body[0].permissions
        const value = '_' + data.body[0].permissions
        callback(value)
      } else {
        codeList[rightCode] = 'H'
        const value = '_H'
        callback(value)
      }
    } else {
      const value = '_' + body
      callback(value)
    }
  }
}
export default RightUtil
