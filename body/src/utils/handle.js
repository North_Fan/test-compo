/*
 * @Author: yangchao52396
 * @Date: 2023-11-28 10:27:44
 * @LastEditors: yangchao52396
 * @LastEditTime: 2023-12-26 10:02:25
 * @FilePath: /cmos-frontend-sqcdp/src/utils/handle.js
 * @Description: function
 *
 */
import axios from 'axios'
import cloneDeep from 'lodash.clonedeep'
/**
 * 设置滚动高度
 * @param {number} y - 滚动高度
 * @param {number} x - 滚动宽度
 * @returns {object} - 返回包含滚动宽度和滚动高度的对象
 */
export function setScrollHeight ({ x = 1500, y = 320 } = {}) {
  return {
    x,
    y: window.innerHeight - y
  }
}
/**
 * 选择对象的一个属性并返回。
 * @param {Object} obj - 要选择属性的对象。
 * @param {string} key - 属性的名称。
 * @returns {any} - 是否存在属性
 */
export function selectProperty (obj, key) {
  return key in obj
}
/**
 * @description: 处理一个JSON里面要的一个数据 state：JSON或者对象 key：需要遍历出来的属性值 backType没有数据返回的内容 （默认为null,为真就返回[]） type:为真就是返回字符串类型，反之为数组
 * @param {array | object} state - 要处理的JSON或者对象。
 * @param {string} key - 属性的名称。
 * @param {boolean} type -需要转成字符还是数组真为字符串，反之为数组。
 * @return {string | array} - 返回处理后的字符串或者数组。
 */
export function handleState (state, key, type) {
  // 数组类型
  if (Array.isArray(state) && state.length > 0) {
    const arr = []
    state.forEach(item => {
      if (selectProperty(item, key)) {
        arr.push(item[key])
      }
    })
    return type ? arr.join() : arr
  } else if (
    Object.keys(state || '').length > 0 &&
    selectProperty(state, key)
  ) {
    // 对象类型
    const arr = [state[key]]
    return type ? arr.join() : arr
  } else {
    return backType ? [] : null
  }
}
/**
 * 正确处理两个数字相乘的结果。
 * @param {string} arg2
 * @param {string} arg1
 * @returns {string}
 */
export function accMul (arg1, arg2) {
  var m = 0
  var s1 = arg1.toString()
  var s2 = arg2.toString()
  try {
    m += s1.split('.')[1].length
  } catch (e) { }
  try {
    m += s2.split('.')[1].length
  } catch (e) { }
  return (
    (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) /
    Math.pow(10, m)
  )
}

/**
 * @description: 获取浏览器参数
 * @param {string} name - 参数名称
 * @param {string} customUrl - 自定义url
 * @return {any} - 返回参数值
 */
export function urlParamsFun(name, customUrl) {
  const urlToParse = customUrl || window.location.href;
  const urlParams = new URLSearchParams(new URL(urlToParse).search);
  if (name) {
    // 如果提供了参数名，则返回相应的参数值
    return urlParams.get(name);
  } else {
    // 如果未提供参数名，则返回整个参数对象
    const paramsObject = {};
    for (const [key, value] of urlParams) {
      paramsObject[key] = value;
    }
    return paramsObject;
  }
}
/**
 * @description: 去掉object里面的数组为[]
 * @param {*} obj
 * @return {*} - 返回处理后的对象
 */
export function removeEmptyArrayProperties (obj) {
  for (const prop in obj) {
    if (Array.isArray(obj[prop]) && obj[prop].length === 0) {
      delete obj[prop]
    } else if (Array.isArray(obj[prop]) && obj[prop].length > 0) {
      obj[prop] = obj[prop][0].id
    }
  }
  return obj
}
/**
 * 根据查询ID移除数据中的项目
 * @param {Object} data - 包含表单项目列表和布局的对象
 * @param {Array} queryIdsToRemove - 要移除的查询ID数组
 * @returns {Object} - 更新后的包含表单项目列表和布局的对象
 */
export function removeItemsByQueryIds (data, queryIdsToRemove) {
  const { formItemList, layout } = data

  const updatedFormItemList = formItemList.filter(item => !queryIdsToRemove.includes(item.queryId))
  const updatedLayout = layout.filter(item => !queryIdsToRemove.includes(item.item.queryId))

  return {
    formItemList: updatedFormItemList,
    layout: updatedLayout
  }
}

/**
 * @description: 导出文件
 * @param {*} code
 * @param {*} name
 * @param {*} url
 * @param {*} method
 * @return {*}
 */
export async function exportFileFormOss ({ code, name, url, method }) {
  const datas = await axios({
    url: url || `/filenode/biz/service/cmos-doc-node/normal/download-file/${code}`,
    responseType: 'blob',
    method: method || 'post',
    headers: {
      __portal_is_backend_api: 'TRUE'
    }
  })
  const blob = new Blob([datas.data])
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, name)
  } else {
    const downloadElement = document.createElement('a')
    var href = window.URL.createObjectURL(blob)
    downloadElement.href = href
    downloadElement.download = name
    document.body.appendChild(downloadElement)
    downloadElement.click()
    document.body.removeChild(downloadElement)
    window.URL.revokeObjectURL(href)
  }
  return true
}

/**
 * @description:构建包含参数的 URL
 * @param {string} baseUrl - 基础 URL
 * @param {object} params - 参数对象
 * @returns {string} - 拼接好的 URL
 * @throws {Error} - 如果基础 URL 为空，则抛出错误
 */
export function buildUrlWithParams (baseUrl, params) {
  // 确保基础 URL 存在
  if (!baseUrl) {
    throw new Error('Base URL is required.');
  }

  // 如果没有参数，直接返回基础 URL
  if (!params || Object.keys(params).length === 0) {
    return baseUrl;
  }
  // 将参数拼接到 URL 上
  const queryString = Object.keys(params)
    .map(key => {
      // 判断属性值是否为 null 或 undefined，如果是则不拼接
      if (params[key] != null) {
        return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
      }
      return '';
    })
    .filter(Boolean) // 过滤掉空字符串
    .join('&');
  // 判断是否已经有查询字符串
  const separator = baseUrl.includes('?') ? '&' : '?';
  // 返回拼接好的 URL
  return `${baseUrl}${separator}${queryString}`;
}
/**
 * @description 判断一个值是否为对象
 * @param {*} val - 待判断的值
 * @returns {boolean} 返回布尔值，true表示是对象，false表示不是对象
 */
export function isObject (val) {
  return typeof val === 'object' && val !== null
}
/**
 * @description 比较两个值是否相等
 * @param {*} val1 - 第一个值
 * @param {*} val2 - 第二个值
 * @returns {boolean} - 返回比较结果，若相等则返回true，否则返回false
 */
export function equals (val1, val2) {
  if (!isObject(val1) || !isObject(val2)) return Object.is(val1, val2)
  if (val1 === val2) return true
  const val1Keys = Object.keys(val1)
  const val2Keys = Object.keys(val2)
  if (val1Keys.length !== val2Keys.length) return false
  for (const key of val1Keys) {
    if (!val2Keys.includes(key)) {
      return false
    }
    const res = equals(val1[key], val2[key])

    if (!res) return false
  }
  return true
}
/**
 * @description 去重函数
 * @param {Array} arr - 需要去重的数组
 * @returns {Array} - 去重后的数组
 */
export function deduplication (arr) {
  const newArr = cloneDeep([...arr])
  for (let i = 0; i < newArr.length; i++) {
    for (let j = i + 1; j < newArr.length; j++) {
      if (equals(newArr[i], newArr[j])) {
        newArr.splice(j, 1)
        j--
      }
    }
  }
  return newArr
}
/**
 * @description 根据指定条件去重的数组
 * @param {Array} array - 需要去重的数组
 * @param {Function} conditionFn - 用于生成条件的函数
 * @param {boolean} keepSingleOccurrences - 是：只保留只出现一次的数据 ，否：就去重
 * @returns {Array} - 去重后的数组
 */
export function filterUniqueElements (array, conditionFn, keepSingleOccurrences = false) {
  // 获取数组中元素或对象的唯一标识的出现次数
  const elementCount = array.reduce((acc, item) => {
    const identifier = conditionFn(item);
    acc[identifier] = (acc[identifier] || 0) + 1;
    return acc;
  }, {});
  // 过滤数组
  const filteredArray = array.filter(item => {
    const identifier = conditionFn(item);
    // 判断是否保留只出现一次的数据
    return keepSingleOccurrences ? elementCount[identifier] === 1 : true;
  });
  return filteredArray;
}
/**
 * @description: js动画
 * @param {*} duration 动画执行时间
 * @param {*} from 开始值
 * @param {*} to 结束值
 * @param {*} onProgress 函数
 * @return {*}
 */
export function animation (duration, from, to, onProgress) {
  const speed = (to - from) / duration
  const startTime = Date.now()
  let value = from
  function _run () {
    const now = Date.now()
    const time = now - startTime
    if (time > duration) {
      value = to
      onProgress && onProgress(value)
      return
    }
    value = from + speed * time
    onProgress && onProgress(value)
    requestAnimationFrame(_run)
  }
  _run()
}

/**
 * @description: 随机颜色
 * @return {*}
 */
export function randomColor () {
  return (
    '#' +
    Math.floor(Math.random() * 0xffffff)
      .toString(16)
      .padEnd(6, '0')
  )
}