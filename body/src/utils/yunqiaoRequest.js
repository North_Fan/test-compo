import axios from 'axios'
// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: '/portal',
  timeout: 6000, // 请求超时时间
  headers: {
  }
})
// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    // 从 localstorage 获取 token
    if (error.response.status === 403) {
      // notification.error({
      //   message: 'Forbidden',
      //   description: data.message
      // })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      // notification.error({
      //   message: 'Unauthorized',
      //   description: 'Authorization verification failed'
      // })
    }
  }
  return Promise.reject(error)
}
// request interceptor
request.interceptors.request.use(config => {
  config.headers.__yunqiao_site_code = 'CMOS'
  config.headers.Accept = '*/*'
  config.headers['x-frame-options'] = 'content-security-policy'
  // 切换账号
  // config.headers.__gsuite_user_id = '99801136037'
  // config.headers.__gsuite_user_name = 'QYY'
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  // 成功
  if (response.data.code === 501) {
    let jumpUrl = ''
    if (response.data.loginPageUrl) {
      jumpUrl = response.data.loginPageUrl
    }
    if (response.data.data && response.data.data.loginPageUrl) {
      jumpUrl = response.data.data.loginPageUrl
    }
    window.location.href = jumpUrl + window.location.href
  }

  if (response.data.code === 'ConsoleNeedLogin') {
    window.location.href = '/logout'
  }
  return response.data
}, errorHandler)

export default request

//  menus = menuInfoResp.data.map(_generateMenu);
// window.YUNQIAO_PORTAL = {
//   siteInfo: siteInfoResp.data,
//   menuInfo: menus,
//   siteCode: 'devSite'
// };
// microApps = siteInfoResp.data.apps.filter(function (item) {
//   return item.type === 'MICRO_FE';
// }).map(function (item) {
//   return {
//     name: item.name,
//     entry: '//' + item.publicEndpoint + '/index.yunqiao.html'
//     /*item.entry*/
//     ,
//     container: container,
//     activeRule: "/" + item.name
//   };
// });
// registerMicroApps(microApps);
// start({
//   prefetch: 'all'
// });
