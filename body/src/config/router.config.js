// eslint-disable-next-line
import {
  BasicLayout,
  BlankLayout
} from '@/layouts'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [{
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: {
      keepAlive: true,
      title: '主页'
    },
    redirect: '/control/SQCDP/task/home-task',
    children: [
      {
        path: '/control/SQCDP/task/home-task',
        name: 'HomeTask',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/task/homeTask'),
        meta: {
          title: '主页',
          keepAlive: true,
          permission: ['home-task']
        }
      },
      {
        path: '/control/SQCDP/',
        name: 'control',
        redirect: '/control/SQCDP/task/home-task',
        component: RouteView,
        meta: {
          keepAlive: true,
          title: '管控',
          icon: 'unordered-list',
          permission: ['control']
        },
        children: [
      ]
      },
      {
        path: '/control/SQCDP/indicator-library-maintenance',
        name: 'IibraryMaintenance',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/sqcdp/indexManagement/index.vue'),
        meta: {
          title: '指标库维护',
          keepAlive: true,
          permission: ['home-index']
        }
      },
      {
        path: '/control/SQCDP/plans/todo',
        name: 'Todo',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/plans/Todo'),
        meta: {
          title: '待办',
          keepAlive: true,
          permission: ['todo'],
          hiddenHeaderContent: true
        }
      }, {
        path: '/control/SQCDP/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "classificationManagement" */ '@/views/About'),
        meta: {
          title: '关于',
          keepAlive: true,
          permission: ['about'],
          hiddenHeaderContent: true
        }
      }

    ]
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [{
    path: '/user',
    component: BlankLayout,
    redirect: '/user/login',
    hidden: true,
    children: [{
      path: 'login',
      name: 'login',
      component: () => import(/* webpackChunkName: "user" */ '@/views/system/user/Login')
    }]
  },
  {
    path: '/403',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/system/exception/403')
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/system/exception/404')
  },
  {
    path: '/500',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/system/exception/500')
  }
]
