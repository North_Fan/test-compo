import store from '@/store'

export default function Initializer () {
  store.dispatch('setUserRole')
}
