import Vue from 'vue'
const touch = { lastTime: 0, interval: 300, startX: 0 }
let move = 0
let element
let bind
function mousemove_ (e) {
    if (!touch.init) return
    move = e.pageX - touch.startX
    element.style.width = `${touch.width + move}px`
    bind.value(move)
}
function mouseup_ () {
    if (!touch.init) return
    touch.init = false
    document.removeEventListener('mousemove', mousemove_)
    document.removeEventListener('mouseup', mouseup_)
}
const drag = Vue.directive('drag', {
    bind (el, binding) {
        if (el) {
            element = el
            bind = binding
            el.style.position = 'relative'
            const dom = document.createElement('DIV')
            dom.style.height = '100%'
            dom.style.width = '3px'
            dom.style.position = 'absolute'
            dom.style.top = '0px'
            dom.style.right = '0px'
            dom.style.cursor = 'col-resize'
            dom.style.backgroundColor = '#eee'
            dom.style.userSelect = 'none'
            dom.style.touchAction = 'none'
            dom.addEventListener('mousedown', (e) => {
                const now = +new Date()
                if (now - touch.lastTime < touch.interval) {
                    return (touch.init = false)
                }
                touch.init = true
                touch.startX = e.pageX
                touch.width = el.offsetWidth
                document.addEventListener('mousemove', mousemove_, { passive: false })
                document.addEventListener('mouseup', mouseup_)
            })
            el.appendChild(dom)
        }
    },
    unbind (el) {
        document.removeEventListener('mousemove', mousemove_)
        document.removeEventListener('mouseup', mouseup_)
    }
  })

  export default drag
