import Vue from 'vue'
import RightUtil from '../../utils/rightUtil'

Vue.use(() => {
  Vue.directive('right', {
    bind (el, binding, vnode) {
      const rightStr = el.getAttribute('right-str') || ''
      const scope = vnode.context
      const rightUtil = new RightUtil(el, binding.value, rightStr, scope)
      console.log(rightUtil.rightValue)
    }
    // update: (el, binding, vnode) => {
    //   // if (binding.value !== binding.oldValue) {
    //   const rightStr = el.getAttribute('right-str') || '';
    //   const scope = vnode.context;
    //   rightUtil.handleNeedRightElements(el, binding.value, rightStr, scope);
    //   // }
    // }
  })
})
