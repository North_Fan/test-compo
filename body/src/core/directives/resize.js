import Vue from 'vue'
const resize = Vue.directive('resize', {
    bind (el, binding) {
        let width = ''
        let height = ''
        function isReize () {
            const style = document.defaultView.getComputedStyle(el)
            if (width !== style.width || height !== style.height) {
                binding.value({ width: style.width, height: style.height })
            }
            width = style.width
            height = style.height
        }
        el.__vueSetInterval__ = setInterval(isReize, 300)
    },
    unbind (el) {
        clearInterval(el.__vueSetInterval__)
    }
  })

  export default resize
