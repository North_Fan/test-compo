import resize from './resize'

const install = function (Vue) {
  Vue.directive('resize-observer', resize)
}

if (window.Vue) {
  window['resize-observer'] = resize
  Vue.use(install); // eslint-disable-line
}

resize.install = install
export default resize
