/*
 * @Author: yangchao52396
 * @Date: 2023-12-25 17:15:55
 * @LastEditors: yangchao52396
 * @LastEditTime: 2023-12-25 17:24:56
 * @FilePath: /cmos-frontend/src/directive/resize/index.js
 * @Description:自定义指令resize事件
 *
 */
const map = new WeakMap()
const ob = new ResizeObserver(entries => {
  for (const entry of entries) {
    const handler = map.get(entry.target)
    if (handler) {
      handler({
        width: entry.borderBoxSize[0].inlineSize,
        height: entry.borderBoxSize[0].blockSize
      })
    }
  }
}
)
export default {
  inserted (el, binding) {
    if (binding.value) {
      map.set(el, binding.value)
      ob.observe(el)
    }
  },
  unbind (el) {
    ob.unobserve(el)
  }
}
