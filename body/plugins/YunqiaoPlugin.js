function YunqiaoPlugin (options) {
  this.options = options
  }

  YunqiaoPlugin.prototype.apply = function (compiler) {
      compiler.plugin('compilation', function (compilation, options) {
          compilation.plugin('html-webpack-plugin-after-html-processing', function (htmlPluginData, callback) {
            const yunqiaoScript = '<script th:inline="javascript">window.YUNQIAO_SITE_CODE = /*[[${siteCode}]]*/ null;</script></head>'
            htmlPluginData.html = htmlPluginData.html.replace('</head>', yunqiaoScript)
            callback && callback(null, htmlPluginData)
          })
      })
  }

  module.exports = YunqiaoPlugin
