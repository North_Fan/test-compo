import request from '@/utils/request'

// 任务单关注接口
export function changeWatch (params) {
  return request({
    url: '/basecenter/biz/service/login/watch',
    method: 'post',
    data: params
  })
}

// 保存表头配置
export function columnsSave (params) {
  return request({
    url: '/basecenter/biz/service/login/auto/list',
    method: 'post',
    data: params
  })
}

// 查询表头配置
export function searchColums (params) {
  return request({
    url: '/basecenter/biz/service/login/get/auto/list/' + params,
    method: 'get'
  })
}

// 获取用户信息
export function getUserInfo (params) {
  return request({
    url: '/basecenter/biz/service/login/get/user/info',
    method: 'get'
  })
}

// 获取用户信息
export function getUserDetail (data) {
  return request({
    url: '/basecenter/feignApi/userCenter/user/qryUserInfo',
    method: 'post',
    data
  })
}

// 获取选择的年度目标
export function getCrtAnnualtarget (params) {
  return request({
    url: `/basecenter/biz/service/login/get/annualtargetheader/${params}`,
    method: 'get'
  })
}

// 获取选择的年度目标
export function setCrtAnnualtarget (params) {
  return request({
    url: '/basecenter/biz/service/login/annualtargetheader',
    method: 'post',
    data: params
  })
}
// 获取自定义视图
export function requestCustomViewList (params) {
  return request({
    url: '/basecenter/biz/service/login/get/view/' + params,
    method: 'get'
  })
}
// 保存自定义视图
export function saveCustomViewList (params) {
  return request({
    url: '/basecenter/biz/service/login/view',
    method: 'post',
    data: params
  })
}

// 接口日志
export function dailyLogTrace (params) {
  return request({
    url: '/basecenter/biz/service/util/trace',
    method: 'get'
  })
}
export function dataDict (params) {
  return request({
    url: '/basecenter/biz/service/codetype/pageSyscodetype',
    method: 'post',
    data: params
  })
}
export function getSubdataDict (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/syscodeitemsByTypeCodeExtend?typeCode=' + params,
    method: 'get'
  })
}

// 查询数据字典类型
export function getSyscodetypes (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetypes?typeCode=' + params,
    method: 'get'
  })
}
// 添加数据字典一级节点
export function addNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype',
    method: 'post',
    data: params
  })
}
// 修改数据字典一级节点
export function updateNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype/update',
    method: 'post',
    data: params
  })
}

// 删除数据字典一级节点
export function deleteNode (params) {
  return request({
    url: '/basecenter/biz/service/codetype/syscodetype/' + params,
    method: 'delete'
  })
}

// 添加数据字典
export function addSyscodeitem (params) {
  return request({
    url: ' /basecenter/biz/service/codeitem/syscodeitem',
    method: 'post',
    data: params
  })
}

// 查询数据字典
export function getSyscodeitem (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/getItemByIdExtend?typeId=' + params,
    method: 'get'
  })
}

// 修改数据字典
export function updateSyscodeitem (params) {
  return request({
    url: '/basecenter/biz/service/codeitem/syscodeitem/update',
    method: 'post',
    data: params
  })
}

// 删除数据字典 /basecenter/biz/service/codeitem/syscodeitem/12
export function deleteSubNode (params) {
  return request({
    url: ' /basecenter/biz/service/codeitem/syscodeitem/' + params,
    method: 'delete'
  })
}

/**
 * 按钮权限查询
 * @returns
 */
 export function resourcePathQuery (path) {
  return request({
    url: '/basecenter/biz/service/user/resource?resourcePath=' + path,
    method: 'get'
  })
}

/**
 * 获取当前用户cookie
 * @returns
 */
export function xxlSsoSessionid () {
  return request({
    url: '/basecenter/biz/service/login/get/xxl_sso_sessionid',
    method: 'get'
  })
}

/**
 * 获取所有供应商信息
 * @returns
 */
export function querySupplierMsg (params) {
  return request({
    url: '/basecenter/biz/service/querySupplierMsgApi/querySupplierMsg',
    method: 'post',
    data: params
  })
}
