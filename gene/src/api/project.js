import request from '@/utils/request'

// 新增/暂存评估模型接口
export function evaluationModelForSave (params) {
    return request({
      url: '/project/biz/service/evaluationModel/save ',
      method: 'post',
      data: params
    })
  }
  // 查询评估模型接口
  export function evaluationModelForQuery (params) {
    return request({
      url: '/project/biz/service/evaluationModel/list ',
      method: 'post',
      data: params
    })
  }
// 删除评估模型接口
  export function evaluationModelForDelete (params) {
    return request({
      url: '/project/biz/service/evaluationModel/delete ',
      method: 'post',
      data: params
    })
  }
// 读取数据字典
export function getCommonSelectByCode (parameter) {
  return request({
    url: '/basecenter/biz/service/codeitem/syscodeitemsByTypeCode',
    method: 'get',
    params: parameter
  })
}

export function getPhaseConfig (params) {
  getCommonSelectByCode({ typeCode: params }).then((res) => {
    if (res['success']) {
      if (res['data']) {
        const stageSelect = res['data'].map((item) => {
          const stageItem = {
            label: item.itemName,
            value: item.itemCode
          }
          return stageItem
        })
        console.log('stageSelectstageSelectstageSelectstageSelect', stageSelect)
        return stageSelect
      }
    }
 })
 }
  // 新增/修改检查单
  export function editChecklistForSave (params) {
    return request({
      url: '/project/biz/service/checkItem/save',
      method: 'post',
      data: params
    })
  }
  // 删除检查单
  export function editChecklistForDelete (params) {
    return request({
      url: '/project/biz/service/checkItem/delete',
      method: 'post',
      data: params
    })
  }
  // 获取检查单列表
  export function editChecklistList (params) {
    return request({
      url: '/project/biz/service/checkItem/list',
      method: 'post',
      data: params
    })
  }
  // 获取评估项目列表
  export function evaluationProjectList (params) {
    return request({
      url: '/project/biz/service/evaluatePRJ/list',
      method: 'post',
      data: params
    })
  }
  // 创建任务单模板
  export function evaluatePRJItemProcess (params) {
    return request({
      url: '/project/biz/service/evaluatePRJItemProcess/create',
      method: 'post',
      data: params
    })
  }

  // 创建评估项目任务单模板
  export function evaluatePRJItemProcessCreate (params) {
    return request({
      url: 'project/biz/service/evaluatePRJItemProcess/create',
      method: 'post',
      data: params
    })
  }

  // 根据评估项ID查询不符合项的ID
  export function getNonconformRectInstanceID (params) {
    return request({
      url: '/project/biz/service/rectOfNoncon/listUnfinished',
      method: 'post',
      data: params
    })
  }

  // 监控与统计
  export function evaluationSheetlist (params) {
    return request({
      url: '/project/biz/service/evaluatePRJ/evaluationSheetList',
      method: 'post',
      data: params
    })
  }

  // 删除不符合项流程
  export function deleteNonconformRectInstanceID (params) {
    return request({
      url: '/project/biz/service/rectOfNoncon/endFlow',
      method: 'post',
      data: params
    })
  }
  // 评估项目任务单
  export function evaluationProjectTask (params) {
    return request({
      url: '/project/biz/service/evaluateList/list',
      method: 'post',
      data: params
    })
  }
   // 合规审核list
   export function getAuditList (params) {
    return request({
      url: '/process/biz/service/compliance/audit/page',
      method: 'post',
      data: params
    })
  }
  // 合规审核表单详情
   export function getAuditDetail (params) {
    return request({
      url: '/process/biz/service/compliance/audit/detail/' + params,
      method: 'get'
    })
   }
 // 批量删除合规审核表单
 export function deleteAudit (params) {
  return request({
    url: '/process/biz/service/compliance/audit/delete',
    method: 'post',
    data: params
  })
 }
