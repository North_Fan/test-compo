import request from '@/utils/request'
const interfaceMap = {
  createTaskLink: '/plan/biz/service/task/link/createTaskLink', /* 关联任务项-源索引必填、目标对象索引-列表必填 */
  deleteTaskLink: '/plan/biz/service/task/link/deleteTaskLink', /*  删除任务项-源索引必填、目标对象索引-列表必填 */
  listLinkedTask: '/plan/biz/service/task/link/listLinkedTask' /* 查询已关联任务项-源索引必填 */

}

/**
 * 关联任务项
 */
 export function createTaskLink (parameter) {
  return request({
    url: interfaceMap.createTaskLink,
    method: 'post',
    data: parameter
  })
}
/**
 * 查询已关联任务项
 */
 export function deleteTaskLink (parameter) {
  return request({
    url: interfaceMap.deleteTaskLink,
    method: 'post',
    data: parameter
  })
}
/**
 * 删除任务项
 */
 export function listLinkedTask (parameter) {
  return request({
    url: interfaceMap.listLinkedTask,
    method: 'post',
    data: parameter
  })
}

/**
 * 任务列表数据
 * @param {Object} parameter
 * @returns
 */
 export function taskRunAssemblyList (parameter) {
  return request({
    url: '/assignment/biz/service/assignment/page',
    method: 'post',
    data: parameter
  })
}

/**
 * 待办列表数据
 * @param {Object} parameter
 * @returns
 */
 export function getWfDetailList (data) {
  return request({
    // url: '/flow-platform/process/todo',
    url: '/flow-platform/api/v1.0/public/listTask',
    method: 'post',
    data
  })
}

/**
 * 待办详情数据
 * @param {Object} parameter
 * @returns
 */
 export function queryWfDetail (params) {
  return request({
    url: '/assignment/biz/service/instance/wf/todo',
    method: 'get',
    params
  })
}

/**
 * 待阅详情数据
 * @param {Object} parameter
 * @returns
 */
 export function queryToReadDetail (params) {
  return request({
    url: '/assignment/biz/service/instance/wf/toread',
    method: 'get',
    params
  })
}

/**
 * 作业详情接口
 * @param {Object} parameter
 * @returns
 */
 export function taskRunAssemblyDetail (parameter) {
  return request({
    url: '/assignment/biz/service/assignment/model/scene/detail',
    method: 'post',
    data: parameter
  })
}

/**
 * 任务运行组件
 * @param {Object} parameter
 * @returns
 */
 export function taskRunAssembly (parameter) {
  return request({
    url: '/assignment/biz/service/tab/list',
    method: 'post',
    data: parameter
  })
}

/**
 * 任务定义模板
 * @param {Object} parameter
 * @returns
 */
 export function taskMould (parameter) {
  return request({
    url: '/assignment/biz/service/stemp/header/list',
    method: 'post',
    data: parameter
  })
}

/**
 * 业务领域
 * @param {Object} parameter
 * @returns
 */
 export function businessDomain (parameter) {
  return request({
    url: '/assignment/biz/service/biz/index/list',
    method: 'post',
    data: parameter
  })
}

/**
 * 任务模板参数
 * @param {Object} parameter
 * @returns
 */
 export function taskMouldParameter (parameter) {
  return request({
    url: '/assignment/biz/service/stemp/param/list',
    method: 'post',
    data: parameter
  })
}

/**
 * 任务模板参数定义
 * @param {Object} parameter
 * @returns
 */
 export function taskMouldParameterDefine (parameter) {
  return request({
    url: '/assignment/biz/service/param/define/list',
    method: 'post',
    data: parameter
  })
}

/**
 * 新增任务
 * @param {Object} parameter
 * @returns
 */
 export function createTask (parameter) {
  return request({
    url: '/assignment/biz/service/assignment/save',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除作业
 * @param {Object} parameter
 * @returns
 */
 export function delTaskRunAssembly (id) {
  return request({
    url: `/assignment/biz/service/assignment/del/${id}`,
    method: 'delete'
  })
}

/**
 * 任务列表查询
 * @param {Object} parameter
 * @returns
 */
 export function getTaskList (parameter) {
  return request({
    url: '/plan/biz/service/task/pageList',
    method: 'post',
    data: parameter
  })
}

/**
 * 任务列表查询
 * @param {Object} parameter
 * @returns
 */
// 前台不直接调用作业服务取模板
//  export function getTaskMould (parameter) {
//   return request({
//     url: '/assignment/api/assignment/model/scene/create',
//     method: 'post',
//     data: parameter
//   })
// }

/**
 * 任务列表编辑
 * @param {Object} parameter
 * @returns
 */
//  export function getTaskMouldEdit (parameter) {
//   return request({
//     url: '/assignment/service/assignment/get/' + parameter,
//     method: 'get',
//     data: parameter
//   })
// }

/**
 * 任务列表查询
 * @param {Object} parameter
 * @returns
 */
 export function searchaActHandle (parameter) {
  return request({
    url: '/assignment/biz/service/executor/page',
    method: 'post',
    data: parameter
  })
}

/**
 * 对象列表查询
 * @param {Object} parameter
 * @returns
 */
 export function getObjectTreeData (parameter) {
  return request({
    url: '/object-component/api/object-metas/' + parameter,
    method: 'get'
  })
}

/**
 * 对象索引查询
 * @param {Object} parameter
 * @returns
 */
 export function getObjectIndexData (params) {
  return request({
    url: '/object-component/api/object-metalayouts',
    method: 'get',
    params
  })
}

/** 通过任务项获取检查项列表
* @param {Object} parameter
* @returns
*/
export function getInspectTemplateListByTaskId (params) {
 return request({
   url: '/plan/biz/service/task/inspect/item/non-page',
   method: 'get',
   params
 })
}
/** 通过检查单获取检查项列表
* @param {Object} parameter
* @returns
*/
export function getInspectTemplateList (data) {
 return request({
   url: '/plan/biz/service/inspect/item/page',
   method: 'post',
   data
 })
}
/**
*  添加检查项
* @param {Object} parameter
* @returns
*/
export function addInspect (data) {
 return request({
   url: '/plan/biz/service/task/inspect/item',
   method: 'post',
   data
 })
}
/**
*  添加检查单
* @param {Object} parameter
* @returns
*/
export function addTemplate (data) {
 return request({
   url: '/plan/biz/service/inspect/template',
   method: 'post',
   data
 })
}
/**
*  检查单列表
* @param {Object} parameter
* @returns
*/
export function getTemplateList (data) {
 return request({
   url: '/plan/biz/service/inspect/template/page',
   method: 'post',
   data
 })
}
/**
*  删除检查项
* @param {Object} parameter
* @returns
*/
export function removeInspect (params) {
 return request({
   url: `/plan/biz/service/task/inspect/item/del`,
   method: 'post',
   data: params
  })
}
/**
*  查看检查单
* @param {Object} parameter
* @returns
*/
export function getInspectTemplate (params) {
  return request({
    url: '/plan/api/task/getInspectTemplate',
    method: 'get',
    params
  })
 }

 /**
*  更换执行器
* @param {Object} parameter
* @returns
*/
export function updateExecutor (data) {
  return request({
    url: '/assignment/biz/service/assignment/update/executor',
    method: 'post',
    data
  })
 }

  /**
*  更换执行器
* @param {Object} parameter
* @returns
*/
export function flowCheckBeforeSubmit (data) {
  return request({
    url: '/assignment/biz/service/flow/check',
    method: 'post',
    data
  })
 }

   /**
*  首页我的任务
* @param {Object} parameter
* @returns
*/
export function getHomeMyTask (data) {
  return request({
    url: '/plan/biz/service/pending/getMyTask',
    method: 'post',
    data
  })
 }

 /**
*  首页我的创建
* @param {Object} parameter
* @returns
*/
export function getHomeMyCreate (data) {
  return request({
    url: '/plan/biz/service/pending/getMyCreate',
    method: 'post',
    data
  })
 }
 /**
*  首页我的创建未读数
* @param {Object} parameter
* @returns
*/
export function getMyCreateCount () {
  return request({
    url: '/plan/biz/service/pending/getMyCreateCount',
    method: 'get'
  })
 }

    /**
*  首页统计
* @param {Object} parameter
* @returns
*/
export function homeTaskRecord (data) {
  return request({
    url: '/plan/biz/service/pending/getCount',
    method: 'post',
    data
  })
 }
/**
*  启动一卡一单编制流程
* @param {Object} parameter
* @returns
*/
export function batchStartCardProcess (data) {
  return request({
    url: '/plan/biz/service/cardOrderHeader/batchStartCardProcess',
    method: 'post',
    data
  })
 }
/**
*  合同履约测试接口
* @param {Object} parameter
* @returns
*/
export function contractPerformance (data) {
  return request({
    url: '/contract/ext/service/contract/performance',
    method: 'post',
    data
  })
 }

/**
*  获取消息列表
* @param {Object} parameter
* @returns
*/
export function getMyMessList (data) {
  return request({
    url: '/basecenter/biz/service/message/pageMyMessage',
    method: 'post',
    data
  })
 }
/**
*  获取我的关注列表
* @param {Object} parameter
* @returns
*/
export function getMyAttentionCount () {
  return request({
    url: '/plan/biz/service/pending/getMyAttentionCount',
    method: 'post'
  })
}
/**
*  获取我的关注列表
* @param {Object} parameter
* @returns
*/
export function getNoticeList (data) {
  return request({
    url: '/plan/biz/service/pending/getMyAttention',
    method: 'post',
    data
  })
}

/**
*  获取未读消息数
* @param {Object} parameter
* @returns
*/
export function getUnreadCount () {
  return request({
    url: '/basecenter/biz/service/message/getUnreadCount',
    method: 'post'
  })
 }

/**
*  更新未读信息
* @param {Object} parameter
* @returns
*/
export function updateRead (params) {
  return request({
    url: `/basecenter/biz/service/message/updateRead/${params}`,
    method: 'post'
  })
 }
