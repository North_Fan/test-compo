import request from '@/utils/request'
import notification from 'ant-design-vue/es/notification'
const interfaceMap = {
  getReadDoList: '/quality/biz/tasks/to-read/getReadDoList',
  getProcessInstanceId: '/assignment/biz/service/instance/children/query',
  getFlowInstanceId: '/assignment/biz/service/instance/deep/query',
  getProblemList: '/quality/biz/service/question/page',
  createProblem: '/quality/biz/service/question/save',
  getProblemDetail: '/quality/biz/service/question/detail/',
  editProblem: '/quality/biz/service/question/edit',
  removeProblem: '/quality/biz/service/question/remove',
  getProblemImportConfig: '/quality/biz/service/question/get/import/configure/',
  setProblemImportConfig: '/quality/biz/service/question/save/import/configure',
  importProblem: '/quality/biz/service/question/import',
  exportProblem: '/quality/biz/service/question/export',
  exportAllProblem: '/quality/biz/service/question/exportAll',
  getQualityProblemsType: '/problem/getQualityProblemsType',
  selectLargeScreenShow: '/quality/biz/service/process/selectLargeScreenShow',
  selectNumByAircraftModel: '/quality/biz/service/process/selectNumByAircraftModel',
  selectNumByIssueSource: '/quality/biz/service/process/selectNumByIssueSource',
  selectNumByIssueState: '/quality/biz/service/process/selectNumByIssueState',
  selectNumByIssueRespUnit: '/quality/biz/service/process/selectNumByIssueRespUnit',
  getAtaPSection: '/problem/getAtaPSection',
  getVehicles: '/quality/biz/service/code/manuSerialNum/page',
  getCauseAnalysis: '/quality/biz/service/code/reason/page',
  getCauseChildAnalysis: '/quality/biz/service/code/reason/child/',
  filterGetCauseChild: '/quality/biz/service/code/reason/usedChild',
  getProblemProcessData: '',
  getSourceProblemData: '/quality/biz/service/code/source/page',
  getLocationData: '/quality/biz/service/code/place/page',
  getAtaSectionData: '/quality/biz/service/code/ata/page',
  createEscapeTel: '/quality/biz/service/escape/template',
  getEscapeDetail: '/quality/biz/service/escape/',
  createAuditPlan: '/quality/biz/service/audit/add',
  editAuditPlan: '/quality/biz/service/audit/update',
  removeAuditPlan: '/quality/biz/service/audit/delete',
  queryAuditPlan: '/quality/biz/service/audit/list',
  startAuditPlan: '/quality/biz/service/audit/advice/execute',
  initAuditPlan: '/quality/biz/service/audit/init',
  getProblemNature: '/quality/biz/service/code/problemNature/queryTree',
  createAddressSupport: '/quality/biz/service/gse/template',
  createAddqzeroCenter: '/quality/biz/service/qzeroCenter/create',
  getRectificationInfo: '/quality/biz/service/feedback/getRectificationInfo',
  getFeedBackInfo: '/quality/biz/service/feedback/getFeedBackInfo',
  getFeedBackUpdateLog: '/quality/biz/service/feedback/getFeedBackUpdateLog',
  updateFeed: '/quality/biz/service/feedback/updateFeed',
  getPlanAdviceInfo: '/quality/biz/service/audit/getPlanAdviceInfo',
  getProblemAdviceInfo: '/quality/biz/service/audit/getProblemAdviceInfo',
  /* 创建 地面支援设备问题  */
  CauseAnalysisMeasuresMade: '/quality/biz/service/equipment/causeAnalysis/render' /* 原因分析 */,
  GeneralRender: '/quality/biz/service/reasonAnalysis/render',
  renderFaultForm: '/quality/biz/service/fault/rapidProcess/render',
  productCreatRecord: '/quality/biz/productFailure/create',
  DailyQualityManagementIssues: '/quality/biz/service/dailyManagement/create',
  getRiskPoint: '/quality/biz/service/riskPoint/create',
  getTurnIssue: '/quality/biz/service/turnIssue/init',
  trainManagement: '/quality/biz/service/train/create',
  tecPubManagement: '/quality/biz/service/tecPubManagement/create',
  satisfactionDegree: '/quality/biz/service/satisfaction/create',
  releaseCreate: '/quality/biz/productfailure/release/create', // 例外放行
  turndownCreate: '/quality/biz/productfailure/turndown/create', // 驳回
  engineModel: '/quality/biz/service/code/engineModel/queryTree',
  OperationalRiskDetermination: '/quality/biz/service/fault/riskJudgment/render',
  PersonalAccountability: '/quality/biz/service/fault/personAccountability/render', // 个人问责
  InferOtherThingsFromOneFact: '/quality/biz/service/fault/drawInferences/render', // 举一反三
  CorrectiveAction: '/quality/biz/service/fault/correctiveAction/render', // 纠正措施
  getOccurredStage: '/quality/biz/service/code/occurredStage/queryTree',
  controlSheet: '/quality/biz/service/quality/problem/controlSheet', // 管控单
  qzeroControlSheet: '/quality/biz/service/quality/qzeroControl/controlSheet',
  ProblemReportingCriteria: '/quality/biz/service/code/guideline/page', // 问题上报准则
  repetitiveProblem: '/quality/biz/service/quality/problem/repetitiveProblem', // 重复问题
  // FaultDetermineRespUnit: '/quality/biz/service/fault/determineRespUnit/render', // 确定责任单位
  correctiveMeasures: '/quality/biz/service/quality/problem/correctiveMeasures/list',
  // CorrectiveActionValidation: '/quality/biz/service/fault/correctiveAction/render', // 纠正措施及有效性验证
  // CorrectiveAction: '/quality/biz/service/fault/correctiveAction/render', // 纠正措施及有效性验证
  FaultDetermineRespUnit: '/quality/biz/service/fault/determineRespUnit/render', // 确认责任单位
  FaultCauseAnalysisReview: '/quality/biz/service/fault/causeAnalysisReview/render', // 原因分析与评审
  getSponsor: '/quality/biz/service/sponsor/sponsorSearch',
  // FaultDetermineRespUnit: '/quality/biz/service/fault/determineRespUnit/render', // 确认责任单位
  problemExport: '/quality/biz/service/quality/problem/correctiveMeasures/export',
  controlInsert: '/quality/biz/service/quality/problem/control/insert',
  controlQuery: '/quality/biz/service/quality/problem/control/query',
  pageInfoReportView: '/quality/biz/service/process/pageInfoReportView', // a.	信息报告
  qzeroStart: '/quality/biz/service/qzero/qzeroStart',
  pageScreenJudgeView: '/quality/biz/service/process/pageScreenJudgeView', // 筛选判断
  pageEventSurveyView: '/quality/biz/service/process/pageEventSurveyView', // c.	事件调查
  pageMeasureExecuteView: '/quality/biz/service/process/pageMeasureExecuteView', // 措施落实
  pageFaultInformationView: '/quality/biz/service/process/pageFaultInformationView', // 故障件信息
  getChangeUrl: '/contract/biz/contract/change/getChangeUrl',
  cardGetToken: '/quality/biz/quality/card/getToken',
  // cardGetToken: 'http://cmos-comment-disassemble.cmos2uat.comac.intra/cmos-comment-disassemble/ticket/create',
  treeGetTree: '/quality/biz/service/tree/get-tree',
  treeGetTODOTree: '/quality/biz/service/tree/get-toDotree',
  processExport: '/quality/biz/service/process/export', // 问题管控-导出
  qzeroExport: '/quality/biz/service/qzero/export', // 双五归零-导出1
  getPageQualityEscapesView: '/quality/biz/service/process/pageQualityEscapesView', // 问题管控-审核问题-默认视图
  getOverdueWarnState: '/quality/biz/service/question/getOverdueWarnState/', // 问题管控-超期预警
  getBlueLetterNotice: '/quality/biz/service/question/blueLetterNotice',
  pageQualityProcess: '/quality/biz/service/process/pageQualityProcess', // 产品故障分页查询
  addFormLink: '/quality/biz/service/commonFeedback/link', // 双五归零link新增
  complaintMgr: '/quality/biz/service/customerComplaint/create', // 双五归零link新增
  deleteFormLink: '/quality/biz/service/commonFeedback/unLink', // 双五归零link删除
  getQzeroToDotree: '/quality/biz/service/todoTree/get-toDotree',
  commonCorrectTaskLink: '/quality/biz/service/common/feedback/link', // 添加措施行动项
  commonCorrectTaskUnLink: '/quality/biz/service/common/feedback/unLink', // 删除措施行动项
  addTechnicalFormLink: '/quality/biz/service/common/feedback/link', // 技术出版物问题处置link新增
  deleteTechnicalFormLink: '/quality/biz/service/common/feedback/unLink', // 技术出版物问题处置link删除
  addRiskPointLink: '/quality/biz/service/riskPoint/addObj', // 风险管理类
  updateRiskPointLink: '/quality/biz/service/riskPoint/updateObj', // 风险点link修改
  deleteRiskPointUnLink: '/quality/biz/service/riskPoint/delObj', // 风险管理类删除
  deleteMeasureLink: '/quality/biz/service/measureProgressFeedback/delObj', // 风险管控link删除
  getTargetBySource: '/quality/biz/service/measureProgressFeedback/getFeedBackList', // 获取管控措施link
  getMeasureProgressFeedback: '/quality/biz/service/riskPoint/getMeasureProgressFeedback', // 获取风险因子分解link
  queryProblemType: '/quality/biz/service/code/problemType/queryTree', // 查询ProblemType树
  queryProblemTypeList: '/quality/biz/service/code/problemType/child/', // 查询ProblemType 字典
  queryDataByProblemType: '/quality/biz/service/code/problemType/queryDataByProblemType', // 查询ProblemType 字典

  /** 风险管控 */
  qryByObjId: '/quality/biz/service/risk/factor/recognize/qryByObjId', // 查询详情
  qryLinkObj: '/quality/biz/service/risk/factor/recognize/qryLinkObj', // 查询行列
  delLinkObj: '/quality/biz/service/risk/factor/recognize/delLinkObj', // 删除行列
  linkRiskFactor: '/quality/biz/service/risk/factor/recognize/linkRiskFactor', // 添加风险因子行列
  createAssignment: '/quality/biz/service/risk/factor/recognize/create', // 新建作业
  selectIssuetree: '/quality/biz/service/issueTree/selectIssuetree',
  issueManagement: '/quality/biz/service/issueManagement/create',
  issueControlSheet: '/quality/biz/service/issue/controlSheet',
  selectIssueToDoTree: '/quality/biz/service/issueToDoTree/selectIssueToDoTree',
  // 工程技术问题
  engineTechCreat: '/quality/biz/service/engineeringTech/create',
  getTwoProTechData: '/quality/biz/service/engineeringTech/professionalEngineer/getTree',
  totalScore: '/quality/biz/service/engineeringTech/totalScore'
}

export default interfaceMap

export const assignmentVersionIdMap = {
  RapidProcess: '1491309126420975616',
  CostStatistics: '1492090573143052288',
  QualityReversal: '1492090892656742400',
  IssueScreen: '1493473703500312576',
  ExpandInspect: '1493829868603228160',
  RiskAssessment: '1493830143745376256',
  RiskAssessmentReview: '1493830431524962304',
  CaacInspect: '1493830813407952896',

  Process_RapidProcess: '1498192180797177856', // 快速处理过程
  Process_ExpandInspect: '1498534639150497792', // 扩大检查过程
  Process_RiskAssessment: '1498535138360754176', // 局方审查过程
  Process_PersonalAccountability: '1499251630716092416'// 个人问责子过程
}

// 查询 父级 ProblemType
export function queryProblemType (data) {
  return request({
    url: interfaceMap.queryProblemType,
    method: 'post',
    data: { 'likeCondition': '', 'pageSize': 20, 'pageNum': 1, 'size': 20, 'current': 1, 'isAdvancedSearch': false }
  })
}
// 查询 ProblemType 字典
export function queryProblemTypeList (data) {
  return request({
    url: interfaceMap.queryProblemTypeList + data.id,
    method: 'post',
    data
  })
}// 查询 ProblemType 字典
export function queryDataByProblemType (data) {
  return request({
    url: interfaceMap.queryDataByProblemType,
    method: 'post',
    data
  })
}

// 查询全面问题管理树
export function selectIssuetree (data) {
  return request({
    url: interfaceMap.selectIssuetree,
    method: 'post',
    data
  })
}
// 创建全面管理问题
export function issueManagement (data) {
  return request({
    url: interfaceMap.issueManagement,
    method: 'post',
    data
  })
}
// 全面管理管控单数据接口
export function issueControlSheet (data) {
  return request({
    url: interfaceMap.issueControlSheet,
    method: 'post',
    data
  })
}
// 全面管理待办树
export function selectIssueToDoTree (data) {
  return request({
    url: interfaceMap.selectIssueToDoTree,
    method: 'post',
    data
  })
}
// 新增措施行动项
export function commonCorrectTaskLink (data) {
  return request({
    url: interfaceMap.commonCorrectTaskLink,
    method: 'post',
    data
  })
}
// 删除措施行动项
export function commonCorrectTaskUnLink (data) {
  return request({
    url: interfaceMap.commonCorrectTaskUnLink,
    method: 'post',
    data
  })
}

export function renderFaultForm (parameter) {
  return request({
    url: interfaceMap.renderFaultForm,
    method: 'post',
    data: parameter
  })
}
// 获取问题记录列表
export function getProblemList (parameter) {
  return request({
    url: interfaceMap.getProblemList,
    method: 'post',
    data: parameter
  })
}
// 双五归零首页启动单
export function qzeroStart (parameter) {
  return request({
    url: interfaceMap.qzeroStart,
    method: 'post',
    data: parameter
  })
}
// 信息报告
export function pageInfoReportView (parameter) {
  return request({
    url: interfaceMap.pageInfoReportView,
    method: 'post',
    data: parameter
  })
}
// 事件筛选
export function pageScreenJudgeView (parameter) {
  return request({
    url: interfaceMap.pageScreenJudgeView,
    method: 'post',
    data: parameter
  })
}
// 事件调查
export function pageEventSurveyView (parameter) {
  return request({
    url: interfaceMap.pageEventSurveyView,
    method: 'post',
    data: parameter
  })
}
// 措施落实
export function pageMeasureExecuteView (parameter) {
  return request({
    url: interfaceMap.pageMeasureExecuteView,
    method: 'post',
    data: parameter
  })
}
// 故障件信息
export function pageFaultInformationView (parameter) {
  return request({
    url: interfaceMap.pageFaultInformationView,
    method: 'post',
    data: parameter
  })
}
// 创建记录
export function createProblem (parameter) {
  return request({
    url: interfaceMap.createProblem,
    method: 'post',
    data: parameter
  })
}

// 获取记录详情
export function getProblemDetail (params) {
  return request({
    url: interfaceMap.getProblemDetail + params.id,
    method: 'POST',
    data: params
  })
}

// 修改记录详情
export function editProblem (params) {
  return request({
    url: interfaceMap.editProblem,
    method: 'POST',
    data: params
  })
}

// 删除记录
export function removeProblem (params) {
  return request({
    url: interfaceMap.removeProblem,
    method: 'POST',
    data: params
  })
}

// 获取问题记录导入配置
export function getProblemImportConfig (params) {
  return request({
    url: interfaceMap.getProblemImportConfig + params.typeCode,
    method: 'POST',
    data: params
  })
}

// 设置问题记录导入配置
export function setProblemImportConfig (params) {
  return request({
    url: interfaceMap.setProblemImportConfig,
    method: 'POST',
    data: params
  })
}

// 导入问题记录
export function importProblem (params) {
  return request({
    url: interfaceMap.importProblem,
    method: 'POST',
    data: params
  })
}

// 导出问题记录
export function exportProblem (params) {
  return request({
    url: interfaceMap.exportProblem,
    method: 'POST',
    data: params
  })
}

// 导出全部问题记录
export function exportAllProblem (params) {
  return request({
    url: interfaceMap.exportAllProblem,
    method: 'POST',
    data: params
  })
}

export function getQualityProblemsType (parameter) {
  return request({
    url: interfaceMap.getQualityProblemsType,
    method: 'get',
    params: parameter
  })
}

// 全面质量管理大屏数据
export function selectLargeScreenShow (params) {
  return request({
    url: interfaceMap.selectLargeScreenShow,
    method: 'post',
    data: params
  })
}
// 型号
export function selectNumByAircraftModel (parameter) {
  return request({
    url: interfaceMap.selectNumByAircraftModel,
    method: 'post',
    data: parameter
  })
}
// 问题来源
export function selectNumByIssueSource (parameter) {
  return request({
    url: interfaceMap.selectNumByIssueSource,
    method: 'post',
    data: parameter
  })
}
// 问题状态
export function selectNumByIssueState (parameter) { // selectNumByIssueRespUnit
  return request({
    url: interfaceMap.selectNumByIssueState,
    method: 'post',
    data: parameter
  })
}
// 责任单位
export function selectNumByIssueRespUnit (parameter) {
  return request({
    url: interfaceMap.selectNumByIssueRespUnit,
    method: 'post',
    data: parameter
  })
}

// 获取ATA章节
export function getAtaPSection (parameter) {
  return request({
    url: interfaceMap.getQualityProblemsType,
    method: 'get',
    params: parameter
  })
}

// 获取飞机序列号
export function getVehicles (parameter) {
  return request({
    url: interfaceMap.getVehicles,
    method: 'post',
    data: parameter
  })
}

// 获取原因代码
export function getCauseAnalysis (parameter) {
  return request({
    url: interfaceMap.getCauseAnalysis,
    method: 'post',
    data: parameter
  })
}

// 获取原因代码的子集(过滤)
export function filterGetCauseChild (parameter) {
  return request({
    url: interfaceMap.filterGetCauseChild,
    method: 'post',
    data: parameter
  })
}

// 获取原因代码的子集
export function getCauseChildAnalysis (parameter) {
  return request({
    url: interfaceMap.getCauseChildAnalysis + parameter.ID,
    method: 'post',
    data: parameter
  })
}

// 数据字典动态获取配置
export function getConfig (url, parameter) {
  return request({
    url,
    method: 'get',
    params: parameter
  })
}

// 动态的保存接口
export function saveDict (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

// 动态的加载字典表格
export function loadDictData (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

// 动态加载子集
export function loadChildDictData (url, parameter) {
  return request({
    url: url + parameter.id,
    method: 'post',
    data: parameter
  })
}

// 动态加载子集,9.28修改接口,问题管控 问题过程管理 左侧树点开,只展示为true的
export function filterChildDictData (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}
// 动态详情接口
export function getDictDetail (url, parameter) {
  return request({
    url: url + parameter.id,
    method: 'post',
    data: parameter
  })
}

// 动态删除接口
export function removeDictData (url, parameter) {
  return request({
    url,
    method: 'delete',
    data: parameter
  })
}

// 动态更新状态
export function updateDictState (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}

export function getProblemProcessData (parameter) {
  return request({
    url: interfaceMap.getProblemProcessData,
    method: 'post',
    data: parameter
  })
}
export function getSourceProblemData (parameter) {
  return request({
    url: interfaceMap.getSourceProblemData,
    method: 'post',
    data: parameter
  })
}
export function getLocationData (parameter) {
  return request({
    url: interfaceMap.getLocationData,
    method: 'post',
    data: parameter
  })
}
export function getAtaSectionData (parameter) {
  return request({
    url: interfaceMap.getAtaSectionData,
    method: 'post',
    data: parameter
  })
}

export function getChildData (url, parameter) {
  return request({
    url: url + parameter.ID,
    method: 'post',
    data: parameter
  })
}

// 新增产品故障 9.28修改接口,问题来源,发生阶段,发生地点,ATA只展示为true的
export function filterGetChildData (url, parameter) {
  return request({
    url,
    method: 'post',
    data: parameter
  })
}
// 创建质量逃逸目模板
export function createEscapeTel (parameter) {
  return request({
    url: interfaceMap.createEscapeTel,
    method: 'post',
    data: parameter
  })
}

// 创建 地面支援设备问题
export function createAddressSupport (parameter) {
  return request({
    url: interfaceMap.createAddressSupport,
    method: 'post',
    data: parameter
  })
}
// 创建双五归零_中心级
export function createAddqzeroCenter (parameter) {
  return request({
    url: interfaceMap.createAddqzeroCenter,
    method: 'post',
    data: parameter
  })
}

// 地面支持问题 原因分析
export function CauseAnalysisMeasuresMade (parameter) {
  return request({
    url: interfaceMap.CauseAnalysisMeasuresMade,
    method: 'post',
    data: parameter
  })
}
// 运行风险判断
export function OperationalRiskDetermination (parameter) {
  return request({
    url: interfaceMap.OperationalRiskDetermination,
    method: 'post',
    data: parameter
  })
}
// 个人问责
export function PersonalAccountability (parameter) {
  return request({
    url: interfaceMap.PersonalAccountability,
    method: 'post',
    data: parameter
  })
}
// 原因分析与评审
export function FaultCauseAnalysisReview (parameter) {
  return request({
    url: interfaceMap.FaultCauseAnalysisReview,
    method: 'post',
    data: parameter
  })
}
// 确认责任单位
export function FaultDetermineRespUnit (parameter) {
  return request({
    url: interfaceMap.FaultDetermineRespUnit,
    method: 'post',
    data: parameter
  })
}
// 纠正措施及有效性验证
// export function CorrectiveActionValidation (parameter) {
//   return request({
//     url: interfaceMap.CorrectiveActionValidation,
//     method: 'post',
//     data: parameter
//   })
// }

export function InferOtherThingsFromOneFact (parameter) {
  return request({
    url: interfaceMap.InferOtherThingsFromOneFact,
    method: 'post',
    data: parameter
  })
}
// 纠正措施及有效性验证
export function CorrectiveAction (parameter) {
  return request({
    url: interfaceMap.CorrectiveAction,
    method: 'post',
    data: parameter
  })
}

// 通用问题创建
export function getGeneralRender (parameter) {
  return request({
    url: interfaceMap.GeneralRender,
    method: 'post',
    data: parameter
  })
} // 通用问题创建
export function createGeneralProblem (type, parameter) {
  return request({
    url: interfaceMap.GeneralRender,
    method: 'post',
    data: parameter
  })
}

// 通过作业实例ID获取过程的ID
export function getProcessInstanceId (params) {
  return request({
    url: interfaceMap.getProcessInstanceId,
    method: 'get',
    params
  })
}

// 通过过程的节点获取流程组件的实例ID
export function getFlowInstanceId (params) {
  return request({
    url: interfaceMap.getFlowInstanceId,
    method: 'get',
    params
  })
}

// 查看问题过程详情
export function getEscapeDetail (parameter) {
  return request({
    url: interfaceMap.getEscapeDetail + parameter.id,
    method: 'get'
  })
}

// 添加审核计划
export function createAuditPlan (parameter) {
  return request({
    url: interfaceMap.createAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 修改审核计划
export function editAuditPlan (parameter) {
  return request({
    url: interfaceMap.editAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 删除审核计划
export function removeAuditPlan (parameter) {
  return request({
    url: interfaceMap.removeAuditPlan,
    method: 'post',
    data: parameter
  })
}
// 查询审核计划列表
export function queryAuditPlan (parameter) {
  return request({
    url: interfaceMap.queryAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 启动审核计划
export function startAuditPlan (parameter) {
  return request({
    url: interfaceMap.startAuditPlan,
    method: 'post',
    data: parameter
  })
}
// 审核计划作业
export function initAuditPlan (parameter) {
  return request({
    url: interfaceMap.initAuditPlan,
    method: 'post',
    data: parameter
  })
}

// 问题性质
export function getProblemNature (parameter) {
  return request({
    url: interfaceMap.getProblemNature,
    method: 'post',
    data: parameter
  })
}

// 产品故障新增问题记录
export function getProductCreatRecord (params) {
  return request({
    url: interfaceMap.productCreatRecord,
    method: 'post',
    data: params
  })
}

// 日常质量管理问题
export function DailyQualityManagementIssues (params) {
  return request({
    url: interfaceMap.DailyQualityManagementIssues,
    method: 'post',
    data: params
  })
}
// 风险点问题
export function getRiskPoint (params) {
  return request({
    url: interfaceMap.getRiskPoint,
    method: 'post',
    data: params
  })
}

// 转问题
export function getTurnIssue (params) {
  return request({
    url: interfaceMap.getTurnIssue,
    method: 'post',
    data: params
  })
}

// 技术出版物问题
export function tecPubManagement (params) {
  return request({
    url: interfaceMap.tecPubManagement,
    method: 'post',
    data: params
  })
}// 培训问题
export function trainManagement (params) {
  return request({
    url: interfaceMap.trainManagement,
    method: 'post',
    data: params
  })
}
// 满意度
export function satisfactionDegree (params) {
  return request({
    url: interfaceMap.satisfactionDegree,
    method: 'post',
    data: params
  })
}
// 客户投诉
export function complaintMgr (params) {
  return request({
    url: interfaceMap.complaintMgr,
    method: 'post',
    data: params
  })
}

// 例外放行
export function releaseCreate (params) {
  return request({
    url: interfaceMap.releaseCreate,
    method: 'post',
    data: params
  })
}
// 驳回
export function turndownCreate (params) {
  return request({
    url: interfaceMap.turndownCreate,
    method: 'post',
    data: params
  })
}

// 数据字典-发动机型号
export function getEngineModel (params) {
  return request({
    url: interfaceMap.engineModel,
    method: 'post',
    data: params
  })
}
// 判断数据类型
function getDataType (data) {
  const temp = Object.prototype.toString.call(data)
  const type = temp.match(/\b\w+\b/g)
  return (type.length < 2) ? 'Undefined' : type[1]
}
// 判断对象是否相同
export function isObjectChanged (source, comparison) {
  // 由于'Object','Array'都属于可遍历的数据类型，所以我们提前定义好判断方法，方便调用
  const iterable = (data) => ['Object', 'Array'].includes(getDataType(data))

  // 如果源数据不是可遍历数据，直接抛错，主要用于判断首次传入的值是否符合判断判断标准。
  if (!iterable(source)) {
    throw new Error(`source should be a Object or Array , but got ${getDataType(source)}`)
  }

  // 如果数据类型不一致，说明数据已经发生变化，可以直接return结果
  if (getDataType(source) !== getDataType(comparison)) {
    return true
  }

  // 提取源数据的所有属性名
  const sourceKeys = Object.keys(source)

  // 将对比数据合并到源数据，并提取所有属性名。
  // 在这里进行对象合并，首先是要保证 对比数据>=源数据，好处一：后边遍历的遍历过程就不用做缺省判断了。
  const comparisonKeys = Object.keys({
    ...source,
    ...comparison
  })

  // 好处二：如果属性数量不一致说明数据必然发生了变化，可以直接return结果
  if (sourceKeys.length !== comparisonKeys.length) {
    return true
  }

  // 这里遍历使用some，some的特性一旦找到符合条件的值，则会立即return，不会进行无意义的遍历。完美符合我们当前的需求

  return comparisonKeys.some(key => {
    // 如果源数据属于可遍历数据类型，则递归调用
    if (iterable(source[key])) {
      return this.isObjectChanged(source[key], comparison[key])
    } else {
      return source[key] !== comparison[key]
    }
  })
}
export function handleComparisonKeysDelete (sourceArr, deleteArr) {
  const sourceCP = [...sourceArr]
  if (deleteArr.length > 0) {
    deleteArr.forEach((item) => {
      sourceCP.forEach((target, index) => {
        console.log('isObjectChanged', isObjectChanged)
        if (!isObjectChanged(target, item)) {
          sourceCP.splice(index, 1)
        }
      })
    })
  } else {
    notification.error({
      message: '删除数据不能为空',
      description: '请至少选择一条数据！'
    })
  }
  return sourceCP
}
// 深拷贝函数
export function deepCopy (target) {
  let result
  if (target !== null && typeof target === 'object') {
    try {
      if (Array.isArray(target)) {
        result = []
        for (let key = 0; key < target.length; key++) {
          result[key] = deepCopy(target[key])
        }
      } else {
        result = {}
        for (const key in target) {
          result[key] = deepCopy(target[key])
        }
      }
    } catch (e) {
      return result
    }
  } else {
    result = target
  }
  return result
}

// 数据字典-发生阶段
export function getOccurredStage (params) {
  return request({
    url: interfaceMap.getOccurredStage,
    method: 'post',
    data: params
  })
}
export function controlSheet (params) {
  return request({
    url: interfaceMap.controlSheet,
    method: 'post',
    data: params
  })
}
// 双五归零管控单
export function qzeroControlSheet (params) {
  return request({
    url: interfaceMap.qzeroControlSheet,
    method: 'post',
    data: params
  })
}

export function ProblemReportingCriteria (params) {
  return request({
    url: interfaceMap.ProblemReportingCriteria,
    method: 'post',
    data: params
  })
}
export function repetitiveProblem (params) {
  return request({
    url: interfaceMap.repetitiveProblem,
    method: 'post',
    data: params
  })
}

export function getSponsor (params) {
  return request({
    url: interfaceMap.getSponsor,
    method: 'post',
    data: params
  })
}

export function correctiveMeasures (params) {
  return request({
    url: interfaceMap.correctiveMeasures,
    method: 'post',
    data: params
  })
}
export function problemExport (params) {
  return request({
    url: interfaceMap.problemExport,
    method: 'post',
    data: params
  })
}
// controlInsert
export function controlInsert (params) {
  return request({
    url: interfaceMap.controlInsert,
    method: 'post',
    data: params
  })
}
export function controlQuery (params) {
  return request({
    url: interfaceMap.controlQuery,
    method: 'get',
    params
  })
}
// 查看整改单
export function getRectificationInfo (params) {
  return request({
    url: interfaceMap.getRectificationInfo,
    method: 'get',
    params: params
  })
}

// 查看措施反馈单
export function getFeedBackInfo (params) {
  return request({
    url: interfaceMap.getFeedBackInfo,
    method: 'get',
    params: params
  })
}

// 查看修改日志
export function getFeedBackUpdateLog (params) {
  return request({
    url: interfaceMap.getFeedBackUpdateLog,
    method: 'get',
    params: params
  })
}

// 更新措施反馈单
export function updateFeed (data) {
  return request({
    url: interfaceMap.updateFeed,
    method: 'post',
    data
  })
}

// 查看计划通知单
export function getPlanAdviceInfo (params) {
  return request({
    url: interfaceMap.getPlanAdviceInfo,
    method: 'get',
    params: params
  })
}

// 查看问题通知单
export function getProblemAdviceInfo (params) {
  return request({
    url: interfaceMap.getProblemAdviceInfo,
    method: 'get',
    params: params
  })
}
// 合同变更
export function getChangeUrl (params) {
  return request({
    url: interfaceMap.getChangeUrl,
    method: 'get',
    params
  })
}

// 我的待阅分页查询
export function getReadDoList (data) {
  return request({
    url: interfaceMap.getReadDoList,
    method: 'post',
    data
  })
}

// 获取quick BI 报表地址
export function cardGetToken (data) {
  return request({
    url: interfaceMap.cardGetToken,
    method: 'post',
    data
  })
}

export function treeGetTree (data) {
  return request({
    url: interfaceMap.treeGetTree,
    method: 'post',
    data
  })
}
export function treeGetTODOTree (data) {
  return request({
    url: interfaceMap.treeGetTODOTree,
    method: 'post',
    data
  })
}

export function processExport (data) {
  return request({
    url: interfaceMap.processExport,
    method: 'post',
    data
  })
}
export function qzeroExport (data) {
  return request({
    url: interfaceMap.qzeroExport,
    method: 'post',
    data
  })
}

export function getPageQualityEscapesView (data) {
  return request({
    url: interfaceMap.getPageQualityEscapesView,
    method: 'post',
    data
  })
}

// 扩大检查措施工作计划详情查询
export function getProblemBigDetail (data) {
  return request({
    url: '/plan/biz/service/vessel/' + data,
    method: 'get'
  })
}

//
export function getOverdueWarnState (data) {
  return request({
    url: interfaceMap.getOverdueWarnState + data,
    method: 'post'
  })
}
// getBlueLetterNotice
export function getBlueLetterNotice (data) {
  return request({
    url: interfaceMap.getBlueLetterNotice,
    method: 'post',
    data
  })
}

export function pageQualityProcess (data) {
  return request({
    url: interfaceMap.pageQualityProcess,
    method: 'post',
    data
  })
}
// addDoubleFiveReturnToZeroForm: 'quality/api/service/feedBack/link', // 双五归零link新增
//   deleteDoubleFiveReturnToZeroForm: 'quality/api/service/feedBack/unlink ' // 双五归零link删除
export function addFormLink (data) {
  return request({
    url: interfaceMap.addFormLink,
    method: 'post',
    data
  })
}

export function deleteFormLink (data) {
  return request({
    url: interfaceMap.deleteFormLink,
    method: 'post',
    data
  })
}
export function addTechnicalFormLink (data) {
  return request({
    url: interfaceMap.addTechnicalFormLink,
    method: 'post',
    data
  })
}

export function deleteTechnicalFormLink (data) {
  return request({
    url: interfaceMap.deleteTechnicalFormLink,
    method: 'post',
    data
  })
}
export function addRiskPointLink (data) {
  return request({
    url: interfaceMap.addRiskPointLink,
    method: 'post',
    data
  })
}
export function updateRiskPointLink (data) {
  return request({
    url: interfaceMap.updateRiskPointLink,
    method: 'post',
    data
  })
}

export function deleteMeasureLink (parameter) {
  return request({
    url: interfaceMap.deleteMeasureLink,
    method: 'delete',
    params: parameter
  })
}
// 删除行列
export function deleteRiskPointUnLink (parameter) {
  return request({
    url: interfaceMap.deleteRiskPointUnLink,
    method: 'delete',
    params: parameter
  })
}

// 获取管控措施link
export function getTargetBySource (params) {
  return request({
    url: interfaceMap.getTargetBySource,
    method: 'get',
    params: params
  })
}
// 获取风险因子分解link
export function getMeasureProgressFeedback (params) {
  return request({
    url: interfaceMap.getMeasureProgressFeedback,
    method: 'get',
    params: params
  })
}

// 双五归零
export function getQzeroToDotree (data) {
  return request({
    url: interfaceMap.getQzeroToDotree,
    method: 'post',
    data
  })
}

/** 风险管控 */
// 查询详情
export function qryByObjId (parameter) {
  return request({
    url: interfaceMap.qryByObjId,
    method: 'get',
    params: parameter
  })
}

// 删除行列
export function delLinkObj (parameter) {
  return request({
    url: interfaceMap.delLinkObj,
    method: 'delete',
    params: parameter
  })
}

// 查询行列
export function qryLinkObj (parameter) {
  return request({
    url: interfaceMap.qryLinkObj,
    method: 'get',
    params: parameter
  })
}

// 添加风险因子行列
export function linkRiskFactor (data) {
  return request({
    url: interfaceMap.linkRiskFactor,
    method: 'post',
    data
  })
}

// 启动作业
export function createAssignment (data) {
  return request({
    url: interfaceMap.createAssignment,
    method: 'post',
    data
  })
}
// engineTechCreat创建工程技术问题
export function engineTechCreat (data) {
  return request({
    url: interfaceMap.engineTechCreat,
    method: 'post',
    data
  })
}
// getTwoProTechData二级专业
export function getTwoProTechData (data) {
  return request({
    url: interfaceMap.getTwoProTechData,
    method: 'post',
    data
  })
}
// 获取积分
export function totalScore (parameter) {
  return request({
    url: interfaceMap.totalScore,
    method: 'get',
    params: parameter

  })
}
