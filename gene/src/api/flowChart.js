import request from '@/utils/request'
// import axios from 'axios'
/**
 * 流程图数据
 * @param {Object} parameter
 * @returns
 */
export function diagramAndInfo(parameter) {
  return request({
    url: '/flow-platform/api/v1.0/public/processInstance',
    method: 'post',
    data: parameter
  })
}

/**
* 流程图模板数据
* @param {Object} parameter
* @returns
*/
export function getBlankFlowChart(parameter) {
  return request({
    url: '/flow-platform/model/detail',
    method: 'post',
    data: parameter
  })
}

/**
 * 实例监控-流转记录列表查询
 * @param {Object} parameter
 * @returns
 */
export function getFlowRecords(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getFlowRecords',
    method: 'get',
    params
  })
}

// 获取节点候选人列表
export function getCandidateInfoList(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getChildCandidate',
    method: 'post',
    data: params
  })
}

/**
* 指定下一节点处理人
* @param {Object} parameter
* @returns
*/
export function nextNodeAssignee(parameter) {
  return request({
    url: '/flow-platform/task/nextNodeAssignee',
    method: 'post',
    data: parameter
  })
}

/**
* 获取当前节点
* @param {Object} parameter
* @returns
*/
export function getCurrentNodeCandidateInfoList(data) {
  const { nodeCode = '', processInstanceId = '' } = data
  return request({
    url: `/flow-platform/api/v1.0/public/getNodeCandidateInfoList?processInstanceId=${processInstanceId}&nodeCode=${nodeCode}`,
    method: 'post',
    data: {}
  })
}

/**
* 回退节点
* @param {Object} parameter
* @returns
*/
export function rollback(url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

// 查询人员信息
export function selectEmployeeList(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/selectEmployeeList',
    method: 'post',
    data: params
  })
}

// 查询角色信息
export function getUserRolePageList(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getUserRolePageList',
    method: 'post',
    data: params
  })
}

// 查询用户组信息
export function getUserGroupPageList(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getUserGroupPageList',
    method: 'post',
    data: params
  })
}

// 查询岗位信息
export function getUserPositionPageList(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getUserPositionPageList',
    method: 'post',
    data: params
  })
}

// 获取上次设置的候选人列表（已废弃）
// let getLastCandidatePrev, getLastCandidateCancelToken, getLastCandidateRequestSource

// export function getLastCandidate (data) {
//   const { nodeCode = '', taskId = '' } = data
//   if (getLastCandidatePrev && getLastCandidateRequestSource) {
//     getLastCandidateRequestSource.cancel()
//   }
//   getLastCandidateCancelToken = axios.CancelToken
//   getLastCandidateRequestSource = getLastCandidateCancelToken.source()
//   getLastCandidatePrev = request({
//     url: `/flow-platform/api/v1.0/public/getLastCandidate?taskId=${taskId}&nodeCode=${nodeCode}`,
//     method: 'post',
//     data: {},
//     cancelToken: getLastCandidateRequestSource.token
//   }).then(res => {
//     getLastCandidatePrev = null
//     getLastCandidateCancelToken = null
//     getLastCandidateRequestSource = null
//     return res
//   }).catch(() => {
//     getLastCandidatePrev = null
//     getLastCandidateCancelToken = null
//     getLastCandidateRequestSource = null
//   }).finally(() => {
//     getLastCandidatePrev = null
//     getLastCandidateCancelToken = null
//     getLastCandidateRequestSource = null
//   })
//   return getLastCandidatePrev
// }

/**
* 关闭流程
* @param {Object} parameter
* @returns
*/
export function veto(url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}

/**
 * 响应历史
 * @param {Object} parameter
 * @returns
 */
export function history(params) {
  return request({
    url: '/flow-platform/api/v1.0/public/getFlowRecords',
    method: 'get',
    params
  })
}

// 获取上次设置的候选人列表
export function getAllLastCandidate(data) {
  const { processInstanceId = '' } = data
  return request({
    url: `/flow-platform/api/v1.0/public/getAllLastCandidate?processInstanceId=${processInstanceId}`,
    method: 'post',
    data: {}
  })
}
// 获取节点上次设置的候选人列表
export function getLastCandidate (data) {
  const { taskId = '', nodeCode = '' } = data
  return request({
    url: `/flow-platform/api/v1.0/public/getLastCandidate?nodeCode=${nodeCode}&taskId=${taskId}`,
    method: 'post',
    data: {}
  })
}
// 获取节点上次设置的候选人列表
export function getLastCandidateOrOperator (data) {
  const { processInstanceId = '', nodeCode = '' } = data
  return request({
    url: `/flow-platform/api/v1.0/public/getLastCandidateOrOperator?nodeCode=${nodeCode}&processInstanceId=${processInstanceId}`,
    method: 'post',
    data: {}
  })
}
