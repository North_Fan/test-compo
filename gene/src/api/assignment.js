import request from '@/utils/request'

// 任务单关注接口
export function planCheck (taskId) {
  return request({
    url: `/assignment/biz/service/flow/next/node/task/${taskId}`,
    method: 'get'
  })
}

// 获取待办详情 Mock接口
export function queryWfDetailMock (params) {
  return request({
    url: '/assignment/biz/service/instance/wf/todo/mock',
    method: 'get',
    params
  })
}
// 作业设置变量
export function setTaskVariable (data) {
  return request({
    url: '/assignment/biz/service/flow/setTaskVariable',
    method: 'post',
    data
  })
}
// 作业设置变量
export function getAssignmentInfoByNodeCode (data) {
  return request({
    url: '/assignment/biz/service/instance/children/by/process',
    method: 'post',
    data
  })
}

// 转办
export function transferApi (url, data) {
  return request({
    url,
    method: 'post',
    data
  })
}
