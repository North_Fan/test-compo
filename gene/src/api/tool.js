import request from '@/utils/request'

const interfaceMap = {
  getObjectMetas: '/object-component/api/object-metas'
}
export default interfaceMap

// 获取类型编号和排序号
export function getObjectMetas (params) {
  return request({
    url: interfaceMap.getObjectMetas + '/' + params,
    method: 'get'
  })
}

 export const delayTime = 3000
