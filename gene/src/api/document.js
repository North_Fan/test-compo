import request from '@/utils/request'

const interfaceMap = {
  getCodeAndOrderNum: '/document/biz/service/type/getCodeAndOrderNum',
  getHlepCodeAndOrderNum: '/document/biz/service/helpType/getCodeAndOrderNum',
  addGroup: '/document/service/type/addGroup',
  getTreeData: '/document/biz/service/subject/getSubjectTree',
  getSubjectNoList: '/document/biz/service/subjectNumber/list',
  initSubjectNo: '/document/biz/service/subjectNumber/initInfo',
  saveSubjectNo: '/document/biz/service/subjectNumber/save',
  editSubjectNo: '/document/biz/service/subjectNumber/edit',
  deleteSubjectNo: '/document/biz/service/subjectNumber/delete',
  exportSubjectNo: '/document/biz/service/subjectNumber/export',
  justifySubjectNo: '/document/biz/service/subjectNumber/justifyNumber',
  getChildrenData: '/documents/getChildrenData',
  getSubjectFile: '/document/biz/service/subject/render',
  getFileList: '/filenode/biz/service/cmos-doc-node/materials',
  getInstanceInfo: '/document/biz/service/common/getInstanceInfo',
  getCommonFile: '/document/biz/service/common/render',
  getCommonInitFile: '/document/biz/service/common/initInfo',
  getCommonDetailInfo: '/document/biz/service/common/queryByObjId',
  getCommonDocumentChildrenList: '/document/biz/service/common/list',
  getCommonVolumeList: '/document/biz/service/common/volume/getVolumeList',
  getCommonAddVolumeFile: '/document/biz/service/common/volume/render',
  getCommonAddVolumeInitFile: '/document/biz/service/common/volume/initInfo',
  getCommonUpgradeFile: '/document/biz/service/common/upgrade/render',
  getCommonUpgradeInitFile: '/document/biz/service/common/upgrade/initInfo',
  getCommonUpgradeDetailInfo: '/document/biz/service/common/upgrade/queryByObjId',
  getCommonReissueFile: '/document/biz/service/common/reissue/render',
  getCommonReissueInitFile: '/document/biz/service/common/reissue/initInfo',
  queryRessiueCountByObjId: '/document/biz/service/common/queryRessiueCountByObjId',
  queryRessiueCountByCountAndObjId: '/document/biz/service/common/queryRessiueCountByCountAndObjId',
  getCommonAdvanceFile: '/document/biz/service/common/alter/render',
  getCommonAdvanceInitFile: '/document/biz/service/common/alter/initInfo',
  getCommonAdvanceDetailInfo: '/document/biz/service/common/alter/queryByObjId',
  invalidCommonFile: '/document/biz/service/common/invalid/render',
  getInvalidCommonFileInfo: '/document/biz/service/common/invalid/initInfo',
  getDistributionRoute: '/document/biz/service/common/getInnerDistribute',
  getCompanyInitInfo: '/document/biz/service/company/approve/initCode',
  getCompanyStandardDetailInfo: '/document/biz/service/company/approve/detailByObjId',
  getCompanyStandardFile: '/document/biz/service/company/approve/render',
  getInvalidCompanyStandardFile: '/document/biz/service/company/invalid/reissue/render',
  getCompanyStandardFileList: '/document/biz/service/company/approve/list',
  getCompanyStandardFileDetail: '/document/biz/service/company/approve/detail',
  getDocTableData: '/document/biz/service/type/list',
  getHelpTableData: '/document/biz/service/helpType/list',
  getSubTableData: '/document/biz/service/subject/list',
  getCommonDocumentLists: '/document/biz/service/common/list',
  getCompanyStandardData: '/documents/getCompanyStandardData',
  getExternalStandardData: '/documents/getExternalStandardData',
  getSubList: '/document/biz/service/type/subList',
  getHelpSubList: '/document/biz/service/helpType/subList',
  addItem: '/document/biz/service/type/addItem',
  addHelpItem: '/document/biz/service/helpType/addItem',
  getItemDetail: '/document/biz/service/type/itemDetail?id=',
  getHelpItemDetail: '/document/biz/service/helpType/itemDetail?id=',
  itemUpdate: '/document/biz/service/type/itemUpdate',
  itemHelpUpdate: '/document/biz/service/helpType/itemUpdate',
  itemDelete: '/document/biz/service/type/itemDelete?id=',
  itemHelpDelete: '/document/biz/service/helpType/itemDelete?id=',
  getDocumentList: '/document/biz/service/search',
  exportFile: '/document/biz/service/export',
  standardExport: '/document/biz/service/standard/exportWithQuery',
  getStandardList: '/document/biz/service/standard/standardList',
  getHelpFileList: '/document/biz/service/helpFile/list',
  SaveStandard: '/document/biz/service/standard/save',
  SaveHelpStandard: '/document/biz/service/helpFile/save',
  getStandardContentList: '/document/biz/service/company/approve/list',
  getStandardDetail: '/document/biz/service/standard/detail',
  getHelpDetail: '/document/biz/service/helpFile/detail',
  updateStandard: '/document/biz/service/standard/update',
  updateHelpData: '/document/biz/service/helpFile/update',
  deleteStandard: '/document/biz/service/standard/delete',
  deleteHelp: '/document/biz/service/helpFile/delete',
  getCommonTypeSubList: '/document/biz/service/getCommonTypeSubList',
  initCode: '/document/biz/service/company/approve/initCode',
  getDocTypeByCode: '/document/biz/service/getDocTypeByCode',
  getDefaultInnerDistribute: '/document/biz/service/general/getDefaultInnerDistribute',
  getInnerDistributeByObjId: '/document/biz/service/general/getInnerDistributeByObjId',
  getReissuedSupplierData: '/document/biz/service/common/reissuedSupplier/list',
  outSubjectInfo: '/document/biz/service/subject/initInfo',
  justifyReplaceNo: '/document/biz/service/subject/justifyReplaceNo',
  getSupplierReissueFile: '/document/biz/service/common/reissuedSupplier/render',
  getSupplierReissueInitInfo: '/document/biz/service/common/reissuedSupplier/initInfo',
  extStandardList: '/document/biz/service/extStandard/extStandardList',
  extStandardSave: '/document/biz/service/extStandard/addExtStandard',
  extStandardDelete: '/document/biz/service/extStandard/deleteExtStandard',
  extStandardQueryByObjId: '/document/biz/service/extStandard/queryByObjId',
  extStandardImport: '/document/biz/service/extStandard/import',
  extStandardExport: '/document/biz/service/extStandard/export',
  getLifecycle: '/document/biz/service/base/getLifecycleStatusByTemplateCode?templateCode=',
  formManagementList: '/document/biz/service/formManagement/formManagementList',
  formManagementAdd: '/document/biz/service/formManagement/addForm',
  formManagementDelete: '/document/biz/service/formManagement/deleteForm',
  formManagementDetail: '/document/biz/service/formManagement/detailFormManagement',
  formManagementDownload: '/document/biz/service/formManagement/export',
  justifyProcess: '/document/biz/service/subject/justifyProcess',
  getObjectByObjId: '/document/biz/service/base/searchObject',
  getOrgNameByOrgId: '/document/biz/service/base/searchUserOrDept',
  checkOrg: '/document/biz/service/company/approve/checkOrg',
  structuredDocument: '/document/biz/service/structuredDocument/detail',
  generateWatermarkFile: '/document/biz/service/base/generateWatermarkFile',
  judgeEffectiveVersion: '/document/biz/service/base/judgeEffectiveVersion',
  getProfessionDutyList: '/document/biz/service/professionDuty/getProfessionDutyList',
  saveStructuredDocument: '/document/biz/service/structuredDocument/save',
  enterpriseNormsList: '/document/biz/service/enterpriseNorms/enterpriseNormsList',
  downloadWord: '/document/biz/service/structuredDocument/downloadWord?ossId=',
  standardCreator: '/document/biz/service/base/standardCreator',
  searchDictLabel: '/document/biz/service/base/searchDictLabel',
  downloadPermission: '/document/biz/service/base/downloadPermission',
  downCommonFile: '/document/biz/service/systemFeedBack/export',
  getGeneralBidReview: '/document/biz/service/commonDownload/export',
  downLookBackCommonFile: '/document/biz/service/systemDocument/check/export',
  downLoadAuth: '/document/biz/service/permission/downLoad',
  standardLevelList: '/document/biz/service/standard/standardLevelList',
  getDocTypeByCodeNotContainChild: '/document/biz/service/getDocTypeByCodeNotContainChild',
  addExtStandardList: '/document/biz/service/extStandard/addExtStandardList',
  generateOperateLog: '/document/biz/service/base/generateOperateLog',
  getSupList: '/document/biz/service/company/distribute/getSupList',
  getNormalDetails: '/document/biz/service/enterpriseNorms/details',
  canStartProcess: '/document/biz/service/subject/canStartProcess',
  addRelatedBusinessList: '/document/biz/service/relationBusinessDomain/updateExtStandardList',
  getReissueSupplierRecordsByObjIdAndCount: '/document/biz/service/company/distribute/getReissueSupplierRecordsByObjIdAndCount',
  getAllCountByObjId: '/document/biz/service/company/distribute/getAllCountByObjId',
  syncDistribute: '/document/biz/service/common/syncDistribute',
  getSubjectDistribute: '/document/biz/service/general/getSubjectDistribute',
  officialPolicyDocList: '/document/biz/service/officialPolicyDoc/list',
  officialPolicyDocDetail: '/document/biz/service/officialPolicyDoc/queryByObjId',
  officialPolicyDocAdd: '/document/biz/service/officialPolicyDoc/add',
  officialPolicyDocAddList: '/document/biz/service/officialPolicyDoc/addList',
  officialPolicyDocDelete: '/document/biz/service/officialPolicyDoc/delete',
  officialPolicyDocAllDelete: '/document/biz/service/officialPolicyDoc/phyDelete',
  officialPolicyDocExport: '/document/biz/service/officialPolicyDoc/export',
  officialPolicyDocV2: '/document/biz/service/officialPolicyDocV2/export',
  officialPolicyDocImport: '/document/biz/service/officialPolicyDoc/import',
  getDomainDetailByOnlyCode: '/document/biz/service/common/apply/getDomainDetailByOnlyCode/',
  getSerialNumberByNumber: '/document/biz/service/subject/getSerialNumberByNumber',
  getAllSupplierByObjId: '/document/biz/service/common/reissuedSupplier/getAllSupplierByObjId',
  getInvalidSupplierReissueFile: '/document/biz/service/common/invalid/reissue/render',
  getComVolumeInitCode: '/document/biz/service/company/approve/volume/initCode', /* 企业标准分册标准编号初始化 */
  getComVolumeQueryByObjId: '/document/biz/service/company/approve/volume/queryByObjId' /* 企业标准 分册 详情 */,
  getCompanyStandardVolume: '/document/biz/service/company/approve/volume/render' /* 企业标准 新建分册 */,
  getSAMCList: '/document/biz/service/samc/form/list',
  querySupplier: '/document/biz/service/samc/form/querySupplier',
  supplyReceivePage: '/document/biz/service/samc/form/supplyReceivePage',
  exportSupplyReceive: '/document/biz/service/samc/form/exportSupplyReceive',
  distributeSAMC: '/document/biz/service/samc/form/distribute',
  batchGetFiles: '/document/biz/service/data/repair/tool/batchGetFiles',
  getDeptAndUnit: '/document/biz/service/base/searchUserOrDeptByCookie',
  hasNewVersion: '/document/biz/service/common/hasNewVersion'

}
export default interfaceMap

// 受控表格列表接收+SR 查询
export function batchGetFiles (params) {
  return request({
    url: interfaceMap.batchGetFiles,
    method: 'post',
    data: params
  })
}
// 受控表格列表接收+SR 查询
export function supplyReceivePage (params) {
  return request({
    url: interfaceMap.supplyReceivePage,
    method: 'post',
    data: params
  })
}
// 受控表格列表接收+SR 导出
export function exportSupplyReceive (params) {
  return request({
    url: interfaceMap.exportSupplyReceive,
    method: 'post',
    data: params
  })
}

// 受控表格列表
export function getSAMCList (params) {
  return request({
    url: interfaceMap.getSAMCList,
    method: 'post',
    data: params
  })
}
// 供应商分发查询
export function querySupplier (params) {
  return request({
    url: interfaceMap.querySupplier,
    method: 'post',
    data: params
  })
}
// 受控表格发放
export function distributeSAMC (params) {
  return request({
    url: interfaceMap.distributeSAMC,
    method: 'post',
    data: params
  })
}
// 通用文件子列表
export function getCommonDocumentChildrenList (params) {
  return request({
    url: interfaceMap.getCommonDocumentChildrenList,
    method: 'post',
    data: params
  })
}
// 企业标准 新建分册
export function getComVolumeInitCode (params) {
  return request({
    url: interfaceMap.getComVolumeInitCode,
    method: 'post',
    data: params
  })
}// 企业标准分册标准编号初始化
export function getCompanyStandardVolume (params) {
  return request({
    url: interfaceMap.getCompanyStandardVolume,
    method: 'post',
    data: params
  })
}
// 企业标准分册 详情
export function getComVolumeQueryByObjId (params) {
  return request({
    url: interfaceMap.getComVolumeQueryByObjId,
    method: 'post',
    data: params
  })
}

// 通用文件列表
export function getCommonDocumentLists (params) {
  return request({
    url: interfaceMap.getCommonDocumentLists,
    method: 'post',
    data: params
  })
}

// 获取外部标准数据
export function getExternalStandardData (params) {
  return request({
    url: interfaceMap.getExternalStandardData,
    method: 'get',
    params: params
  })
}

// 获取公司标准数据
export function getCompanyStandardData (params) {
  return request({
    url: interfaceMap.getCompanyStandardData,
    method: 'get',
    params: params
  })
}

// 获取类型编号和排序号
export function getCodeAndOrderNum (params) {
  return request({
    url: interfaceMap.getCodeAndOrderNum,
    method: 'post',
    data: params
  })
}

// 获取帮助类型编号和排序号
export function getHlepCodeAndOrderNum (params) {
  return request({
    url: interfaceMap.getHlepCodeAndOrderNum,
    method: 'post',
    data: params
  })
}
// 新增类型组
export function addGroup (params) {
  return request({
    url: interfaceMap.addGroup,
    method: 'post',
    data: params
  })
}

// 获取文档类型表格数据
export function getDocTableData (params) {
  return request({
    url: interfaceMap.getDocTableData,
    method: 'post',
    data: params
  })
}

// 获取帮助文档类型表格数据
export function getHelpTableData (params) {
    return request({
      url: interfaceMap.getHelpTableData,
      method: 'post',
      data: params
    })
  }

// 获取课题文件树数据
export function getTreeData (params) {
  return request({
    url: interfaceMap.getTreeData,
    method: 'post',
    data: params
  })
}

// 获取课题文件树子文件
export function getChildrenData (params) {
  return request({
    url: interfaceMap.getChildrenData,
    method: 'get'
  })
}

// 获取课题编号列表
export function getSubjectNoList (params) {
  return request({
    url: interfaceMap.getSubjectNoList,
    method: 'post',
    data: params
  })
}

// 初始化课题编号
export function initSubjectNo (params) {
  return request({
    url: interfaceMap.initSubjectNo,
    method: 'post',
    data: params
  })
}

// 保存课题编号
export function saveSubjectNo (params) {
  return request({
    url: interfaceMap.saveSubjectNo,
    method: 'post',
    data: params
  })
}

// 修改课题编号
export function editSubjectNo (params) {
  return request({
    url: interfaceMap.editSubjectNo,
    method: 'post',
    data: params
  })
}

// 删除课题编号
export function deleteSubjectNo (params) {
  return request({
    url: interfaceMap.deleteSubjectNo,
    method: 'post',
    Content: 'application/json',
    data: params
  })
}

// 删除课题编号
export function exportSubjectNo (params) {
  return request({
    url: interfaceMap.exportSubjectNo,
    method: 'post',
    Content: 'application/json',
    data: params
  })
}

// 校验课题编号是否重复
export function justifySubjectNo (params) {
  return request({
    url: `${interfaceMap.justifySubjectNo}?number=${params}`,
    method: 'get'
  })
}

// 获取上传文件源数据，资料组件
export function getFileList (params) {
  return request({
    url: interfaceMap.getFileList,
    method: 'post',
    data: params
  })
}

// 查询审批记录
export function getInstanceInfo (params) {
  return request({
    url: interfaceMap.getInstanceInfo,
    method: 'post',
    data: params
  })
}

// 新建通用文件
export function getCommonFile (params) {
  return request({
    url: interfaceMap.getCommonFile,
    method: 'post',
    data: params
  })
}

// 初始化通用文件的值
export function getCommonInitFile (params) {
  return request({
    url: interfaceMap.getCommonInitFile,
    method: 'post',
    data: params
  })
}

// 初始化通用文件先行更改的值
export function getCommonAdvanceInitFile (params) {
  return request({
    url: interfaceMap.getCommonAdvanceInitFile,
    method: 'post',
    data: params
  })
}

// 初始化通用文件补发的值
export function getCommonReissueInitFile (params) {
  return request({
    url: interfaceMap.getCommonReissueInitFile,
    method: 'post',
    data: params
  })
}

// 获取补发记录
export function queryRessiueCountByObjId (params) {
  return request({
    url: interfaceMap.queryRessiueCountByObjId,
    method: 'post',
    data: params
  })
}

// 获取补发信息
export function queryRessiueCountByCountAndObjId (params) {
  return request({
    url: interfaceMap.queryRessiueCountByCountAndObjId,
    method: 'post',
    data: params
  })
}

// 查看内部分发路线
export function getCompanyInitInfo (params) {
  return request({
    url: interfaceMap.getCompanyInitInfo,
    method: 'post',
    data: params
  })
}

// 初始化公司标准的值
export function getDistributionRoute (params) {
  return request({
    url: interfaceMap.getDistributionRoute,
    method: 'post',
    data: params
  })
}

// 新建标准报批文件
export function getCompanyStandardFile (params) {
  return request({
    url: interfaceMap.getCompanyStandardFile,
    method: 'post',
    data: params
  })
}

// 查询标准报批文件
export function getCompanyStandardFileList (params) {
  return request({
    url: interfaceMap.getCompanyStandardFileList,
    method: 'post',
    data: params
  })
}
// 查询标准报批文件--精确查询接口
export function getCompanyStandardFileDetail (params) {
  return request({
    url: interfaceMap.getCompanyStandardFileDetail,
    method: 'post',
    data: params
  })
}

// 通用文件-新建分册
export function getCommonVolumeList (params) {
  return request({
    url: interfaceMap.getCommonVolumeList,
    method: 'post',
    data: params
  })
}

// 通用文件-新建分册
export function getCommonAddVolumeFile (params) {
  return request({
    url: interfaceMap.getCommonAddVolumeFile,
    method: 'post',
    data: params
  })
}

// 升版通用文件
export function getCommonUpgradeFile (params) {
  return request({
    url: interfaceMap.getCommonUpgradeFile,
    method: 'post',
    data: params
  })
}

// 作废通用文件
export function invalidCommonFile (params) {
  return request({
    url: interfaceMap.invalidCommonFile,
    method: 'post',
    data: params
  })
}

// 获取通用文件作废的详细信息
export function getInvalidCommonFileInfo (params) {
  return request({
    url: interfaceMap.getInvalidCommonFileInfo,
    method: 'post',
    data: params
  })
}

// 获取通用文件的详细信息
export function getCommonDetailInfo (params) {
  return request({
    url: interfaceMap.getCommonDetailInfo,
    method: 'post',
    data: params
  })
}

// 获取公司标准的详细信息
export function getCompanyStandardDetailInfo (params) {
  return request({
    url: interfaceMap.getCompanyStandardDetailInfo,
    method: 'post',
    data: params
  })
}

// 获取通用文件升版后的初始化信息
export function getCommonAddVolumeInitFile (params) {
  return request({
    url: interfaceMap.getCommonAddVolumeInitFile,
    method: 'post',
    data: params
  })
}

// 获取通用文件升版后的初始化信息
export function getCommonUpgradeInitFile (params) {
  return request({
    url: interfaceMap.getCommonUpgradeInitFile,
    method: 'post',
    data: params
  })
}

// 获取通用文件先行更改后的详细信息
export function getCommonAdvanceDetailInfo (params) {
  return request({
    url: interfaceMap.getCommonAdvanceDetailInfo,
    method: 'post',
    data: params
  })
}

// 获取通用文件升版后的详细信息
export function getCommonUpgradeDetailInfo (params) {
  return request({
    url: interfaceMap.getCommonUpgradeDetailInfo,
    method: 'post',
    data: params
  })
}

// 先行更改通用文件
export function getCommonAdvanceFile (params) {
  return request({
    url: interfaceMap.getCommonAdvanceFile,
    method: 'post',
    data: params
  })
}

// 补发通用文件
export function getCommonReissueFile (params) {
  return request({
    url: interfaceMap.getCommonReissueFile,
    method: 'post',
    data: params
  })
}

// 补发供应商通用文件初始化数据  '/document/biz/service/common/reissuedSupplier/initInfo'
export function getSupplierReissueInitInfo (params) {
  return request({
    url: interfaceMap.getSupplierReissueInitInfo,
    method: 'post',
    data: params
  })
}

// 补发供应商通用文件  /document/biz/service/common/reissuedSupplier/render
export function getSupplierReissueFile (params) {
  return request({
    url: interfaceMap.getSupplierReissueFile,
    method: 'post',
    data: params
  })
}
// SR补发供应商通用文件
export function getInvalidSupplierReissueFile (params) {
  return request({
    url: interfaceMap.getInvalidSupplierReissueFile,
    method: 'post',
    data: params
  })
}

// 新建课题文件
export function getSubjectFile (params) {
  return request({
    url: interfaceMap.getSubjectFile,
    method: 'post',
    data: params
  })
}

// 课题文件表格数据
export function getSubTableData (params) {
  return request({
    url: interfaceMap.getSubTableData,
    method: 'post',
    data: params
  })
}

// 课题文件表格子数据
export function getSubList (params) {
  return request({
    url: interfaceMap.getSubList,
    method: 'post',
    data: params
  })
}

// 帮助文档子数据
export function getHelpSubList (params) {
  return request({
    url: interfaceMap.getHelpSubList,
    method: 'post',
    data: params
  })
}
// 添加文档类型
export function addItem (params) {
  return request({
    url: interfaceMap.addItem,
    method: 'post',
    data: params
  })
}

// 添加帮助文档类型
export function addHelpItem (params) {
  return request({
    url: interfaceMap.addHelpItem,
    method: 'post',
    data: params
  })
}

// 文档类型-详情
export function getItemDetail (params) {
  return request({
    url: interfaceMap.getItemDetail + params,
    method: 'post'
  })
}

// 帮助文档类型-详情
export function getHelpItemDetail (params) {
  return request({
    url: interfaceMap.getHelpItemDetail + params,
    method: 'post'
  })
}

// 文档类型-修改
export function itemUpdate (params) {
  return request({
    url: interfaceMap.itemUpdate,
    method: 'post',
    data: params
  })
}

// 帮助文档类型-修改
export function itemHelpUpdate (params) {
  return request({
    url: interfaceMap.itemHelpUpdate,
    method: 'post',
    data: params
  })
}

// 文档类型-删除
export function itemDelete (params) {
  return request({
    url: interfaceMap.itemDelete + params,
    method: 'post'
  })
}

// 帮助文档类型-删除
export function itemHelpDelete (params) {
  return request({
    url: interfaceMap.itemHelpDelete + params,
    method: 'post'
  })
}
// 文档统一搜索
export function getDocumentList (params) {
  return request({
    url: interfaceMap.getDocumentList,
    method: 'post',
    data: params
  })
}

// 文档统一搜索--导出
export function getExportFile (params) {
  return request({
    url: interfaceMap.exportFile,
    method: 'post',
    data: params
  })
}

// 标准规范体系-导出标准
export function getStandardExport (params) {
  return request({
    url: interfaceMap.standardExport,
    method: 'post',
    data: params
  })
}

// 通用文件反馈报表 - 导出反馈信息
export function getFeedbackExport (params) {
  return request({
    url: interfaceMap.downCommonFile,
    method: 'post',
    data: params
  })
}

// 下载通用文件标审消息
export function getGeneralBidReview (params) {
  return request({
    url: interfaceMap.getGeneralBidReview,
    method: 'post',
    data: params
  })
}

// 通用文件制度文件回顾检查报表 - 导出回顾检查信息
export function getLookBackExport (params) {
  return request({
    url: interfaceMap.downLookBackCommonFile,
    method: 'post',
    data: params
  })
}

// 标准规范体系--列表
export function getStandardList (params) {
  return request({
    url: interfaceMap.getStandardList,
    method: 'post',
    data: params
  })
}

// 帮助文档--列表
export function getHelpFileList (params) {
  return request({
    url: interfaceMap.getHelpFileList,
    method: 'post',
    data: params
  })
}

// 标准规范体系-- 保存
export function SaveStandard (params) {
  return request({
    url: interfaceMap.SaveStandard,
    method: 'post',
    data: params
  })
}
// 帮助文档-- 保存
export function SaveHelpStandard (params) {
  return request({
    url: interfaceMap.SaveHelpStandard,
    method: 'post',
    data: params
  })
}

// 标准规范体系--添加标准--选择标准
export function getStandardContentList (params) {
  return request({
    url: interfaceMap.getStandardContentList,
    method: 'post',
    data: params
  })
}

// 标准规范体系--详情
export function getStandardDetail (params) {
  return request({
    url: interfaceMap.getStandardDetail,
    method: 'post',
    data: params
  })
}
// 帮助文档--详情
export function getHelpDetail (params) {
  return request({
    url: interfaceMap.getHelpDetail,
    method: 'post',
    data: params
  })
}

// 标准规范体系--修改
export function updateStandard (params) {
  return request({
    url: interfaceMap.updateStandard,
    method: 'post',
    data: params
  })
}
// 帮助文档--修改
export function updateHelpData (params) {
  return request({
    url: interfaceMap.updateHelpData,
    method: 'post',
    data: params
  })
}

// 标准规范体系-- 删除
export function deleteStandard (params) {
  return request({
    url: interfaceMap.deleteStandard,
    method: 'post',
    data: params
  })
}

// 帮助文档-- 删除
export function deleteHelp (params) {
  return request({
    url: interfaceMap.deleteHelp,
    method: 'post',
    data: params
  })
}

// 预览文件获取二进制流 oss
export function getFileBlobPreviewFileOss (docId) {
  return request({
    url: `/filenode/biz/service/cmos-doc-node/normal/online-preview/${docId}`,
    responseType: 'blob',
    method: 'get'
  })
}

// 预览文件获取文件地址
export function getFileUrlPreviewFileOss (docId) {
  return request({
    url: `/filenode/biz/service/ftp-preview/get/url/${docId}`,
    method: 'get'
  })
}

// 删除预览文件
export function deletePreviewFile (filename) {
  return request({
    url: `/filenode/biz/service/ftp-preview/delete/${filename}`,
    method: 'delete'
  })
}

// 预览文件获取临时路径 wps
export function getFileBlobPreviewFile (oddId) {
  return request({
    url: `/filenode/biz/service/file-preview/get/url/${oddId}`,
    method: 'get'
  })
}

// 文档统一搜索-获取通用文件类型
export function getCommonTypeSubList () {
  return request({
    url: interfaceMap.getCommonTypeSubList,
    method: 'post'
  })
}

// 获取标准编码
export function initCode (params) {
  return request({
    url: interfaceMap.initCode,
    method: 'post',
    data: params
  })
}

// 获取标准编码
export function getDocTypeByCode (params) {
  return request({
    url: `${interfaceMap.getDocTypeByCode}/${params}`,
    method: 'get'
  })
}

// 获取公司内部分发路线（默认）
export function getDefaultInnerDistribute () {
  return request({
    url: `${interfaceMap.getDefaultInnerDistribute}`,
    method: 'get'
  })
}

// 获取公司内部分发路线（实例）
export function getInnerDistributeByObjId (params) {
  return request({
    url: `${interfaceMap.getInnerDistributeByObjId}`,
    method: 'post',
    data: params
  })
}
// 获取公司内部分发路线（实例）
export function getInvalidCompanyStandardFile (params) {
  return request({
    url: `${interfaceMap.getInvalidCompanyStandardFile}`,
    method: 'post',
    data: params
  })
}
// 供应商分发路线详情数据：getReissuedSupplierData
export function getReissuedSupplierData (params) {
    return request({
      url: interfaceMap.getReissuedSupplierData,
      method: 'post',
      data: params
    })
  }

// 新建外协初始化
export function outSubjectInfo (params) {
  return request({
    url: `${interfaceMap.outSubjectInfo}`,
    method: 'post',
    data: params
  })
}

// 代替号查询
export function justifyReplaceNo (params) {
  return request({
    url: `${interfaceMap.justifyReplaceNo}?replaceNo=${params}`,
    method: 'get'
  })
}

// 外部标准列表
export function extStandardList (params) {
  return request({
    url: `${interfaceMap.extStandardList}`,
    method: 'post',
    data: params
  })
}

// 外部标准新增
export function extStandardSave (params) {
  return request({
    url: `${interfaceMap.extStandardSave}`,
    method: 'post',
    data: params
  })
}
// 外部标准新增
export function extStandardDelete (params) {
  return request({
    url: `${interfaceMap.extStandardDelete}`,
    method: 'post',
    data: params
  })
}
// 外部标准详情
export function extStandardQueryByObjId (params) {
  return request({
    url: `${interfaceMap.extStandardQueryByObjId}`,
    method: 'post',
    data: params
  })
}

// 外部标准导入
export function extStandardImport (params) {
  return request({
    url: `${interfaceMap.extStandardImport}?ossId=${params}`,
    method: 'get'
  })
}

// 外部标准导入
export function extStandardExport (params) {
  return request({
    url: `${interfaceMap.extStandardExport}`,
    method: 'post',
    data: params
  })
}

export function getLifecycle (code) {
  return request({
    url: interfaceMap.getLifecycle + code,
    method: 'post'
  })
}

// 表单列表
export function formManagementList (params) {
  return request({
    url: `${interfaceMap.formManagementList}`,
    method: 'post',
    data: params
  })
}

// 表单新增
export function formManagementAdd (params) {
  return request({
    url: `${interfaceMap.formManagementAdd}`,
    method: 'post',
    data: params
  })
}
// 表单删除
export function formManagementDelete (params) {
  return request({
    url: `${interfaceMap.formManagementDelete}`,
    method: 'post',
    data: params
  })
}
// 表单详情
export function formManagementDetail (params) {
  return request({
    url: `${interfaceMap.formManagementDetail}`,
    method: 'post',
    data: params
  })
}

// 下载
export function formManagementDownload (params) {
  return request({
    url: `${interfaceMap.formManagementDownload}`,
    method: 'post',
    data: params
  })
}

// 是否有流程进行中
export function justifyProcess (params) {
  return request({
    url: `${interfaceMap.justifyProcess}`,
    method: 'post',
    data: params
  })
}

// 获取课题文件信息
export function getObjectByObjId (params) {
  return request({
    url: `${interfaceMap.getObjectByObjId}`,
    method: 'post',
    data: params
  })
}

// 获取课题文件信息
export function getOrgNameByOrgId (params) {
  return request({
    url: `${interfaceMap.getOrgNameByOrgId}`,
    method: 'post',
    data: params
  })
}

// 获取课题文件信息
export function checkOrg (params) {
  return request({
    url: `${interfaceMap.checkOrg}`,
    method: 'post',
    data: params
  })
}
// 添加专业及活动-加载
export function structuredDocument (params) {
  return request({
    url: `${interfaceMap.structuredDocument}`,
    method: 'post',
    data: params
  })
}

// 水印
export function generateWatermarkFile (params) {
  return request({
    url: `${interfaceMap.generateWatermarkFile}`,
    method: 'post',
    data: params
  })
}

// 判断是否为有效版本
export function judgeEffectiveVersion (params) {
  return request({
    url: `${interfaceMap.judgeEffectiveVersion}`,
    method: 'post',
    data: params
  })
}

// 从专业库中选择
export function getProfessionDutyList (params) {
  return request({
    url: `${interfaceMap.getProfessionDutyList}`,
    method: 'post',
    data: params
  })
}

// 添加专业及活动-保存
export function saveStructuredDocument (params) {
  return request({
    url: `${interfaceMap.saveStructuredDocument}`,
    method: 'post',
    data: params
  })
}

// 标准规范列表
export function enterpriseNormsList (params) {
  return request({
    url: `${interfaceMap.enterpriseNormsList}`,
    method: 'post',
    data: params
  })
}

// 添加专业及活动-下载
export function downloadWord (params) {
  return request({
    url: `${interfaceMap.downloadWord}` + params.id,
    method: 'post',
    data: params
  })
}

// 权限
export function standardCreator (params) {
  return request({
    url: interfaceMap.standardCreator,
    method: 'get',
    params
  })
}

// 新增权限
export function insertCustomizePermission (params) {
  return request({
    url: interfaceMap.insertCustomizePermission,
    method: 'post',
    data: params
  })
}

// 获取字典数据
export function searchDictLabel (params) {
  return request({
    url: interfaceMap.searchDictLabel,
    method: 'post',
    data: params
  })
}

// 查询单个权限
export function getCustomizePermission (params) {
  return request({
    url: interfaceMap.getCustomizePermission,
    method: 'post',
    data: params
  })
}

// 查询权限列表
export function getCustomizePermissions (params) {
  return request({
    url: interfaceMap.getCustomizePermissions,
    method: 'post',
    data: params
  })
}

// 判断是否可以发起
export function downloadPermission (params) {
  return request({
    url: interfaceMap.downloadPermission,
    method: 'post',
    data: params
  })
}

// 下载按钮权限控制
export function downLoadAuth (params) {
  return request({
    url: interfaceMap.downLoadAuth,
    method: 'post',
    data: params
  })
}

//
export function standardLevelList (params) {
  return request({
    url: interfaceMap.standardLevelList,
    method: 'post',
    data: params
  })
}

//
export function getDocTypeByCodeNotContainChild (params) {
  return request({
    url: interfaceMap.getDocTypeByCodeNotContainChild,
    method: 'get',
    params
  })
}

// 外部标准批量插入
export function addExtStandardList (params) {
  return request({
    url: interfaceMap.addExtStandardList,
    method: 'post',
    data: params
  })
}

// 记录下载日志
export function generateOperateLog (params) {
  return request({
    url: interfaceMap.generateOperateLog,
    method: 'post',
    data: params
  })
}

// 查询供应商
export function getSupList (params) {
  return request({
    url: interfaceMap.getSupList,
    method: 'post',
    data: params
  })
}

// 查询规范详情
export function getNormalDetails (params) {
  return request({
    url: interfaceMap.getNormalDetails,
    method: 'post',
    data: params
  })
}

// 文档-外协文件权限控制
export function canStartProcess (params) {
  return request({
    url: interfaceMap.canStartProcess,
    method: 'get',
    params
  })
}

// 查看每次分发数据
export function getSupplierList (params) {
  return request({
    url: interfaceMap.getReissueSupplierRecordsByObjIdAndCount,
    method: 'post',
    data: params
  })
}

// 查看分发次数
export function getAllCountByObjId (params) {
  return request({
    url: interfaceMap.getAllCountByObjId,
    method: 'post',
    data: params
  })
}

// 同步分发路线
export function syncDistribute (params) {
  return request({
    url: interfaceMap.syncDistribute,
    method: 'post',
    data: params
  })
}

// 外部标准批量插入
export function addRelatedBusinessList (params) {
  return request({
    url: interfaceMap.addRelatedBusinessList,
    method: 'post',
    data: params
  })
}

// 获取课题默认分发路线（北研中心）
export function getSubjectDistribute () {
  return request({
    url: `${interfaceMap.getSubjectDistribute}`,
    method: 'get'
  })
}

// 公文形式制度文件列表
export function officialPolicyDocList (params) {
  return request({
    url: interfaceMap.officialPolicyDocList,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件详情
export function officialPolicyDocDetail (params) {
  return request({
    url: interfaceMap.officialPolicyDocDetail,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件新增和修改
export function officialPolicyDocAdd (params) {
  return request({
    url: interfaceMap.officialPolicyDocAdd,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件批量新增
export function officialPolicyDocAddList (params) {
  return request({
    url: interfaceMap.officialPolicyDocAddList,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件删除
export function officialPolicyDocDelete (params) {
  return request({
    url: interfaceMap.officialPolicyDocDelete,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件全部清除:
export function officialPolicyDocAllDelete (params) {
  return request({
    url: interfaceMap.officialPolicyDocAllDelete,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件导出
export function officialPolicyDocExport (params) {
  return request({
    url: interfaceMap.officialPolicyDocExport,
    method: 'post',
    data: params
  })
}
// 通用文件 -公文形式制度文件导出
export function officialPolicyDocV2 (params) {
  return request({
    url: interfaceMap.officialPolicyDocV2,
    method: 'post',
    data: params
  })
}

// 公文形式制度文件导入
export function officialPolicyDocImport (params) {
  return request({
    url: `${interfaceMap.officialPolicyDocImport}?ossId=${params}`,
    method: 'get'
  })
}

// 过程所有者审批人获取节点详情
export function getDomainDetailByOnlyCode (params) {
  return request({
    url: interfaceMap.getDomainDetailByOnlyCode + params,
    method: 'get'
  })
}

// 依据number获取课题编号
export function getSerialNumberByNumber (params) {
  return request({
    url: interfaceMap.getSerialNumberByNumber,
    method: 'post',
    data: params
  })
}

// 依据ObjId获取供应商信息
export function getAllSupplierByObjId (params) {
  return request({
    url: interfaceMap.getAllSupplierByObjId,
    method: 'post',
    data: params
  })
}
// 获取标准编码
export function getDeptAndUnit (params) {
  return request({
    url: interfaceMap.getDeptAndUnit,
    method: 'post',
    data: params
  })
}

// 是否为最新版文件
export function hasNewVersion (params) {
  return request({
    url: `${interfaceMap.hasNewVersion}`,
    method: 'post',
    data: params
  })
}

 export const delayTime = 3000
