import request from '@/utils/request'

const interfaceMap = {
  getProcessData: '/process/getProcessData',
  getUUid: '/basecenter/biz/service/util/getuuid',
  getProcessList: '/procintelligence/biz/service/processHeader/page',
  getPiDetail: '/procintelligence/biz/service/processHeader/get/',
  getPiInstanceDetail: '/procintelligence/biz/service/processHeader/instanceId/info/',
  saveProcess: '/procintelligence/biz/service/processHeader/save/process',
  getProcessTel: '/process/getProcessTel',
  getCreateTabs: '/process/getCreateTabs',
  getPiList: '/procintelligence/biz/service/processHeader/page',
  publishProcess: '/procintelligence/biz/service/processLine/publish',
  searchTreeData: '/process/biz/service/business/domain/get/lists',
  getTreeData: '/process/biz/service/business/domain/get/children',
  moveSaveTree: '/process/biz/service/business/domain/update/tree',
  AddDomain: '/process/biz/service/business/domain/add',
  getDomainDetail: '/process/biz/service/business/domain/detail/',
  getDomainDetailPerson: '/document/biz/service/common/apply/domianDetail/',
  getDomainHistory: '/process/biz/service/business/domain/history/list',
  responsibilityUpdate: '/process/biz/service/business/domain/responsibility/update',
  updateDomain: '/process/biz/service/business/domain/update',
  deleteDominNode: '/process/biz/service/business/domain/del/',
  exportDomain: '/process/biz/service/business/domain/export/',
  getProcessTemplateList: '/process/biz/service/vessel/page',
  getChildrenTree: '/process/biz/service/vessel/start/domain/merge/list',
  getProcessForm: '/process/biz/service/business/domain/sync/processForm',
  getInfluence: '/process/biz/service/business/domain/influence/',
  getProcessTreeData: '/process/biz/service/vessel/start/domain/like/list',
  checkSaveTree: '/process/biz/service/business/domain/move/influence',
  getFlowModalId: '/process/biz/service/vessel/query/flow/version/',
  getProcessInstanceList: '/flow-platform/process/getProcessInstanceList',
  getProcessInstanceList2: '/flow-platform/process/getSingleProcessInstanceList',
  checkIndtanceId: '/process/biz/service/vessel/view/check/',
  gerFlowsList: '/assignment/biz/service/assignment/flows/get',

  getCustomSignList: '/document/biz/service/signSheet/list', /* 查询客服签审单 */
  GeneralRender: '/document/biz/service/creditBalance/render',
  initInfo: '/document/biz/service/creditBalance/initInfo',
  CMSCList: '/document/biz/service/creditBalance/list',
  exportPdf: '/document/biz/service/creditBalance/exportPdf',

  /** 合规管理-政策与法规 */
  delPolicy: '/process/biz/service/compliance/policy/delete',
  getPolicyList: '/process/biz/service/compliance/policy/page',
  savePolicy: '/process/biz/service/compliance/policy/save',
  submitPolicy: '/process/biz/service/compliance/policy/submit',
  commonRender: '/process/biz/service/base/render',
  qryByObjId: '/process/biz/service/base/qryByObjId',

  /** 合规管理-合规案例 */
  delComplianseCase: '/process/biz/service/compliance/case/delete',
  getComplianseCaseDetail: '/process/biz/service/compliance/case/detail/',
  getComplianseCaseList: '/process/biz/service/compliance/case/page',
  saveComplianceCase: '/process/biz/service/compliance/case/save',
  submitComplianceCase: '/process/biz/service/compliance/case/submit',

  /** 合规管理-重点岗位合规 */
  getEmphasisCompliancePage: '/process/biz/service/majorPostCompliance/page', // 获取重点岗位合规表单分页接口
  getEmphasisComplianceDetail: '/process/biz/service/majorPostCompliance/detail/',
  removeEmphasisCompliance: '/process/biz/service/majorPostCompliance/delete',

  /** 合规管理-合规培训 */
  getComplianceTrainPage: '/process/biz/service/compliance/train/page',
  getRequisitionForm: '/process/biz/marketingCenter/requisitionForm/list',
  // deleteRequisitionForm: '/process/biz/marketingCenter/requisitionForm/del',
  initiateProcess: '/process/biz/marketingCenter/requisitionForm/init',
  requisitionDetail: '/process/biz/marketingCenter/requisitionDetail/list',
  recipientListSave: '/process/biz/marketingCenter/recipientList/save'
}

export default interfaceMap

// 业务域列表
export function getProcessData (parameter) {
  return request({
    url: interfaceMap.getProcessData,
    method: 'post',
    data: parameter
  })
}

// 获取uid
export function getUUid (params) {
  return request({
    url: interfaceMap.getUUid,
    method: 'get',
    params
  })
}

// 获取过程列表
export function getProcessList (params) {
  return request({
    url: interfaceMap.getProcessList,
    method: 'post',
    data: params
  })
}

// 获取PI列表
export function getPiList (params) {
  return request({
    url: interfaceMap.getPiList,
    method: 'post',
    data: params
  })
}

// 获取过程设计器详情
export function getPiDetail (params) {
  return request({
    url: interfaceMap.getPiDetail + params.id,
    method: 'get'
  })
}

// 获取过程实例图的详情
export function getPiInstanceDetail (params) {
  return request({
    url: interfaceMap.getPiInstanceDetail + params.id,
    method: 'get'
  })
}

// 保存过程设计器
export function saveProcess (params) {
  return request({
    url: interfaceMap.saveProcess,
    method: 'post',
    data: params
  })
}

// 获取流程模板
export function getProcessTel (params) {
  return request({
    url: interfaceMap.getProcessTel,
    method: 'post',
    data: params
  })
}

// 获取tabs模板
export function getCreateTabs (params) {
  return request({
    url: interfaceMap.getCreateTabs,
    method: 'post',
    data: params
  })
}

// 发布过程
export function publishProcess (params) {
  return request({
    url: interfaceMap.publishProcess,
    method: 'post',
    data: params
  })
}

// 业务域树-模糊搜索
export function searchTreeData (params) {
  return request({
    url: interfaceMap.searchTreeData,
    method: 'post',
    data: params
  })
}

// 业务域树-获取业务域子节点
export function getTreeData (params) {
  return request({
    url: interfaceMap.getTreeData,
    method: 'post',
    data: params
  })
}
// 发起流程-业务域树-获取业务域子节点
export function getChildrenTree (params) {
  return request({
    url: interfaceMap.getChildrenTree,
    method: 'post',
    data: params
  })
}

// 业务域树移动保存
export function moveSaveTree (params) {
  return request({
    url: interfaceMap.moveSaveTree,
    method: 'post',
    data: params
  })
}

// 业务域树移动校验
export function checkSaveTree (params) {
  return request({
    url: interfaceMap.checkSaveTree,
    method: 'post',
    data: params
  })
}

// 添加业务域节点
export function AddDomain (params) {
  return request({
    url: interfaceMap.AddDomain,
    method: 'post',
    data: params
  })
}
// 过程所有者审批人获取节点详情
export function getDomainDetailPerson (params) {
  return request({
    url: interfaceMap.getDomainDetailPerson + params,
    method: 'get'
  })
}
// 业务域节点详情
export function getDomainDetail (params) {
  return request({
    url: interfaceMap.getDomainDetail + params,
    method: 'get'
  })
}

// 业务域历史记录
export function getDomainHistory (params) {
  return request({
    url: interfaceMap.getDomainHistory,
    method: 'post',
    data: params
  })
}

// 业务域职责
export function responsibilityUpdate (params) {
  return request({
    url: interfaceMap.responsibilityUpdate,
    method: 'post',
    data: params
  })
}
// 修改业务域节点
export function updateDomain (params) {
  return request({
    url: interfaceMap.updateDomain,
    method: 'post',
    data: params
  })
}

// 删除业务域节点
export function deleteDominNode (params) {
  return request({
    url: interfaceMap.deleteDominNode + params,
    method: 'get'
  })
}

// 导出业务域节点
export function exportDomain (params) {
  return request({
    url: interfaceMap.exportDomain + params,
    method: 'get'
  })
}

// 一般任务模板创建
export function templateCreate (params) {
  return request({
    url: '/process/biz/service/common/vessel/template',
    method: 'post',
    data: params
  })
}

// 废止模板创建
export function delFlowListCreate (params) {
  return request({
    url: '/process/biz/service/flowList/create',
    method: 'post',
    data: params
  })
}

// 升级模板创建
export function updateFlowListCreate (params) {
  return request({
    url: '/process/biz/service/flow/upGrade/create',
    method: 'post',
    data: params
  })
}

// 升级模板创建 --- 任务单
export function updateTaskListCreate (params) {
  return request({
    url: '/process/biz/service/common/vessel/upGrade/create',
    method: 'post',
    data: params
  })
}

// 过程模板列表查询
export function getProcessTemplateList (params) {
  return request({
    url: interfaceMap.getProcessTemplateList,
    method: 'post',
    data: params
  })
}

// 流程任务单详情
export function processFlowDetail (params) {
  return request({
    url: '/process/biz/service/vessel/flow/detail/' + params,
    method: 'get'
  })
}

// 过程任务单详情
export function processDetail (params) {
  return request({
    url: '/process/biz/service/common/vessel/detail/' + params,
    method: 'get'
  })
}

// 废止的前置check
export function delFlowListCheck (params) {
  return request({
    url: '/process/biz/service/flowList/check/' + params,
    method: 'get'
  })
}

// 升级的前置check---流程表单的check
export function updateFlowListCheck (params) {
  return request({
    url: '/process/biz/service/flow/upGrade/check/' + params,
    method: 'get'
  })
}

// 升级的前置check---任务表的check
export function updateTaskListCheck (params) {
  return request({
    url: '/process/biz/service/common/vessel/upGrade/check/' + params,
    method: 'get'
  })
}

// 过程任务单详情--common
export function processDetailCommon (params) {
  return request({
    url: '/process/biz/service/start/flow/detail/' + params,
    method: 'get'
  })
}

// 过程任务单详情--common
export function startTemplate (params) {
  return request({
    url: '/process/biz/service/common/vessel/start/template',
    method: 'post',
    data: params
  })
}

// 检测发起流程
export function checkCreateProcess (params) {
  return request({
    url: '/process/biz/service/flow/start/check/' + params.objectId + '/' + params.deptId,
    method: 'get'
  })
}

// 检测发起流程
export function oftenSet (params) {
  return request({
    url: '/process/biz/service/vessel/start/domain/setCommonUse/' + params.objectId + '/' + params.operateType,
    method: 'get'
  })
}

// 检测发起流程
export function createProcessCheck (params) {
  return request({
    url: '/process/biz/service/common/vessel/start/check/' + params,
    method: 'get'
  })
}

// 当编辑业务域树为无效时 对下面流程得影响提示
export function getInfluence (params) {
  return request({
    url: interfaceMap.getInfluence + params.id + '/' + params.validFlag,
    method: 'get'
  })
}

// 过程模板列表查询
export function getProcessForm (params) {
  return request({
    url: interfaceMap.getProcessForm,
    method: 'post',
    data: params
  })
}

// 发起流程左侧树

export function getProcessTreeData (params) {
  return request({
    url: interfaceMap.getProcessTreeData,
    method: 'post',
    data: params
  })
}
//  根据业务域ID获取流程模板ID
export function getFlowModalId (params) {
  return request({
    url: interfaceMap.getFlowModalId + params,
    method: 'get'
  })
}
//  获取流程实例列表
export function getProcessInstanceList (params) {
  return request({
    url: interfaceMap.getProcessInstanceList,
    method: 'post',
    data: params
  })
}
//  获取流程实例列表2
export function getProcessInstanceList2 (params) {
  return request({
    url: interfaceMap.getProcessInstanceList2,
    method: 'post',
    data: params
  })
}

// checkIndtanceId
export function checkIndtanceId (params) {
  return request({
    url: interfaceMap.checkIndtanceId + params,
    method: 'get'
  })
}

//
export function gerFlowsList (params) {
  return request({
    url: interfaceMap.gerFlowsList,
    method: 'post',
    data: params
  })
}

export function generalRender (params) {
  return request({
    url: interfaceMap.GeneralRender,
    method: 'post',
    data: params
  })
}
export function getCustomSignList (params) {
  return request({
    url: interfaceMap.getCustomSignList,
    method: 'post',
    data: params
  })
}
// 用户信息
export function getUserInfo (parameter) {
  return request({
    url: interfaceMap.initInfo,
    method: 'post',
    data: parameter
  })
}
// 营销中心List
export function getCMSCList (parameter) {
  return request({
    url: interfaceMap.CMSCList,
    method: 'post',
    data: parameter
  })
}
// List
export function getRequisitionForm (parameter) {
  return request({
    url: interfaceMap.getRequisitionForm,
    method: 'post',
    data: parameter
  })
}
// 营销中心List
export function exportPdf (parameter) {
  return request({
    url: interfaceMap.exportPdf,
    method: 'post',
    data: parameter
  })
}

// 过程模块基础渲染
export function commonRender (parameter) {
  return request({
    url: interfaceMap.commonRender,
    method: 'post',
    data: parameter
  })
}

// 过程模块基础详情查询
export function qryByObjId (parameter) {
  return request({
    url: interfaceMap.qryByObjId,
    method: 'post',
    data: parameter
  })
}

// 政策与法规-删除
export function delPolicy (parameter) {
  return request({
    url: interfaceMap.delPolicy,
    method: 'post',
    data: parameter
  })
}

// 政策与法规-查询列表
export function getPolicyList (parameter) {
  return request({
    url: interfaceMap.getPolicyList,
    method: 'post',
    data: parameter
  })
}

// 政策与法规-暂存
export function savePolicy (parameter) {
  return request({
    url: interfaceMap.savePolicy,
    method: 'post',
    data: parameter
  })
}

// 政策与法规-提交
export function submitPolicy (parameter) {
  return request({
    url: interfaceMap.submitPolicy,
    method: 'post',
    data: parameter
  })
}

// 合规案例-删除
export function delComplianseCase (parameter) {
  return request({
    url: interfaceMap.delComplianseCase,
    method: 'post',
    data: parameter
  })
}

// 合规案例-查询单个对象
export function getComplianseCaseDetail (parameter) {
  return request({
    url: interfaceMap.getComplianseCaseDetail + parameter,
    method: 'get'
  })
}

// 合规案例-查询列表
export function getComplianseCaseList (parameter) {
  return request({
    url: interfaceMap.getComplianseCaseList,
    method: 'post',
    data: parameter
  })
}
// 合规案例-查询单个对象
export function initiateProcess (parameter) {
  return request({
    url: interfaceMap.initiateProcess,
    method: 'get',
    params: parameter
  })
}
// 合规案例-查询单个对象
export function requisitionDetail (parameter) {
  return request({
    url: interfaceMap.requisitionDetail,
    method: 'post',
    data: parameter
  })
}
// 宣传用品领用保存接口
export function recipientListSave (parameter) {
  return request({
    url: interfaceMap.recipientListSave,
    method: 'post',
    data: parameter
  })
}
// 删除管控页面流程
// export function deleteRequisitionForm (parameter) {
//   return request({
//     url: interfaceMap.deleteRequisitionForm,
//     method: 'post',
//     data: parameter
//   })
// }
export const emphasisCompliance = {
  getEmphasisCompliancePage:
    function getEmphasisCompliancePage (parameter) {
      return request({
        url: interfaceMap.getEmphasisCompliancePage,
        method: 'post',
        data: parameter
      })
    },
  getEmphasisComplianceDetail:
    function getEmphasisComplianceDetail (parameter) {
      return request({
        url: interfaceMap.getEmphasisComplianceDetail + parameter,
        method: 'get'
      })
    },
  removeEmphasisCompliance:
    function removeEmphasisCompliance (parameter) {
      return request({
        url: interfaceMap.removeEmphasisCompliance,
        method: 'post',
        data: parameter
      })
    }
}

export const complianceTraining = {
  getPage:
  function getPage (parameter) {
    return request({
      url: interfaceMap.getComplianceTrainPage,
      method: 'post',
      data: parameter
    })
  },
  getDetail:
  function getDetail (parameter) {
    return request({
      url: '/process/biz/service/compliance/train/detail/' + parameter,
      method: 'get'
    })
  },
  remove:
  function remove (parameter) {
    return request({
      url: '/process/biz/service/compliance/train/delete',
      method: 'post',
      data: parameter
    })
  }
}
