import axios from 'axios'
import {
    message
} from 'ant-design-vue';

// import router from '@/router'
let host = ""
// create an axios instance
const service = axios.create({
    baseURL: host + '/api', // url = base url + request url
    withCredentials: true, // send cookies when cross-domain requests
    timeout: 30000 // request timeout
})

const codeMessage = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户没有权限（令牌、用户名、密码错误）。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};

const CustomErrorCodeMap = {
    '1': '不需要特殊处理的错误',
    '2': '登录异常请重新登陆',
    '3': '缺少需要填写的字段',
    '4': '数据错误',
    '5': '没有访问权限',
}

// request interceptor
service.interceptors.request.use(
    config => {
        let refreshtoken = localStorage.getItem('refreshtoken')
        let assesstoken = localStorage.getItem('assesstoken')
        config.headers['source'] = 'Web'
        // 检测到token 就在请求头里添加
        if (refreshtoken && assesstoken) {
            config.headers['refreshtoken'] = localStorage.getItem('refreshtoken')
            config.headers['assesstoken'] = localStorage.getItem('assesstoken')
        }

        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data
        // 请求拦截 如果有就替换
        if (response.headers.refreshtoken && response.headers.assesstoken) {
            localStorage.setItem('refreshtoken', response.headers.refreshtoken)
            localStorage.setItem('assesstoken', response.headers.assesstoken)
        }
        // if the custom code is not 20000, it is judged as an error.
        if (res.code !== 0) {

            const msg = res.message || CustomErrorCodeMap[res.code]

            // 如果 fileRequest 为true，不做异常弹窗提示
            if (response.config.headers.fileRequest) return Promise.reject(msg)

            // message({
            //     message: msg,
            //     type: 'error',
            //     duration: 5 * 1000
            // })
            message.error(msg)

            // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
            // if (res.code === 2 || res.code === 50012 || res.code === 50014) {
            //     // to re-login
            //     localStorage.removeItem("userToken")
            //     router.push({
            //         name: "login"
            //     })
            // }
            return Promise.reject(new Error(res.message || 'Error'))
        } else {
            return res.data
        }
    },
    error => {
        console.log('err' + error) // for debug
        const errorText = codeMessage[error.status] || error.statusText || error.message;
        // 如果 fileRequest 为true，不做异常弹窗提示
        if (error.config.headers.fileRequest) return Promise.reject(errorText)

        message.error(errorText)
        return Promise.reject(error)
    }
)

export default service