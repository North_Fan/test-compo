
export default function (url, name) {
    // base64
    if (url.startsWith('data:')) return base64download(url, name)


}

export function download_href(blob, name) {

    if (window.navigator.msSaveBlob) {
        // sb IE
        try {
            window.navigator.msSaveBlob(blob, name)
        } catch (e) {
            console.log(e);
        }
    } else {
        // 正常浏览器

        // 下载链接
        const href = window.URL.createObjectURL(blob);

        let downloadElement = document.createElement('a');
        downloadElement.href = href;
        downloadElement.target = '_blank';
        downloadElement.download = name;

        downloadElement.click();

        //释放
        setTimeout(() => {
            downloadElement = null;
            window.URL.revokeObjectURL(href)
        }, 100)
    }

}