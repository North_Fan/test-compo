import axios from './baseRequest'


export function getAction(url, parameter) {
    return axios({
        url: url,
        method: 'get',
        params: parameter,
    })
}

export function postAction(url, parameter) {
    return axios({
        url: url,
        method: 'post',
        data: parameter,
    })
}

export function putAction(url, parameter) {
    return axios({
        url: url,
        method: 'put',
        data: parameter
    })
}

export function deleteAction(url, parameter) {
    return axios({
        url: url,
        method: 'delete',
        data: parameter
    })
}

export function formdataAction(url, parameter, fileRequest) {
    return axios({
        url: url,
        method: 'post',
        data: parameter,
        // 转换数据的方法
        transformRequest: [
            function (data) {
                let ret = ''
                for (const it in data) {
                    ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
                }
                ret = ret.substring(0, ret.length - 1)
                return ret
            }
        ],
        // 设置请求头
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            "fileRequest": fileRequest
        }
    })
}

// fileRequest 用作判断是否是附件上传的接口，对异常处理做直接抛出
export function uploadAction(url, data, callback, fileRequest) {
    return axios({
        url: url,
        method: 'post',
        data,
        headers: {
            "Content-Type": "multipart/form-data",
            "fileRequest": fileRequest
        },
        onUploadProgress(progressEvent) {
            if (progressEvent.lengthComputable && typeof callback === 'function') {
                let val = (
                    (progressEvent.loaded / progressEvent.total) *
                    100
                ).toFixed(0);
                callback(val)
            }
        }
    })
}