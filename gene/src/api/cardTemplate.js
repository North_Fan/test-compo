/*
 * @Author: yangchao52396
 * @Date: 2023-12-14 11:18:13
 * @LastEditors: yangchao52396
 * @LastEditTime: 2024-01-08 15:12:45
 * @FilePath: /cmos-frontend/src/api/cardTemplate.js
 * @Description:知识卡片模板api
 *
 */
import request from '@/utils/request'
// 知识卡片模板库列表
export function getCardKnowledgeData (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/pageResultTemplate`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片模板库列表的新增
export function addCardKnowledgeInfo (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/createTemplate`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片模板库的详情
export function getCardKnowledgeDetail (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/detailTemplate?objectIndex=${parameter.objectId}`,
    method: 'post'
  })
}
// 知识卡片模板库的编辑
export function editCardKnowledgeInfo (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/updateTemplate`,
    method: 'post',
    data: parameter
  })
}
// 模板对应实例
export function getPageInstanceByTPIndex (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/pageResultInstance`,
    method: 'post',
    data: parameter
  })
}
// 知识卡片实例详情
export function getDetailInstance (id) {
  return request({
    url: `/plan/biz/service/knowledgeCard/detailInstance?objectIndex=${id}`,
    method: 'post'
  })
}
// 知识卡片实例编辑
export function editInstance (parameter) {
  return request({
    url: `/plan/biz/service/knowledgeCard/updateInstance`,
    method: 'post',
    data: parameter
  })
}
export function unitBelong (data) {
  return request({
    url: `/plan/biz/service/targetProject/unitBelong`,
    method: 'get',
    params: data
  })
}
