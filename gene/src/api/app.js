import request from '@/utils/request'

/**
 * 获取用户树数据（Mock）
 * @param {Object} parameter
 * @returns
 */
export function getUserTree (parameter) {
  return request({
    url: '/user/userTree',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取岗位列表
 * @param {Object} parameter
 * @returns
 */
 export function getStationTreeData (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/positions/selectPage',
      method: 'post',
      data: params
  })
}

/**
 * 获取用户树数据
 * @param {Object} parameter
 * @returns
 */
 export function getUserTreeData (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/org/getFirstLevelOrgList',
      method: 'post',
      data: params
  })
}
/**
 * 获取用户角色
 * @param {Object} parameter
 * @returns
 */
 export function getRoleTreeData (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/role/qryRoleListByApp',
      method: 'post',
      data: params
  })
}

/**
 * 获取群组用户
 * @param {Object} parameter
 * @returns
 */
 export function getGroupTreeData (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/sysGroup/selectPage',
      method: 'post',
      data: params
  })
}

/**
 * 获取用户角色用户
 * @param {Object} parameter
 * @returns
 */
 export function getRoleUserList (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/role/qryUserListByRoleId',
      method: 'post',
      data: params
  })
}
/**
 * 获取用户角色用户(分页)
 * @param {Object} parameter
 * @returns
 */
 export function getRoleUserListByPage (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/role/selectPage',
      method: 'post',
      data: params
  })
}
/**
 * 获取岗位用户
 * @param {Object} parameter
 * @returns
 */
 export function getUserByStationId (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/positions/selectUserListByPosition',
      method: 'post',
      data: params
  })
}
/**
 * 获取用户树子数据
 * @param {Object} parameter
 * @returns
 */
 export function getChildrenPlanclassifyListNew (params) {
  return request({
      url: '/usercenter-admin-app/admin/sysOrg/getChildOrgListByParentId',
      method: 'post',
      data: params
  })
}

/**
 * 获取用户树子数据用户信息
 * @param {Object} parameter
 * @returns
 */
 export function getUserList (params) {
 return request({
     url: '/basecenter/feignApi/userCenter/org/qryEmployeeListByOrgId',
     method: 'post',
     data: params
 })
}

/**
 * 获取子组织树
 * @param {Object} parameter
 * @returns
 */
 export function getOrgListByParentOrgId (params) {
 return request({
     url: '/basecenter/feignApi/userCenter/org/qryOrgListByParentOrgId',
     method: 'post',
     data: params
 })
}

/**
 * 获取用户列表by用户组id
 * @param {Object} parameter
 * @returns
 */
 export function getEmployeeDetailPageByGroupId (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/sysGroup/selectEmployeeDetailPageByGroupId',
      method: 'post',
      data: params
  })
 }

/**
 * 获取推荐用户数据
 * @param {Object} parameter
 * @returns
 */
 export function getRecommandUserList (parameter) {
  return request({
    url: '/user/recommandUser/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取用户树数据
 * @param {Object} parameter
 * @returns
 */
 export function getDepartmentTree (parameter) {
  return request({
    url: '/basecenter/feignApi/userCenter/org/getFirstLevelOrgList',
    method: 'post',
    params: parameter
  })
}
export function getDepartmentTreeByParam (parameter) {
  return request({
    url: '/basecenter/feignApi/userCenter/org/getFirstLevelOrgListByParam',
    method: 'post',
    data: parameter
  })
}
/**
 * 获取用户树子节点数据
 * @param {Object} parameter
 * @returns
 */
 export function getDepartmentTreeNode (parameter) {
  return request({
    url: '/basecenter/feignApi/userCenter/org/qryOrgListByParentOrgId',
    method: 'post',
    data: parameter
  })
}
/**
 * 部门用户的模糊搜索
 * @param {Object} parameter
 * @returns
 */
 export function getDepartmentTreeByUser (parameter) {
  return request({
    url: '/basecenter/feignApi/userCenter/org/selectOrgTreeByCondition',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取岗位列表--用户--模糊
 * @param {Object} parameter
 * @returns
 */
 export function selectPositionListByUser (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/positions/selectPositionListByUser',
      method: 'post',
      data: params
  })
}

/**
 * 获取角色列表--用户模糊
 * @param {Object} parameter
 * @returns
 */
 export function selectRoleListByUser (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/role/selectRoleListByUser',
      method: 'post',
      data: params
  })
}

/**
 * 获取群组列表--用户--模糊
 * @param {Object} parameter
 * @returns
 */
 export function selectGroupPageByEmployeeCondition (params) {
  return request({
      url: '/basecenter/feignApi/userCenter/sysGroup/selectGroupPageByEmployeeCondition',
      method: 'post',
      data: params
  })
}
/**
 * 获取推荐用户数据
 * @param {Object} parameter
 * @returns
 */
 export function getRecommandDeptList (parameter) {
  return request({
    url: '/user/recommandDepartment/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 上传文件
 * @param {Object} parameter
 * @returns
 */
 export function singleFileUploader (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'get',
    params: data
  })
}
/**
 * 门禁管理上传文件
 * @param {Object} parameter
 * @returns
 */
 export function entranceGuardUploader (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    params: data
  })
}
/**
 * 提交文件
 * @param {Object} parameter
 * @returns
 */
 export function singleFileSubmit (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'post',
    data
  })
}

/**
 * 上传文件错误详情文件的文件下载
 * @returns
 */
 export function singleFileUploadErrorFileDownload (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'get',
    params: data
  })
}
/**
 * 问题记录管理文件校验
 * @returns
 */
export function checkoutFile (serviceUrl, data) {
  return request({
    url: serviceUrl,
    method: 'get',
    params: data
  })
}

/**
 * 下载文件
 * @returns
 */
 export function singleFileUploadFileDownload (serviceUrl, data) {
  return request({
    url: serviceUrl + data,
    responseType: 'blob',
    method: 'post'
  })
}
/**
 * 下载模板文件
 * @returns
 */
 export function downloadTempService (serviceUrl) {
  return request({
    url: serviceUrl,
    method: 'get',
    responseType: 'blob'
  })
}

/**
 * 下载模板文件
 * @returns
 */
 export function departmentList (parameter) {
  return request({
    url: '/api/department',
    method: 'get',
    params: parameter
  })
}

/**
 * 下载模板文件
 * @returns
 */
 export function getRoleList (parameter) {
  return request({
    url: '/api/roleList',
    method: 'post',
    params: parameter
  })
}

/**
 * 获取审批人部门单位的人员
 * @returns
 */
 export function getProcessUserData (params) {
  return request({
    url: '/api/process/reviewers',
    method: 'get',
    params
  })
}

/**
 * 审批人员提交
 * @returns
 */
 export function reviewerUserData (data) {
  return request({
    url: '/api/process/reviewers/submit',
    method: 'post',
    data
  })
}

/**
 * 重置密码
 * @returns
 */
 export function resetPassWd (data) {
  return request({
    url: '/basecenter/feignApi/userCenter/user/resetPassWd',
    method: 'post',
    data
  })
}

/**
 * 用户信息更新
 * @returns
 */
 export function userUpdate (data) {
  return request({
    url: '/basecenter/feignApi/userCenter/user/update',
    method: 'post',
    data
  })
}

/**
 * 用户信息更新--组织
 * @returns
 */
 export function insertOrUpdateCurrentOrg (data) {
  return request({
    url: '/basecenter/feignApi/userCenter/org/insertOrUpdateCurrentOrg',
    method: 'post',
    data
  })
}

/**
 * 异步上传-状态轮询接口
 * @returns
 */
export function syncImportFileState (url, ossId) {
  return request({
    url,
    method: 'get',
    params: {
      ossId
    }
  })
}

/**
 * 异步上传-状态轮询接口
 * @returns
 */
 export function syncImportFileResult (url, ossId) {
  return request({
    url,
    method: 'get',
    params: {
      ossId
    }
  })
}

/**
 * 提交-异步查询提交状态接口
 * @returns
 */
 export function syncSubmitGetStatus (ossId) {
  return request({
    url: '/plan/biz/service/wbs/syncSubmitFileState',
    method: 'get',
    params: {
      ossId
    }
  })
}
