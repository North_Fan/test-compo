import request from '@/utils/request'
import qs from 'qs'

const interfaceMap = {
  getContractTreeData: '/contract/biz/contract/getLeftTree',
  getContractTypeTreeData: '/contract/getContractTypeTreeData',
  getContractTaskList: '/plan/biz/service/vessel/page',
  getContractTableData: '/contract/biz/contract/getContractList',
  getContractTemplate: '/contract/biz/service/contract/template/query',
  getNewContractTemplate: '/contract/biz/service/contract/template/query/enable ',
  saveNewTemplate: '/contract/biz/service/contract/template/save',
  saveEditTemplate: '/contract/biz/service/contract/template/update',
  updateTemplateStatus: '/contract/biz/service/contract/template/changeStatus',
  getContractDemonstration: '/contract/getContractDemonstration',
  getContractnegotiationRecord: '/contract/getContractnegotiationRecord',
  getContractProgress: '/contract/getContractProgress',
  getEncryptionUsercode: '/contract/biz/service/common/getEncryptionUsercode',
  getProcessStatisticsList: '/contract/getProcessStatisticsList',
  getProcessStatisticsRecords: '/contract/getProcessStatisticsRecords',
  getProcessApplicationStatisticByCondition: '/contract/getProcessApplicationStatisticByCondition',
  getTaskdetailStatistics: '/contract/getTaskdetailStatistics',
  getDetailprocessApplicationData: '/contract/getDetailprocessApplicationData',
  getOverallprocessApplicationData: '/contract/getOverallprocessApplicationData',
  getContractCreate: '/contract/biz/service/contract/init',
  contractPreview: '/contract/biz/service/entry/contractPreview',
  contractEdit: '/contract/biz/service/entry/contractEdit',
  contractDownload: '/contract/biz/service/entry/contractDownload',
  contractEntry: '/contract/biz/service/entry/contractEntry',
  getContractBaseInfo: '/contract/biz/service/entry/getBaseInfo',
  getContractSeal: '/contract/biz/service/seal/getContractSeal',
  getApproveHistory: '/contract/biz/service/review/getApproveHistory',
  getTemplatePreviewUrl: '/contract/biz/service/contract/template/getPreviewUrl',
  getTemplateDownUrl: '/contract/biz/service/contract/template/getDownloadUrl',
  templateCreateUpload: '/contract/biz/service/wps/createUpload',
  contractTempHistoryTags: '/contract/biz/contract/getBookMark',
  deleteContractFile: '/contract/biz/service/contract/template/deleteFile',
  checkBookMark: '/contract/biz/service/contract/template/checkBookMark',
  detailDecision: '/contract/biz/negotiate/decision/detailDecision',
  detailRecord: '/contract/biz/negotiate/record/detailRecord',
  contractView: '/contract/biz/service/review/contractView',
  getBookMark: '/contract/biz/contract/getBookMark',
  copyTemplate: '/contract/biz/contract/copyTemplate',
  negotiateCreateRecord: '/contract/biz/negotiate/record/create',
  negotiateCreateDecision: '/contract/biz/negotiate/decision/create',
  getTaskListDetail: '/contract/biz/contract/taskDetail',
  contractTextQuery: '/contract/biz/service/contract/text/query',
  contractTextSave: '/contract/biz/service/contract/text/save',
  contractTextUpdate: '/contract/biz/service/contract/text/update',
  contractTextChangeStatus: '/contract/biz/service/contract/text/changeStatus',
  contractReportPath: '/contract/biz/contract/card/getToken',
  contractDemonstrationUpload: '/contract/biz/service/contract/text/batchUpload',
  contractDemonstrationOneUpload: '/contract/biz/service/contract/template/createUpload',
  removeContractText: '/contract/biz/service/contract/text/delete',
  searchInstanceId: '/contract/biz/contract/searchInstanceId/',
  refreshContractNegotiationList: '/contract/biz/negotiate/recordList',
  getTaskDetailByContractNo: '/contract/biz/contract/getTaskDetailByContractNo',
  getPurchaseContractCreate: '/contract/biz/service/contract/contractPurchaseInit',
  getPurchaseOrderContractCreate: '/contract/biz/purchaseOrder/init',
  getPurchaseContractInfo: '/contract/biz/purchaseOrder/getContractInfol',
  getOrgDetail: '/basecenter/feignApi/userCenter/org/getOrgDetail',
  getSerialNum: '/contract/biz/service/common/queryContractCode',
  getContractIndex: '/contract/biz/service/common/getContractIndex',
  contractPreliminaryExamination: '/contract/biz/service/contract/contractPreliminaryExamination',
  getTaskStatus: '/contract/biz/service/contract/getTaskStatus',
  getTemplate: '/contract/biz/service/common/getTemplate',
  compChecked: '/contract/biz/contract/compChecked',
  getPurchaseContract: '/contract/biz/purchaseOrder/getContract',
  getAijson: '/contract/biz/service/common/getAijson',
  getLvplan: '/contract/biz/service/common/getLvplan',
  getShowDataTree: '/contract/biz/contract/getContractLeftTree',
  getControDisburselMsg: '/contract/biz/contract/control/getControDisburselMsg',
  getControlImplementationPlanMsg: '/contract/biz/contract/control/getControlImplementationPlanMsg',
  getControlExpenditurePlanMsg: '/contract/biz/contract/control/getControlExpenditurePlanMsg',
  getControlPieChartMsg: '/contract/biz/contract/control/getControlPieChartMsg',
  getContrctDetailTable: '/contract/biz/contract/control/getContractList',
  getContractDetail: '/contract/biz/contract/control/getContractOneNumMsg',
  getTaskContractMoney: '/contract/biz/service/contract/getTaskContractMoney',
  getArchivistTree: '/contract/biz/contract/getArchivistTree'
}

export default interfaceMap

// 获取系统视图树
export function getTreeData (parameter) {
  return request({
    url: interfaceMap.getContractTreeData,
    method: 'get',
    params: parameter
  })
}

// 获取类型视图树
export function getTypeTreeData (parameter) {
  return request({
    url: interfaceMap.getContractTypeTreeData,
    method: 'get',
    params: parameter
  })
}

// 获取合同列表
export function getTableData (parameter) {
  return request({
    url: interfaceMap.getContractTableData,
    method: 'post',
    data: parameter
  })
}

// 获取任务单列表
export function getTaskList (parameter) {
  return request({
    url: interfaceMap.getContractTaskList,
    method: 'post',
    data: parameter
  })
}

// 获取合同模板列表
export function getContractTemplate (parameter) {
  return request({
    url: interfaceMap.getContractTemplate,
    method: 'post',
    data: parameter
  })
}

// (新)获取合同模板列表
export function getNewContractTemplate (parameter) {
  return request({
    url: interfaceMap.getNewContractTemplate,
    method: 'post',
    data: parameter
  })
}

// 新增合同模板
export function saveNewTemplate (parameter) {
  return request({
    url: interfaceMap.saveNewTemplate,
    method: 'post',
    data: parameter
  })
}

// 编辑合同模板
export function saveEditTemplate (parameter) {
  return request({
    url: interfaceMap.saveEditTemplate,
    method: 'post',
    data: parameter
  })
}

 //  获取收付款人接口id
 export function getEncryptionUsercode (parameter) {
  return request({
    url: interfaceMap.getEncryptionUsercode,
    method: 'get',
    params: parameter
  })
}

// 修改合同模板状态
export function updateTemplateStatus (parameter) {
  return request({
    url: interfaceMap.updateTemplateStatus,
    method: 'post',
    data: parameter
  })
}

// 获取合同示范文本
export function getContractDemonstration (parameter) {
  return request({
    url: interfaceMap.getContractDemonstration,
    method: 'get',
    params: parameter
  })
}

// 获取合同谈判记录
export function getContractnegotiationRecord (parameter) {
  return request({
    url: interfaceMap.getContractnegotiationRecord,
    method: 'get',
    params: parameter
  })
}

// 获取合同谈判记录
export function refreshContractNegotiationList (parameter) {
  return request({
    url: interfaceMap.refreshContractNegotiationList + '?instanceId=' + parameter.instanceId + '&processNodeCode=' + parameter.processNodeCode,
    method: 'post'
  })
}

// 获取进展列表
export function getContractProgress (parameter) {
  return request({
    url: interfaceMap.getContractProgress,
    method: 'get',
    params: parameter
  })
}
// 获取合同审批记录
export function getApprovalRecord (parameter) {
  return request({
    url: interfaceMap.getApproveHistory,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计
export function getProcessStatisticsList (parameter) {
  return request({
    url: interfaceMap.getProcessStatisticsList,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计2
export function getProcessStatisticsRecords (parameter) {
  return request({
    url: interfaceMap.getProcessStatisticsRecords,
    method: 'get',
    params: parameter
  })
}

// 获取流程单应用情况统计-按业务域按单位
export function getProcessApplicationStatisticByCondition (parameter) {
  return request({
    url: interfaceMap.getProcessApplicationStatisticByCondition,
    method: 'get',
    params: parameter
  })
}

// 获取任务单详情统计
export function getTaskdetailStatistics (parameter) {
  return request({
    url: interfaceMap.getTaskdetailStatistics,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单详情实施及应用情况
export function getDetailprocessApplicationData (parameter) {
  return request({
    url: interfaceMap.getDetailprocessApplicationData,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单总体实施及应用情况
export function getOverallprocessApplicationData (parameter) {
  return request({
    url: interfaceMap.getOverallprocessApplicationData,
    method: 'post',
    params: parameter
  })
}

// 获取流程表单总体实施及应用情况
export function getContractCreate (parameter) {
  return request({
    url: interfaceMap.getContractCreate,
    method: 'get',
    params: parameter
  })
}

// 获取采购合同流程表单总体实施及应用情况
export function getPurchaseContractCreate (parameter) {
  return request({
    url: interfaceMap.getPurchaseContractCreate,
    method: 'get',
    params: parameter
  })
}
// 获取采购订单表单总体实施及应用情况
export function getPurchaseOrderContractCreate (parameter) {
  return request({
    url: interfaceMap.getPurchaseOrderContractCreate,
    method: 'post',
    params: parameter
  })
}
// 合同预览
export function contractPreview (parameter) {
  return request({
    url: interfaceMap.contractPreview + '/' + parameter,
    method: 'get'
  })
}

// 合同在线编辑
export function contractOnLineEdit (parameter) {
  return request({
    url: interfaceMap.contractEdit + '/' + parameter,
    method: 'get'
  })
}

// 合同下载
export function contractDownload (parameter) {
  return request({
    url: interfaceMap.contractDownload + '/' + parameter,
    method: 'get'
  })
}

// 合同录入
export function contractEntry (parameter) {
  return request({
    url: interfaceMap.contractEntry + '/' + parameter,
    method: 'get'
  })
}
// 查看合同基本信息
export function getContractBaseInfo (parameter) {
  return request({
    url: interfaceMap.getContractBaseInfo + '?' + qs.stringify(parameter),
    method: 'get'
  })
}

// 合同用印
export function getContractSeal (parameter) {
  return request({
    url: interfaceMap.getContractSeal,
    method: 'get',
    params: parameter
  })
}

// 合同模板在线预览
export function getTemplatePreviewUrl (parameter) {
  return request({
    url: interfaceMap.getTemplatePreviewUrl + '/' + parameter,
    method: 'get'
  })
}

// 合同模板下载
export function getTemplateDownUrl (parameter) {
  return request({
    url: interfaceMap.getTemplateDownUrl + '/' + parameter,
    method: 'get'
  })
}

// 合同模板单个上传
export function contractDemonstrationOneUpload (parameter) {
  console.log(parameter)
  return request({
    url: interfaceMap.contractDemonstrationOneUpload + '/' + parameter.fileName + '/' + parameter.fileType,
    method: 'post',
    data: parameter.file
  })
}

// 合同模板上传
export function templateCreateUpload (parameter) {
  console.log(parameter)
  return request({
    url: interfaceMap.templateCreateUpload + '/' + parameter.fileName + '/' + parameter.fileType,
    method: 'post',
    data: parameter.file
  })
}

// 合同模板获取历史记录标签
export function getContractBookMarks (fileId) {
  return request({
    url: interfaceMap.contractTempHistoryTags + '/' + fileId,
    method: 'get'
  })
}

// 合同范本上传
export function contractDemonstrationUpload (parameter) {
  return request({
    url: interfaceMap.contractDemonstrationUpload,
    method: 'post',
    data: parameter
  })
}

// 合同模板--删除
export function deleteContractFile (parameter) {
  return request({
    url: interfaceMap.deleteContractFile + '/' + parameter,
    method: 'get'
  })
}

// 合同模板--校验书签
export function checkBookMark (parameter) {
  return request({
    url: interfaceMap.checkBookMark,
    method: 'post',
    data: parameter
  })
}
// 合同初审创建审核任务:
export function contractPreliminaryExamination (parameter) {
  return request({
    url: interfaceMap.contractPreliminaryExamination,
    method: 'post',
    data: parameter
  })
}
// 合同初审查询任务状态:
export function getTaskStatus (parameter) {
  return request({
    url: interfaceMap.getTaskStatus,
    method: 'get',
    params: parameter
  })
}

// 谈判决策详情
export function detailDecision (parameter) {
  return request({
    url: interfaceMap.detailDecision + '?recordObjIndex=' + parameter,
    method: 'post'
  })
}

// 谈判记录详情
export function detailRecordInfo (parameter) {
  return request({
    url: interfaceMap.detailRecord + '?recordObjIndex=' + parameter,
    method: 'post'
  })
}

// 合同查看
export function contractView (parameter) {
  return request({
    url: interfaceMap.contractView + '?' + qs.stringify(parameter),
    method: 'get'
  })
}

// 合同获取书签
export function getBookMark (parameter) {
  return request({
    url: interfaceMap.getBookMark + '/' + parameter,
    method: 'get'
  })
}

// 合同获取 id
export function copyTemplate (parameter) {
  return request({
    url: interfaceMap.copyTemplate + '/' + parameter,
    method: 'get'
  })
}

// 合同谈判记录添加
export function negotiateCreateRecord (parameter) {
  return request({
    url: interfaceMap.negotiateCreateRecord,
    method: 'post',
    data: parameter
  })
}

// 合同谈判决策添加
export function negotiateCreateDecision (parameter) {
  return request({
    url: interfaceMap.negotiateCreateDecision,
    method: 'post',
    data: parameter
  })
}

// 任务单详情
export function getTaskListDetail (parameter) {
  return request({
    url: interfaceMap.getTaskListDetail,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本list查询
export function contractTextQuery (parameter) {
  return request({
    url: interfaceMap.contractTextQuery,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本保存
export function contractTextSave (parameter) {
  return request({
    url: interfaceMap.contractTextSave,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本编辑
export function contractTextUpdate (parameter) {
  return request({
    url: interfaceMap.contractTextUpdate,
    method: 'post',
    data: parameter
  })
}

// 合同示范文本修改状态
export function contractTextChangeStatus (parameter) {
  return request({
    url: interfaceMap.contractTextChangeStatus,
    method: 'post',
    data: parameter
  })
}
// 合同示范文本删除
export function removeContractText (parameter) {
  return request({
    url: `${interfaceMap.removeContractText}?id=${parameter.id}`,
    method: 'post'
  })
}
// 合同报表路径
export function contractReportPath (parameter) {
  return request({
    url: interfaceMap.contractReportPath,
    method: 'post',
    data: parameter
  })
}
export function searchInstanceId (parameter) {
  return request({
    url: interfaceMap.searchInstanceId + parameter,
    method: 'get'
  })
}
// 通过合同流水号查询任务单详情
export function getTaskDetailByContractNo (parameter) {
  return request({
    url: interfaceMap.getTaskDetailByContractNo + '/' + parameter,
    method: 'post'
  })
}
// 合同编号查询相关订单
export function getPurchaseContractInfo (parameter) {
  return request({
    url: interfaceMap.getPurchaseContractInfo,
    method: 'get',
    params: parameter
  })
}
// 根据code查部门名称
export function getOrgDetail (parameter) {
  return request({
    url: interfaceMap.getOrgDetail,
    method: 'post',
    data: parameter
  })
}
// 取得合同流水号
export function getSerialNum () {
  return request({
    url: interfaceMap.getSerialNum,
    method: 'post'
  })
}

// 合同编制中获取合同流水号
export function getContractIndex () {
  return request({
    url: interfaceMap.getContractIndex,
    method: 'GET'
  })
}
// 获取在线编辑模板文件的ossID
export function getTemplate (params) {
  return request({
    url: interfaceMap.getTemplate,
    method: 'GET',
    params
  })
}
// 合同编制字段显示隐藏
export function compChecked (parameter) {
  return request({
    url: interfaceMap.compChecked,
    method: 'post',
    data: parameter
  })
}

// 采购订到获取合同名称和相关链接
export function getPurchaseContract (params) {
  return request({
    url: interfaceMap.getPurchaseContract,
    method: 'post',
    data: params
  })
}
// 获取合同要素
export function getAijson (params) {
  // contractIndex
  return request({
    url: interfaceMap.getAijson,
    method: 'get',
    params
  })
}

// 获取合同履约收付款计划
export function getLvplan (params) {
  return request({
    url: interfaceMap.getLvplan,
    method: 'post',
    data: params
  })
}

// 获取合同可视化页面左侧树
export function getShowDataTree (params) {
  return request({
    url: interfaceMap.getShowDataTree + '?configType=' + params,
    method: 'get'
  })
}

// 支出数据
export function getControDisburselMsg (params) {
  return request({
    url: interfaceMap.getControDisburselMsg,
    method: 'post',
    data: params
  })
}
export function getControlImplementationPlanMsg (params) {
  return request({
    url: interfaceMap.getControlImplementationPlanMsg,
    method: 'post',
    data: params
  })
}
export function getControlExpenditurePlanMsg (params) {
  return request({
    url: interfaceMap.getControlExpenditurePlanMsg,
    method: 'post',
    data: params
  })
}
export function getControlPieChartMsg (params) {
  return request({
    url: interfaceMap.getControlPieChartMsg,
    method: 'post',
    data: params
  })
}
export function getContrctDetailTable (params) {
  return request({
    url: interfaceMap.getContrctDetailTable,
    method: 'post',
    data: params
  })
}
export function getContractDetail (params) {
  return request({
    url: interfaceMap.getContractDetail,
    method: 'post',
    data: params
  })
}
export function getTaskContractMoney (params) {
  return request({
    url: interfaceMap.getTaskContractMoney,
    method: 'post',
    data: params
  })
}
// 获取合同要素
export function getArchivistTree (params) {
  // contractIndex
  return request({
    url: interfaceMap.getArchivistTree,
    method: 'get',
    params
  })
}
