import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: () =>
    import('@/home/layout/Home.vue'),
    children: [
        
        {
            path: "GeRadio",
            name: "GeRadio",
            component: () =>
                import('@/home/views/GeRadio.vue'),
            meta: {
                title: "GeRadio 单选框"
            }
        },
        /* {
        path: 'GeInput',
        name: 'GeInput',
        component: () =>
            import('@/home/views/GeInput.vue'),
        meta: {
            title: 'GeInput 输入框',
        },
    },
    {
        path: 'GeInputArea',
        name: 'GeInputArea',
        component: () =>
            import('@/home/views/GeInputArea.vue'),
        meta: {
            title: 'GeInputArea 文本域输入框',
        },
    },
    {
        path: 'GeFile',
        name: 'GeFile',
        component: () =>
            import('@/home/views/GeFile.vue'),
        meta: {
            title: 'GeFile 附件上传',
        },
    },
    {
        path: 'GeCascader',
        name: 'GeCascader',
        component: () =>
            import('@/home/views/GeCascader.vue'),
        meta: {
            title: 'GeCascader 级联选择器',
        },
    },
    {
        path: 'GeSelect',
        name: 'GeSelect',
        component: () =>
            import('@/home/views/GeSelect.vue'),
        meta: {
            title: 'GeSelect 下拉框',
        },
    },
    {
        path: "GeImage",
        name: "GeImage",
        component: () =>
            import('@/home/views/GeImage.vue'),
        meta: {
            title: "GeImage 图片上传"
        }
    },
    {
        path: "GeDataPicker",
        name: "GeDataPicker",
        component: () =>
            import('@/home/views/GeDataPicker.vue'),
        meta: {
            title: "GeDataPicker 日期选择框"
        }
    },
    {
        path: "GeCheckbox",
        name: "GeCheckbox",
        component: () =>
            import('@/home/views/GeCheckbox.vue'),
        meta: {
            title: "GeCheckbox 多选框"
        }
    },
    {
        path: "GeTabel",
        name: "GeTabel",
        component: () =>
            import('@/home/views/GeTabel.vue'),
        meta: {
            title: "GeTabel 表格"
        }
    }, {
        path: "GeMember",
        name: "GeMember",
        component: () =>
            import('@/home/views/GeMember.vue'),
        meta: {
            title: "GeMember 成员选择器"
        }
    }, {
        path: "GeDepart",
        name: "GeDepart",
        component: () =>
            import('@/home/views/GeDepart.vue'),
        meta: {
            title: "GeDepart 部门选择器"
        }
    },
    {
        path: "GeSelector",
        name: "GeSelector",
        component: () =>
            import('@/home/views/GeSelector.vue'),
        meta: {
            title: "GeSelector 人员选择器"
        }
    },
    {
        path: "GeSig",
        name: "GeSig",
        component: () =>
            import('@/home/views/GeSig.vue'),
        meta: {
            title: "GeSig 电子签名"
        }
    },
    {
        path: "GeRadio",
        name: "GeRadio",
        component: () =>
            import('@/home/views/GeRadio.vue'),
        meta: {
            title: "GeRadio 单选框"
        }
    },
    {
        path: "GePost",
        name: "GePost",
        component: () =>
            import('@/home/views/GePost.vue'),
        meta: {
            title: "GePost 岗位选择器"
        }
    },
    {
        path: "GeIdentity",
        name: "GeIdentity",
        component: () =>
            import('@/home/views/GeIdentity.vue'),
        meta: {
            title: "GeIdentity 身份选择器table形式"
        }
    },
    {
        path: "GePark",
        name: "GePark",
        component: () =>
            import('@/home/views/GePark.vue'),
        meta: {
            title: "GePark 园区"
        }
    },
    {
        path: "GePosition",
        name: "GePosition",
        component: () =>
            import('@/home/views/GePosition.vue'),
        meta: {
            title: "GePosition 身份"
        }
    },
    {
        path: "Test",
        name: "Test",
        component: () =>
            import('@/home/views/Test.vue'),
        meta: {
            title: "Test 测试模块"
        }
    },
     */]
},]

const router = new VueRouter({
    routes
})

export default router