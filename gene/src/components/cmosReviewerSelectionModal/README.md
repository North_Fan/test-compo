## ReviewerSelectionModal 审核用户选择弹框

#### 属性

| 名称           | 描述           | 类型    | 必填 | 可选值 | 默认值 |
| -------------- | -------------- | ------- | ---- | ------ | ------ |
| processId      | 流程ID         | String  | 是   |        | ""     |
| v-model(value) | 模态框是否显示 | Boolean | 是   |        | false  |



#### 示例：

```vue
<template>
	<a-button @click="showUserCollection = true">打开模态框</a-button>
	<ReviewerSelectionModal :processId="processId" v-model="showReviewer"></ReviewerSelectionModal>
</template>

<script>
import ReviewerSelectionModal from '@/components/ReviewerSelectionModal/ReviewerSelectionModal.vue'   
    
export default {
    components: { ReviewerSelectionModal },
    data () {
        return {
            showReviewer: false
        }
    },
    methods: {
    }
}
</script>
```

