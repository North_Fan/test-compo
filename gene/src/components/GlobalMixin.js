export default {
    model: { //自定义v-model
        prop: "value",
        event: "backValue",
    },
    props: {
        value: { //接收自定义v-model传过来的值
            type: [String, Number, Array, Object, Boolean],
            default: null,
        },
        /* 配置项 */
        config: {
            type: Object,
            default: () => ({}),
        },
    },
    data() {
        return {
            publicData: {
                width: "100%",
                disabled: false, //是否可编辑
                size: "default",
                /* 组件展示大小,类型：small, default, large */
            }
        }
    },
    computed: {
        vModel: { //实现自定义v-model
            get() {
                return this.value;
            },
            set(e) {
                this.$emit("backValue", e);
            },
        },

        ModifyModel: { //处理绑定的数据
            get() {
                if (!this.value) return []
                let arr = []
                if (Array.isArray(this.value) === true) {
                    this.value.forEach(item => {
                        if (typeof item === "object") {
                            arr.push(item[this.getFieldNames.value])
                        } else {
                            arr.push(item)
                        }
                    })
                } else {
                    arr.push(this.value)
                }
                return arr;
            },
            set(e) {
                if (e instanceof Array) {
                    let arr = []
                    e.forEach(item => {
                        arr.push({
                            [this.getFieldNames.value]: item
                        })
                    })
                    this.$emit("backValue", arr);
                    this.oldOptionSet(arr);
                } else if (e) {
                    this.$emit("backValue", [{
                        [this.getFieldNames.value]: e
                    }]);
                    this.oldOptionSet([{
                        [this.getFieldNames.value]: e
                    }]);
                } else {
                    this.$emit("backValue", []);
                    this.oldOptionSet([]);
                }
            },
        },
        getConfig() {
            return {
                ...this.publicData,
                ...this.privateData ? this.privateData : {},
                ...this.config
            }
        },
        getFieldNames() { //自定义字段名
            if (this.config.fieldNames) {
                return this.config.fieldNames;
            } else {
                return {
                    label: "label",
                    value: "value"
                };
            }
        },
        getWidth() { //获取自定义宽
            let width = this.config.width
            if (!width) {
                return "100%;"
            } else {
                if (typeof width === "string") {
                    return width
                } else if (typeof width === "number") {
                    return width + "px"
                }
            }
        },
        getSlot() {
            let slots = []
            for (const key in this.$slots) {
                slots.push(key)
            }
            return slots
        },

    },
    created() {},
    methods: {
        oldOptionSet(v) {
            const option = this.config.option;
            if (option && option instanceof Array) { // 有option
                const vList = v.map(item => item[this.getFieldNames.value])
                const value = option.reduce((pre, now) => {
                    if (vList.includes(now[this.getFieldNames.value])) {
                        pre.push(now);
                        return pre;
                    }
                    return pre;
                }, []);
                this.$emit('getObject', value, option);
            }
        }
    }
}