const files = require.context('@/components/', true, /\.vue$/);
// import GlobalMixin from "@/components/GlobalMixin.js";
let names = {}
    // 遍历输出匹配结果
files.keys().forEach(key => {

    const component = files(key).default;
    // component["mixins"]=[GlobalMixin]
    // component.install = function(Vue) {
    //     if (component.name) {
    //         Vue.component(component.name, component)
    //     }
    // }
    names[component.name] = component
})
export default names
// import GeCascader from './geCascader/GeCascader.vue';
// import GeCheckbox from './geCheckbox/GeCheckbox.vue';
// import GeDataPicker from './geDataPicker/GeDataPicker.vue';
// import GeDepartNew from './geDepartNew/GeDepartNew.vue';
// import GeFileNew from './GeFileNew/GeFileNew.vue';
// import GeImageNew from './GeImageNew/GeImageNew.vue';
// import GeInfoTable from './geInfoTable/GeInfoTable.vue';
// import GeInput from './geInput/GeInput.vue';
// import GeInputArea from './geInputArea/GeInputArea.vue';
// import GeMember from './geMember/GeMember.vue';
// import GeSelect from './geSelect/GeSelect.vue';
// import GeTable from './geTable/GeTable.vue';
// GeCascader.install = function(Vue) {
//     Vue.component(GeCascader.name, GeCascader);
// };
// GeCheckbox.install = function(Vue) {
//     Vue.component(GeCheckbox.name, GeCheckbox);
// };
// GeDataPicker.install = function(Vue) {
//     Vue.component(GeDataPicker.name, GeDataPicker);
// };
// GeDepartNew.install = function(Vue) {
//     Vue.component(GeDepartNew.name, GeDepartNew);
// };
// GeFile.install = function(Vue) {
//     Vue.component(GeFile.name, GeFile);
// };
// GeImage.install = function(Vue) {
//     Vue.component(GeImage.name, GeImage);
// };
// GeInfoTable.install = function(Vue) {
//     Vue.component(GeInfoTable.name, GeInfoTable);
// };
// GeInput.install = function(Vue) {
//     Vue.component(GeInput.name, GeInput);
// };
// GeInputArea.install = function(Vue) {
//     Vue.component(GeInputArea.name, GeInputArea);
// };
// GeMember.install = function(Vue) {
//     Vue.component(GeMember.name, GeMember);
// };
// GeSelect.install = function(Vue) {
//     Vue.component(GeSelect.name, GeSelect);
// };
// GeTable.install = function(Vue) {
//     Vue.component(GeTable.name, GeTable);
// };
// let names = [
//     GeCascader,
//     GeCheckbox,
//     GeDataPicker,
//     GeDepartNew,
//     GeFile,
//     GeImage,
//     GeInfoTable,
//     GeInput,
//     GeInputArea,
//     GeMember,
//     GeSelect,
//     GeTable,
// ]
// export default names