## UserSelectionModal 用户选择弹框

#### 属性

| 名称               | 描述           | 类型    | 必填 | 可选值 | 默认值 |
| ------------------ | -------------- | ------- | ---- | ------ | ------ |
| showUserCollection | 模态框是否显示 | Boolean | 是   |        | false  |
| multiCheck         | 是否支持多选   | Boolean | 否   |        | true   |



#### 事件

| 名称        | 描述           | 参数                                                         | 是否必填 | 备注 |
| ----------- | -------------- | ------------------------------------------------------------ | -------- | ---- |
| handleClose | 模态框关闭回调 | -                                                            | 是       |      |
| handleOk    | 模态框确认回调 | multiCheck为true时，Array<SelectedUser><br />multiCheck为false时，SelectedUser | 否       |      |

#### 示例：

```vue
<template>
	<a-button @click="showUserCollection = true">打开模态框</a-button>
	<user-selection-modal
    	:showUserCollection="showUserCollection"
        @handleClose="handleClose"
        @handleOk="handleOk">
    </user-selection-modal>
</template>

<script>
import UserSelectionModal from '@/components/UserSelectionModal/index.vue'    
    
export default {
    components: { UserSelectionModal },
    data () {
        return {
            showUserCollection: false
        }
    },
    methods: {
        handleClose () {
            this.showUserCollection = false
        },
        handleOk (data) {
          console.log(data)
        }
    }
}
</script>
```

