### AdvTable组件 API

| 属性名称               | 描述                              | 是否必填                            | 属性类型                              | 可选值 | 默认值 |
| ---------------------- | --------------------------------- | ----------------------------------- | ------------------------------------- | ------ | ------ |
| <extend Table.Props>   | Ant Design Vue -- Table的所有属性 |                                     |                                       |        |        |
| showSearchOfTableData  | 是否显示表内模糊查询              | 否                                  | Boolean                               |        | true   |
| showLegend             | 是否显示图例                      | 否                                  | Boolean                               |        | true   |
| showViews              | 是否显示视图                      | 否                                  | Boolean                               |        | true   |
| showAdvSearch          | 是否显示高级搜索                  | 否                                  | Boolean                               |        | true   |
| cusColumns             | 自定义列对象集合                  | 是                                  | Array<CusColumn>                      |        | []     |
| fuzzyQueryMainFieldKey | 表内模糊查询的key值               | showSearchOfTableData为true时，必填 | String                                |        | ""     |
| tableName              | 表名（该组件于项目中的唯一值）    | 是                                  | String                                |        | ""     |
| legends                | 图例列表                          | showLegend为true时，必填            | Array<Legend>                         |        | []     |
| customeViewList        | 自定义视图集合                    | 否                                  | Array<CustomeView>                    |        | []     |
| dataLoader             | 表格数据加载函数(接受Promise函数) | 是                                  | Function: (params) => { return data } |        | null   |
| customeTable           | 自定义表格标识                    | 否                                  | Boolean                               |        | false  |
| showDownloadButton     | 是否显示下载按钮                  | 否                                  | Boolean                               |        | true   |
| showColumnFilterButton | 是否显示表头过滤按钮              | 否                                  | Boolean                               |        | true   |
| showPagination         | 是否显示分页功能                  | 否                                  | Boolean                               |        | true   |

### CusColumn属性

| 属性名称              | 描述                                       | 是否必填 | 属性类型   | 可选值               | 默认值 |
| --------------------- | ------------------------------------------ | -------- | ---------- | -------------------- | ------ |
| <extend Column.Props> | Ant Design Vue -- Table > Column的所有属性 |          |            |                      |        |
| searchFlag            | 高级搜索的显示标识位                       | 否       | Boolean    |                      | null   |
| inputType             | 高级搜索的表单项类型                       | 是       | String     | input, select, radio |        |
| selectList            | inputType为select时生效，为下拉选列表数据  | 否       | Array<any> |                      | []     |
| checkDisabled         | 是否禁用自定义列选择按钮                   | 否       | Boolean    |                      | false  |

### CustomeView

| 属性名称             | 描述               | 是否必填 | 属性类型      | 可选值 | 默认值    |
| -------------------- | ------------------ | -------- | ------------- | ------ | --------- |
| colsNames            | 显示列的dataIndex  | 是       | Array<String> |        | []        |
| viewName             | 视图名称           | 是       | String        |        | ""        |
| advSearchForm        | 高级搜索表单数据   | 否       | Object        |        | {}        |
| tableDataSearchValue | 表格数据模糊查询值 | 否       | String        |        | ""        |
| icon                 | 自定义视图的图标   | 否       | String        |        | "cluster" |

##### CustomeView：

```javascript
const customeView = {
    colsNames: [
        'col1',
        'col2',
        'col3'
    ],
    viewName: '全部视图',
    advSearchForm: {
        col1: 'aa',
        col2: 'bb'
    },
    tableDataSearchValue: 'cc',
    icon: 'cluster'，
    iconStyle： {
    	color: 'green'
	}
}
```



### Legend

| 属性名称 | 描述                     | 是否必填 | 属性类型 | 可选值 | 默认值 |
| -------- | ------------------------ | -------- | -------- | ------ | ------ |
| id       | 列表项key                | 是       | String   |        |        |
| icon     | 列表项图标(参考icon图标) | 是       | String   |        |        |
| label    | 列表项名称               | 是       | String   |        |        |



### 插入自定义表格

```vue
<adv-table
   customeTable>
    <template slot="customeTable">
    	<a-table>
        </a-table>
    </template>
</adv-table>
```



### 插入自定义工具栏

```vue
<adv-table>
    <template slot="toolsGroup">
		<div>
            <a-button></a-button>
            <a-button></a-button>
            <a-button></a-button>
        </div>
    </template>
</adv-table>
```



### 自定义列布局

```vue
<adv-table
   :cusColumns="columns"
   tableName="cc"
   fuzzyQueryMainFieldKey="bb"
   :customeViewList="customeViewList">
    <template slot="input_1" slot-scope="record">{{ record.value + '哈哈哈' }}</template>
</adv-table>

<script>
export default {
    data() {
        return {
            columns: [
                {
                  title: 'input_1',
                  dataIndex: 'input_1',
                  inputType: 'input',
                  width: 150,
                  searchFlag: true,
                  scopedSlots: { customRender: 'input_1' }
                }
             ]
        }
    }
}
</script>
```



### dataLoader

```vue
<adv-table
   :dataLoader="getTableData">
</adv-table>

<script>
export default {
    methods: {
        // 请自行处理异常
        async getTableData(params) {
          let tableData = {}
          const { result: { data, pageSize, pageNum, totalCount } } = await advancedTabQuery(params)
          tableData.list = data
          tableData.pageSize = pageSize
          tableData.pageNum = pageNum
          tableData.total = totalCount
          return tableData
        }
    }
}
</script>
```

