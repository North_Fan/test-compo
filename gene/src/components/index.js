import {
    antComponents
} from '@/utils/ant-design-vue-ui'
import jsPlumb from 'jsplumb'
import "@/assets/style/index.less"
import "@/assets/style/cmos-global.less"
import names from "@/components/export.js"
import VueDraggableResizable from 'vue-draggable-resizable'
import PageLoading from '@/components/PageLoading'

import moment from 'moment'
import 'moment/locale/zh-cn';
moment.locale('zh-cn'); //配置moment中文环境
// 工具函数
import * as tools from '@/utils/tools.js';
import Bus from '@/utils/bus'

/**
 * 注册install方法
 * @param Vue 传入的Vue实例
 * @param api 处理过的项目请求文件
 * @param suffix 组件自定义后缀名称 如suffix="New",则使用组件时为ge-file-new
 * */
const install = function (Vue, api, suffix = "") {
    antComponents.forEach(com => {
        console.log(com.name,'====1')
        if (com.name === 'message') {
            Vue.prototype.$message = com
        }
        if (com.name === 'notification') {
            console.log(com,'====1')
            Vue.prototype.$notification = com
        }
        if (com.name === 'AModal') {
            Vue.prototype.$confirm = com.confirm
            Vue.prototype.$info = com.info
            Vue.prototype.$success = com.success
            Vue.prototype.$error = com.error
            Vue.prototype.$warning = com.warning
        }
        Vue.use(com)
    })
    for (const key in names) {
        Vue.component(key + suffix, names[key])
    }
    Vue.prototype.$jsPlumb = jsPlumb.jsPlumb
    Vue.prototype.$EventController = Bus
    Vue.prototype.$req = api
    Vue.prototype.$moment = moment

    Vue.use(PageLoading)
    Vue.component('vue-draggable-resizable', VueDraggableResizable)

}
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

export const geSig = names['geSig']

export default {
    install,
    ...tools,
    ...names
}