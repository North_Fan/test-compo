### AdvTable组件 API

| 属性名称               | 描述                           | 是否必填                            | 属性类型           | 可选值 | 默认值 |
| ---------------------- | ------------------------------ | ----------------------------------- | ------------------ | ------ | ------ |
| buttonTitle            | 按钮名称                       | 是                                  | String            |        | ""   |
| modelTitle             | 弹窗名称                       | 是                                  | String            |        | ""   |
| ConfigId               | 后端服务入参ID                 | 是                                  | String            |        | ""   |
| modelBol               | 弹窗显隐                       | 是                                  | Boolean            |        | false   |



### 自定义列布局

```vue
<VueFileTabs
    :modelBol="modelBol"
    @handleSave="handleSave"
    @handleSubmit="handleSubmit"
    :configId="configId"
    buttonTitle="配置"
    modelTitle="测试配置"
/>

<script>
export default {
    data() {
        return {
            tabArray:[],
            configId:''//modelBol为true前赋值
        }
    },
    methods:{
        showModal(){
            businessTagList({ id: this.groupId }).then((res) => {
                this.tabArray = res.result.data
            })
        },
        handleSave(){
            //暂存调用的方法
        },
        handleSubmit(){
            //提交调用的方法
        }
    }
}   
</script>
```

