const acceptList = {
  GIF: 'image/gif',
  JPG: 'image/jpg',
  JPEG: 'image/jpeg',
  PNG: 'image/png',
  DOC: 'application/msword',
  EXCEL: 'application/vnd.ms-excel',
  PDF: 'application/pdf',
  TXT: 'text/plain',
  PPT: 'appication/powerpoint'
}

const acceptTypeList = ['doc', 'docm', 'docx', 'dot', 'dotm', 'dotx', 'epub', 'fodt', 'htm', 'html', 'mht', 'odt', 'ott', 'pdf', 'rtf', 'txt', 'djvu', 'xps', 'csv', 'fods', 'ods', 'ots', 'xls', 'xlsm', 'xlsx', 'xlt', 'xltm', 'xltx', 'fodp', 'odp', 'otp', 'pot', 'potm', 'potx', 'pps', 'ppsm', 'ppsx', 'ppt', 'pptm', 'pptx', 'png', 'jpg', 'jpeg']

export { acceptList, acceptTypeList }
