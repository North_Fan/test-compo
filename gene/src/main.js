import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {TestURLFylUI} from 'test-url-fyl' // 注册Ge组件
import * as api from "@/api/base/manage.js"
import "@/assets/style/index.less"
Vue.use(TestURLFylUI)
Vue.config.productionTip = false

import {
    Menu
} from 'ant-design-vue'
// import '@/style/index.less'
Vue.use(Menu)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')