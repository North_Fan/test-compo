export const resVueFileTab = {
    "errCode": "200",
    "errMessage": "SERVICE SUCCESS",
    "data": {
        "extValues": {},
        "assignmentVersionId": "1513834279404486656",
        "startTemplateData": {
            "extValues": {},
            "startTemplateCode": "mouldFrame",
            "startTemplateName": "产品故障",
            "startTemplateUrl": "components/MouldFrame/MouldFrame",
            "businessCode": "quality",
            "formCode": "ProblemRecord",
            "formUrl": "home/views/Test",
            "objectType": "quality_problem|product_failure|issue_record",
            "objectViewIndex": "ProblemRecord",
            "params": {}
        },
        "tab": [],
        "commonToolBar": {
            "submit": {
                "permission": "s",
                "url": "/quality/biz/productFailure/submit"
            },
            "adjust": {
                "permission": "h",
                "url": "/assignment/biz/service/instance/wf/submit"
            },
            "save": {
                "permission": "s",
                "url": "/quality/biz/productFailure/save"
            },
            "history": {
                "permission": "s"
            },
            "knowledge": {
                "permission": "s"
            }
        },
        "taskData": {
            "ProblemRecord": {
                "FORM_NO": "20240118-0033",
                "EDITED_DATE": "2024-01-18T15:11:21+0800"
            }
        },
        "permissions": {
            "global": {
                "readOnly": "false"
            },
        },
        "processInstanceId": null,
        "flowInstanceId": null,
        "actionContent": null
    },
    "success": true
}