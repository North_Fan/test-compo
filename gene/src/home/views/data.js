import { findById } from '@/utils/util'

const formItemList = [
  {
    queryId: 'supplierRejectMessage',
    queryName: '驳回供应商意见',
    placeholder: '',
    inputType: 'input'
  },{
    queryId: 'select',
    queryName: '选择',
    placeholder: '',
    inputType: 'select',
    selectCode:'ITEM_TPYE'
  }
]
const layout = [
  {
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('supplierRejectMessage', formItemList)
  } , {
    col: [8, 12, 24],
    flexLayout: false,
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
    item: findById('select', formItemList)
  }
]

export const dynamicFormData = {
  formItemList,
  layout
}
