import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

const TableKey = 'Advanced-Table'

export function getToken () {
  return Cookies.get(TokenKey)
}

export function setToken (token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken () {
  return Cookies.remove(TokenKey)
}

export function setTableQuery (v) {
  return Cookies.set(TableKey, v)
}

export function getTableQuery (v) {
  return Cookies.get(TableKey)
}

export function removeTableQuery (v) {
  return Cookies.remove(TableKey)
}
