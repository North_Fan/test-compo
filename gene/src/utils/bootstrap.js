import request from './yunqiaoRequest'
// import { registerMicroApps, loadMicroApp, start } from 'qiankun'
var apps = []
export function CmosBootstrap (siteCode, container) {
    return new Promise(async (resolve, reject) => {
        const siteInfoResp = await request.get('/site', { maxRedirects: 2 })
        console.log('云巧站点信息：', siteInfoResp)
        apps = siteInfoResp.data.apps.filter(function (item) {
            return item.type === 'MICRO_FE'
        })
        console.log(apps)
        const menuInfoResp = await request.get('/site/menus', { maxRedirects: 2 })
        console.log('云巧菜单信息：', menuInfoResp)
        const menus = menuInfoResp.data.map(generateMenu)
        window.YUNQIAO_PORTAL = {
            siteInfo: siteInfoResp.data,
            menuInfo: menus,
            siteCode: 'devSite'
        }
        const microApps = siteInfoResp.data.apps.filter(function (item) {
            return item.type === 'MICRO_FE'
        }).map(function (item) {
            return {
                name: item.name,
                entry: '//' + item.publicEndpoint + '/index.yunqiao.html', /* item.entry */
                container: container,
                activeRule: '/' + item.name
            }
        })
        microApps.push(
            {
                name: 'vueApp', // 应用的名字
                entry: '//localhost:3001', // 默认会加载这个html 解析里面的js 动态的执行 （子应用必须支持跨域）fetch
                container: '#CMOS_CONTAINER', // 容器id
                activeRule: '/cmos/task' // 根据路由 激活的路径
            }
        )
        console.log(microApps, 'microApps')

        // loadMicroApp(app, {
        //     fetch (url, ...args) {
        //         // 给指定的微应用 entry 开启跨域请求
        //         if (url === 'http://app.alipay.com/entry.html') {
        //             return window.fetch(url, {
        //                 ...args,
        //                 mode: 'cors',
        //                 credentials: 'include'
        //             })
        //         }

        //         return window.fetch(url, ...args)
        //     }
        // })
        // registerMicroApps(microApps)
        // start({
        //     // fetch (url, ...args) {
        //     //     // 给指定的微应用 entry 开启跨域请求
        //     //     if (url === 'http://localhost:8080') {
        //     //         return window.fetch(url, {
        //     //             ...args,
        //     //             mode: 'cors',
        //     //             credentials: 'include'
        //     //         })
        //     //     }

        //     //     return window.fetch(url, ...args)
        //     // }
        //     // prefetch: 'all'
        // })
        resolve(true)
    })
}

export function generateMenu (menu) {
    var ItemChildren = null

    if (menu.children) {
        ItemChildren = menu.children.map(generateMenu)
    }
    var path = menu.itemId
    if (menu.page) {
        if (menu.appCode && menu.appCode === 'cmos') {
            path = '/' + menu.appCode + menu.page
        } else {
            path = menu.page
        }
    }
    return {
        path: path,
        name: menu.name,
        title: menu.name,
        icon: menu.icon,
        children: ItemChildren
    }
}
