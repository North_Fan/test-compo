/* eslint-disable no-unused-vars */
/* eslint-disable no-inner-declarations */
import Vue from 'vue'
import * as JSZip from 'jszip';
import moment from 'moment'
import axios from 'axios'
import { message } from 'ant-design-vue';

/* 
 *下载图片
 */
export function downloadIamge(file) {
    
}
/* 
 *通过OSS下载附件
 */
export function downloadFile() {
   
}
/* 
 *通过OSS下载附件 - 顺丰
 @param file 下载附件
 @return 数据流
 */
function getDownloadUrl_SF_OSS() {
    
}

/* 
 * 通过内部网址下载
 */
export function downloadFileApi() {

}

/**
 * 获取数据流
 * @param url 附件的下载路径
 * @return Promise对象 数据流
 * */
export function getFile(url) {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url,
            responseType: 'blob'
        }).then(data => {
            resolve(data.data)
        }).catch(error => {
            // 在此写入你的错误处理
            // 可以在此处处理promise.all失败的情况，放在methods里主要也是为了方便对data里的变量处理
            reject(error.toString())
        })
    })
}

/**
 * 通过OSS批量下载
 * @param name 压缩包名
 * @param filelist 附件数组
 * */
export function batchDownload() {
   
}

/**
 * 通过内部网址批量下载
 * @param name 压缩包名
 * @param filelist 附件数组
 * */
export function batchDownloadApi() {
    
}

/**
 * 获取混合前的文件名
 * @param fileName 混合过的文件名
 * @return 混合前的文件名
 * */
/* function getFileName(fileName) {
    const name1 = fileName.substring(0, fileName.indexOf("_"))
    const name2 = fileName.substring(fileName.lastIndexOf("."), fileName.length);
    return name1 + name2
} */

/* 用于节流处理的标识 */
let flag = false
/* 请求体，一直接收一个Promise对象 */
let request = null
/**
 * 获取OSS token 
 * @return promise 返回请求token的信息
 * */
export function getOSSToken() {
    const time = localStorage.getItem('ossTokenTime')
    const ossInfo = localStorage.getItem('ossInfo')
    // 判断是否超时
    if ((!time || parseInt(time) + 60 * 60 * 1000 <= parseInt(Date.now())) || ossInfo === 'undefined' || ossInfo === 'null') {
        // 判断是否是第一次请求
        if (!flag) {
            flag = true
            const req = Vue.prototype.$req
            request = new Promise((resolve, reject) => {
                req.getAction('/client/file/token').then(res => {
                    localStorage.setItem('ossTokenTime', Date.now())
                    localStorage.setItem('ossInfo', JSON.stringify(res))
                    resolve(localStorage.getItem('ossInfo'))
                    flag = false
                }).catch(error => {
                    reject(error)
                })
            })
            return request
        } else {
            return request
        }

    } else {
        return Promise.resolve(ossInfo)
    }
}
/**
 * 上传附件
 * @param file 上传的附件
 * @param onUploadProgress 上传进度的方法，返回上传的进度
 * */
export function uploadFile({
    file,
    onUploadProgress
}) {
    return new Promise((resolve, reject) => {
        getOSSToken().then((OSSInfo) => {
            if (!OSSInfo) {
                reject('Error!')
            }
            OSSInfo = JSON.parse(OSSInfo)
            // 上传到阿里云
            if (OSSInfo.useOss && OSSInfo.ossType === 'SF_OSS') {
                // 上传到顺丰
                upload_SF_OSS({ file, onUploadProgress, resolve, reject }, OSSInfo)

            } else {
                upload_ALI_OSS({ file, onUploadProgress, resolve, reject }, OSSInfo)
            }
        })
    })
}

/**
 * 上传附件到阿里云
 * @param file 上传的附件
 * @param onUploadProgress 上传进度的方法，返回上传的进度
 * @param resolve 成功回调
 * @param reject 失败回调
 * @param OSSInfo 上传需要的配置参数
 * */
function upload_ALI_OSS({ resolve, reject }) {
    // const OSS = require("ali-oss");
    // const client = new OSS(OSSInfo);
    // 定义上传方法。
    async function multipartUpload() {
        try {
            // 填写Object完整路径。Object完整路径中不能包含Bucket名称。
            // 您可以通过自定义文件名（例如exampleobject.txt）或目录（例如exampledir/exampleobject.txt）的形式，实现将文件上传到当前Bucket或Bucket中的指定目录。
            // const name = mixinName(file.name)
            // const inFile = new File([file], name)
            // const result = await client.multipartUpload('pcUploadFolder/' + name, inFile, {
            //     progress: function (p, cpt) {
            //         console.log(p, cpt, client);
            //         // checkpoint参数用于记录上传进度，断点续传上传时将记录的checkpoint参数传入即可。浏览器重启后无法直接继续上传，您需要手动触发上传操作。
            //         if (judgeType(onUploadProgress, 'function').isType) {
            //             onUploadProgress(p, cpt, client)
            //         }
            //     },
            //     parallel: 4,
            //     // 设置分片大小。默认值为1 MB，最小值为100 KB。
            //     partSize: 1024 * 1024,
            //     meta: {
            //         year: 2020,
            //         people: 'test'
            //     },
            //     mime: 'text/plain'
            // })
            resolve({ })
        } catch (e) {
            reject(e)
        }
    }
    // 开始分片上传。
    multipartUpload();
}

/**
 * 上传附件到顺丰
 * @param file 上传的附件
 * @param onUploadProgress 上传进度的方法，返回上传的进度
 * @param resolve 成功回调
 * @param reject 失败回调
 * @param OSSInfo 上传需要的配置参数
 * */
function upload_SF_OSS({ file, onUploadProgress, resolve, reject }, OSSInfo) {
    let CancelToken = axios.CancelToken;//axios的config中提供了一个cancelToken属性，可以通过传递一个新的CancelToken对象来在请求的任何阶段关闭请求。
    let source = CancelToken.source();

    const { stsToken, ossUrl, bucket } = OSSInfo
    // 处理后的文件名称(防止重名覆盖)
    const name = 'pcUploadFolder/' + mixinName(file.name)
    const reader = new FileReader();
    reader.readAsArrayBuffer(file)
    reader.onload = () => {
        const data = reader.result;
        axios.put(`${ossUrl}/${bucket}/${name}`, data, {
            headers: {
                'X-Storage-Token': stsToken
            },
            cancelToken: source.token,
            onUploadProgress: progressEvent => {
                let p = progressEvent.loaded / progressEvent.total
                if (judgeType(onUploadProgress, 'function').isType) {
                    // 模拟一个阿里的client对象
                    let client = {
                        abortMultipartUpload: () => {
                            source.cancel('中止上传');
                            source = CancelToken.source();
                        }
                    }
                    onUploadProgress(p, {}, client)
                }
            }
        })
            .then((response) => {
                if (response.status === 201) {
                    resolve({ name, ossType: OSSInfo.ossType })
                } else {
                    reject()
                }
            }).catch(error => {
                reject(error)
            })
    }
}

/**
 * 生成上传附件的名称
 * @param fileName 上传的附件名称
 * @return 处理后的附件名称
 * */
export function mixinName(fileName) {
    // console.log('fileName', fileName);
    let imageName = "";
    let timestamp = moment(Date.now()).format('YYYYMMDDHHmmss')
    imageName = timestamp.toString() + "_" + parseInt((Math.random() * 9 + 1) * 100000);
    // let name = fileName.substring(0, fileName.lastIndexOf("."));
    let type = fileName.substring(fileName.lastIndexOf("."), fileName.length);
    // console.log('123456789-', name + '_' + imageName + type);    
    return imageName + type;
}
/**     
 * 获取附件下载的路径
 * @param file 上传的附件
 * @return 附件下载的路径
 * */
export function getDownloadUrl() {
    return new Promise((resolve) => {
        // let url = "";
        // getOSSToken().then((OSSInfo) => {
        //     if (OSSInfo) {
        //         OSSInfo = JSON.parse(OSSInfo)
        //         const OSS = require("ali-oss");
        //         const client = new OSS(OSSInfo);
        //         // 配置响应头实现通过URL访问时自动下载文件，并设置下载后的文件名。
        //         // const filename = this.file.name; // 自定义下载后的文件名。
        //         const response = {
        //             "content-disposition": `attachment; filename=${encodeURIComponent(
        //                 file.url
        //             )}`,
        //         };
        //         // 填写Object完整路径。Object完整路径中不能包含Bucket名称。
        //         url = client.signatureUrl(file.url, {
        //             response
        //         });
        //         resolve(url)
        resolve('url')
        // }).catch(error => {
        //     reject(error)
        // })
    })
}

/**     
 * 将下载的图片转成base64
 * @param file 上传的附件
 * @return base64
 * */
export function downloadFile2base64(file) {
    return new Promise((resolve) => {
        // 判断是否通过阿里服务器下载图片
        if (file.useOss || file.url.indexOf("pcUploadFolder") !== -1 || file.url.indexOf("mUploadFolder") !== -1) {
            getDownloadUrl(file).then(url => {
                getFile(url).then(blob => {
                    const fileReader = new FileReader();
                    fileReader.onload = (e) => {
                        resolve(e.target.result);
                    };
                    fileReader.readAsDataURL(blob);
                    fileReader.onerror = () => {
                        resolve(null);
                    };
                }).catch(() => {
                    // 下载失败，处理成功状态，返回null
                    resolve(null);
                })
            }).catch(() => {
                resolve(null);
            })
        } else {
            getFile('/api' + file.url).then(blob => {
                const fileReader = new FileReader();
                fileReader.onload = (e) => {
                    resolve(e.target.result);
                };
                fileReader.readAsDataURL(blob);
                fileReader.onerror = () => {
                    resolve(null);
                };
            }).catch(() => {
                // 下载失败，处理成功状态，返回null
                resolve(null);
            })
        }

    })
}

/**
 * 获取附件预览的路径
 * @param file 上传的附件
 * @return 附件预览的路径
 * */
export function previewImg(file) {
    return new Promise((resolve, reject) => {
        let url = "";
        // 附件格式处理
        file = isJSON(file.url) ? JSON.parse(file.url) : file
        // 本地服务器获取URL
        if (!judgeOssType(file)) {
            // 获取当前项目的完整路径
            let host = `${window.location.protocol}//${window.location.host}`
            // let host = 'https://dev.gean.cloud:32238'
            resolve(host + "/api" + file.url)
        } else {
            getOSSToken().then((OSSInfo) => {
                if (OSSInfo) {
                    OSSInfo = JSON.parse(OSSInfo)
                    if (file.useOss && file.ossType === 'SF_OSS') {
                        // 顺丰预览
                        getPreviewUrl_SF_OSS({ file, resolve, reject, OSSInfo })
                    } else {
                        // 阿里云预览
                        getPreviewUrl_ALI_OSS({ file, resolve, reject, url, OSSInfo })
                    }
                }
            }).catch(error => {
                reject(error)
            })
        }

    })

}

/**
 * 获取附件预览的路径 - 阿里云
 * @param file 上传的附件
 * @param url 返回的预览路径
 * @param resolve 成功回调
 * @param reject 失败回调
 * @param OSSInfo 上传需要的配置参数
 * */
function getPreviewUrl_ALI_OSS({  resolve }) {
    // const OSS = require("ali-oss");
    // const client = new OSS(OSSInfo);
    // // 填写Object完整路径。Object完整路径中不能包含Bucket名称。文件URL的有效时长默认为1800秒，即30分钟。
    // let isNumber = typeof file
    // if (isNumber === 'object') {
    //     url = isJSON(file.url) ? client.signatureUrl(JSON.parse(file.url).url) : client.signatureUrl(file.url)
    // } else {
    //     url = isJSON(file.url) ? client.signatureUrl(file.url) : client.signatureUrl(JSON.parse(file).url);
    // }
    resolve('urlurl')
}

/**
 * 获取附件预览的路径 - 顺丰
 * @param file 上传的附件
 * @param resolve 成功回调
 * @param OSSInfo 上传需要的配置参数
 * */
function getPreviewUrl_SF_OSS({ file, resolve, OSSInfo }) {
    const { bucket } = OSSInfo
    // 获取后缀名
    // let n = file.name.lastIndexOf(".");
    // let sufName = file.name.slice(n + 1);
    // let host = 'https://dev.gean.cloud:32238'
    let fileUrl = `${bucket}/${file.url}`
    // let fileUrl = `https://dev.gean.cloud:32238/api/client/file/skip/sf/remote-file-stream/?url=${ossUrl}/${bucket}/${file.url}&token=${stsToken}`
    // 图片类型需要将blob类型转成img可展示的url
    // if (getFileType(sufName) === 'png') {
    //     axios({
    //         method: 'get',
    //         url: fileUrl,
    //         responseType: 'blob'
    //     }).then(res => {
    //         resolve(window.URL.createObjectURL(new Blob([res.data])))
    //     })
    // } else {
    //     resolve(fileUrl)
    // }
    Vue.prototype.$req.getAction('/client/file/skip/sf/temp-link', { path: fileUrl }).then(res => {
        resolve(res)
    })

}

/**
 * 判断数据类型
 * 类型有：string,object,array,null,undefined,boolean,number,function,symbol,date
 * @param data 需要判断的参数
 * @param type 需要比对的参数类型
 * @return {Object} {isType:(Boolean)是否符合需要判断的类型，type:(String)参数的具体类型}
 * */
export function judgeType(data, type) {
    let currentType = ""
    //如果不是object类型的数据，直接用typeof就能判断出来
    if (typeof data !== 'object') {
        currentType = typeof data
    } else {
        //如果是object类型数据，准确判断类型必须使用Object.prototype.toString.call(obj)的方式才能判断
        //.replace(/^\[object (\S+)\]$/, '$1');  主要是把  第一个对象去掉
        // 并且把结果转成小写
        currentType = (Object.prototype.toString.call(data).replace(/^\[object (\S+)\]$/, '$1')).toLowerCase();
    }
    return {
        isType: currentType === type.toLowerCase(),
        type: currentType
    }
}

/**
 * 判断变量是否是JSON格式
 * @param data 需要判断的参数
 * @return Boolean json格式判断结果，true为JSON格式
 * */
export function isJSON(data) {
    if (typeof data == 'string') {
        try {
            var obj = JSON.parse(data);
            if (typeof obj == 'object' && obj) {
                return true;
            } else {
                return false;
            }

        } catch (e) {
            return false;
        }
    }
}

// 文件后缀名获取
export const getFileType = (fileName) => {
    let suffix = fileName.toLocaleLowerCase();
    let result;
    // 图片格式
    const imglist = ["png", "jpg", "jpeg", "bmp", "gif", "svg"];
    // 进行图片匹配
    result = imglist.find((item) => item === suffix);
    if (result) {
        return "png";
    }
    // 匹配txt
    const txtlist = ["txt"];
    result = txtlist.find((item) => item === suffix);
    if (result) {
        return "txt";
    }
    // 匹配 excel
    const excelist = ["xls", "xlsx"];
    result = excelist.find((item) => item === suffix);
    if (result) {
        return "Excel";
    }
    // 匹配 word
    const wordlist = ["doc", "docx"];
    result = wordlist.find((item) => item === suffix);
    if (result) {
        return "word";
    }
    // 匹配 pdf
    const pdflist = ["pdf"];
    result = pdflist.find((item) => item === suffix);
    if (result) {
        return "pdf";
    }
    // 匹配 ppt
    const pptlist = ["ppt", "pptx"];
    result = pptlist.find((item) => item === suffix);
    if (result) {
        return "ppt";
    }
    // 匹配 视频
    const videolist = ["mp4", "m2v", "mkv", "rmvb", "wmv", "avi", "flv", "mov", "m4v"];
    result = videolist.find((item) => item === suffix);
    if (result) {
        return "video";
    }
    // 匹配 音频
    const radiolist = ["mp3", "wav", "wmv"];
    result = radiolist.find((item) => item === suffix);
    if (result) {
        return "radio";
    }

    const ziplist = ["zip"];
    result = ziplist.find((item) => item === suffix);
    if (result) {
        return "zip";
    }
    // 其他 文件类型
    return "other";
};

/**
 * 分片上传处理
 * @param options 配置参
 * file：上传文件
 * pieceSize：每片的大小
 * baseURL：分片上传的接口地址
 * mergeURL:合并上传接口
 * selectedSize：用于抽取文件首尾各多少字节作为md5值
 * success：成功后回调
 * uploading：分片传输中回调
 * error：报错回调
 * requestTime: 分片请求失败重新请求不得超过次数
 * @return Boolean json格式判断结果，true为JSON格式
 * */
export function uploadByPieces({
    file,
    pieceSize = 1,
    baseURL = '/client/file/webUploader',
    mergeURL = '/client/file/webMerge',
    selectedSize = 1024,
    success,
    uploading,
    error,
    requestTime = 4
}) {
    let curRequest = 0 //分片上传次数
    // 上传过程中用到的变量
    const randomNumber = Date.now() + String(Math.round(Math.random() * 100000)) // 生成随机数
    const fileSize = file.size //附件的大小
    const chunkSize = pieceSize * 1024 * 1024; // pieceSize:10 MB一片
    const chunkCount = Math.ceil(fileSize / chunkSize); // 总片数
    let identifier = ''
    // 计算md5的值
    const countMd5 = async () => {
        return new Promise(function async(resolve) {
            const fileReader = new FileReader()
            if (fileSize > selectedSize) {
                // 文件字节大于分割字节
                const loadFile = (start, end) => {
                    const slice = file.slice(start, end);
                    fileReader.readAsBinaryString(slice);
                }
                loadFile(0, selectedSize);
            } else {
                fileReader.readAsBinaryString(file);
            }
        })
    }
    // 获取file分片
    const getChunkInfo = (file, currentChunk, chunkSize) => {
        let start = (currentChunk - 1) * chunkSize;
        let end = Math.min(fileSize, start + chunkSize);
        let chunk = file.slice(start, end);
        return {
            start,
            end,
            chunk
        };
    };
    // 调用接口上传文件分片
    const uploadChunk = chunkInfo => {
        let fetchForm = new FormData();
        fetchForm.append("chunk", chunkInfo.currentChunk - 1); //第几分片
        fetchForm.append("chunks", chunkInfo.chunkCount); //分片总数
        fetchForm.append("guid", identifier); // 文件唯一标识 md5
        fetchForm.append("size", fileSize); // 文件大小
        fetchForm.append("name", file.name); // 文件名称
        fetchForm.append("upload", chunkInfo.chunk, file.name); // 分块文件传输对象
        Vue.prototype.$req.uploadAction(baseURL, fetchForm, null, true).then(() => {
            // 重置请求次数
            curRequest = 0
            if (chunkInfo.currentChunk < chunkInfo.chunkCount) {
                const {
                    chunk
                } = getChunkInfo(
                    file,
                    chunkInfo.currentChunk + 1,
                    chunkSize
                );
                uploadChunk({
                    chunk,
                    currentChunk: chunkInfo.currentChunk + 1,
                    chunkCount: chunkInfo.chunkCount
                });
                uploading && uploading({
                    file,
                    percentage: chunkInfo.currentChunk / chunkInfo.chunkCount
                });
            } else {
                // 当总数大于等于分片个数的时候
                if (chunkInfo.currentChunk >= chunkInfo.chunkCount - 1) {
                    let fileInfo = {
                        chunks: chunkInfo.chunkCount,
                        guid: identifier,
                        name: file.name,
                    }
                    // 调用合并接口
                    mergeFile(fileInfo)
                }
            }
        }).catch(e => {
            // 达到请求上限，不再做重复请求
            if (curRequest === requestTime) {
                uploading && uploading({
                    file,
                    percentage: 1
                });
                error && error(e);
            } else {
                // 再次尝试请求
                curRequest++
                uploadChunk(chunkInfo)
            }
        })
    };
    const mergeFile = (fileInfo) => {
        Vue.prototype.$req.formdataAction(mergeURL, fileInfo, true).then((res) => {
            // 重置请求次数
            curRequest = 0
            uploading && uploading({
                file,
                percentage: 1
            });
            success && success(res.file);
        }).catch(e => {
            // 达到请求上限，不再做重复请求
            if (curRequest === requestTime) {
                uploading && uploading({
                    file,
                    percentage: 1
                });
                error && error(e);
            } else {
                // 再次尝试请求
                curRequest++
                mergeFile(fileInfo)
            }
        })
    }
    // 针对每个文件进行chunk处理
    const readChunk = () => {
        // 针对单个文件进行chunk上传
        const {
            chunk
        } = getChunkInfo(file, 1, chunkSize);
        uploadChunk({
            chunk,
            currentChunk: 1,
            chunkCount
        });
    };
    countMd5().then(function (result) {
        identifier = result + "_" + randomNumber
        // 附件小于10M时进行直接上传，否则分片上传
        if (fileSize < chunkSize) {
            uploadFile()
        } else {
            readChunk(); // 开始执行代码
        }
    })

    // 整片上传
    const uploadFile = () => {
        let fetchForm = new FormData();
        fetchForm.append("guid", identifier); // 文件唯一标识 md5
        fetchForm.append("size", fileSize); // 文件大小
        fetchForm.append("name", file.name); // 文件名称
        fetchForm.append("upload", file, file.name); // 分块文件传输对象
        Vue.prototype.$req.uploadAction(baseURL, fetchForm).then((res) => {
            uploading && uploading({
                file,
                percentage: 1
            });
            success && success(res.file);
        }).catch(e => {
            uploading && uploading({
                file,
                percentage: 1
            });
            error && error(e);
        })
    }
}

/**
 * 判断当前文件是不是oss文件
 * @param file 当前文件
 * @return Boolean 判断结果，true为OSS
 * */
export function judgeOssType(file) {
    // 数据处理
    file = isJSON(file) ? JSON.parse(file) : file
    // 判断对象内部是否存在useOss
    let existence = Object.keys(file).includes("useOss");
    // useOss 为true表示是阿里云上传 为false则代表上传本地 如果没有该字段表示是老数据
    // 存在并且等于true
    if ((existence === true && file.useOss === true) || ((file.url.indexOf("pcUploadFolder") !== -1 || file.url.indexOf("mUploadFolder") !== -1))) {
        return true
    } else {
        return false
    }
}