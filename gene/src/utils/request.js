import axios from 'axios'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
// 创建 axios 实例
import Cookies from 'js-cookie'
// 切换账号
Cookies.set('xxl_sso_sessionid', '99801136037_247b6ff41b4f4fe0910eb51a93477593')
const request = axios.create({
  // API 请求的默认前缀
  baseURL: '',
  timeout: 6000, // 请求超时时间
  headers: {
    __portal_is_backend_api: 'TRUE'
  }
})

// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed'
      })
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  config.headers['Cache-Control'] = 'no-cache'
  config.headers.Pragma = 'no-cache'
  // 注释
  config.headers.__gsuite_user_id = '99801136037'
  config.headers.__gsuite_user_name = 'QYY'
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  // 成功
  if (response.data.code === 501) {
    // 方便联调，501跳转暂时屏蔽
    let jumpUrl = ''
    if (response.data.loginPageUrl) {
      jumpUrl = response.data.loginPageUrl
    }
    if (response.data.data && response.data.data.loginPageUrl) {
      jumpUrl = response.data.data.loginPageUrl
    }
    window.location.href = jumpUrl + window.location.href
  }

  if (response.data.code === 'ConsoleNeedLogin') {
    window.location.href = '/logout'
  }
  return response.data
}, errorHandler)

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}