import moment from 'moment'
import {
  getFileBlobPreviewFileOss,
  getOrgNameByOrgId,
  getDefaultInnerDistribute,
  getInnerDistributeByObjId,
  getFileList,
  getLifecycle,
  downLoadAuth,
  standardCreator,
  getSubjectDistribute,
  getAttachmentByOssIds
} from '@/api/document.js'
import { getCommonSelectByCode } from '@/api/processManage'
import { resourcePathQuery } from '@/api/basecenter'
import Vue from 'vue'

/**
 * 上传文档的最大大小
 */
const uploadFileMaxSize = 200

 /**
  * 两个节点过程的文档类型
  * @returns
  */
const twoNodesObjectMetaCode = ['D|common_file|P', 'D|common_file|M', 'D|common_file|MR', 'D|common_file|TE']

 /**
  * 三个节点过程的文档类型
  * @returns
  */
const threeNodesObjectMetaCode = ['D|common_file|PR']

const processNodeCodeList = ['PUBLISH', 'VOLUME', 'UPGRADE']

 /**
  *
  * @param {*} version
  * @param {*} skipVerisons
  * @returns
  */
 const calNewVersion = (version, skipVerisons) => {
  if (version.length === 1) {
    // 原版本转ascii码
    const charCode = version.charCodeAt()
    // 计算下一版本ascii码
    let nextCode = parseInt(charCode) + 1
    // 计算下一版本
    let nextSingleVersion = String.fromCharCode(nextCode)
    // 下一版本存在于跳版规则中
    if (skipVerisons.indexOf(nextSingleVersion) !== -1) {
      // 判断下一版本是否为'Z'
      if (nextCode === 90) {
         return { char: 'A', step: 1 }
      } else {
        nextCode = nextCode + 1
        nextSingleVersion = String.fromCharCode(nextCode)
      }
    }
    // 跳出条件,判断是否为'Z'
    if (charCode === 90) {
      // 需要进位（二十六进制）
      return { char: 'A', step: 1 }
    } else {
      return { char: nextSingleVersion, step: 0 }
    }
  }
  // 获取版本最末位字母的ascii码
  const currentCharCode = version[0].charCodeAt()
  // 调用递归
  const tempChar = this.calNewVersion(version.slice(1, version.length))
  // 计算下一版本编码
  let nextCharCode = parseInt(currentCharCode) + tempChar.step
  // 计算下一版本
  let nextChar = String.fromCharCode(nextCharCode)
  // 下一版本存在于跳版规则中
  if (skipVerisons.indexOf(nextChar) !== -1) {
    // 判断下一版本是否为'Z'
    if (nextCharCode === 90) {
        return { char: `A${tempChar.char}`, step: 1 }
    } else {
      nextCharCode = nextCharCode + 1
      nextChar = String.fromCharCode(nextCharCode)
    }
  }
  if (nextCharCode > 90) {
    return { char: `A${tempChar.char}`, step: 1 }
  }
  return { char: `${nextChar}${tempChar.char}`, step: 0 }
}

/**
 * @param {*} version 当前版本
 * @param {*} list []   已经更改的数据
 * @returns
 */
const updateVersion = (version, list) => {
  if ((list || []).length > 0) {
    const index = list.length + 1
    const newVersion = version + index
    return { index, newVersion }
  }
  return { index: 1, newVersion: version + 1 }
}

/**
 *  js 数组去重
 * @param {*} arr
 * @returns
 */
const unique = (arr) => {
  var newArr = []
  for (var i = 0; i < arr.length; i++) {
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[i].id.toString() === arr[j].id.toString()) {
        if (arr[i].type === arr[j].type) {
            ++i
        }
      }
    }
    newArr.push(arr[i])
  }
  return newArr
}

/**
 *
 * @param {*} form
 */
const formatNumber = (form) => {
  if (form.VOLUME_FLAG === 'true' && form.number.indexOf('.') === -1) {
    form.number = `${form.number}.0`
  }
}

/**
 *
 * @param {*} array
 */
const batchFormatNumber = (array) => {
  array.forEach(ele => {
    formatNumber(ele)
  })
}

/**
 * 日期格式化
 * @param {*} data
 */
const dateFormat = (data) => {
  if (data) {
    if (data.indexOf('T') > -1) {
      return data.split('T')[0]
    }
    return data.split(' ')[0]
  }
  return ''
}

/**
 * 预览文件
 * @param {*} id
 */
const previewFile = (id) => {
  if (id) {
    Vue.prototype.$loading.show()
    getAttachmentByOssIds([id]).then(res => {
      const { success, data = [] } = res
      if (success && (data || []).length > 0) {
        getFileBlobPreviewFileOss(data[0].id).then(res => {
          Vue.prototype.$loading.hide()
          const file = new Blob([res], `${data.fileName}.${data.fileType}`, { type: 'application/pdf;charset=utf-8' })
          const objectUrl = window.URL.createObjectURL(file)
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(file, `${data.fileName}.${data.fileType}`)
          } else {
            window.open(objectUrl)
          }
        })
      }
    })
  }
}

/**
 * 通过部门id获取组织名称和单位名称
 * @param {*} orgId
 */
const getOrgId = async (orgId) => {
  const { success, data } = await getOrgNameByOrgId({ orgId })
  if (success) {
    return data
  }
}

/**
 * 通过当前版本算出新的版本号
 * @param {*} version
 */
const upgrade = (version) => {
  const skipVerisons = ['O', 'I']
  const tempChar = calNewVersion(version.replace(/\d$/, ''), skipVerisons)
  return tempChar.step === 1 ? `A${tempChar.char}` : `${tempChar.char}`
}

/**
 * 历史版本操作
 * @param {*} detailData
 */
const versionOpera = (detailData) => {
  if (detailData.HISTORY_VERSION) {
    return detailData.HISTORY_VERSION + ',' + detailData.DOC_VERSION
  }
  return detailData.DOC_VERSION || ''
}

/**
 * 课题历史版本操作
 * @param {*} detailData
 */
 const subjectVersionOpera = (detailData) => {
  if (detailData.HISTORY_VERSION) {
    return detailData.HISTORY_VERSION + ',' + detailData.version
  }
  return detailData.version || ''
}

/**
 * 是否存在某一个key
 * @param {*} arr
 * @param {*} key
 * @param {*} value
 * @returns
 */
const isExistKey = (arr, key, value) => {
  if (arr.indexOf(key) > -1) {
    return value === 'true'
  }
  return value
}

/**
 * 获取分发路线
 * @param {*} objId
 * @returns
 */
const getDistribute = async (objId) => {
  if (!objId) {
    const { success, data } = await getDefaultInnerDistribute()
    if (success) {
      if ((data || []).length > 0) {
        return data.map(item => {
          if (item) {
            const { orgName = '', orgCode = '', id = '' } = item
            return { disname: orgName, code: orgCode, id, type: '部门' }
          }
          return { disname: '', code: '', id: '', type: '' }
        })
      }
    }
  } else {
    const { data = [] } = await getInnerDistributeByObjId({ objId })
    if ((data || []).length > 0) {
      return data || []
    } else {
      const newData = await getDistribute()
      return newData
    }
  }
  return []
}

/**
 * 获取课题分发路线  默认北研中心
 * @returns
 */
 const getSubjectDefaultDistribute = async () => {
  const { success, data } = await getSubjectDistribute()
  if (success) {
    if ((data || []).length > 0) {
      return data.map(item => {
        if (item) {
          const { orgName = '', orgCode = '', id = '' } = item
          return { disname: orgName, code: orgCode, id, type: '部门' }
        }
        return { disname: '', code: '', id: '', type: '' }
      })
    }
  }
  return []
}

/**
 * 获取文件列表
 * @param {*} params
 * @returns
 */
const getFiles = async (params) => {
  if (params) {
    const { success, data = [] } = await getFileList(params.split(','))
    if (success) {
      const newData = data.map(item => {
        const { id, fileName, length, fileType } = item
        return { OSSID: id, FILENAME: fileName, FILESIZE: length, FILETYPE: fileType }
      })
      return newData || []
    }
    return []
  }
  return []
}

/**
 * 获取公共的字典数据
 * @param {*} itemkey
 * @returns
 */
const getCode = async (itemkey) => {
  const { success, data } = await getCommonSelectByCode({ typeCode: itemkey })
  if (success) {
    const json = {}
    data.map(item => {
      const { itemCode, itemName } = item
      json[itemCode] = itemName
    })
    return json
  }
  return {}
}

/**
 * 获取生命周期状态
 * @param {*} type
 * @returns
 */
const lifecycle = async (type) => {
  const { success, data } = await getLifecycle(type)
  if (success) {
    const json = {}
    data.map(item => {
      const { code, name } = item
      json[code] = name
    })
    return { data, json }
  }
  return {}
}

/**
 * 深拷贝变量-乞丐版
 * @param {*} param
 * @returns
 */
const deepCopy = (param) => {
  if (param !== undefined) {
    const paramString = JSON.stringify(param)
    return JSON.parse(paramString)
  }
  return param
}

/**
 * 下载权限按钮控制
 * @param {*} isPermission
 * @param {*} oid
 * @returns
 */
const isDownloadPermission = async (isPermission, oid) => {
  if (isPermission) {
    const { data } = await downLoadAuth({ oid })
    return data
  }
  return true
}

/**
 * 双层数组解析
 * @param {*} arr
 * @returns
 */
const arrayResolution = (arr) => {
  const array = []
  const data = arr || []
  data.map(item => {
    const json = {}
    const neaData = item || []
    neaData.map(ele => {
      const { attributeCode, value } = ele
      json[attributeCode] = value
    })
    array.push(json)
  })
  return array
}

/**
 * 校验当前用户的按钮权限
 * @param {*} type
 * @returns
 */
const resourcePathQueryData = async (type) => {
  const { success, data } = await resourcePathQuery(type)
  if (success && data && data.buttonList) {
    const json = {}
    data.buttonList.forEach(item => {
       if ((item.roleIdList || []).length > 0) {
        json[item.actionCode] = true
       } else {
        json[item.actionCode] = false
       }
    })
    return json
  }
  return {}
}

/**
 * 校验当前用户是否为组织部门
 * @param {*} roleCode
 * @returns
 */
const checkUser = async (roleCode) => {
  const { success, data } = await standardCreator({ roleCode })
  return success && data
}

/**
 * 代替号解析
 * @param {*} replaceNo
 * @returns
 */
const replaceChange = (replaceNo) => {
  if (replaceNo) {
    if (replaceNo.indexOf('CMOS-DOC') > -1) {
      const list = replaceNo.split(',')
      const number = []
      const numberVersion = []
      list.map(item => {
        if (item) {
          const arr = item.split(':')
          if (arr.length >= 3) {
            const data = arr.filter((item, index) => index !== 0 && index !== 1 && index !== (arr.length - 1))
            number.push(data.join(':'))
            numberVersion.push(data.join(':') + ':' + arr[arr.length - 1])
          } else {
            number.push(item)
            numberVersion.push(item)
          }
        }
      })
      return { list, number: number.join(','), numberVersion: numberVersion.join(',') }
    } else {
      return { list: [replaceNo], number: replaceNo, numberVersion: replaceNo }
    }
  }
  return { list: [], number: '', numberVersion: '' }
}

/**
 * 计算日期
 * @param {*} dateStr
 * @param {*} dayCount
 * @returns
 */
const dateAddDays = (dateStr, dayCount) => {
  if (dateStr) {
    const day = dayCount ? parseInt(dayCount) : 0
    const newData = dateStr.split('T')
    const tempDate = new Date(newData[0].replace(/-/g, '/'))
    const resultDate = new Date((tempDate / 1000 + (86400 * day)) * 1000)
    const resultDateStr = resultDate.getFullYear() + '-' + (resultDate.getMonth() + 1) + '-' + (resultDate.getDate())
    return newData.length > 1 ? (resultDateStr + 'T' + newData[1]) : resultDateStr
  }
  return dayCount || ''
}

/*
计算时间与当前时间的比较 文档发布专用
 */
const compareDateWithNow = (typeStr, dateStr, dayCount) => {
try {
  if (typeStr === '失效') {
    return '限用'
  } else if (typeStr === '已发布') {
    if (dateStr) {
      const day = dayCount ? parseInt(dayCount) : 0
      const newData = dateStr.split('T')
      const resultDate = moment(newData[0]).utc().add(day || 0, 'days')
      if (moment().utc() >= resultDate) {
      return '现行'
     } else {
      return '即将实施'
     }
      // return newData.length > 1 ? (resultDateStr + 'T' + newData[1]) : resultDateStr
    } else {
      return '现行'
    }
  } else if (typeStr === '作废') {
    return '作废'
  } else {
    return ''
  }
} catch (error) {
  console.log('转换失败')
  return typeStr
}
}
/**
 * 从url中获取流程实例ID
 * @returns
 */
const getProcessInstanceIdFromUrl = () => {
  const {
    processInstanceId
  } = this.$route.query
  return processInstanceId || ''
}

/**
 * 计算经过时间
 * @param {*} openTime
 * @param {*} dealTime
 * @returns
 */
const calSpentTime = (openTime, dealTime) => {
  const startTime = new Date(openTime)
  const endTime = new Date(dealTime)
  const total = (endTime.getTime() - startTime.getTime()) / 1000
  // 计算整数天数
  const spentDays = parseInt(total / (24 * 60 * 60))
  // 取得算出天数后剩余的秒数
  const afterDay = total - spentDays * 24 * 60 * 60
  // 计算整数小时数
  const hour = parseInt(afterDay / (60 * 60))
  // 取得算出小时数后剩余的秒数
  const afterHour = total - spentDays * 24 * 60 * 60 - hour * 60 * 60
  // 计算整数分
  const min = parseInt(afterHour / 60)
  // 取得算出分后剩余的秒数
  const afterMin = total - spentDays * 24 * 60 * 60 - hour * 60 * 60 - min * 60
  const times = {
    day: spentDays,
    hour,
    minute: min,
    second: afterMin
  }
  return times
}

/**
 * 类型转换，将对象组件中嵌套属性LIST中的值转换成字典类型便于检索
 * @param {*} form
 * @param {*} listName
 * @param {*} mapKeyWord
 * @returns
 */
const changeListToMap = (form, listName, mapKeyWord) => {
  const map = {}
  if (Object.keys(form).indexOf(listName) !== -1) {
    if (form[listName].length > 0) {
      form[listName].forEach((ele, index) => {
        let keyWordsValue = null
        const changeAttributes = {}
        ele.forEach(item => {
          if (item.attributeCode === mapKeyWord) { keyWordsValue = item.value }
          changeAttributes[item.attributeCode] = item.value
        })
        map[keyWordsValue] = changeAttributes
      })
    }
  }
  return map
}

/**
 * 根据后台返回的字段权限来展示页面字段
 * @param {*} permissions  权限数据
 * @param {*} itemData     页面布局和数据
 *                         其中itemData的格式如下：{ layout: [] , formItemList: []}
 *                         layout表示页面布局数据 []，formItemList字段数据 []
 */
const fieldPermission = (permissions, itemData) => {
  function isObject (data, type) {
    if (type === 'object') {
      return Object.prototype.toString.call(data) === '[object Object]'
    } else if (type === 'array') {
      return Object.prototype.toString.call(data) === '[object Array]'
    }
    return false
  }
  if (!isObject(permissions, 'object')) {
    throw new Error('传入的权限数据必须是对象！')
  }
  if (!isObject(itemData, 'object')) {
    throw new Error('传入的页面布局数据必须是数组！')
  }
  if (!itemData.layout || !isObject(itemData.layout, 'array')) {
    throw new Error('传入的页面布局数据的格式不正确！')
  }
  if (!itemData.formItemList || !isObject(itemData.formItemList, 'array')) {
    throw new Error('传入的页面布局数据的格式不正确！')
  }
  const newObject = {}
  const rightList = {}
  for (const key in permissions) {
    if (isObject(permissions[key], 'object')) {
      for (const item in permissions[key]) {
        if (item === 'attributes' && isObject(permissions[key][item], 'array')) {
          permissions[key][item].map(ele => {
            if (isObject(ele, 'object') && Object.keys(ele).length > 0) {
              const keyData = Object.keys(ele)
              const valueData = Object.values(ele)
              rightList[keyData[0]] = valueData[0]
            }
          })
        }
      }
    }
  }
  for (const key in itemData) {
    if (isObject(itemData[key], 'array')) {
      const array = []
      itemData[key].map(item => {
        const json = JSON.parse(JSON.stringify(item))
        const queryId = key === 'formItemList' ? json.queryId : json.item.queryId
        if (rightList[queryId] === 'r') {
          json.disabledFlag = true
          array.push(json)
        } else if (rightList[queryId] === 's') {
          json.disabledFlag = false
          array.push(json)
        }
      })
      newObject[key] = array
    }
  }
  return newObject
}
/**
 *
 * @param {*} words 字符串
 * @returns 字符串个数
 */
const getCharCount = (words) => {
  if (words) {
    let iTotal = 0
    let sTotal = 0
    let eTotal = 0
    for (let i = 0; i < words.length; i++) {
      const c = words.charAt(i)
      if (c.match(/[\u4e00-\u9fa5]/)) { // 基本汉字
        iTotal += 1
      } else if (c.match(/[\u9FA6-\u9fcb]/)) { // 基本汉字补充
        iTotal += 1
      }
      // eslint-disable-next-line no-control-regex
      if (c.match(/[^\x00-\xff]/)) {
        sTotal += 1
      } else {
        eTotal += 1
      }
    }
    return iTotal * 2 + (sTotal - iTotal) * 2 + eTotal
  }
  return 0
}

export {
  uploadFileMaxSize,
  twoNodesObjectMetaCode,
  threeNodesObjectMetaCode,
  processNodeCodeList,
  calNewVersion,
  updateVersion,
  unique,
  formatNumber,
  batchFormatNumber,
  dateFormat,
  previewFile,
  getOrgId,
  upgrade,
  versionOpera,
  subjectVersionOpera,
  isExistKey,
  getDistribute,
  getSubjectDefaultDistribute,
  getFiles,
  getCode,
  lifecycle,
  deepCopy,
  isDownloadPermission,
  arrayResolution,
  resourcePathQueryData,
  replaceChange,
  checkUser,
  dateAddDays,
  compareDateWithNow,
  getProcessInstanceIdFromUrl,
  calSpentTime,
  changeListToMap,
  fieldPermission,
  getCharCount
}
