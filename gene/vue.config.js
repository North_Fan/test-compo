const path = require("path");
const uglify = require("uglifyjs-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {

  productionSourceMap: false,
  runtimeCompiler: true,
  lintOnSave: false,
  outputDir: "dist",
  chainWebpack: (config) => {
    config.resolve.alias.set("@$", resolve("src"));
    config.module
      .rule("jsx")
      .test(/\.jsx$/)
      .exclude.add(resolve("node_modules"))
      .end()
      .use("babel-loader")
      .loader("babel-loader")
      .options({
        presets: ["@babel/preset-env", "@babel/preset-react"],
      })
      .end();
      
    config.module
      .rule("less").exclude.add(/\.less$/);

      
    config.module
      .rule("less-custom")
      .test(/\.less$/)
      .use("style-loader")
      .loader("style-loader")
      .end()
      .use("css-loader")
      .loader("css-loader")
      .end().use("less-loader")
      .loader("less-loader")
      .end()
    config.module
      .rule("images")
      .test(/\.(png|jpeg|jpg)$/)
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 1024 * 10, // 小于10k的图片采用baseurl，大于和等于8k的就正常打包成图片
        name: "static/[name].[ext]", //图片大于等于10k时，设置打包后图片的存放位置 name是文件名   ext是文件后缀
      })
      .end();
  },
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === "production") {
      return {
        output: {
          ...config.output,
          libraryExport: "default",
          libraryTarget: "umd",
          umdNamedDefine: true,
        },
        // 打包去掉console 必须引入TerserPlugin
        optimization: {
          minimize: true,
          usedExports: true,
          minimizer: [
            new TerserPlugin({
              terserOptions: {
                compress: {
                  drop_debugger: true, // 打包去除 debugger
                  drop_console: true, // 打包去除 console
                },
              },
            }),
          ],
        },
        // 关闭 webpack 的性能提示
        performance: {
          hints: false,
        },
        resolve: {
          alias: {
            "@": resolve("src"),
          },
        },
        plugins: [new uglify()],
      };
    } else {
      // 为开发环境修改配置...
      return {
        resolve: {
          alias: {
            "@": resolve("src"),
          },
        },
        devtool: "source-map",
        output: {
          ...config.output,
          libraryExport: "default",
        },
      };
    }
    // if (process.env.NODE_ENV !== 'development') {
    //     config.plugins.push(new uglify())
    // }
  },
  devServer: {
    open: true,
    // host: '0.0.0.0',
    // port: 8080,
    https: false,
    hotOnly: true,
    proxy: {
      "/contract/biz/": {
        target: "http://cmos2dev.comac.int",
        // target: 'http://192.168.1.8:7021/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // PI编辑器 默认端口：7041
      "/procintelligence/biz/": {
        target: "http://cmos2dev.comac.int",
        // target: 'http://192.168.1.127:7041',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 质量 默认端口：7016
      "/quality/biz/": {
        target: "http://cmos2dev.comac.int",
        // target: 'http://192.168.1.87:7016/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 工作流 无需本地联调 直接调用环境上服务
      "/flow-platform/": {
        target: "http://cmos2dev.comac.int",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 过程 默认端口：7011
      "/process/biz/": {
        target: "http://cmos2dev.comac.int",
        // target: 'http://192.168.1.87:7011/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 文件服务 默认端口：7046
      "/filenode/biz/": {
        target: "http://cmos2dev.comac.int",
        // target: 'http://192.168.1.8:7046/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 对象 无需本地联调 直接调用环境上服务
      "/object-component/": {
        target: "http://cmos2dev.comac.int/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 数据字典 默认端口：7001
      "/basecenter/biz/": {
        target: "http://cmos2dev.comac.int/",
        // target: 'http://192.168.1.8:7001/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      "/basecenter/feignApi/": {
        target: "http://cmos2dev.comac.int/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 作业服务 默认端口：7036
      "/assignment/biz/": {
        target: "http://cmos2dev.comac.int/",
        // target: 'http://192.168.3.144:7036/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 计划模块 默认端口：7006
      "/plan/biz/": {
        target: "http://cmos2dev.comac.int/",
        // target: 'http://192.168.1.115:7006/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // 外围接口对接适配服务 默认端口：7056
      "/adapter/": {
        target: "http://cmos2dev.comac.int/",
        // target: 'http://192.168.1.115:7056/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      "/cmos-frontend/": {
        target: "http://cmos2dev.comac.int/",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      // SQCDP
      "/sqcdp/biz/": {
        target: "http://cmos2dev.comac.int/",
        // target: 'http://10.58.5.68:7101/',
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
      "/portal": {
        target: "http://cmos.yunqiao.cmos2dev.comac.int/",
        changeOrigin: true,
        secure: false,
      },
      "/document/biz/": {
        target: "http://cmos2dev.comac.int",
        ws: false,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_API_BASE_URL]: "",
        },
      },
    },
  },
  // css: {
  //   loaderOptions: {
  //     less: {
  //       lessOptions: {
  //         modifyVars: {
  //           "primary-color": "#0086FB",
  //           themeColorHover: "#8CC8FD",
  //           "link-color": "#0086FB",
  //           "success-color": "#00C668",
  //           "warning-color": "#EF7800",
  //           "error-color": "#EF3E31",
  //           "font-size-base": "14px",
  //           "border-radius-base": "2px",
  //           "box-shadow-base": "0 2px 8px rgba(0, 0, 0, 0.15)",
  //           "font-family": "PingFang SC",
  //           "btn-font-weight": "400",
  //           "hint-color": "#ffc919",
  //           tableHeaderBg: "#f9fafc",
  //           tableHoverBg: "#f4f9ff",
  //           "heading-color": "rgba(0, 0, 0, 0.85)",
  //           "text-color": "rgba(0, 0, 0, 0.65)",
  //           "text-color-secondary": "rgba(0, 0, 0, 0.45)",
  //           "disabled-color": "rgba(0, 0, 0, 0.25)",
  //           "border-color-base": "rgba(0, 0, 0, 0.15",
  //           "halvingline-color": "rgba(0, 0, 0, 0.06)",
  //           "background-color": "rgba(0, 0, 0, 0.04)",
  //           "tableHeader-color": "rgba(189, 93, 93, 0.02)",
  //           "bg-color": "#f5f5f5",
  //           "table-padding-vertical": "8px",
  //           "table-padding-horizontal": "8px",
  //         },
  //         javascriptEnabled: true,
  //       },
  //     },
  //   },
  // },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [path.resolve(__dirname, "./src/assets/style/index.less")],
    },
  },
};
