# gean-components

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

npm login --registry='http://nexus.mom.comac.int/repository/comac_npm_repo/'
repoadmin
nexus!repo
cmos@comac.intra

npm publish  --registry='http://nexus.mom.comac.int/repository/comac_npm_repo/'