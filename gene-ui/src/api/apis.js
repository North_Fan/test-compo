import req from '@/utils/request.js'
export function getData(url, kv) {
    return req(url, kv, 'get')
}
export function postData(url, kv) {
    return req(url, kv, 'post')
}
export function uploadFile(url, kv) {
    return req(url, kv, 'upload')
}