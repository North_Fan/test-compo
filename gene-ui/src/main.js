import Vue from 'vue'
import App from './App';
import TestURLFylUI from '@/components/index.js' // 注册Ge组件
Vue.use(TestURLFylUI)
import '@/icons' //全局引入icons

import router from './router'
Vue.config.devtools = true
/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        App
    },
    router,
    template: '<App/>',
});