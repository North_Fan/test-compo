import VueRouter from 'vue-router'
import Vue from 'vue'

// const VueRouter = window.VueRouter
// const Vue = window.Vue
Vue.use(VueRouter)

export const constantRoutes = [{
        path: '/',
        redirect: '/main'
    },
    {
        path: '/main',
        name: 'Main',
        component: () =>
            import('@/views/main.vue')
    }  
]

const router = new VueRouter({
    routes: constantRoutes
})

export default router