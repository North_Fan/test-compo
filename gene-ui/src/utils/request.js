import axios from "axios";
import {
    resetMessage
} from '@/utils/resetMessage';
const hasToken = eval(localStorage.getItem('hasToken'))

const service = axios.create({
    withCredentials: true, // send cookies when cross-domain requests
    timeout: 30000 // request timeout
})
const codeMessage = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户没有权限（令牌、用户名、密码错误）。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};

const CustomErrorCodeMap = {
    '1': '不需要特殊处理的错误',
    '2': '登录异常请重新登陆',
    '3': '缺少需要填写的字段',
    '4': '数据错误',
    '5': '没有访问权限',
}


// request interceptor
service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data
            // if the custom code is not 20000, it is judged as an error.
        if (res.code !== 0) {
            const msg = res.message || CustomErrorCodeMap[res.code]
            resetMessage.error(msg)
            if (res.code === 2 || res.code === 50012 || res.code === 50014) {
                // localStorage.removeItem('userId')
                //     // router.push('/login')
                // let login = window.location.href.split('#')
                // window.location.href = login[0] + '#/login'
            }
            return Promise.reject(new Error(res.message || 'Error'))
        } else {
            return res.data
        }
    },
    error => {
        console.log('err' + error) // for debug
        const errorText = codeMessage[error.status] || error.statusText || error.message;
        resetMessage.error(errorText)
        return Promise.reject(error)
    }
)

export default function req(url, value = {}, method = 'post', options = {}) {
    let url_ = window.location.host.split('.')
    let yn = url_.includes("you") || hasToken
    let userToken = null
    if (yn) {
        userToken = localStorage.getItem('userToken')
    }
    if (['put', 'post', 'delete'].includes(method)) {
        if (yn) {
            return service({
                url,
                data: value,
                method,
                ...options,
                // 设置请求头
                headers: {
                    'Authorization': userToken
                }
            })
        } else {
            return service({
                url,
                data: value,
                method,
                ...options,
            })
        }
    } else if (method === 'get') {
        if (yn) {
            return service.get(url, {
                params: value,
                ...options,
                // 设置请求头
                headers: {
                    'Authorization': userToken
                }
            })
        } else {
            return service.get(url, {
                params: value,
                ...options,
            })
        }

    } else if (method === 'formdata') {
        if (yn) {
            return service({
                method: 'post',
                url: url,
                data: value,
                // 转换数据的方法
                transformRequest: [
                    function(data) {
                        let ret = ''
                        for (const it in data) {
                            ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
                        }
                        ret = ret.substring(0, ret.length - 1)
                        return ret
                    }
                ],
                // 设置请求头
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': userToken
                }
            })
        } else {
            return service({
                method: 'post',
                url: url,
                data: value,
                // 转换数据的方法
                transformRequest: [
                    function(data) {
                        let ret = ''
                        for (const it in data) {
                            ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
                        }
                        ret = ret.substring(0, ret.length - 1)
                        return ret
                    }
                ],
                // 设置请求头
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            })
        }
    } else if (method === 'upload') {
        if (yn) {
            return service({
                method: 'post',
                url: url,
                data: value,
                // 设置请求头
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': userToken
                }
            })
        } else {
            return service({
                method: 'post',
                url: url,
                data: value,
                // 设置请求头
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
        }
    }
}