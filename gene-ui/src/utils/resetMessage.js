/**重置message，防止重复点击重复弹出message弹框 */
import {
    message
} from 'ant-design-vue';
const showMessage = Symbol('showMessage')
class DoneMessage {
    [showMessage](type, options, single) {
        if (single) {
            if (document.getElementsByClassName('ant-message').length === 0) {
                message[type](options)
            }
        } else {
            message[type](options)
        }
    }
    info(options, single = true) {
        this[showMessage]('info', options, single)
    }
    warning(options, single = true) {
        this[showMessage]('warning', options, single)
    }
    error(options, single = true) {
        this[showMessage]('error', options, single)
    }
    success(options, single = true) {
        this[showMessage]('success', options, single)
    }
}
export const resetMessage = new DoneMessage();