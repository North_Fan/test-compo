/*
 * @Author: YANG CHAO CHAO
 * @Date: 2022-11-23 11:20:59
 * @LastEditors: YANG CHAO CHAO
 * @LastEditTime: 2023-01-04 14:04:35
 * @FilePath: /gean-ui-component/src/utils/antd-icon.js
 * @Description:
 *
 */
export { default as CaretDownFill } from '@ant-design/icons/lib/fill/CaretDownFill'
export { default as CheckCircleFill } from '@ant-design/icons/lib/fill/CheckCircleFill'
export { default as CloseCircleFill } from '@ant-design/icons/lib/fill/CloseCircleFill'
export { default as ExclamationCircleFill } from '@ant-design/icons/lib/fill/ExclamationCircleFill'
export { default as InfoCircleFill } from '@ant-design/icons/lib/fill/InfoCircleFill'
export { default as CalendarOutline } from '@ant-design/icons/lib/outline/CalendarOutline'
export { default as CloseCircleOutline } from '@ant-design/icons/lib/outline/CloseCircleOutline'
export { default as CloseOutline } from '@ant-design/icons/lib/outline/CloseOutline'
export { default as CloudDownloadOutline } from '@ant-design/icons/lib/outline/CloudDownloadOutline'
export { default as CloudUploadOutline } from '@ant-design/icons/lib/outline/CloudUploadOutline'
export { default as DownOutline } from '@ant-design/icons/lib/outline/DownOutline'
export { default as ExclamationCircleOutline } from '@ant-design/icons/lib/outline/ExclamationCircleOutline'
export { default as FolderOpenOutline } from '@ant-design/icons/lib/outline/FolderOpenOutline'
export { default as LeftOutline } from '@ant-design/icons/lib/outline/LeftOutline'
export { default as MinusOutline } from '@ant-design/icons/lib/outline/MinusOutline'
export { default as PlusOutline } from '@ant-design/icons/lib/outline/PlusOutline'
export { default as QuestionCircleOutline } from '@ant-design/icons/lib/outline/QuestionCircleOutline'
export { default as RightOutline } from '@ant-design/icons/lib/outline/RightOutline'
export { default as UpOutline } from '@ant-design/icons/lib/outline/UpOutline'
export { default as SearchOutline } from '@ant-design/icons/lib/outline/SearchOutline'
// export {<FolderOpenOutlined />
//     default as InfoCircleFill
// }<CloudDownloadOutlined /><CloudUploadOutlined />
// from '@ant-design/icons/lib/fill/InfoCircleFill';
