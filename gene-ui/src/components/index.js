import '@/assets/style/antd.less'
import { antComponents } from '@/utils/ant-design-vue-ui'

// 引入element-ui
// import { Cascader, Checkbox, Tree, Upload } from 'element-ui'

// 引入其他插件

/* 样式引用 */
import '@/assets/font/geanicon.css'
import '@/assets/style/ant-design.scss'
import '@/assets/style/index.scss'


// 配置中文包
import moment from 'moment'
import 'moment/locale/zh-cn'
moment.locale('zh-cn') //配置moment中文环境

import GeTitle from '@/components/GeTitle/index.js' // 标题
// import GeForm from "@/components/GeForm/index.js"; // 表单

// 流程表单
// import FlowStartFormAdd from "@/components/FlowStartFormAdd/index.js"; // 新增流程

// 存储组件列表
const geComponents = [

  GeTitle

]

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
// 接收host作为组件内部请求接口的baseUrl
// hastToken给需要提供token的接口使用
const install = function (Vue, host, hasToken) {
  // 判断是否安装
  if (install.installed) return
  // 遍历注册全局组件
  antComponents.forEach(com => {
    Vue.use(com)
    if ((com.name = 'Message')) {
      Vue.prototype.$message = com
    }
  })
  // Vue.use(Storage, config.storageOptions)
  // Vue.use(Cascader)
  // Vue.use(Checkbox)
  // Vue.use(Tree)
  // Vue.use(Upload)
  geComponents.forEach(com => {
    Vue.component(com.name, com)
  })
}
// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}
export default {
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install,
  // 以下是具体的组件列表
  GeTitle,
}
