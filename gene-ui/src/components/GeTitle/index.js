// 导入组件，组件必须声明 name
import GeTitle from './src/GeTitle.vue'

// 为组件提供 install 安装方法，供按需引入
GeTitle.install = function(Vue) {
        Vue.component(GeTitle.name, GeTitle)
    }
    // 默认导出组件
export default GeTitle